webpackJsonp([0],{

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QrcodePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__review_review__ = __webpack_require__(148);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { NgxQRCodeModule } from 'ngx-qrcode2';


var QrcodePage = (function () {
    function QrcodePage(navCtrl, navParams, backendProv, modalCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.createdCode = null;
        this.realexpression = (0).toFixed(2);
    }
    QrcodePage.prototype.ionViewDidLoad = function () {
        var newdate = new Date();
        var datetime = Math.round(newdate.getTime() / 1000);
        this.createdCode = '&' + this.backendProv.myid + '&' + datetime + '&' + this.backendProv.makeid() + '&12341234';
        this.realexpression = this.navParams.get('realexpression');
        this.realexpression = (this.realexpression / 100).toFixed(2);
    };
    QrcodePage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    QrcodePage.prototype.reviewAsk = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Оставить отзыв',
            message: 'Хотите оставить отзыв?',
            buttons: [
                {
                    text: 'Нет',
                    role: 'cancel',
                    handler: function () {
                        _this.navCtrl.popToRoot();
                    }
                },
                {
                    text: 'Да',
                    handler: function () {
                        _this.openReview();
                    }
                }
            ]
        });
        alert.present();
    };
    QrcodePage.prototype.openReview = function () {
        var _this = this;
        var reviewModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__review_review__["a" /* ReviewPage */], {});
        reviewModal.onDidDismiss(function (data) {
            if (data.goroot) {
                _this.navCtrl.popToRoot();
            }
        });
        reviewModal.present();
    };
    return QrcodePage;
}());
QrcodePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-qrcode',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/qrcode/qrcode.html"*/'<ion-header>\n\n\n\n  <ion-navbar color="viol">\n\n    <ion-title>Предъявите код</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content no-padding>\n\n  \n\n  <ion-card no-margin no-border *ngIf="createdCode" style="width:100%;box-shadow:none;">\n\n    <ngx-qrcode [qrc-value]="createdCode" qrc-errorCorrectionLevel="Q"></ngx-qrcode>\n\n    <ion-card-content>\n\n      <!-- <p>Value: {{ createdCode }}</p> -->\n\n      <ion-grid>\n\n        <!-- <ion-row>\n\n          <ion-col>\n\n            <h1 style="font-size:36px;text-align:center;">{{realexpression}}</h1>\n\n            <h3 style="font-size:10px;text-align:center;">руб</h3>\n\n          </ion-col>\n\n        </ion-row> -->\n\n        <ion-row>\n\n          <ion-col text-center>\n\n            <button ion-button block color="viol" (click)="reviewAsk()">готово</button>\n\n          </ion-col>\n\n        </ion-row>\n\n        <!-- <ion-row>\n\n          <ion-col text-center>\n\n            <button ion-button clear icon-left color="dark" (click)="goBack()"><ion-icon name="arrow-back"></ion-icon>изменить сумму</button>\n\n          </ion-col>\n\n        </ion-row> -->\n\n      </ion-grid>\n\n    </ion-card-content>\n\n  </ion-card>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/qrcode/qrcode.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], QrcodePage);

//# sourceMappingURL=qrcode.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_image_picker__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ReviewPage = (function () {
    function ReviewPage(navCtrl, navParams, viewCtrl, backendProv, alertCtrl, loadingCtrl, imagePicker, actionSheetCtrl, camera, file, transfer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.backendProv = backendProv;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.imagePicker = imagePicker;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.file = file;
        this.transfer = transfer;
        this.places = [];
        this.profiles = [];
        this.selectOptions = {};
        this.piclink = 0;
        this.review = {
            device_id: 0,
            inst_id: 0,
            location: 'В общем',
            rating: 5,
            pic: 0,
            ratingtxt: '',
            newusr: 'rate'
        };
        this.piclink = backendProv.piclink;
        this.backendProv.getOffices()
            .then(function (res) {
            _this.places = res;
        })
            .catch(function (e) { return console.log(e); });
        this.selectOptions = {
            title: 'Выбор отдела',
            subTitle: 'г. Минск'
        };
        this.review.device_id = this.backendProv.uuid;
        this.review.inst_id = this.backendProv.institution;
    }
    ReviewPage.prototype.chImg = function () {
        var _this = this;
        var entryRequestAS = this.actionSheetCtrl.create({
            title: 'Добавить фотографию',
            subTitle: 'Выбрать из галереи или снять новую фотографию?',
            buttons: [
                {
                    text: 'Выбрать из галереи',
                    handler: function () {
                        _this.selPic();
                    }
                },
                {
                    text: 'Снять новое фото',
                    handler: function () {
                        _this.makePic();
                    }
                },
                {
                    text: 'Отменить',
                    role: 'cancel',
                    handler: function () {
                        // console.log('Cancel clicked');
                    }
                }
            ]
        });
        entryRequestAS.present();
    };
    ReviewPage.prototype.makePic = function () {
        var _this = this;
        var optionsselfie = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            targetWidth: 700,
            targetHeight: 700,
            correctOrientation: true
        };
        this.camera.getPicture(optionsselfie).then(function (imageURI) {
            var when = Math.floor(new Date().getTime() / 1000);
            var randpicname = _this.backendProv.myid + '_' + when;
            var namefilesplit = imageURI.split('/');
            var namefile = namefilesplit[namefilesplit.length - 1];
            var oldurlsplit = imageURI.split(namefile);
            var oldurl = oldurlsplit[0];
            var topath = _this.file.dataDirectory + _this.backendProv.instdir + '/' + randpicname + '.jpg';
            var tourl = _this.file.dataDirectory + _this.backendProv.instdir + '/';
            _this.file.moveFile(oldurl, namefile, tourl, randpicname + '.jpg')
                .then(function (res) {
                _this.savePic(topath, randpicname);
            })
                .catch(function (e) { return console.log(e); });
        }, function (err) {
            // Handle error
        });
    };
    ReviewPage.prototype.selPic = function () {
        var _this = this;
        var optionsImg = {
            maximumImagesCount: 1,
            width: 700,
            height: 700,
            quality: 100
        };
        this.imagePicker.getPictures(optionsImg)
            .then(function (results) {
            if (results.length > 0) {
                var _loop_1 = function (i) {
                    // DATE - TIME IN SECONDS
                    var when = Math.floor(new Date().getTime() / 1000);
                    var randpicname = _this.backendProv.myid + '_' + when;
                    var namefilesplit = results[i].split('/');
                    var namefile = namefilesplit[namefilesplit.length - 1];
                    var oldurlsplit = results[i].split(namefile);
                    var oldurl = oldurlsplit[0];
                    var topath = _this.file.dataDirectory + _this.backendProv.instdir + '/' + randpicname + '.jpg';
                    var tourl = _this.file.dataDirectory + _this.backendProv.instdir + '/';
                    _this.file.copyFile(oldurl, namefile, tourl, randpicname + '.jpg').then(function (success) {
                        _this.savePic(topath, randpicname);
                    }, function (er) { });
                };
                for (var i = 0; i < results.length; i++) {
                    _loop_1(i);
                }
            }
        }, function (error) {
        });
    };
    ReviewPage.prototype.savePic = function (imgpath, randpicname) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        // UPLOADING SOUND
        var options = {
            fileKey: "file",
            fileName: randpicname + '.jpg',
            chunkedMode: false,
            mimeType: 'image/jpeg'
        };
        var fileTransfer = this.transfer.create();
        fileTransfer.upload(imgpath, "http://www.olegtronics.com/admin/coms/upload.php?usrupl=1&preview=1&user_id=" + this.backendProv.myid, options)
            .then(function (result) {
            var srtringify = JSON.stringify(result.response);
            var parsingres = JSON.parse(JSON.parse(srtringify));
            var messent = parsingres.user_upd;
            if (messent > 0) {
                _this.review.pic = randpicname + '.jpg';
                _this.file.removeFile(_this.file.dataDirectory + _this.backendProv.instdir + '/', randpicname + '.jpg');
            }
            loading.dismiss();
        }, function (err) {
            loading.dismiss();
        });
    };
    ReviewPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.piclink + val;
        }
        else {
            return 'assets/img/face.jpg';
        }
    };
    ReviewPage.prototype.sendReview = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Отправить',
            message: 'Отправить отзыв?',
            buttons: [
                {
                    text: 'Нет',
                    role: 'cancel',
                    handler: function () {
                        // this.navCtrl.popToRoot();
                        var data = { 'goroot': '1' };
                        _this.viewCtrl.dismiss(data);
                    }
                },
                {
                    text: 'Да',
                    handler: function () {
                        _this.review.ratingtxt += ' - ' + _this.review.location;
                        _this.backendProv.httpRequest(JSON.stringify(_this.review))
                            .subscribe(function (res) {
                            if (res[0].reviewOK == '0') {
                            }
                            else if (res[0].reviewOK == '1') {
                                var alertThx = _this.alertCtrl.create({
                                    title: 'Спасибо',
                                    subTitle: 'Отзыв отправлен!',
                                    buttons: ['Закрыть']
                                });
                                alertThx.present();
                                // this.navCtrl.popToRoot();
                                var data = { 'goroot': '1' };
                                _this.viewCtrl.dismiss(data);
                            }
                            else if (res[0].reviewOK == '2') {
                                var alertThx = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Один отзыв только каждые 12 часов!',
                                    buttons: ['Закрыть']
                                });
                                alertThx.present();
                            }
                        }, function (e) { return console.log(e); });
                    }
                }
            ]
        });
        alert.present();
    };
    ReviewPage.prototype.close = function () {
        // this.navCtrl.popToRoot();
        var data = { 'goroot': '1' };
        this.viewCtrl.dismiss(data);
    };
    ReviewPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad ReviewPage');
    };
    return ReviewPage;
}());
ReviewPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-review',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/review/review.html"*/'<ion-header no-border>\n  <ion-navbar hideBackButton color="viol">\n    <ion-buttons left>\n      <button ion-button (click)="close()" [ngStyle]="{\'font-size\': \'200%\'}">\n        <ion-icon name="arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons right>\n      <button ion-button (click)="sendReview()">\n          Отправить\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <div class="violback" padding>\n\n    <ion-list>\n      <ion-item>\n        <ion-label>Отдел</ion-label>\n        <ion-select [(ngModel)]="review.location" [selectOptions]="selectOptions" okText="Выбрать" cancelText="Отмена">\n          <ion-option value="В общем">Выбор</ion-option>\n          <ion-option *ngFor="let place of places" [value]="place.office_adress">{{ place.office_adress }}</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <h2>Оценка</h2>\n        <!-- <rating [(ngModel)]="rate" readOnly="false" max="5" emptyStarIconName="star-outline" halfStarIconName="star-half" starIconName="star" nullable="false" (ngModelChange)="onModelChange($event)"></rating> -->\n        <rating [(ngModel)]="review.rating" readOnly="false" max="5" emptyStarIconName="star-outline" halfStarIconName="star-half" starIconName="star" nullable="false"></rating>\n      </ion-item>\n      <ion-item (click)="chImg()">\n        <ion-thumbnail item-start>\n          <img [src]="checkPic(review.pic)">\n        </ion-thumbnail>\n        <button ion-button clear item-end>Добавить фото</button>\n      </ion-item>\n      <ion-item style="height:200px;">\n        <ion-textarea [(ngModel)]="review.ratingtxt" placeholder="Пару слов..." clearInput></ion-textarea>\n      </ion-item>\n    </ion-list>\n\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/review/review.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */], __WEBPACK_IMPORTED_MODULE_6__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_image_picker__["a" /* ImagePicker */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */]])
], ReviewPage);

//# sourceMappingURL=review.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic2_super_tabs__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__expresschoice_expresschoice__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = (function () {
    function TabsPage(navCtrl, navParams, superTabsCtrl, backendProv, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.superTabsCtrl = superTabsCtrl;
        this.backendProv = backendProv;
        this.loadingCtrl = loadingCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__expresschoice_expresschoice__["a" /* ExpresschoicePage */];
        this.currentTab = 0;
        this.selexpress = [];
        this.offices = [];
        this.takedate = new Date();
        this.masters = [];
        this.professions = [];
        this.menuecat = 0;
        this.menuegood = 0;
        this.selexpress = this.navParams.get('selexp');
        this.offices = this.navParams.get('offices');
        this.menuecat = this.navParams.get('menuecat');
        this.menuegood = this.navParams.get('menuegood');
        // this.takedate.setHours(0,0,0,0);
        // this.ritualdate = backendProv.timezoneAdd((this.takedate.getTime()/1000).toFixed(0));
        // console.log('DaAAAAATE1 =========> '+new Date(this.ritualdate*1000));
        this.ritualdate = backendProv.timezoneAdd((this.takedate.getTime() / 1000).toFixed(0));
        // console.log('DaAAAAATE2 =========> '+new Date(testdate*1000));
        // console.log('DaAAAAATE3 =========> '+new Date());
        // console.log(JSON.stringify(this.selexpress));
        // PRELOAD DATA
        backendProv.checkWaiter()
            .then(function (res) {
            backendProv.loadSchedule();
            backendProv.loadRoom();
            backendProv.loadOrdering();
            backendProv.getProfessions().then(function (res) {
                _this.professions = res;
                _this.loadMasters();
            }).catch(function (e) { });
        })
            .catch(function (err) {
            backendProv.loadSchedule();
            backendProv.loadRoom();
            backendProv.loadOrdering();
            backendProv.getProfessions().then(function (res) {
                _this.professions = res;
                _this.loadMasters();
            }).catch(function (e) { });
        });
    }
    TabsPage.prototype.selEarliest = function (val) {
        var selearliest = [];
        for (var i = 0; i < this.masters.length; i++) {
            if (val == this.masters[i].user_office) {
                selearliest.push(this.masters[i]);
            }
        }
        return selearliest;
    };
    TabsPage.prototype.loadMasters = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        var datetm = '' + this.takedate.getTime();
        datetm = datetm.slice(0, -3);
        this.backendProv.getMastersEarl(this.selexpress.menue_id, this.selexpress.menue_interval * 60, this.offices, datetm)
            .then(function (res) {
            _this.masters = res;
            for (var v = 0; v < _this.masters.length; v++) {
                for (var p = 0; p < _this.professions.length; p++) {
                    if (_this.masters[v].user_work_pos == _this.professions[p].prof_id) {
                        _this.masters[v].user_prof = _this.professions[p].prof_name;
                    }
                }
            }
            loading.dismiss();
        })
            .catch(function (e) {
            console.log('ERRRROR ' + JSON.stringify(e));
        });
    };
    TabsPage.prototype.onSwipe = function (e) {
        if (e.offsetDirection == '2' && this.currentTab < this.offices.length - 1) {
            var newtab = parseInt(this.currentTab) + 1;
            this.superTabsCtrl.slideTo(newtab);
        }
        else if (e.offsetDirection == '4' && this.currentTab > 0) {
            var newtab = parseInt(this.currentTab) - 1;
            this.superTabsCtrl.slideTo(newtab);
        }
    };
    TabsPage.prototype.onTabSelect = function (ev) {
        this.currentTab = ev.in_id;
        // console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
    };
    TabsPage.prototype.ionViewDidEnter = function () {
    };
    return TabsPage;
}());
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/tabs/tabs.html"*/'<ion-header no-border>\n\n  <ion-navbar color="viol">\n\n    <ion-title>\n\n      Ближайшее время\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding no-bounce style="top:0;margin-top:0;background-image: url(\'assets/img/pattern.jpg\');background-position:center center;background-size: auto 100%;">\n\n  <super-tabs *ngIf="masters?.length > 0" (swipe)="onSwipe($event)" scrollTabs="true" id="mainTabs" selectedTabIndex="{{currentTab}}" toolbarColor="viol" toolbarBackground="" indicatorColor="oran" badgeColor="viol" (tabSelect)="onTabSelect($event)" class="withNavbar" no-border no-shadow>\n\n    <super-tab *ngFor="let office of offices" [root]="tab1Root" [rootParams]="{office: office, masters: selEarliest(office.office_id), menuecat: menuecat, menue: selexpress, menuegood: menuegood}" id="{{office.office_id}}" title="{{office.office_adress}}"></super-tab>\n\n  </super-tabs>\n\n</ion-content>'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/tabs/tabs.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic2_super_tabs__["a" /* SuperTabsController */], __WEBPACK_IMPORTED_MODULE_4__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RitualchoicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__spa_spa__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modalrituals_modalrituals__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RitualchoicePage = (function () {
    function RitualchoicePage(navCtrl, navParams, viewCtrl, loadingCtrl, backendProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this.backendProv = backendProv;
        this.choicerituals = false;
        this.choicespas = false;
        // let loading = this.loadingCtrl.create({
        // spinner: 'hide',
        // cssClass: 'loader',
        // content: `
        //   <img src="assets/img/loading.png" class="ld ldt-bounce-in infinite" width="52" height="52" /><p style="color:#fff !important;">Загрузка..</p>
        // `
        // });
        // loading.present();
        // setTimeout(() => {
        //   loading.dismiss();
        // }, 2000);
    }
    RitualchoicePage.prototype.ionViewWillEnter = function () {
        this.backendProv.startCheck();
    };
    RitualchoicePage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad RitualchoicePage');
    };
    RitualchoicePage.prototype.goSpas = function () {
        var _this = this;
        this.choicerituals = false;
        this.choicespas = true;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__spa_spa__["a" /* SpaPage */], { menue_cat: 185 });
        setTimeout(function () {
            _this.choicerituals = false;
            _this.choicespas = false;
        }, 300);
    };
    RitualchoicePage.prototype.goRituals = function () {
        var _this = this;
        this.choicerituals = true;
        this.choicespas = false;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__modalrituals_modalrituals__["a" /* ModalritualsPage */]);
        setTimeout(function () {
            _this.choicerituals = false;
            _this.choicespas = false;
        }, 300);
    };
    return RitualchoicePage;
}());
RitualchoicePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-ritualchoice',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/ritualchoice/ritualchoice.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-title>Тип записи</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-bounce>\n  <div class="violback">\n\n    <ion-card no-margin class="backtransparent">\n      <ion-grid no-padding>\n        <ion-row justify-content-around (click)="goRituals()">\n          <ion-col col-12 class="card-background-page">\n          <div [ngClass]="{\'doublelinesel\': choicerituals, \'card-title\': 1, \'orange\': choicerituals, \'doubleline\': !choicerituals, \'whitey\': !choicerituals}">\n              <div class="centering">\n                <ion-icon [ngClass]="{\'iconsel\': choicerituals}" name="custom_cook"></ion-icon>\n                ЗАПИСАТЬСЯ\n                <br/>НА РИТУАЛ\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n    <br/>\n    <ion-card no-margin class="backtransparent">\n      <ion-grid no-padding>\n        <ion-row justify-content-around (click)="goSpas()">\n          <ion-col col-12 class="card-background-page">\n            <div [ngClass]="{\'doublelinesel\': choicespas, \'card-title\': 1, \'orange\': choicespas, \'doubleline\': !choicespas, \'whitey\': !choicespas}">\n              <div class="centering">\n                <ion-icon [ngClass]="{\'iconsel\': choicespas}" name="custom_drop"></ion-icon>\n                ЗАПИСАТЬСЯ\n                <br/>НА СПА-ПРОГРАММУ\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n    \n  </div>\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/ritualchoice/ritualchoice.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_backend_backend__["a" /* BackendProvider */]])
], RitualchoicePage);

//# sourceMappingURL=ritualchoice.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__newsdetail_newsdetail__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewsPage = (function () {
    function NewsPage(navCtrl, menuCtrl, navParams, backendProv, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.loadingCtrl = loadingCtrl;
        this.news = [];
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        backendProv.getNews().then(function (res) {
            loading.dismiss();
            _this.news = res;
        }).catch(function (e) { return console.log(e); });
        this.newsPicLink = backendProv.newsPicLink;
    }
    NewsPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.newsPicLink + 'pic/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    NewsPage.prototype.goNewsDetail = function (val) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__newsdetail_newsdetail__["a" /* NewsdetailPage */], { news: val });
    };
    NewsPage.prototype.openMenu = function () {
        this.menuCtrl.open();
    };
    NewsPage.prototype.closeMenu = function () {
        this.menuCtrl.close();
    };
    NewsPage.prototype.toggleMenu = function () {
        this.menuCtrl.toggle();
    };
    NewsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad NewsPage');
    };
    return NewsPage;
}());
NewsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-news',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/news/news.html"*/'<ion-header>\n  <ion-navbar color="viol">\n    <ion-buttons left>\n      <button ion-button icon-end color="light" (click)="toggleMenu()" style="font-size:20px;">\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>\n      Новости\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n    \n  <ion-card class="backtransparent">\n    <ion-grid no-padding>\n      <ion-row justify-content-around margin-bottom *ngFor="let new of news; let i=index">\n        <ion-col col-5 class="card-background-page" (click)="goNewsDetail(news[i])" *ngIf="i < news.length && i % 2 === 0">\n          <img src="{{ checkPic(news[i].news_pic) }}"/>\n          <div class="card-title everyline">\n            <div class="centering" [innerHTML]="news[i].news_name"></div>\n          </div>\n        </ion-col>\n        <ion-col col-5 class="card-background-page" (click)="goNewsDetail(news[i+1])" *ngIf="i + 1 < news.length && i % 2 === 0">\n          <img src="{{ checkPic(news[i+1].news_pic) }}"/>\n          <div class="card-title everyline">\n            <div class="centering" [innerHTML]="news[i+1].news_name"></div>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/news/news.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], NewsPage);

//# sourceMappingURL=news.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GiftsdetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GiftsdetailPage = (function () {
    function GiftsdetailPage(navCtrl, navParams, backendProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.gifts = [];
        this.createdCode = null;
        this.gifts.push(navParams.get('gift'));
        this.giftsPicLink = backendProv.giftsPicLink;
        var newdate = new Date();
        var datetime = Math.round(newdate.getTime() / 1000);
        this.createdCode = this.gifts[0].gifts_id + '&' + backendProv.myid + '&' + datetime + '&' + backendProv.makeid() + '&' + this.gifts[0].gifts_points;
    }
    GiftsdetailPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.giftsPicLink + 'pic/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    GiftsdetailPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad GiftsdetailPage');
    };
    return GiftsdetailPage;
}());
GiftsdetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-giftsdetail',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/giftsdetail/giftsdetail.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-title [innerHTML]="gifts[0].gifts_name"></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-card *ngFor="let gift of gifts">\n    <img src="{{ checkPic(gift.gifts_pic) }}">\n    <ion-card-content>\n      <ion-card-title [innerHTML]="gift.gifts_name"></ion-card-title>\n      <p [innerHTML]="gift.gifts_desc"></p>\n      <br/>\n      <h2>Стоимость: {{ gift.gifts_points }} баллов</h2>\n    </ion-card-content>\n    <div *ngIf="createdCode">\n      <!-- <button ion-button block color="viol">Получить</button> -->\n      <ngx-qrcode [qrc-value]="createdCode" qrc-errorCorrectionLevel="Q"></ngx-qrcode>\n    </div>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/giftsdetail/giftsdetail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__["a" /* BackendProvider */]])
], GiftsdetailPage);

//# sourceMappingURL=giftsdetail.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ServiceDetailsPage = (function () {
    function ServiceDetailsPage(navCtrl, navParams, backendProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.services = [];
        this.services.push(navParams.get('ritual'));
        this.menuePicLink = backendProv.menuePicLink;
    }
    ServiceDetailsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad ServiceDetailsPage');
    };
    ServiceDetailsPage.prototype.priceConvert = function (val) {
        return (val / 100).toFixed(2);
    };
    ServiceDetailsPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.menuePicLink + '300/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    ServiceDetailsPage.prototype.mkOrder = function (id) {
    };
    return ServiceDetailsPage;
}());
ServiceDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-service-details',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/service-details/service-details.html"*/'<ion-header>\n\n  <ion-navbar color="viols">\n    <ion-title [innerHtml]="services[0].menue_name"></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding>\n\n  <ion-card *ngFor="let service of services">\n    <img src="{{ checkPic(service.menue_pic) }}"/>\n    <ion-card-content padding>\n      <ion-card-title uppercase [innerHtml]="service.menue_name"></ion-card-title>\n      <ion-row>\n        <ion-col>\n          от {{ priceConvert(service.menue_cost) }} BYN\n          <br/>\n          <ion-note>цена</ion-note>\n        </ion-col>\n        <ion-col>\n          {{ service.menue_interval }}\n          <br/>\n          <ion-note>продолжительность сеанса</ion-note>\n        </ion-col>\n      </ion-row>\n      <p [innerHtml]="service.menue_desc"></p>\n      <!-- <button ion-button block (click)="mkOrder(service.menue_id)" color="viol">Записаться</button> -->\n    </ion-card-content>\n  </ion-card>\n  \n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/service-details/service-details.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__["a" /* BackendProvider */]])
], ServiceDetailsPage);

//# sourceMappingURL=service-details.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveysPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SurveysPage = (function () {
    function SurveysPage(app, navCtrl, navParams, menuCtrl, backendProv, loadingCtrl, alertCtrl) {
        this.app = app;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.backendProv = backendProv;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.surveys = [];
        this.answers = {};
    }
    SurveysPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        this.backendProv.getSurveys()
            .then(function (res) {
            _this.surveys = res;
            loading.dismiss();
        })
            .catch(function (e) { });
    };
    SurveysPage.prototype.goHome = function () {
        this.nav = this.app.getRootNavById('n4');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    SurveysPage.prototype.decodeEntities = function (val) {
        return this.backendProv.decodeEntities(val);
    };
    SurveysPage.prototype.sendAnswers = function () {
        var _this = this;
        if (this.answers) {
            if (Object.keys(this.answers).length > 0) {
                var m_1 = 0;
                Object.keys(this.answers).forEach(function (item) {
                    _this.backendProv.httpRequest(JSON.stringify({
                        device: _this.backendProv.model,
                        device_id: _this.backendProv.uuid,
                        device_version: _this.backendProv.version,
                        device_os: _this.backendProv.platform,
                        inst_id: _this.backendProv.institution,
                        newusr: 'asks',
                        asks_id: item,
                        asks_answ: _this.answers[item],
                        asks_vers: 2
                    }))
                        .subscribe(function (res) {
                        _this.backendProv.saveRequestRes(res, item, _this.answers[item]);
                        if (m_1 == Object.keys(_this.answers).length - 1) {
                            _this.surveys = [];
                            setTimeout(function () {
                                _this.goHome();
                                var alert = _this.alertCtrl.create({
                                    title: 'Благодарим',
                                    subTitle: 'Спасибо за ответы!',
                                    buttons: ['Закрыть']
                                });
                                alert.present();
                            }, 1000);
                        }
                        m_1++;
                    }, function (e) {
                        console.log(e);
                    });
                });
            }
        }
    };
    SurveysPage.prototype.toggleMenu = function () {
        this.menuCtrl.toggle();
    };
    return SurveysPage;
}());
SurveysPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-surveys',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/surveys/surveys.html"*/'<ion-header>\n  <ion-navbar color="viol">\n    <ion-buttons left>\n      <button ion-button icon-end color="light" (click)="toggleMenu()" style="font-size:20px;">\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <p *ngIf="surveys?.length > 0">\n    <ion-card text-wrap *ngFor="let survey of surveys; let i=index">\n      \n      <ion-card-header *ngIf="survey.asks_name != 0 && survey.asks_name">\n        {{ decodeEntities(survey.asks_name) }}\n      </ion-card-header>\n\n      <ion-card-content *ngIf="survey.asks_message != 0 && survey.asks_message" class="violback">\n        {{ decodeEntities(survey.asks_message) }}\n      </ion-card-content>\n\n      <ion-list radio-group *ngIf="survey.asks_type == \'0\'" [(ngModel)]="answers[survey.asks_id]">\n        <ion-item *ngFor="let answ of survey.asks_answ; let u=index">\n          <ion-label>{{ decodeEntities(answ) }}</ion-label>\n          <ion-radio value="{{u}}"></ion-radio>\n        </ion-item>\n      </ion-list>\n      \n      <ion-card-content *ngIf="survey.asks_type == \'1\'">\n        <textarea placeholder="Ваш ответ.." [(ngModel)]="answers[survey.asks_id]" style="padding:15px;width:100%;height:100px;word-wrap: break-word;"></textarea>\n      </ion-card-content>\n\n    </ion-card>\n  </p>\n\n  <ion-card *ngIf="!surveys || surveys?.length == 0">\n    <ion-card-content>\n      В данный момент нет никаких опросов.\n    </ion-card-content>\n  </ion-card>\n\n  <button ion-button block (click)="sendAnswers()" color="viol" *ngIf="surveys?.length > 0">Отправить</button>\n\n  <p>&nbsp;</p>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/surveys/surveys.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], SurveysPage);

//# sourceMappingURL=surveys.js.map

/***/ }),

/***/ 182:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 182;

/***/ }),

/***/ 225:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 225;

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QrsummPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__qrcode_qrcode__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var QrsummPage = (function () {
    function QrsummPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.expression = (0).toFixed(2);
        this.realexpression = 0;
        this.device_id = 1234567890;
        this.calcbill = function () {
            if (this.realexpression != "" && this.realexpression != '0') {
                this.goQRCode();
            }
            else {
                var alert_1 = this.alertCtrl.create({
                    title: 'Какая сумма чека?',
                    subTitle: 'Укажите сумму чека!',
                    buttons: ['Закрыть']
                });
                alert_1.present();
            }
        };
        this.expression = (0).toFixed(2);
        this.realexpression = 0;
    }
    QrsummPage.prototype.addVal = function (value) {
        if (this.realexpression === "" || this.realexpression === undefined || this.realexpression === 0) {
            this.realexpression = value;
            this.expression = (this.realexpression / 100).toFixed(2);
        }
        else if (this.realexpression.length === undefined || this.realexpression.length <= 7) {
            this.realexpression = this.realexpression + "" + value;
            this.expression = (parseInt(this.realexpression) / 100).toFixed(2);
        }
    };
    QrsummPage.prototype.removeText = function () {
        if (this.realexpression != "" && this.realexpression != undefined && this.realexpression != 0 && this.realexpression.length != undefined) {
            if (this.realexpression.length == 1) {
                this.realexpression = 0;
                this.expression = (0).toFixed(2);
            }
            else {
                this.realexpression = this.realexpression.slice(0, -1);
                this.expression = (this.realexpression / 100).toFixed(2);
            }
        }
        else {
            this.realexpression = 0;
            this.expression = (0).toFixed(2);
        }
    };
    QrsummPage.prototype.goQRCode = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__qrcode_qrcode__["a" /* QrcodePage */], {
            type: 'points',
            realexpression: this.realexpression,
            device_id: this.device_id,
        });
    };
    QrsummPage.prototype.add = function (val) {
    };
    QrsummPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad QrsummPage');
    };
    return QrsummPage;
}());
QrsummPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-qrsumm',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/qrsumm/qrsumm.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n\n    <ion-title>\n\n      Сумма\n\n    </ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button icon-begin color="light" (click)="calcbill()">\n\n        Далее\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content no-padding scroll="false">\n\n  \n\n  <div class="violback list2bottom" style="padding-top:50px;padding-bottom:20px;">\n\n\n\n  <ion-card class="backtransparent">\n\n    <ion-grid no-padding>\n\n      <ion-row justify-content-around>\n\n        <ion-col style="text-align: center;">\n\n          <h6 class="whitey" style="margin:0 auto;font-size:13px;">Укажите потраченную сумму</h6>\n\n          <h1 class="whitey" style="font-size:40px;margin-bottom:0px;padding-bottom:0px;">{{ expression }}</h1>\n\n          <h4 class="whitey" style="font-size:13px;padding-top:0px;margin-top:0px;">руб</h4>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-4 class="bordtop">\n\n          <button ion-button clear full color="violcontr" (click)="addVal(1)">1</button>\n\n        </ion-col>\n\n        <ion-col col-4 class="bordleftrighttop">\n\n          <button ion-button clear full color="violcontr" (click)="addVal(2)">2</button>\n\n        </ion-col>\n\n        <ion-col col-4 class="bordtop">\n\n          <button ion-button clear full color="violcontr" (click)="addVal(3)">3</button>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-4 class="bordtop">\n\n          <button ion-button clear full color="violcontr" (click)="addVal(4)">4</button>\n\n        </ion-col>\n\n        <ion-col col-4 class="bordleftrighttop">\n\n          <button ion-button clear full color="violcontr" (click)="addVal(5)">5</button>\n\n        </ion-col>\n\n        <ion-col col-4 class="bordtop">\n\n          <button ion-button clear full color="violcontr" (click)="addVal(6)">6</button>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-4 class="bordtop">\n\n          <button ion-button clear full color="violcontr" (click)="addVal(7)">7</button>\n\n        </ion-col>\n\n        <ion-col col-4 class="bordleftrighttop">\n\n          <button ion-button clear full color="violcontr" (click)="addVal(8)">8</button>\n\n        </ion-col>\n\n        <ion-col col-4 class="bordtop">\n\n          <button ion-button clear full color="violcontr" (click)="addVal(9)">9</button>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-4 class="bordtopbottom">\n\n          <button ion-button clear full color="violcontr"></button> \n\n        </ion-col>\n\n        <ion-col col-4 class="bordleftrighttopbottom">\n\n          <button ion-button clear full color="violcontr" (click)="addVal(0)">0</button>\n\n        </ion-col>\n\n        <ion-col col-4 class="bordtopbottom">\n\n          <button ion-button clear full color="violcontr" (click)="removeText()">\n\n            <ion-icon name="backspace"></ion-icon>\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n\n\n  </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/qrsumm/qrsumm.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], QrsummPage);

//# sourceMappingURL=qrsumm.js.map

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpresschoicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_timepoints_timepoints__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modalmasters_modalmasters__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ordered_ordered__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__profiledetails_profiledetails__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ExpresschoicePage = (function () {
    function ExpresschoicePage(navCtrl, navParams, modalCtrl, backendProv, menuCtrl, actionSheetCtrl, loadingCtrl, tpp, datePipe, app, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.backendProv = backendProv;
        this.menuCtrl = menuCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadingCtrl = loadingCtrl;
        this.tpp = tpp;
        this.datePipe = datePipe;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.masters = [];
        this.selplace = 0;
        this.initval = 0;
        this.offices = 0;
        this.officeid = 0;
        this.grouped = [];
        this.menuecat = 0;
        this.ritual = 0;
        this.menuegood = 0;
        this.steaming = 'Без распаривания.';
        this.offices = this.navParams.get('office');
        this.masters = this.navParams.get('masters');
        // console.log(JSON.stringify(this.masters));
        this.menuecat = this.navParams.get('menuecat');
        this.ritual = this.navParams.get('menue');
        this.menuegood = this.navParams.get('menuegood');
        this.officeid = this.offices.office_id;
        this.piclink = backendProv.piclink;
        this.groupByDate();
    }
    ExpresschoicePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.backendProv.getProfile()
            .then(function (res) {
            _this.profile = res;
        })
            .catch(function (e) { return console.log(e); });
    };
    ExpresschoicePage.prototype.compare = function (a, b) {
        if (a.dayday < b.dayday)
            return -1;
        if (a.dayday > b.dayday)
            return 1;
        return 0;
    };
    ExpresschoicePage.prototype.preEntry = function (val, val2) {
        var _this = this;
        var preRequestAS = this.actionSheetCtrl.create({
            title: 'Нужно распаривание?',
            cssClass: 'action-subtitle',
            buttons: [
                {
                    text: 'Да',
                    handler: function () {
                        _this.steaming = 'Нужно распаривание!';
                        _this.entryRequest(val, val2);
                    }
                },
                {
                    text: 'Нет',
                    handler: function () {
                        _this.steaming = 'Без распаривания.';
                        _this.entryRequest(val, val2);
                    }
                }
            ]
        });
        preRequestAS.present();
    };
    ExpresschoicePage.prototype.entryRequest = function (val, val2) {
        var _this = this;
        this.takedate = new Date(val.user_unixtime * 1000);
        this.takedate.setHours(0, 0, 0, 0);
        var entryRequestAS = this.actionSheetCtrl.create({
            title: val.user_name,
            subTitle: this.datePipe.transform(this.takedate, 'dd') + ' ' + this.datePipe.transform(this.takedate, 'MMM') + ' ' + this.tpp.transform(val.user_daytime),
            cssClass: 'action-subtitle',
            buttons: [
                {
                    text: 'Записаться',
                    handler: function () {
                        var loading = _this.loadingCtrl.create({
                            spinner: 'hide',
                            cssClass: 'loader',
                            content: "\n                <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n              "
                        });
                        loading.present();
                        loading.dismiss();
                        var orderstr = {
                            inst_id: _this.backendProv.institution,
                            newusr: 'calender',
                            device_id: _this.backendProv.uuid,
                            getset: 1,
                            menueId: _this.ritual.menue_id,
                            menueName: 'Экспресс запись - ' + _this.ritual.menue_name,
                            menueCost: 0,
                            menueDesc: 'Экспресс запись - ' + _this.ritual.menue_name,
                            workerId: val.user_real_id,
                            workerName: val.user_name,
                            workerPic: val.user_pic,
                            workerProfession: val.user_prof,
                            orderHour: val.user_unixtime,
                            orderHourName: val.user_daytime,
                            orderDayName: _this.datePipe.transform(_this.takedate, 'dd') + '.' + _this.datePipe.transform(_this.takedate, 'MM') + '.' + _this.datePipe.transform(_this.takedate, 'y'),
                            ordercats: _this.menuecat,
                            ordgood: _this.menuegood,
                            name: _this.profile[0].user_name,
                            phone: _this.profile[0].user_mob,
                            email: _this.profile[0].user_email,
                            ordoffice: _this.officeid,
                            comments: _this.steaming,
                            reminder: '',
                            smsconf: ''
                        };
                        // console.log('SENT: '+JSON.stringify(orderstr));
                        _this.backendProv.httpRequest(JSON.stringify(orderstr))
                            .subscribe(function (res) {
                            // console.log('GOT: '+JSON.stringify(res));
                            loading.dismiss();
                            if (res[0].orderOK == '3') {
                                var alert_1 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Повторный запрос только через 5 минут!',
                                    buttons: ['Закрыть']
                                });
                                alert_1.present();
                            }
                            else if (res[0].orderOK == '2') {
                                var alert_2 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Вы уже записались на выбранную услугу либо на выбранное время. Ожидайте одобрения или звонка!',
                                    buttons: ['Закрыть']
                                });
                                alert_2.present();
                            }
                            else if (res[0].orderOK == '1') {
                                _this.backendProv.orderSentRecord(res[0]);
                                _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_5__ordered_ordered__["a" /* OrderedPage */], { dateday: _this.datePipe.transform(_this.takedate, 'dd'), datemonth: _this.datePipe.transform(_this.takedate, 'MMMM'), datetime: val.user_daytime, dateplace: _this.offices, unixtime: val.user_unixtime });
                            }
                            else if (res[0].orderOK == '5') {
                                var alert_3 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Необходимо подтвердить номер телефона в личном кабинете',
                                    buttons: [
                                        {
                                            text: 'Закрыть'
                                        },
                                        {
                                            text: 'Подтвердить',
                                            handler: function (data) {
                                                _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_6__profiledetails_profiledetails__["a" /* ProfiledetailsPage */], { profile: _this.profile[0] });
                                            }
                                        }
                                    ]
                                });
                                alert_3.present();
                            }
                            else if (res[0].orderOK == '8') {
                                var alert_4 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Нет свободной комнаты!',
                                    buttons: ['Закрыть']
                                });
                                alert_4.present();
                            }
                            else if (res[0].orderOK == '9') {
                                var alert_5 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Нет свободной комнаты!',
                                    buttons: ['Закрыть']
                                });
                                alert_5.present();
                            }
                        }, function (e) {
                            console.log(e);
                            loading.dismiss();
                        });
                    }
                },
                {
                    text: 'Отменить',
                    role: 'cancel',
                    handler: function () {
                        // console.log('Cancel clicked');
                    }
                }
            ]
        });
        entryRequestAS.present();
    };
    ExpresschoicePage.prototype.masterDetails = function (val, val2) {
        var _this = this;
        this.takedate = new Date(val.unixtime * 1000);
        this.takedate.setHours(0, 0, 0, 0);
        var masterDetailModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modalmasters_modalmasters__["a" /* ModalmastersPage */], { master: val, mastername: val.user_name, takedate: this.takedate, selwork: 0, dateplace: val2 });
        masterDetailModal.onDidDismiss(function (data) {
            if (data) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__ordered_ordered__["a" /* OrderedPage */], data);
            }
        });
        masterDetailModal.present();
    };
    ExpresschoicePage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.piclink + val;
        }
        else {
            return 'assets/img/girl3.png';
        }
    };
    ExpresschoicePage.prototype.groupByDate3 = function (val1, val2) {
        var goon = 0;
        if (this.grouped.length > 0) {
            for (var i = 0; i < this.grouped.length; i++) {
                if (goon == 0) {
                    if (val1.dayday == this.grouped[i].dayday) {
                        var newval2 = {
                            user_real_id: val2.user_real_id,
                            user_pic: val2.user_pic,
                            user_name: val2.user_name,
                            user_prof: val2.user_prof,
                            user_daytime: val1.daytime,
                            user_unixtime: val1.unixtime
                        };
                        this.grouped[i].masters.push(newval2);
                        this.grouped.sort(this.compare);
                        // console.log(JSON.stringify(this.grouped))
                        goon = 1;
                    }
                    else if (i == this.grouped.length - 1 && val1 != this.grouped[i].dayday) {
                        var newGroup = {
                            dayday: val1.dayday,
                            masters: []
                        };
                        var newval2 = {
                            user_real_id: val2.user_real_id,
                            user_pic: val2.user_pic,
                            user_name: val2.user_name,
                            user_prof: val2.user_prof,
                            user_daytime: val1.daytime,
                            user_unixtime: val1.unixtime
                        };
                        newGroup.masters.push(newval2);
                        this.grouped.push(newGroup);
                        this.grouped.sort(this.compare);
                        // console.log(JSON.stringify(this.grouped))
                        goon = 1;
                    }
                }
            }
        }
        else {
            var newGroup = {
                dayday: val1.dayday,
                masters: []
            };
            var newval2 = {
                user_real_id: val2.user_real_id,
                user_pic: val2.user_pic,
                user_name: val2.user_name,
                user_prof: val2.user_prof,
                user_daytime: val1.daytime,
                user_unixtime: val1.unixtime
            };
            newGroup.masters.push(newval2);
            this.grouped.push(newGroup);
            this.grouped.sort(this.compare);
            // console.log(JSON.stringify(this.grouped))
        }
    };
    ExpresschoicePage.prototype.groupByDate2 = function (val) {
        if (val.user_work_time.length > 0) {
            for (var i = 0; i < val.user_work_time.length; i++) {
                this.groupByDate3(val.user_work_time[i], val);
            }
        }
    };
    ExpresschoicePage.prototype.groupByDate = function () {
        for (var i = 0; i < this.masters.length; i++) {
            this.groupByDate2(this.masters[i]);
        }
    };
    ExpresschoicePage.prototype.switchPlace = function (id) {
        this.selplace = id;
    };
    ExpresschoicePage.prototype.getByDate = function (date) {
        var master = [];
        if (this.masters.length > 0) {
            for (var i = 0; i < this.masters.length; i++) {
                if (this.masters[i].id <= this.navParams.get('test')) {
                    master.push(this.masters[i]);
                }
            }
            return master;
        }
        return master;
    };
    return ExpresschoicePage;
}());
ExpresschoicePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-expresschoice',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/expresschoice/expresschoice.html"*/'<!-- <ion-header no-border>\n\n  <ion-navbar color="viol">\n\n    <ion-title>\n\n      Ближайшее время\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header> -->\n\n\n\n<ion-content no-padding>\n\n\n\n  <!-- <ion-toolbar no-border>\n\n    <ion-segment color="oran">\n\n      <ion-segment-button *ngFor="let place of places" value="{{place.id}}" [ngClass]="place.id == selplace ? \'segment-activated\' : \'\'" (click)="switchPlace(place.id)">\n\n          {{ place.name }}\n\n      </ion-segment-button>\n\n    </ion-segment>\n\n  </ion-toolbar> -->\n\n  <ion-card *ngIf="grouped?.length > 0" no-border style="margin:0 auto;">\n\n    <ion-grid no-padding>\n\n      <ion-list no-border *ngFor="let group of grouped">\n\n        <ion-list-header>\n\n          <h2>{{ group.dayday }}</h2>\n\n        </ion-list-header>\n\n        <ion-item text-wrap *ngFor="let master of group.masters">\n\n          <ion-thumbnail item-start (click)="preEntry(master, group)">\n\n            <img src="{{checkPic(master.user_pic)}}">\n\n          </ion-thumbnail>\n\n          <h2 (click)="preEntry(master, group)">\n\n            <span class="violethalf">Время: </span><span class="whitey">{{ master.user_daytime | timepoints }}</span>\n\n          </h2>\n\n          <p (click)="preEntry(master, group)">\n\n            <span class="violethalf">{{ master.user_prof }}: </span><span class="whitey">{{ master.user_name }}</span>\n\n          </p>\n\n          <button (click)="masterDetails(master, group)" ion-button clear icon-only item-end large color="golds"><ion-icon name="ios-information-circle-outline"></ion-icon></button>\n\n        </ion-item>\n\n      </ion-list>\n\n    </ion-grid>\n\n  </ion-card>\n\n\n\n  <ion-card *ngIf="grouped?.length == 0" class="backtransparent">\n\n    <ion-card-content>\n\n      <span text-wrap style="color:#f00;font-size:1.2em;">К сожалению в ближайшее время все занято.</span>\n\n    </ion-card-content>\n\n  </ion-card>\n\n\n\n  <p>&nbsp;</p>\n\n  <p>&nbsp;</p>\n\n  <p>&nbsp;</p>\n\n\n\n</ion-content>\n\n  '/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/expresschoice/expresschoice.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */], __WEBPACK_IMPORTED_MODULE_7__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__pipes_timepoints_timepoints__["a" /* TimepointsPipe */], __WEBPACK_IMPORTED_MODULE_2__angular_common__["c" /* DatePipe */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], ExpresschoicePage);

//# sourceMappingURL=expresschoice.js.map

/***/ }),

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_timepoints_timepoints__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modalmasters_modalmasters__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ordered2_ordered2__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SpaPage = (function () {
    function SpaPage(navCtrl, navParams, modalCtrl, backendProv, menuCtrl, actionSheetCtrl, loadingCtrl, tpp, datePipe, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.backendProv = backendProv;
        this.menuCtrl = menuCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadingCtrl = loadingCtrl;
        this.tpp = tpp;
        this.datePipe = datePipe;
        this.alertCtrl = alertCtrl;
        this.datetoday = new Date();
        this.masterworks = 0;
        this.currentIndex = 0;
        this.currentIndex2 = 0;
        this.dates = [];
        this.offices = [];
        this.masters = [];
        this.masterwork = [];
        this.spas = [];
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        backendProv.checkWaiter()
            .then(function (res) {
            loading.dismiss();
            _this.startingLoads();
        })
            .catch(function (err) {
            loading.dismiss();
            _this.startingLoads();
        });
    }
    SpaPage.prototype.startingLoads = function () {
        var _this = this;
        // FROM DB
        this.piclink = this.backendProv.piclink;
        this.officePicLink = this.backendProv.officePicLink;
        this.datetoday = this.backendProv.timezoneAdd((new Date().getTime() / 1000).toFixed(0));
        this.datetoday = new Date(this.datetoday * 1000);
        this.datetoday.setHours(0, 0, 0, 0);
        this.takedate = this.datetoday;
        this.selunixtime = (this.takedate.getTime() / 1000).toFixed(0);
        this.backendProv.getDates()
            .then(function (res) { _this.dates = res; })
            .catch(function (e) { return console.log(e); });
        this.loadOffices();
        this.loadSpas();
        this.backendProv.getProfile()
            .then(function (res) { return _this.profile = res; })
            .catch(function (e) { return console.log(e); });
        this.backendProv.getGoods()
            .then(function (res) { return _this.goodsid = res[0].goods_id; })
            .catch(function (e) { return console.log(e); });
    };
    SpaPage.prototype.ionViewDidLoad = function () {
        this.navCtrl.swipeBackEnabled = false;
        // READY SLIDE SETTS
        this.slides.loop = false;
        this.slides.pager = false;
        this.slides.effect = "slide";
        this.slides.initialSlide = 0;
        this.slides.centeredSlides = true;
        this.slides.slidesPerView = 2;
        this.slides.spaceBetween = 30;
        this.slides2.loop = false;
        this.slides2.pager = false;
        this.slides2.effect = "slide";
        this.slides2.initialSlide = 0;
        this.slides2.centeredSlides = false;
        this.slides2.slidesPerView = 7;
        this.slides2.spaceBetween = 30;
        // this.slides3.loop = false;
        // this.slides3.pager = false;
        // this.slides3.effect = "slide";
        // this.slides3.initialSlide = 0;
        // this.slides3.slidesPerView = 2;
        // this.slides3.spaceBetween = 30;
        // this.slides3.centeredSlides = true;
        this.slides.control = this.slides4;
        this.slides4.control = this.slides;
    };
    SpaPage.prototype.nextSlide = function () {
        this.slides.slideNext();
    };
    SpaPage.prototype.prevSlide = function () {
        this.slides.slidePrev();
    };
    SpaPage.prototype.entryRequest = function () {
        var _this = this;
        var entryRequestAS = this.actionSheetCtrl.create({
            title: 'Записаться?',
            subTitle: this.datePipe.transform(this.takedate, 'dd') + ' ' + this.datePipe.transform(this.takedate, 'MMM'),
            cssClass: 'action-subtitle',
            buttons: [
                {
                    text: 'Записаться',
                    handler: function () {
                        var loading = _this.loadingCtrl.create({
                            spinner: 'hide',
                            cssClass: 'loader',
                            content: "\n                <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n              "
                        });
                        loading.present();
                        var orderstr = {
                            inst_id: _this.backendProv.institution,
                            newusr: 'calender',
                            device_id: _this.backendProv.uuid,
                            getset: 1,
                            menueId: _this.spaid,
                            menueName: _this.spaname,
                            menueCost: 0,
                            menueDesc: _this.spadesc,
                            workerId: 0,
                            workerName: 0,
                            workerPic: 0,
                            workerProfession: 0,
                            orderHour: _this.backendProv.timezoneSub(_this.selunixtime),
                            orderHourName: 0,
                            orderDayName: _this.datePipe.transform(_this.takedate, 'dd') + '.' + _this.datePipe.transform(_this.takedate, 'MM') + '.' + _this.datePipe.transform(_this.takedate, 'y'),
                            ordercats: _this.navParams.get('menue_cat'),
                            ordgood: _this.goodsid,
                            name: _this.profile[0].user_name,
                            phone: _this.profile[0].user_mob,
                            email: _this.profile[0].user_email,
                            ordoffice: _this.selOffice(_this.currentIndex).office_id,
                            comments: '',
                            reminder: '',
                            smsconf: ''
                        };
                        // console.log('ORDER: '+JSON.stringify(orderstr));
                        _this.backendProv.httpRequest(JSON.stringify(orderstr))
                            .subscribe(function (res) {
                            loading.dismiss();
                            if (res[0].orderOK == '3') {
                                var alert_1 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Повторный запрос только через 5 минут!',
                                    buttons: ['Закрыть']
                                });
                                alert_1.present();
                            }
                            else if (res[0].orderOK == '2') {
                                var alert_2 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Вы уже записались на выбранную услугу либо на выбранное время. Ожидайте одобрения или звонка!',
                                    buttons: ['Закрыть']
                                });
                                alert_2.present();
                            }
                            else if (res[0].orderOK == '1') {
                                _this.backendProv.orderSentRecord(res[0]);
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__ordered2_ordered2__["a" /* Ordered2Page */], { dateday: _this.datePipe.transform(_this.takedate, 'dd'), datemonth: _this.datePipe.transform(_this.takedate, 'MMMM'), dateplace: _this.selOffice(_this.currentIndex), unixtime: _this.selunixtime });
                                _this.navCtrl.popToRoot();
                            }
                            else if (res[0].orderOK == '1') {
                                _this.backendProv.orderSentRecord(res[0]);
                            }
                            else if (res[0].orderOK == '5') {
                                var alert_3 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Укажите Ваш номер телефона!',
                                    buttons: ['Закрыть']
                                });
                                alert_3.present();
                            }
                            else if (res[0].orderOK == '8') {
                                var alert_4 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Нет свободной комнаты!',
                                    buttons: ['Закрыть']
                                });
                                alert_4.present();
                            }
                            else if (res[0].orderOK == '9') {
                                var alert_5 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Нет свободной комнаты!',
                                    buttons: ['Закрыть']
                                });
                                alert_5.present();
                            }
                        }, function (e) { console.log(e); loading.dismiss(); });
                    }
                },
                {
                    text: 'Отменить',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        entryRequestAS.present();
    };
    SpaPage.prototype.checkStatus = function () {
        if (!this.selspa && this.selspa != 0) {
            this.statusmessage = 'Выберите услугу';
        }
        else if (this.masterwork.length == 0 && this.masterworks > 0 && this.selspa >= 0) {
            this.statusmessage = 'К сожалению данный мастер уже расписан на весь день.';
        }
        else if (this.masterworks == 0 && this.selspa >= 0) {
            this.statusmessage = 'К сожалению на выбранную услугу все мастера заняты.';
        }
        else {
            this.statusmessage = undefined;
        }
    };
    SpaPage.prototype.loadSpas = function () {
        var _this = this;
        this.backendProv.getRituals(this.navParams.get('menue_cat'))
            .then(function (res) {
            _this.spas = res;
            _this.checkStatus();
        })
            .catch();
    };
    SpaPage.prototype.loadOffices = function () {
        var _this = this;
        this.backendProv.getOffices()
            .then(function (res) {
            _this.offices = res;
            // initial Slide visible
            _this.currentIndex = _this.slides.initialSlide;
            _this.currentIndex2 = _this.slides2.initialSlide;
            // this.currentIndex3 = this.slides3.initialSlide;
            _this.slides.centeredSlides = true;
            _this.checkStatus();
            setTimeout(function () {
                _this.slides.slideTo(0, 100);
            }, 200);
        })
            .catch();
    };
    SpaPage.prototype.chooseSpa = function (val) {
        this.selspa = val;
        for (var i = 0; i < this.spas.length; i++) {
            if (val == this.spas[i].in_id) {
                this.spaid = this.spas[i].menue_id;
                this.spaname = this.spas[i].menue_name;
                this.spadesc = this.spas[i].menue_desc;
            }
        }
        this.content.scrollToBottom(1000);
    };
    SpaPage.prototype.recalcPhone = function (tel) {
        if (!tel) {
            return '';
        }
        var value = tel.toString().trim().replace(/^\+/, '');
        if (value.match(/[^0-9]/)) {
            return tel;
        }
        var country, city, number;
        switch (value.length) {
            case 10:
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;
            case 11:
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;
            case 12:
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;
            case 13:
                country = value.slice(0, 2);
                city = value.slice(2, 5);
                number = value.slice(5);
                break;
            default:
                return tel;
        }
        if (country == 1) {
            country = "";
        }
        number = number.slice(0, 3) + '-' + number.slice(3);
        return (country + " (" + city + ") " + number).trim();
    };
    SpaPage.prototype.formatBusHours = function (val) {
        return val.slice(0, 2) + ':' + val.slice(2, 4) + '-' + val.slice(4, 6) + ':' + val.slice(6, 8);
    };
    SpaPage.prototype.recalcBusHours = function (val) {
        var valold = '';
        var startday = '';
        var endday = '';
        var valfinal = [];
        var days = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
        for (var i = 0; i < val.length; i++) {
            if (valold == val[i]) {
                endday = ' - ' + days[i];
                if (i == val.length - 1) {
                    valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                }
            }
            else {
                if (startday != '') {
                    valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    startday = days[i];
                    endday = '';
                    valold = val[i];
                    if (i == val.length - 1) {
                        valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    }
                }
                else {
                    startday = days[i];
                    valold = val[i];
                    if (i == val.length - 1) {
                        valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    }
                }
            }
        }
        return valfinal;
    };
    SpaPage.prototype.selOffice = function (val) {
        var seloffice;
        for (var i = 0; i < this.offices.length; i++) {
            if (val == this.offices[i].in_id) {
                seloffice = this.offices[i];
            }
        }
        return seloffice;
    };
    SpaPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.piclink + val;
        }
        else {
            return 'assets/img/girl3.png';
        }
    };
    SpaPage.prototype.checkPicComp = function (val) {
        if (val && val != '0') {
            return this.officePicLink + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    SpaPage.prototype.masterDetails = function () {
        var masterDetailModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modalmasters_modalmasters__["a" /* ModalmastersPage */], { master: this.masterid });
        masterDetailModal.onDidDismiss(function (data) { });
        masterDetailModal.present();
    };
    SpaPage.prototype.selWork = function (work) {
        this.selwork = work;
    };
    SpaPage.prototype.masterChanged = function () {
        // selected slide visible, others not
        // this.currentIndex3 = this.slides3.realIndex;
        // this.masterid = this.masters[this.currentIndex3].in_id;
        // this.mastername = this.masters[this.currentIndex3].user_name;
        // this.masterwork = this.masters[this.currentIndex3].user_work_time;
    };
    SpaPage.prototype.officeChanged = function () {
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 1000);
        this.masterwork = [];
        this.selspa = undefined;
        this.masterworks = 0;
        // selected slide visible, others not
        this.currentIndex = this.slides.realIndex;
        this.selOffice(this.currentIndex);
        this.loadSpas();
    };
    SpaPage.prototype.toggleDate = function (dat) {
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 1000);
        this.takedate = dat;
        this.selunixtime = (this.takedate.getTime() / 1000).toFixed(0);
        this.content.scrollToTop(1000);
        this.masterwork = [];
        this.selspa = undefined;
        this.masterworks = 0;
        this.slides.initialSlide = this.currentIndex;
        this.loadOffices();
        this.loadSpas();
    };
    return SpaPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slidesOne'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], SpaPage.prototype, "slides", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slidesTwo'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], SpaPage.prototype, "slides2", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slidesFour'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], SpaPage.prototype, "slides4", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Content */])
], SpaPage.prototype, "content", void 0);
SpaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-spa',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/spa/spa.html"*/'<ion-header no-border>\n  <ion-navbar color="viol">\n    <ion-title>\n      Минск\n    </ion-title>\n  </ion-navbar>\n\n  <ion-toolbar class="subheader">\n    <ion-slides class="slidertwo" #slidesTwo>\n      <ion-slide class="backtransparent" *ngFor="let dat of dates" [ngClass]="{\'doublelineoran\':dat.dat.getDate()==takedate.getDate()}" (click)="toggleDate(dat.dat)">\n        <ion-grid no-padding style="height:100%;">\n          <ion-row justify-content-around>\n            <ion-col col-12 class="card-background-page">\n                <div class="card-title">\n                  <div class="centering">\n                    {{ dat.dat | date: \'dd\' }}<br/>{{ dat.dat | date: \'EEE\' }}\n                  </div>\n                </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-slide>\n    </ion-slides>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-slides class="sliderone" #slidesOne style="min-height:47vw;max-height:25vh;margin-bottom:10px;" (ionSlideWillChange)="officeChanged()">\n    <ion-slide class="backtransparent" *ngFor="let office of offices" [ngClass]="{\'opacityshrink\':office.in_id!=currentIndex, \'opacityfull\':office.in_id==currentIndex}">\n      <ion-grid no-padding style="height:100%;">\n        <ion-row justify-content-around>\n            <ion-col col-12 class="card-background-page" [ngStyle]="{\'background-image\':\'url(\'+checkPicComp(office.office_logo)+\')\',\'background-size\':\'100%\',\'background-repeat\':\'no-repeat\',\'background-position\':\'center center\', \'width\':\'45vw\',\'height\':\'45vw\'}">\n              <div class="card-title everyline" style="background:none;">\n                <!-- <div class="centering"> -->\n                  <!-- <ion-icon color="light" name="custom_stones"></ion-icon>\n                  <br/>SPA\n                  <br/>РИТУАЛ -->\n                <!-- </div> -->\n              </div>\n            </ion-col>\n          </ion-row>\n      </ion-grid>\n    </ion-slide>\n  </ion-slides>\n  <!-- Add Arrows -->\n  <div class="swiper-button-next swiper-slide-next" (click)="nextSlide()" *ngIf="offices?.length > 0"></div>\n  <div class="swiper-button-prev" (click)="prevSlide()" *ngIf="offices?.length > 0"></div>\n\n  <ion-card class="backviolet" style="margin:0 auto;border-radius:0px;border:none;">\n    <ion-grid no-padding>\n      \n      <ion-row class="violback" justify-content-center>\n        <ion-col col-12>\n          <ion-slides #slidesFour>\n            <ion-slide *ngFor="let office of offices">\n              <p class="aligncenter">\n                <img src="assets/img/logo.png"/>\n              </p>\n              <p *ngIf="office.office_adress" class="aligncenter" justify-content-center>\n                <button ion-button icon-left clear class="infowhite"><ion-icon name="pin"></ion-icon> {{ office.office_adress }}</button>\n                <br/>\n                <span class="violback aligncenter" *ngIf="office.office_tel?.length > 0" justify-content-center>\n                  <!-- <button *ngIf="offices[currentIndex].office_tel[1]" ion-button small clear class="inforange">{{ recalcPhone(offices[currentIndex].office_tel[1]) }}</button> -->\n                  <button *ngFor="let officetel of office.office_tel" ion-button small clear class="inforange">{{ recalcPhone(officetel) }}</button>\n                  <br/>\n                  <button *ngFor="let bushour of recalcBusHours(office.office_bus_hours)" ion-button small clear class="inforange">{{ bushour.day }} {{ bushour.times }}</button>\n                </span>\n              </p>\n            </ion-slide>\n          </ion-slides>\n        </ion-col>\n      </ion-row>\n\n      <div *ngFor="let spa of spas; let i=index">\n        <ion-row justify-content-around margin-bottom *ngIf="i % 2 === 0">\n          <ion-col col-5 class="card-background-page" (click)="chooseSpa(spas[i].in_id)" *ngIf="i < spas.length">\n            <img src="assets/img/placeholder.png"/>\n            <div [ngClass]="{\'doublelinesel\': selspa == spas[i].in_id, \'card-title\': 1, \'orange\': selspa == spas[i].in_id, \'doubleline\': selspa != spas[i].in_id, \'whitey\': selspa != spas[i].in_id}">\n              <div class="centering" text-uppercase>\n                  <ion-icon [ngClass]="{\'iconsel\': selspa == spas[i].in_id}" name="{{ spas[i].menue_icon }}"></ion-icon>\n                  {{ spas[i].menue_name }}\n              </div>\n            </div>\n          </ion-col>\n          <ion-col col-5 class="card-background-page" (click)="chooseSpa(spas[i+1].in_id)" *ngIf="i + 1 < spas.length">\n            <img src="assets/img/placeholder.png"/>\n            <div [ngClass]="{\'doublelinesel\': selspa == spas[i+1].in_id, \'card-title\': 1, \'orange\': selspa == spas[i+1].in_id, \'doubleline\': selspa != spas[i+1].in_id, \'whitey\': selspa != spas[i+1].in_id}">\n              <div class="centering" text-uppercase>\n                <ion-icon [ngClass]="{\'iconsel\': selspa == spas[i+1].in_id}" name="{{ spas[i+1].menue_icon }}"></ion-icon>\n                {{ spas[i+1].menue_name }}\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </div>\n\n    </ion-grid>\n  </ion-card>\n\n  <ion-card *ngIf="statusmessage && !selspa && selspa != 0" class="backtransparent">\n    <ion-card-content>\n      <span text-wrap style="color:#f00;font-size:1.2em;">{{ statusmessage }}</span>\n    </ion-card-content>\n  </ion-card>\n\n  <!-- <ion-slides class="sliderthree" #slidesThree style="height:30%;margin-top:10px;" (ionSlideDidChange)="masterChanged()" (ionSlideTap)="masterDetails()">\n    <ion-slide class="backtransparent" *ngFor="let master of masters" [ngClass]="{\'opacityshrink\':master.id!=masterid, \'opacityfull\':master.id==masterid}">\n      <ion-grid no-padding style="height:100%;">\n        <ion-row justify-content-center>\n            <ion-col *ngIf="master.id==\'0\'" col-12 class="card-background-page backviolet" [ngStyle]="{\'background-image\':\'url(\'+master.img+\')\',\'background-size\':\'50%\',\'background-repeat\':\'no-repeat\',\'background-position\':\'center center\'}">\n              <div class="card-title doubleline whitey">\n              </div>\n            </ion-col>\n            <ion-col *ngIf="master.id!=\'0\'" col-12 class="card-background-page backviolet" [ngStyle]="{\'background-image\':\'url(\'+master.img+\')\',\'background-size\':\'cover\',\'background-repeat\':\'no-repeat\',\'background-position\':\'center center\'}">\n              <div class="card-title oneline whitey">\n              </div>\n            </ion-col>\n          </ion-row>\n      </ion-grid>\n    </ion-slide>\n  </ion-slides>\n\n  <ion-card class="backtransparent">\n    <button ion-button clear color="viol">{{ mastername }}</button>\n  </ion-card>\n\n  <ion-card class="backtransparent" justify-content-around>\n    <button style="border-radius:0;" margin-right ion-button small [outline]="work != selwork" color="viol" *ngFor="let work of masterwork" (click)="selWork(work)">{{ work | timepoints }}</button>\n  </ion-card> -->\n\n  <ion-card style="width:100%;padding:0;" *ngIf="selspa >= 0" class="backtransparent" no-padding no-margin justify-content-center>\n    <ion-card-content>\n\n      <button ion-button block color="viol" (click)="entryRequest()">ЗАПИСАТЬСЯ</button>\n\n    </ion-card-content>\n  </ion-card>\n\n  <br/>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/spa/spa.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */], __WEBPACK_IMPORTED_MODULE_6__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__pipes_timepoints_timepoints__["a" /* TimepointsPipe */], __WEBPACK_IMPORTED_MODULE_2__angular_common__["c" /* DatePipe */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], SpaPage);

//# sourceMappingURL=spa.js.map

/***/ }),

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Ordered2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pipes_timepoints_timepoints__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__intro_intro__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Ordered2Page = (function () {
    function Ordered2Page(navCtrl, navParams, tpp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tpp = tpp;
        this.dateplace = [];
        this.dateday = this.navParams.get('dateday');
        this.datemonth = this.navParams.get('datemonth');
        // this.datetime = this.tpp.transform(this.navParams.get('datetime'));
        this.dateplace = [this.navParams.get('dateplace')];
    }
    Ordered2Page.prototype.ionViewDidLoad = function () {
    };
    Ordered2Page.prototype.goToRoots = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__intro_intro__["a" /* IntroPage */]);
        this.navCtrl.popToRoot();
    };
    Ordered2Page.prototype.formatBusHours = function (val) {
        return val.slice(0, 2) + ':' + val.slice(2, 4) + '-' + val.slice(4, 6) + ':' + val.slice(6, 8);
    };
    Ordered2Page.prototype.recalcBusHours = function (val) {
        var valold = '';
        var startday = '';
        var endday = '';
        var valfinal = [];
        var days = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
        for (var i = 0; i < val.length; i++) {
            if (valold == val[i]) {
                endday = ' - ' + days[i];
                if (i == val.length - 1) {
                    valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                }
            }
            else {
                if (startday != '') {
                    valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    startday = days[i];
                    endday = '';
                    valold = val[i];
                    if (i == val.length - 1) {
                        valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    }
                }
                else {
                    startday = days[i];
                    valold = val[i];
                    if (i == val.length - 1) {
                        valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    }
                }
            }
        }
        return valfinal;
    };
    Ordered2Page.prototype.recalcPhone = function (tel) {
        if (!tel) {
            return '';
        }
        var value = tel.toString().trim().replace(/^\+/, '');
        if (value.match(/[^0-9]/)) {
            return tel;
        }
        var country, city, number;
        switch (value.length) {
            case 10:
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;
            case 11:
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;
            case 12:
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;
            case 13:
                country = value.slice(0, 2);
                city = value.slice(2, 5);
                number = value.slice(5);
                break;
            default:
                return tel;
        }
        if (country == 1) {
            country = "";
        }
        number = number.slice(0, 3) + '-' + number.slice(3);
        return (country + " (" + city + ") " + number).trim();
    };
    return Ordered2Page;
}());
Ordered2Page = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-ordered2',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/ordered2/ordered2.html"*/'<ion-header>\n  <ion-navbar hideBackButton="true" color="viol">\n    <ion-title>\n      Вы записались!\n    </ion-title>\n    <ion-buttons right>\n      <button ion-button icon-end color="light" (click)="goToRoots()" style="font-size:20px;">\n        <ion-icon name="checkmark"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-bounce>\n  <div class="violback">\n    <ion-card class="backtransparent">\n      <ion-grid>\n        <ion-row style="margin-bottom:50px;" justify-content-around>\n          <ion-col col-6 class="card-background-page">\n            <img src="assets/img/placeholder.png"/>\n            <div class="card-title everyline">\n              <div class="centering">\n                <h1>{{ dateday }}</h1>\n                <h1>{{ datemonth }}</h1>\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row justify-content-around>\n          <ion-col col-6>\n              <h2 class="whitey">Ожидайте звонка!</h2>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf="dateplace.length > 0" justify-content-around>\n          <ion-col col-12 text-center>\n            <button ion-button icon-left clear class="infowhite"><ion-icon name="pin"></ion-icon> {{ dateplace[0].office_adress }}</button>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf="dateplace.length > 0" justify-content-around>\n          <ion-col text-center>\n            <button *ngIf="dateplace[0].office_tel[0]" ion-button small clear class="inforange">{{ recalcPhone(dateplace[0].office_tel[0]) }}</button>\n            <button *ngIf="dateplace[0].office_tel[1]" ion-button small clear class="inforange">{{ recalcPhone(dateplace[0].office_tel[1]) }}</button>\n            <button *ngIf="dateplace[0].office_tel[2]" ion-button small clear class="inforange">{{ recalcPhone(dateplace[0].office_tel[2]) }}</button>\n            <button *ngIf="dateplace[0].office_tel[3]" ion-button small clear class="inforange">{{ recalcPhone(dateplace[0].office_tel[3]) }}</button>\n            <button *ngIf="dateplace[0].office_tel[4]" ion-button small clear class="inforange">{{ recalcPhone(dateplace[0].office_tel[4]) }}</button>\n            <br/>\n            <button *ngFor="let bushour of recalcBusHours(dateplace[0].office_bus_hours)" ion-button small clear class="inforange">{{ bushour.day }} {{ bushour.times }}</button>\n          </ion-col>\n        </ion-row>\n        <ion-row justify-content-around>\n          <!-- <button ion-button outline color="light" (click)="goToRoots()">Редактировать запись</button> -->\n          <button ion-button outline color="light" (click)="goToRoots()">ОК</button>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n  </div>\n</ion-content>\n  '/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/ordered2/ordered2.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__pipes_timepoints_timepoints__["a" /* TimepointsPipe */]])
], Ordered2Page);

//# sourceMappingURL=ordered2.js.map

/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalritualsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ritual_ritual__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__express_express__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__history_history__ = __webpack_require__(311);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ModalritualsPage = (function () {
    function ModalritualsPage(navCtrl, navParams, viewCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this.choicerituals = false;
        this.choiceexpress = false;
        this.choicehist = false;
        // let loading = this.loadingCtrl.create({
        // spinner: 'hide',
        // cssClass: 'loader',
        // content: `
        //   <img src="assets/img/loading.png" class="ld ldt-bounce-in infinite" width="52" height="52" /><p style="color:#fff !important;">Загрузка..</p>
        // `
        // });
        // loading.present();
        // setTimeout(() => {
        //   loading.dismiss();
        // }, 2000);
    }
    ModalritualsPage.prototype.ionViewDidLoad = function () {
    };
    ModalritualsPage.prototype.goHistory = function () {
        var _this = this;
        this.choicerituals = false;
        this.choiceexpress = false;
        this.choicehist = true;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__history_history__["a" /* HistoryPage */], { menue_cat: 182, menue_good: 23 });
        setTimeout(function () {
            _this.choicerituals = false;
            _this.choiceexpress = false;
            _this.choicehist = false;
        }, 300);
    };
    ModalritualsPage.prototype.goExpress = function () {
        var _this = this;
        this.choicerituals = false;
        this.choiceexpress = true;
        this.choicehist = false;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__express_express__["a" /* ExpressPage */], { menue_cat: 182, menue_good: 23 });
        setTimeout(function () {
            _this.choicerituals = false;
            _this.choiceexpress = false;
            _this.choicehist = false;
        }, 300);
    };
    ModalritualsPage.prototype.goRitual = function () {
        var _this = this;
        this.choicerituals = true;
        this.choiceexpress = false;
        this.choicehist = false;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__ritual_ritual__["a" /* RitualPage */], { menue_cat: 182, menue_good: 23 });
        setTimeout(function () {
            _this.choicerituals = false;
            _this.choiceexpress = false;
            _this.choicehist = false;
        }, 300);
    };
    return ModalritualsPage;
}());
ModalritualsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-modalrituals',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/modalrituals/modalrituals.html"*/'<ion-header>\n  <ion-navbar color="viol">\n    <!-- <ion-buttons left>\n      <button ion-button (click)="close()" [ngStyle]="{\'color\': \'#fff\', \'font-size\': \'200%\'}">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons> -->\n    <ion-title>\n      Записаться\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-bounce>\n  <div class="violback">\n\n    <ion-card no-margin class="backtransparent">\n      <ion-grid no-padding>\n        <ion-row justify-content-around>\n          <ion-col col-12 class="card-background-page" (click)="goRitual()">\n            <div [ngClass]="{\'doublelinesel\': choicerituals, \'card-title\': 1, \'orange\': choicerituals, \'doubleline\': !choicerituals, \'whitey\': !choicerituals}">\n              <div class="centering">\n                <ion-icon [ngClass]="{\'iconsel\': choicerituals}" name="custom_cook"></ion-icon>\n                ЗАПИСАТЬСЯ\n                <br/>НА РИТУАЛ\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n    <br/>\n    <ion-card no-margin class="backtransparent">\n      <ion-grid no-padding>\n        <ion-row justify-content-around>\n          <ion-col col-12 class="card-background-page" (click)="goExpress()">\n            <div [ngClass]="{\'doublelinesel\': choiceexpress, \'card-title\': 1, \'orange\': choiceexpress, \'doubleline\': !choiceexpress, \'whitey\': !choiceexpress}">\n              <div class="centering">\n                <ion-icon [ngClass]="{\'iconsel\': choiceexpress}" name="custom_time"></ion-icon>\n                МНЕ ПОВЕЗЕТ!\n                <br/>ЭКСПРЕСС-ЗАПИСЬ\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n    <br/>\n    <ion-card no-margin class="backtransparent">\n      <ion-grid no-padding>\n        <ion-row justify-content-around>\n          <ion-col col-12 class="card-background-page" (click)="goHistory()">\n            <div [ngClass]="{\'doublelinesel\': choicehist, \'card-title\': 1, \'orange\': choicehist, \'doubleline\': !choicehist, \'whitey\': !choicehist}">\n              <div class="centering">\n                <ion-icon [ngClass]="{\'iconsel\': choicehist}" name="ios-refresh-circle-outline" style="height:10vh;font-size:10vh;"></ion-icon>\n                МНЕ КАК\n                <br/>В ПРОШЛЫЙ РАЗ\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n    \n  </div>\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/modalrituals/modalrituals.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], ModalritualsPage);

//# sourceMappingURL=modalrituals.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RitualPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_timepoints_timepoints__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profiledetails_profiledetails__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modalmasters_modalmasters__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ordered_ordered__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var RitualPage = (function () {
    function RitualPage(navCtrl, navParams, modalCtrl, backendProv, menuCtrl, actionSheetCtrl, loadingCtrl, tpp, datePipe, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.backendProv = backendProv;
        this.menuCtrl = menuCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadingCtrl = loadingCtrl;
        this.tpp = tpp;
        this.datePipe = datePipe;
        this.alertCtrl = alertCtrl;
        this.datetoday = new Date();
        this.masterworks = 0;
        this.currentIndex = 0;
        this.currentIndex2 = 0;
        this.currentIndex3 = 0;
        this.dates = [];
        this.offices = [];
        this.masters = [];
        this.masterwork = [];
        this.rituals = [];
        this.professions = [];
        this.steaming = 'Без распаривания.';
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        backendProv.checkWaiter()
            .then(function (res) {
            loading.dismiss();
            _this.startingLoads();
        })
            .catch(function (err) {
            loading.dismiss();
            _this.startingLoads();
        });
        // var appelem = document.createElement('div');
        // var appelem2 = document.createElement('div');
        // appelem.setAttribute('class', 'swiper-button-next');
        // appelem.setAttribute('class', 'swiper-slide-next');
        // appelem2.setAttribute('class', 'swiper-button-prev');
        // document.getElementsByClassName('swiper-container')[0].appendChild(appelem);
        // document.getElementsByClassName('swiper-container')[0].appendChild(appelem2);
    }
    RitualPage.prototype.ionViewDidLoad = function () {
        this.navCtrl.swipeBackEnabled = false;
        // READY SLIDE SETTS
        this.slides.loop = false;
        this.slides.pager = false;
        this.slides.effect = "slide";
        this.slides.initialSlide = 0;
        this.slides.centeredSlides = true;
        this.slides.slidesPerView = 2;
        this.slides.spaceBetween = 30;
        this.slides2.loop = false;
        this.slides2.pager = false;
        this.slides2.effect = "slide";
        this.slides2.initialSlide = 0;
        this.slides2.centeredSlides = false;
        this.slides2.slidesPerView = 7;
        this.slides2.spaceBetween = 30;
        this.slides3.loop = false;
        this.slides3.pager = false;
        this.slides3.effect = "slide";
        this.slides3.initialSlide = 0;
        this.slides3.slidesPerView = 2;
        this.slides3.spaceBetween = 30;
        this.slides3.centeredSlides = true;
        this.slides.control = this.slides4;
        this.slides4.control = this.slides;
        // document.getElementsByClassName('there')[0].appendChild(
        //   document.getElementsByClassName('swiper-button-next')[0];
        //   document.getElementsByClassName('swiper-button-prev')[0];
        // );
    };
    RitualPage.prototype.nextSlide = function () {
        this.slides.slideNext();
    };
    RitualPage.prototype.prevSlide = function () {
        this.slides.slidePrev();
    };
    RitualPage.prototype.startingLoads = function () {
        var _this = this;
        this.backendProv.getProfessions().then(function (res) {
            _this.professions = res;
        }).catch(function (e) { });
        this.backendProv.getGoods()
            .then(function (res) { return _this.goodsid = res[0].goods_id; })
            .catch(function (e) { return console.log(e); });
        this.backendProv.getProfile()
            .then(function (res) { return _this.profile = res; })
            .catch(function (e) { return console.log(e); });
        // FROM DB
        this.piclink = this.backendProv.piclink;
        this.officePicLink = this.backendProv.officePicLink;
        this.datetoday = this.backendProv.timezoneAdd((new Date().getTime() / 1000).toFixed(0));
        this.datetoday = new Date(this.datetoday * 1000);
        this.datetoday.setHours(0, 0, 0, 0);
        this.takedate = this.datetoday;
        this.backendProv.getDates()
            .then(function (res) { _this.dates = res; })
            .catch(function (e) { return console.log(e); });
        this.loadOffices();
        this.loadRituals();
        // PRELOAD DATA
        this.backendProv.loadSchedule();
        this.backendProv.loadRoom();
        this.backendProv.loadOrdering();
    };
    RitualPage.prototype.preEntry = function () {
        var _this = this;
        if (this.selunixtime && this.selwork) {
            var preRequestAS = this.actionSheetCtrl.create({
                title: 'Нужно распаривание?',
                cssClass: 'action-subtitle',
                buttons: [
                    {
                        text: 'Да',
                        handler: function () {
                            _this.steaming = 'Нужно распаривание!';
                            _this.entryRequest();
                        }
                    },
                    {
                        text: 'Нет',
                        handler: function () {
                            _this.steaming = 'Без распаривания.';
                            _this.entryRequest();
                        }
                    }
                ]
            });
            preRequestAS.present();
        }
    };
    RitualPage.prototype.entryRequest = function () {
        var _this = this;
        var entryRequestAS = this.actionSheetCtrl.create({
            title: this.mastername,
            subTitle: this.datePipe.transform(this.takedate, 'dd') + ' ' + this.datePipe.transform(this.takedate, 'MMM') + ' ' + this.tpp.transform(this.selwork),
            cssClass: 'action-subtitle',
            buttons: [
                {
                    text: 'Записаться',
                    handler: function () {
                        var loading = _this.loadingCtrl.create({
                            spinner: 'hide',
                            cssClass: 'loader',
                            content: "\n                <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n              "
                        });
                        loading.present();
                        var orderstr = {
                            inst_id: _this.backendProv.institution,
                            newusr: 'calender',
                            device_id: _this.backendProv.uuid,
                            getset: 1,
                            menueId: _this.ritualid,
                            menueName: _this.ritualname,
                            menueCost: 0,
                            menueDesc: _this.ritualdesc,
                            workerId: _this.masterrealid,
                            workerName: _this.mastername,
                            workerPic: _this.masterpic,
                            workerProfession: _this.masterprof,
                            orderHour: _this.selunixtime,
                            orderHourName: _this.selwork,
                            orderDayName: _this.datePipe.transform(_this.takedate, 'dd') + '.' + _this.datePipe.transform(_this.takedate, 'MM') + '.' + _this.datePipe.transform(_this.takedate, 'y'),
                            ordercats: _this.navParams.get('menue_cat'),
                            ordgood: _this.goodsid,
                            name: _this.profile[0].user_name,
                            phone: _this.profile[0].user_mob,
                            email: _this.profile[0].user_email,
                            ordoffice: _this.selOffice(_this.currentIndex).office_id,
                            comments: _this.steaming,
                            reminder: '',
                            smsconf: ''
                        };
                        // console.log('ORDER: '+JSON.stringify(orderstr));
                        _this.backendProv.httpRequest(JSON.stringify(orderstr))
                            .subscribe(function (res) {
                            // console.log('ORDER RES '+JSON.stringify(res))
                            loading.dismiss();
                            if (res[0].orderOK == '3') {
                                var alert_1 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Повторный запрос только через 5 минут!',
                                    buttons: ['Закрыть']
                                });
                                alert_1.present();
                            }
                            else if (res[0].orderOK == '2') {
                                var alert_2 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Время уже занято!',
                                    buttons: ['Закрыть']
                                });
                                alert_2.present();
                            }
                            else if (res[0].orderOK == '1') {
                                _this.backendProv.orderSentRecord(res[0]);
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__ordered_ordered__["a" /* OrderedPage */], { dateday: _this.datePipe.transform(_this.takedate, 'dd'), datemonth: _this.datePipe.transform(_this.takedate, 'MMMM'), datetime: _this.selwork, dateplace: _this.selOffice(_this.currentIndex), unixtime: _this.selunixtime });
                                _this.navCtrl.popToRoot();
                            }
                            else if (res[0].orderOK == '5') {
                                var alert_3 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Необходимо подтвердить номер телефона в личном кабинете',
                                    buttons: [
                                        {
                                            text: 'Закрыть'
                                        },
                                        {
                                            text: 'Подтвердить',
                                            handler: function (data) {
                                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__profiledetails_profiledetails__["a" /* ProfiledetailsPage */], { profile: _this.profile[0] });
                                            }
                                        }
                                    ]
                                });
                                alert_3.present();
                            }
                            else if (res[0].orderOK == '8') {
                                var alert_4 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Нет свободной комнаты!',
                                    buttons: ['Закрыть']
                                });
                                alert_4.present();
                            }
                            else if (res[0].orderOK == '9') {
                                var alert_5 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Нет свободной комнаты!',
                                    buttons: ['Закрыть']
                                });
                                alert_5.present();
                            }
                        }, function (e) { console.log('ERROR RES ' + e); loading.dismiss(); });
                    }
                },
                {
                    text: 'Отменить',
                    role: 'cancel',
                    handler: function () {
                        // console.log('Cancel clicked');
                    }
                }
            ]
        });
        entryRequestAS.present();
    };
    RitualPage.prototype.checkStatus = function () {
        if (!this.selritual && this.selritual != 0) {
            this.statusmessage = 'Выберите услугу';
        }
        else if (this.masterwork.length == 0 && this.masterworks > 0 && this.selritual >= 0) {
            this.statusmessage = 'К сожалению данный мастер уже расписан на весь день.';
        }
        else if (this.masterworks == 0 && this.selritual >= 0) {
            this.statusmessage = 'К сожалению на выбранную услугу все мастера заняты.';
        }
        else {
            this.statusmessage = undefined;
        }
    };
    RitualPage.prototype.loadRituals = function () {
        var _this = this;
        this.backendProv.getRituals(this.navParams.get('menue_cat'))
            .then(function (res) {
            _this.rituals = res;
            _this.checkStatus();
        })
            .catch();
    };
    RitualPage.prototype.loadOffices = function () {
        var _this = this;
        this.backendProv.getOffices()
            .then(function (res) {
            _this.offices = res;
            // initial Slide visible
            _this.currentIndex = _this.slides.initialSlide;
            _this.currentIndex2 = _this.slides2.initialSlide;
            _this.currentIndex3 = _this.slides3.initialSlide;
            _this.slides.centeredSlides = true;
            _this.checkStatus();
            setTimeout(function () {
                _this.slides.slideTo(0, 100);
            }, 200);
        })
            .catch();
    };
    RitualPage.prototype.recalcPhone = function (tel) {
        if (!tel) {
            return '';
        }
        var value = tel.toString().trim().replace(/^\+/, '');
        if (value.match(/[^0-9]/)) {
            return tel;
        }
        var country, city, number;
        switch (value.length) {
            case 10:
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;
            case 11:
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;
            case 12:
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;
            case 13:
                country = value.slice(0, 2);
                city = value.slice(2, 5);
                number = value.slice(5);
                break;
            default:
                return tel;
        }
        if (country == 1) {
            country = "";
        }
        number = number.slice(0, 3) + '-' + number.slice(3);
        return (country + " (" + city + ") " + number).trim();
    };
    RitualPage.prototype.formatBusHours = function (val) {
        return val.slice(0, 2) + ':' + val.slice(2, 4) + '-' + val.slice(4, 6) + ':' + val.slice(6, 8);
    };
    RitualPage.prototype.recalcBusHours = function (val) {
        var valold = '';
        var startday = '';
        var endday = '';
        var valfinal = [];
        var days = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
        for (var i = 0; i < val.length; i++) {
            if (valold == val[i]) {
                endday = ' - ' + days[i];
                if (i == val.length - 1) {
                    valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                }
            }
            else {
                if (startday != '') {
                    valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    startday = days[i];
                    endday = '';
                    valold = val[i];
                    if (i == val.length - 1) {
                        valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    }
                }
                else {
                    startday = days[i];
                    valold = val[i];
                    if (i == val.length - 1) {
                        valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    }
                }
            }
        }
        return valfinal;
    };
    RitualPage.prototype.selOffice = function (val) {
        var seloffice;
        for (var i = 0; i < this.offices.length; i++) {
            if (val == this.offices[i].in_id) {
                seloffice = this.offices[i];
            }
        }
        return seloffice;
    };
    RitualPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.piclink + val;
        }
        else {
            return 'assets/img/girl3.png';
        }
    };
    RitualPage.prototype.checkPicComp = function (val) {
        if (val && val != '0') {
            return this.officePicLink + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    RitualPage.prototype.chooseRitual = function (val) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        this.selritual = val;
        this.content.scrollToBottom(1000);
        for (var i = 0; i < this.rituals.length; i++) {
            if (val == this.rituals[i].in_id) {
                this.ritualid = this.rituals[i].menue_id;
                this.ritualinterval = this.rituals[i].menue_interval;
                this.ritualname = this.rituals[i].menue_name;
                this.ritualdesc = this.rituals[i].menue_desc;
            }
        }
        setTimeout(function () {
            _this.masterworks = 0;
            var datetm = '' + _this.takedate.getTime();
            datetm = datetm.slice(0, -3);
            _this.backendProv.getMasters(_this.ritualid, _this.ritualinterval * 60, _this.selOffice(_this.currentIndex), datetm)
                .then(function (res) {
                loading.dismiss();
                _this.masters = res;
                // initial master
                _this.currentIndex3 = _this.slides3.initialSlide;
                _this.masterid = _this.masters[_this.currentIndex3].in_id;
                _this.masterrealid = _this.masters[_this.currentIndex3].user_real_id;
                _this.mastername = _this.masters[_this.currentIndex3].user_name;
                _this.masterpic = _this.masters[_this.currentIndex3].user_pic;
                _this.masterwork = _this.masters[_this.currentIndex3].user_work_time.sort();
                _this.unixtime = _this.masters[_this.currentIndex3].unixtime.sort();
                for (var v = 0; v < _this.masters.length; v++) {
                    if (_this.masters[v].user_work_time.length > 0) {
                        _this.masters[v].user_work_time.sort();
                        _this.masters[v].unixtime.sort();
                        _this.masterworks++;
                    }
                    for (var p = 0; p < _this.professions.length; p++) {
                        if (_this.masters[v].user_work_pos == _this.professions[p].prof_id) {
                            _this.masters[v].user_prof = _this.professions[p].prof_name;
                        }
                    }
                }
                _this.masterprof = _this.masters[_this.currentIndex3].user_prof;
                _this.checkStatus();
                setTimeout(function () {
                    _this.slides3.slideTo(0, 200);
                    _this.content.scrollToBottom(1000);
                    _this.slides3.control = _this.slides5;
                    _this.slides5.control = _this.slides3;
                    _this.slides3.update();
                    _this.slides5.update();
                }, 200);
            })
                .catch(function (err) {
                // console.log('MASTERS ERROR '+JSON.stringify(err))
            });
        }, 1000);
    };
    RitualPage.prototype.masterDetails = function (val) {
        var _this = this;
        var masterDetailModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__modalmasters_modalmasters__["a" /* ModalmastersPage */], { master: val, mastername: this.mastername, takedate: this.takedate, selwork: this.selwork, dateplace: this.selOffice(this.currentIndex) });
        masterDetailModal.onDidDismiss(function (data) {
            if (data) {
                var loading_1 = _this.loadingCtrl.create({
                    spinner: 'hide',
                    cssClass: 'loader',
                    content: "\n            <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n          "
                });
                loading_1.present();
                var orderstr = {
                    inst_id: _this.backendProv.institution,
                    newusr: 'calender',
                    device_id: _this.backendProv.uuid,
                    getset: 1,
                    menueId: _this.ritualid,
                    menueName: _this.ritualname,
                    menueCost: 0,
                    menueDesc: _this.ritualdesc,
                    workerId: _this.masterrealid,
                    workerName: _this.mastername,
                    workerPic: _this.masterpic,
                    workerProfession: _this.masterprof,
                    orderHour: _this.backendProv.timezoneSub(data.unixtime),
                    orderHourName: data.datetime,
                    orderDayName: data.dateday + '.' + data.datemonths + '.' + data.dateyears,
                    ordercats: _this.navParams.get('menue_cat'),
                    ordgood: _this.goodsid,
                    name: _this.profile[0].user_name,
                    phone: _this.profile[0].user_mob,
                    email: _this.profile[0].user_email,
                    ordoffice: _this.selOffice(_this.currentIndex).office_id,
                    comments: data.comments,
                    reminder: '',
                    smsconf: ''
                };
                // console.log('ORDER: '+JSON.stringify(orderstr));
                _this.backendProv.httpRequest(JSON.stringify(orderstr))
                    .subscribe(function (res) {
                    loading_1.dismiss();
                    if (res[0].orderOK == '3') {
                        var alert_6 = _this.alertCtrl.create({
                            title: 'Внимание',
                            subTitle: 'Повторный запрос только через 5 минут!',
                            buttons: ['Закрыть']
                        });
                        alert_6.present();
                    }
                    else if (res[0].orderOK == '2') {
                        var alert_7 = _this.alertCtrl.create({
                            title: 'Внимание',
                            subTitle: 'Вы уже записались на выбранную услугу либо на выбранное время. Ожидайте одобрения или звонка!',
                            buttons: ['Закрыть']
                        });
                        alert_7.present();
                    }
                    else if (res[0].orderOK == '1') {
                        _this.backendProv.orderSentRecord(res[0]);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__ordered_ordered__["a" /* OrderedPage */], data);
                        _this.navCtrl.popToRoot();
                    }
                    else if (res[0].orderOK == '1') {
                        _this.backendProv.orderSentRecord(res[0]);
                    }
                    else if (res[0].orderOK == '5') {
                        var alert_8 = _this.alertCtrl.create({
                            title: 'Внимание',
                            subTitle: 'Необходимо подтвердить номер телефона в личном кабинете',
                            buttons: [
                                {
                                    text: 'Закрыть'
                                },
                                {
                                    text: 'Подтвердить',
                                    handler: function (data) {
                                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__profiledetails_profiledetails__["a" /* ProfiledetailsPage */], { profile: _this.profile[0] });
                                    }
                                }
                            ]
                        });
                        alert_8.present();
                    }
                }, function (e) { console.log(e); loading_1.dismiss(); });
            }
        });
        masterDetailModal.present();
    };
    RitualPage.prototype.selWork = function (work, unix) {
        this.selwork = work;
        this.selunixtime = unix;
    };
    RitualPage.prototype.masterChanged = function () {
        // selected slide visible, others not
        this.currentIndex3 = this.slides3.realIndex;
        this.masterid = this.masters[this.currentIndex3].in_id;
        this.masterrealid = this.masters[this.currentIndex3].user_real_id;
        this.mastername = this.masters[this.currentIndex3].user_name;
        this.masterpic = this.masters[this.currentIndex3].user_pic;
        this.masterprof = this.masters[this.currentIndex3].user_prof;
        this.masterwork = this.masters[this.currentIndex3].user_work_time;
        this.unixtime = this.masters[this.currentIndex3].unixtime;
    };
    RitualPage.prototype.officeChanged = function () {
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 1000);
        this.masterwork = [];
        this.selritual = undefined;
        this.masterworks = 0;
        // selected slide visible, others not
        this.currentIndex = this.slides.realIndex;
        this.selOffice(this.currentIndex);
        this.loadRituals();
    };
    RitualPage.prototype.toggleDate = function (dat) {
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 1000);
        this.takedate = dat;
        this.content.scrollToTop(1000);
        this.masterwork = [];
        this.selritual = undefined;
        this.masterworks = 0;
        this.slides.initialSlide = this.currentIndex;
        this.loadOffices();
        this.loadRituals();
    };
    return RitualPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slidesOne'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], RitualPage.prototype, "slides", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slidesTwo'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], RitualPage.prototype, "slides2", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slidesThree'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], RitualPage.prototype, "slides3", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slidesFour'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], RitualPage.prototype, "slides4", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slidesFive'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], RitualPage.prototype, "slides5", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Content */])
], RitualPage.prototype, "content", void 0);
RitualPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-ritual',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/ritual/ritual.html"*/'  <ion-header no-border>\n    <ion-navbar color="viol">\n      <ion-title>\n        Минск\n      </ion-title>\n    </ion-navbar>\n\n    <ion-toolbar class="subheader">\n      <ion-slides class="slidertwo" #slidesTwo>\n        <ion-slide class="backtransparent" *ngFor="let dat of dates" [ngClass]="{\'doublelineoran\':dat.dat.getDate()==takedate.getDate()}" (click)="toggleDate(dat.dat)">\n          <ion-grid no-padding style="height:100%;">\n            <ion-row justify-content-around>\n              <ion-col col-12 class="card-background-page">\n                <div class="card-title">\n                  <div class="centering">\n                    {{ dat.dat | date: \'dd\' }}<br/>{{ dat.dat | date: \'EEE\' }}\n                  </div>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-slide>\n      </ion-slides>\n    </ion-toolbar>\n  </ion-header>\n  \n  <ion-content>\n  \n    <!-- <ion-toolbar no-border class="scrollable-segments">\n        <ion-segment [(ngModel)]="takedate" color="oran">\n            <ion-segment-button *ngFor="let dat of dates" value="{{dat.dat}}" [ngClass]="{\'segment-button-sel\':dat.dat.getDate()==takedate.getDate()}" (click)="toggleDate(dat.dat)">\n                {{ dat.dat | date: \'dd\' }}<br/>{{ dat.dat | date: \'EEE\' }}\n            </ion-segment-button>\n        </ion-segment>\n    </ion-toolbar> -->\n  \n    <ion-slides class="sliderone swiper-pagination-black" #slidesOne style="min-height:47vw;max-height:25vh;margin-bottom:10px;" (ionSlideWillChange)="officeChanged()">\n      <ion-slide class="backtransparent" *ngFor="let office of offices" [ngClass]="{\'opacityshrink\':office.in_id!=currentIndex, \'opacityfull\':office.in_id==currentIndex}">\n        <ion-grid no-padding style="height:100%;">\n          <ion-row justify-content-around>\n            <ion-col col-12 class="card-background-page" [ngStyle]="{\'background-image\':\'url(\'+checkPicComp(office.office_logo)+\')\',\'background-size\':\'100%\',\'background-repeat\':\'no-repeat\',\'background-position\':\'center center\', \'width\':\'45vw\',\'height\':\'45vw\'}">\n              <div class="card-title everyline" style="background:none;">\n                <!-- <div class="centering"> -->\n                  <!-- <ion-icon color="light" name="custom_stones"></ion-icon>\n                  <br/>SPA\n                  <br/>РИТУАЛ -->\n                <!-- </div> -->\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-slide>\n    </ion-slides>\n    <!-- Add Arrows -->\n    <div class="swiper-button-next swiper-slide-next" (click)="nextSlide()" *ngIf="offices?.length > 0"></div>\n    <div class="swiper-button-prev" (click)="prevSlide()" *ngIf="offices?.length > 0"></div>\n  \n    <ion-card class="backviolet" style="margin:0 auto;border-radius:0px;border:none;">\n      <ion-grid no-padding>\n\n        <ion-row class="violback" justify-content-center>\n          <ion-col col-12>\n            <ion-slides #slidesFour>\n              <ion-slide *ngFor="let office of offices">\n                <p class="aligncenter">\n                  <img src="assets/img/logo.png"/>\n                </p>\n                <p *ngIf="office.office_adress" class="aligncenter" justify-content-center>\n                  <button ion-button icon-left clear class="infowhite"><ion-icon name="pin"></ion-icon> {{ office.office_adress }}</button>\n                  <br/>\n                  <span class="violback aligncenter" *ngIf="office.office_tel?.length > 0" justify-content-center>\n                    <!-- <button *ngIf="offices[currentIndex].office_tel[1]" ion-button small clear class="inforange">{{ recalcPhone(offices[currentIndex].office_tel[1]) }}</button> -->\n                    <button *ngFor="let officetel of office.office_tel" ion-button small clear class="inforange">{{ recalcPhone(officetel) }}</button>\n                    <br/>\n                    <button *ngFor="let bushour of recalcBusHours(office.office_bus_hours)" ion-button small clear class="inforange">{{ bushour.day }} {{ bushour.times }}</button>\n                  </span>\n                </p>\n              </ion-slide>\n            </ion-slides>\n          </ion-col>\n        </ion-row>\n\n        <div *ngFor="let ritual of rituals; let i=index">\n          <ion-row justify-content-around margin-bottom *ngIf="i % 2 === 0">\n            <ion-col col-5 class="card-background-page" (click)="chooseRitual(rituals[i].in_id)" *ngIf="i < rituals.length">\n              <img src="assets/img/placeholder.png"/>\n              <div [ngClass]="{\'doublelinesel\': selritual == rituals[i].in_id, \'card-title\': 1, \'orange\': selritual == rituals[i].in_id, \'doubleline\': selritual != rituals[i].in_id, \'whitey\': selritual != rituals[i].in_id}">\n                <div class="centering" text-uppercase>\n                    <ion-icon [ngClass]="{\'iconsel\': selritual == rituals[i].in_id}" name="{{ rituals[i].menue_icon }}"></ion-icon>\n                    {{ rituals[i].menue_name }}\n                </div>\n              </div>\n            </ion-col>\n            <ion-col col-5 class="card-background-page" (click)="chooseRitual(rituals[i+1].in_id)" *ngIf="i + 1 < rituals.length">\n              <img src="assets/img/placeholder.png"/>\n              <div [ngClass]="{\'doublelinesel\': selritual == rituals[i+1].in_id, \'card-title\': 1, \'orange\': selritual == rituals[i+1].in_id, \'doubleline\': selritual != rituals[i+1].in_id, \'whitey\': selritual != rituals[i+1].in_id}">\n                <div class="centering" text-uppercase>\n                  <ion-icon [ngClass]="{\'iconsel\': selritual == rituals[i+1].in_id}" name="{{ rituals[i+1].menue_icon }}"></ion-icon>\n                  {{ rituals[i+1].menue_name }}\n                </div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </div>\n\n      </ion-grid>\n    </ion-card>\n  \n    <ion-card *ngIf="statusmessage" class="backtransparent">\n      <ion-card-content>\n        <span text-wrap style="color:#f00;font-size:1.2em;">{{ statusmessage }}</span>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-slides #slidesThree [ngClass]="{\'sliderthreemax\': masterworks > 0 && selritual >= 0, \'sliderthree\': masterworks == 0 || !selritual}" (ionSlideWillChange)="masterChanged()">\n      <ion-slide class="backtransparent" *ngFor="let master of masters" [ngClass]="{\'opacityshrink\': master.in_id!=masterid, \'opacityfull\':master.in_id==masterid}">\n\n        <ion-grid *ngIf="masterworks > 0 && selritual >= 0" no-padding style="width:45vw;min-height:45vw;" class="backtransparent" (click)="masterDetails(master)">\n          <ion-row justify-content-center>\n            <ion-col *ngIf="master.in_id==\'0\'" col-12 class="card-background-page backviolet" [ngStyle]="{\'background-image\':\'url(\'+checkPic(master.user_pic)+\')\',\'background-size\':\'50%\',\'background-repeat\':\'no-repeat\',\'background-position\':\'center center\', \'width\':\'45vw\',\'height\':\'45vw\'}">\n              <div class="card-title doubleline whitey">\n                <!-- <div class="centering">\n                    <ion-icon name="custom_lotos"></ion-icon>\n                    <br/>МАГИЯ ЛОТОСА\n                    <br/>ДЛЯ ДВОИХ\n                </div> -->\n              </div>\n            </ion-col>\n            <ion-col *ngIf="master.in_id!=\'0\'" col-12 class="card-background-page backviolet" [ngStyle]="{\'background-image\':\'url(\'+checkPic(master.user_pic)+\')\',\'background-size\':\'cover\',\'background-repeat\':\'no-repeat\',\'background-position\':\'center center\', \'width\':\'45vw\',\'height\':\'45vw\'}">\n              <div class="card-title oneline whitey">\n                <!-- <div class="centering">\n                    <ion-icon name="custom_lotos"></ion-icon>\n                    <br/>МАГИЯ ЛОТОСА\n                    <br/>ДЛЯ ДВОИХ\n                </div> -->\n              </div>\n            </ion-col>\n          </ion-row>\n          <ion-row justify-content-center>\n            <ion-col col-12>\n              <button text-wrap ion-button clear large color="viol">{{ master.user_name }}</button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n\n      </ion-slide>\n    </ion-slides>\n\n    <ion-slides #slidesFive [ngClass]="{\'sliderthreemax\': masterworks > 0 && selritual >= 0, \'sliderthree\': masterworks == 0 || !selritual}">\n      <ion-slide *ngFor="let master of masters">\n      \n        <ion-grid style="width:100vw !important;">\n          <ion-row justify-content-around *ngIf="master.user_work_time?.length > 0">\n            <ion-col>\n              <button style="border-radius:0px;margin:5px;min-width:20vw;" ion-button no-padding [outline]="work != selwork" color="viol" *ngFor="let work of master.user_work_time; let i = index" (click)="selWork(work, unixtime[i])">\n                {{ work | timepoints }}\n              </button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n\n      </ion-slide>\n    </ion-slides>\n      \n    <ion-card style="width:100%;margin:0 0 16px 0;padding:0;" *ngIf="masterwork?.length > 0 && masterworks > 0 && selritual >= 0" class="backtransparent" no-padding no-margin justify-content-center>\n      <ion-card-content>\n\n        <button ion-button block color="viol" (click)="preEntry()">ЗАПИСАТЬСЯ</button>\n\n      </ion-card-content>\n    </ion-card>\n\n  </ion-content>\n  '/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/ritual/ritual.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */], __WEBPACK_IMPORTED_MODULE_7__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__pipes_timepoints_timepoints__["a" /* TimepointsPipe */], __WEBPACK_IMPORTED_MODULE_2__angular_common__["c" /* DatePipe */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], RitualPage);

//# sourceMappingURL=ritual.js.map

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpressPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ExpressPage = (function () {
    function ExpressPage(navCtrl, navParams, backendProv, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.loadingCtrl = loadingCtrl;
        this.expressarr = [];
        this.expresses = [];
        this.offices = [];
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        backendProv.checkWaiter()
            .then(function (res) {
            backendProv.getRituals(_this.navParams.get('menue_cat'))
                .then(function (res) {
                _this.expresses = res;
                loading.dismiss();
            })
                .catch();
            _this.loadOffices();
        })
            .catch(function (err) {
            backendProv.getRituals(_this.navParams.get('menue_cat'))
                .then(function (res) {
                _this.expresses = res;
                loading.dismiss();
            })
                .catch();
            _this.loadOffices();
        });
    }
    ExpressPage.prototype.ionViewDidLoad = function () {
    };
    ExpressPage.prototype.loadOffices = function () {
        var _this = this;
        this.backendProv.getOffices()
            .then(function (res) {
            _this.offices = res;
        })
            .catch();
    };
    ExpressPage.prototype.goExpressChoice = function () {
        // console.log('SELSEND '+JSON.stringify(this.expressarr));
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */], { selexp: this.expressarr, offices: this.offices, menuecat: this.navParams.get('menue_cat'), menuegood: this.navParams.get('menue_good') });
    };
    ExpressPage.prototype.selExpress = function (val1, val2) {
        this.selexpress = val1;
        this.expressarr = val2;
    };
    return ExpressPage;
}());
ExpressPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-express',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/express/express.html"*/'<ion-header no-border>\n  <ion-navbar color="viol">\n    <ion-title>\n      ЭКСПРЕСС-ЗАПИСЬ\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-card class="backviolet" style="margin:0 auto;min-height:100%;">\n    <ion-grid no-padding *ngIf="expresses?.length>0">\n      <ion-row>\n        <ion-col>\n            <h2 class="whitey">&nbsp;</h2>\n        </ion-col>\n      </ion-row>\n\n      <div *ngFor="let express of expresses; let i=index">\n        <ion-row justify-content-around margin-bottom *ngIf="i % 2 === 0">\n          <ion-col col-5 class="card-background-page" (click)="selExpress(expresses[i].in_id, expresses[i])" *ngIf="i < expresses.length">\n            <img src="assets/img/placeholder.png"/>\n            <div [ngClass]="{\'doublelinesel\': selexpress == expresses[i].in_id, \'card-title\': 1, \'orange\': selexpress == expresses[i].in_id, \'doubleline\': selexpress != expresses[i].in_id, \'whitey\': selexpress != expresses[i].in_id}">\n              <div class="centering" text-uppercase>\n                  <ion-icon [ngClass]="{\'iconsel\': selexpress == expresses[i].in_id}" name="{{ expresses[i].menue_icon }}"></ion-icon>\n                  {{ expresses[i].menue_name }}\n              </div>\n            </div>\n          </ion-col>\n          <ion-col col-5 class="card-background-page" (click)="selExpress(expresses[i+1].in_id, expresses[i+1])" *ngIf="i + 1 < expresses.length">\n            <img src="assets/img/placeholder.png"/>\n            <div [ngClass]="{\'doublelinesel\': selexpress == expresses[i+1].in_id, \'card-title\': 1, \'orange\': selexpress == expresses[i+1].in_id, \'doubleline\': selexpress != expresses[i+1].in_id, \'whitey\': selexpress != expresses[i+1].in_id}">\n              <div class="centering" text-uppercase>\n                <ion-icon [ngClass]="{\'iconsel\': selexpress == expresses[i+1].in_id}" name="{{ expresses[i+1].menue_icon }}"></ion-icon>\n                {{ expresses[i+1].menue_name }}\n              </div>\n            </div>\n          </ion-col>\n          </ion-row>\n      </div>\n\n      <p *ngIf="selexpress || selexpress==0" style="margin-bottom:50px;">&nbsp;</p>\n    </ion-grid>\n  </ion-card>\n\n</ion-content>\n\n<ion-footer *ngIf="selexpress || selexpress==0" color="goldy">\n    <ion-toolbar color="goldy">\n        <button ion-button full color="goldy" (click)="goExpressChoice()">ЗАПИСАТЬСЯ</button>\n    </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/express/express.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], ExpressPage);

//# sourceMappingURL=express.js.map

/***/ }),

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_timepoints_timepoints__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modalmasters_modalmasters__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ordered_ordered__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HistoryPage = (function () {
    function HistoryPage(navCtrl, navParams, backendProv, actionSheetCtrl, loadingCtrl, tpp, datePipe, alertCtrl, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadingCtrl = loadingCtrl;
        this.tpp = tpp;
        this.datePipe = datePipe;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.places = [];
        this.masters = [];
        this.professions = [];
        this.offices = [];
        this.dates = [];
        this.history = [];
        this.services = [];
        this.selplace = 0;
        this.datetoday = new Date();
        this.grouped = [];
        this.takedate = new Date();
        this.loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        this.loading.present();
        // PRELOAD DATA
        this.backendProv.loadSchedule();
        this.backendProv.loadRoom();
        this.backendProv.loadOrdering();
        this.piclink = this.backendProv.piclink;
        this.ritualdate = this.backendProv.timezoneAdd((this.datetoday.getTime() / 1000).toFixed(0));
        this.ritualdate = new Date(this.ritualdate * 1000);
        this.ritualdate.setHours(0, 0, 0, 0);
        this.backendProv.getProfile()
            .then(function (res) {
            _this.profile = res;
            _this.backendProv.getRituals(_this.navParams.get('menue_cat'))
                .then(function (res) {
                _this.services = res;
                setTimeout(function () {
                    _this.backendProv.getHistory()
                        .then(function (res) {
                        _this.history = res;
                        if (_this.history.length > 0) {
                            if (_this.history[0].order_order != 0) {
                                _this.loadMasters();
                                _this.lastPlace();
                            }
                            else {
                                _this.loading.dismiss();
                            }
                        }
                        else {
                            _this.loading.dismiss();
                        }
                    })
                        .catch(function (e) { return console.log('ERROR 3: ' + e); });
                }, 1000);
            })
                .catch();
        })
            .catch(function (e) { return console.log('ERROR 1: ' + e); });
        this.backendProv.getProfessions()
            .then(function (res) {
            _this.professions = res;
        })
            .catch(function (e) { });
        this.backendProv.getOffices()
            .then(function (res) {
            _this.offices = res;
        })
            .catch(function (e) { return console.log('ERROR 2: ' + e); });
    }
    HistoryPage.prototype.ionViewDidEnter = function () {
    };
    HistoryPage.prototype.selOffice = function (val) {
        var seloffice;
        for (var i = 0; i < this.offices.length; i++) {
            if (val == this.offices[i].office_id) {
                seloffice = this.offices[i];
            }
        }
        return seloffice;
    };
    HistoryPage.prototype.masterDetails = function () {
        var _this = this;
        var master = [{ user_pic: this.checkPic(this.history[0].order_worker_pic_phone), user_name: this.history[0].order_worker_name_phone, user_prof: this.masters[0].user_prof, user_work_time: [], user_info: this.history[0].order_worker_name_phone }];
        var masterDetailModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modalmasters_modalmasters__["a" /* ModalmastersPage */], { master: master, mastername: this.history[0].order_worker_name_phone, takedate: this.takedate, selwork: 0, dateplace: [] });
        masterDetailModal.onDidDismiss(function (data) {
            if (data) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__ordered_ordered__["a" /* OrderedPage */], data);
            }
        });
        masterDetailModal.present();
    };
    HistoryPage.prototype.entryRequest = function (daytime, unixtime) {
        var _this = this;
        this.takedate = new Date(unixtime * 1000);
        this.takedate.setHours(0, 0, 0, 0);
        var entryRequestAS = this.actionSheetCtrl.create({
            title: this.history[0].order_worker_name_phone,
            subTitle: this.datePipe.transform(this.takedate, 'dd') + ' ' + this.datePipe.transform(this.takedate, 'MMM') + ' ' + this.tpp.transform(daytime),
            cssClass: 'action-subtitle',
            buttons: [
                {
                    text: 'Записаться',
                    handler: function () {
                        var loading = _this.loadingCtrl.create({
                            spinner: 'hide',
                            cssClass: 'loader',
                            content: "\n                <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n              "
                        });
                        loading.present();
                        var orderstr = {
                            inst_id: _this.backendProv.institution,
                            newusr: 'calender',
                            device_id: _this.backendProv.uuid,
                            getset: 1,
                            menueId: _this.history[0].order_order,
                            menueName: _this.history[0].order_name_phone,
                            menueCost: 0,
                            menueDesc: _this.history[0].order_desc,
                            workerId: _this.history[0].order_worker,
                            workerName: _this.history[0].order_worker_name_phone,
                            workerPic: _this.history[0].order_worker_pic_phone,
                            workerProfession: _this.history[0].order_worker_profession_phone,
                            orderHour: unixtime,
                            orderHourName: daytime,
                            orderDayName: _this.datePipe.transform(_this.takedate, 'dd') + '.' + _this.datePipe.transform(_this.takedate, 'MM') + '.' + _this.datePipe.transform(_this.takedate, 'y'),
                            ordercats: _this.navParams.get('menue_cat'),
                            ordgood: _this.history[0].order_goods,
                            name: _this.profile[0].user_name,
                            phone: _this.profile[0].user_mob,
                            email: _this.profile[0].user_email,
                            ordoffice: _this.history[0].order_office,
                            comments: '',
                            reminder: '',
                            smsconf: ''
                        };
                        _this.backendProv.httpRequest(JSON.stringify(orderstr))
                            .subscribe(function (res) {
                            loading.dismiss();
                            if (res[0].orderOK == '3') {
                                var alert_1 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Повторный запрос только через 5 минут!',
                                    buttons: ['Закрыть']
                                });
                                alert_1.present();
                            }
                            else if (res[0].orderOK == '2') {
                                var alert_2 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Вы уже записались на выбранную услугу либо на выбранное время. Ожидайте одобрения или звонка!',
                                    buttons: ['Закрыть']
                                });
                                alert_2.present();
                            }
                            else if (res[0].orderOK == '1') {
                                _this.backendProv.orderSentRecord(res[0]);
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__ordered_ordered__["a" /* OrderedPage */], { dateday: _this.datePipe.transform(_this.takedate, 'dd'), datemonth: _this.datePipe.transform(_this.takedate, 'MMMM'), datetime: daytime, dateplace: _this.selOffice(_this.history[0].order_office), unixtime: unixtime });
                                _this.navCtrl.popToRoot();
                            }
                            else if (res[0].orderOK == '5') {
                                var alert_3 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Укажите Ваш номер телефона!',
                                    buttons: ['Закрыть']
                                });
                                alert_3.present();
                            }
                            else if (res[0].orderOK == '8') {
                                var alert_4 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Нет свободной комнаты!',
                                    buttons: ['Закрыть']
                                });
                                alert_4.present();
                            }
                            else if (res[0].orderOK == '9') {
                                var alert_5 = _this.alertCtrl.create({
                                    title: 'Внимание',
                                    subTitle: 'Нет свободной комнаты!',
                                    buttons: ['Закрыть']
                                });
                                alert_5.present();
                            }
                        }, function (e) { console.log('ERROR 4: ' + e); loading.dismiss(); });
                    }
                },
                {
                    text: 'Отменить',
                    role: 'cancel',
                    handler: function () {
                        // console.log('Cancel clicked');
                    }
                }
            ]
        });
        entryRequestAS.present();
    };
    HistoryPage.prototype.loadMasters = function () {
        var _this = this;
        var userid = this.history[0].order_worker;
        if (this.history[0].order_worker == 0) {
            userid = '-';
        }
        var limit = 0;
        var interval = 3600;
        if (this.services.length > 0) {
            for (var i = 0; i < this.services.length; i++) {
                if (this.services[i].menue_id == this.history[0].order_order) {
                    interval = this.services[i].menue_interval * 60;
                }
            }
        }
        var office = this.offices;
        if (this.offices.length > 0) {
            for (var i = 0; i < this.offices.length; i++) {
                if (this.offices[i].office_id == this.history[0].order_office) {
                    office = [];
                    office.push(this.offices[i]);
                }
            }
        }
        var datetm = '' + this.takedate.getTime();
        datetm = datetm.slice(0, -3);
        this.backendProv.getMastersHist(this.history[0].order_order, interval, office, userid, limit, datetm)
            .then(function (res) {
            _this.masters = res;
            if (_this.masters.length > 0) {
                for (var v = 0; v < _this.masters.length; v++) {
                    for (var p = 0; p < _this.professions.length; p++) {
                        if (_this.masters[v].user_work_pos == _this.professions[p].prof_id) {
                            _this.masters[v].user_prof = _this.professions[p].prof_name;
                        }
                    }
                }
            }
            _this.groupByDate();
            _this.loading.dismiss();
        })
            .catch(function (e) {
            console.log('ERRRROR ' + JSON.stringify(e));
        });
    };
    HistoryPage.prototype.checkPic = function (val) {
        if (val && val != '0' && val != 'user.png') {
            return this.piclink + val;
        }
        else {
            return 'assets/img/girl3.png';
        }
    };
    HistoryPage.prototype.selWork = function (work, unixtime) {
        this.selwork = work;
        this.selunixtime = unixtime;
    };
    HistoryPage.prototype.lastPlace = function () {
        // lastplace
        for (var i = 0; i < this.offices.length; i++) {
            if (this.offices[i].office_id == this.history[0].order_office) {
                this.officeadress = this.offices[i].office_adress;
            }
        }
    };
    HistoryPage.prototype.compare = function (a, b) {
        if (a.dayday < b.dayday)
            return -1;
        if (a.dayday > b.dayday)
            return 1;
        return 0;
    };
    HistoryPage.prototype.groupByDate3 = function (mastersworktime, masters) {
        var goon = 0;
        if (this.grouped.length > 0) {
            for (var i = 0; i < this.grouped.length; i++) {
                if (goon == 0) {
                    if (mastersworktime.dayday == this.grouped[i].dayday) {
                        var masterunix = mastersworktime;
                        var groupedi = this.grouped[i];
                        for (var k = 0; k < groupedi.times.length; k++) {
                            if (goon == 0) {
                                if (groupedi.times.user_unixtime == masterunix.unixtime) {
                                    goon = 1;
                                }
                                else if (k == groupedi.times.length - 1) {
                                    var time = {
                                        user_daytime: masterunix.daytime,
                                        user_unixtime: masterunix.unixtime
                                    };
                                    groupedi.times.push(time);
                                    goon = 1;
                                }
                            }
                        }
                    }
                    else if (i == this.grouped.length - 1 && goon == 0) {
                        var newGroup = {
                            dayday: mastersworktime.dayday,
                            times: []
                        };
                        var newval2 = {
                            user_daytime: mastersworktime.daytime,
                            user_unixtime: mastersworktime.unixtime
                        };
                        newGroup.times.push(newval2);
                        this.grouped.push(newGroup);
                        goon = 1;
                    }
                }
            }
            // setTimeout(() => {console.log(JSON.stringify(this.grouped))}, 500)
        }
        else {
            var newGroup = {
                dayday: mastersworktime.dayday,
                times: []
            };
            var times = {
                user_daytime: mastersworktime.daytime,
                user_unixtime: mastersworktime.unixtime
            };
            newGroup.times.push(times);
            this.grouped.push(newGroup);
            this.grouped.sort(this.compare);
        }
        console.log('MGL: ' + this.grouped.length);
    };
    HistoryPage.prototype.groupByDate2 = function (masters) {
        console.log('MWT: ' + masters.user_work_time.length);
        if (masters.user_work_time.length > 0) {
            for (var i = 0; i < masters.user_work_time.length; i++) {
                this.groupByDate3(masters.user_work_time[i], masters);
            }
        }
    };
    HistoryPage.prototype.groupByDate = function () {
        console.log('ML: ' + JSON.stringify(this.masters));
        if (this.masters.length > 0) {
            for (var i = 0; i < this.masters.length; i++) {
                this.groupByDate2(this.masters[i]);
            }
        }
    };
    return HistoryPage;
}());
HistoryPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-history',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/history/history.html"*/'<ion-header no-border>\n\n  <ion-navbar color="viol">\n\n    <ion-title>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content no-padding no-bounce>\n\n\n\n  <div class="violback" style="min-height:100%;padding-bottom:10px;padding-top:10px;">\n\n\n\n    <ion-card style="width:100%;margin:0;" *ngIf="history?.length>0 && masters?.length>0 && grouped?.length > 0">\n\n      <ion-grid no-padding no-margin *ngFor="let histor of history">\n\n        <ion-row justify-content-around style="background-color: #fff;width:100%;" *ngIf="officeadress && histor.order_name">\n\n            <button ion-button small color="goldy">{{ officeadress }}</button>\n\n            <button ion-button small clear color="golds">{{ histor.order_name }}</button>\n\n        </ion-row>\n\n        <ion-list no-border text-wrap>\n\n          <ion-item>\n\n            <ion-thumbnail item-start class="oneline">\n\n              <img src="{{ checkPic(histor.order_worker_pic_phone) }}">\n\n            </ion-thumbnail>\n\n            <h2>\n\n              <span class="violethalf" *ngIf="histor.order_worker_profession_phone && histor.order_worker_profession_phone != \'0\'">{{ histor.order_worker_profession_phone }}:</span>\n\n              <span class="whitey" *ngIf="histor.order_worker_name_phone && histor.order_worker_name_phone != \'0\'">{{ histor.order_worker_name_phone }}</span>\n\n              <span class="violethalf" *ngIf="!histor.order_worker_profession_phone || histor.order_worker_profession_phone == \'0\'">Не определенно:</span>\n\n              <span class="whitey" *ngIf="!histor.order_worker_name_phone || histor.order_worker_name_phone == \'0\'">Не определенно</span>\n\n            </h2>\n\n            <button ion-button clear icon-only large item-end color="golds" (click)="masterDetails()" *ngIf="histor.order_worker_name_phone && histor.order_worker_name_phone != \'0\' && histor.order_worker_profession_phone && histor.order_worker_profession_phone != \'0\'"><ion-icon name="ios-information-circle-outline"></ion-icon></button>\n\n          </ion-item>\n\n        </ion-list>\n\n      </ion-grid>\n\n    </ion-card>\n\n\n\n    <ion-card style="width:100%;margin:0;" *ngIf="grouped?.length > 0 && history?.length>0 && masters?.length>0">\n\n      <ion-grid no-padding no-margin>\n\n        \n\n        <ion-list no-border text-wrap *ngFor="let group of grouped">\n\n          <ion-item text-wrap>\n\n\n\n            <h2 class="golds2">{{ group.dayday }}</h2>\n\n            \n\n            <button style="border-radius:0px;margin:5px;min-width:20vw;" ion-button default [outline]="time.user_unixtime != selunixtime" color="violcontr" *ngFor="let time of group.times" (click)="entryRequest(time.user_daytime, time.user_unixtime)">\n\n                {{ time.user_daytime | timepoints }}\n\n            </button>\n\n\n\n            <p>&nbsp;</p>\n\n            \n\n          </ion-item>\n\n        </ion-list>\n\n\n\n      </ion-grid>\n\n    </ion-card>\n\n\n\n    <ion-card style="width:100%;padding-bottom:10px;" *ngIf="history?.length==0 || masters?.length==0">\n\n      <h1 text-wrap text-center style="color:#fff;">У Вас нет записей.</h1>\n\n    </ion-card>\n\n\n\n  </div>\n\n\n\n</ion-content>\n\n    '/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/history/history.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_6__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__pipes_timepoints_timepoints__["a" /* TimepointsPipe */], __WEBPACK_IMPORTED_MODULE_2__angular_common__["c" /* DatePipe */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ModalController */]])
], HistoryPage);

//# sourceMappingURL=history.js.map

/***/ }),

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsdetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NewsdetailPage = (function () {
    function NewsdetailPage(navCtrl, navParams, backendProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.news = [];
        this.news.push(this.navParams.get('news'));
        this.newsPicLink = backendProv.newsPicLink;
    }
    NewsdetailPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.newsPicLink + 'pic/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    NewsdetailPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad NewsdetailPage');
    };
    return NewsdetailPage;
}());
NewsdetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-newsdetail',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/newsdetail/newsdetail.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-title [innerHTML]="news[0].news_name"></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card *ngFor="let new of news">\n    <img src="{{ checkPic(new.news_pic) }}">\n    <ion-card-content>\n      <ion-card-title [innerHTML]="new.news_name"></ion-card-title>\n      <p [innerHTML]="new.news_message"></p>\n    </ion-card-content>\n  </ion-card>\n  \n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/newsdetail/newsdetail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__["a" /* BackendProvider */]])
], NewsdetailPage);

//# sourceMappingURL=newsdetail.js.map

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TermsPage = (function () {
    function TermsPage(navCtrl, navParams, backendProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.terms = [];
        this.terms = backendProv.getTerms();
    }
    TermsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad TermsPage');
    };
    return TermsPage;
}());
TermsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-terms',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/terms/terms.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-title>Правила</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card>\n      <img src="assets/img/{{terms[0].img}}">\n      <ion-card-content>\n        <ion-card-title>\n          {{ terms[0].title }}\n        </ion-card-title>\n        <p *ngFor="let term of terms">\n          {{ term.text }}\n        </p>\n      </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/terms/terms.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__["a" /* BackendProvider */]])
], TermsPage);

//# sourceMappingURL=terms.js.map

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sharedetails_sharedetails__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SharePage = (function () {
    function SharePage(navCtrl, navParams, backendProv) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.shares = [];
        backendProv.getShare().then(function (res) { _this.shares = res; }).catch(function (e) { return console.log(e); });
    }
    SharePage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad SharePage');
    };
    SharePage.prototype.goShareDetails = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__sharedetails_sharedetails__["a" /* SharedetailsPage */]);
    };
    return SharePage;
}());
SharePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-share',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/share/share.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-title>Поделиться</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n  <div *ngFor="let share of shares">\n\n    <ion-card class="loyalcardback" *ngIf="share.org_promo_points_owner != \'0\'">\n      <ion-grid no-padding>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row>\n              <ion-col col-4 class="loyaltorch">\n                <div class="leftline"></div>\n                <h1 style="margin:25% auto 25% 25%;">\n                  <ion-icon class="txtshad" name="add"></ion-icon>\n                  <span class="txtshad">{{ share.org_promo_points_owner }}</span>\n                </h1>\n              </ion-col>\n              <ion-col col-8 class="backviol" (click)="goShareDetails()">\n                <ion-card class="inloyalcard">\n                  <div class="rightline"></div>\n                  <ion-card-header no-padding text-wrap color="light">\n                    <h3 class="whitey">ВАМ</h3>\n                  </ion-card-header>\n                  <ion-card-content no-padding text-wrap color="light">\n                    если друг ввел Ваш промокод\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n\n    <ion-card class="loyalcardback" *ngIf="share.org_promo_points_involved != \'0\'">\n      <ion-grid no-padding>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row>\n              <ion-col col-4 class="loyaltorch">\n                <div class="leftline"></div>\n                <h1 style="margin:25% auto 25% 25%;">\n                  <ion-icon class="txtshad" name="add"></ion-icon>\n                  <span class="txtshad">{{ share.org_promo_points_involved }}</span>\n                </h1>\n              </ion-col>\n              <ion-col col-8 class="backviol" (click)="goShareDetails()">\n                <ion-card class="inloyalcard">\n                  <div class="rightline"></div>\n                  <ion-card-header no-padding text-wrap color="light">\n                    <h3 class="whitey">ДРУГУ</h3>\n                  </ion-card-header>\n                  <ion-card-content no-padding text-wrap color="light">\n                    который ввел Ваш промокод\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n\n    <ion-card class="loyalcardback" *ngIf="share.org_promo_points_scan_owner != \'0\'">\n      <ion-grid no-padding>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row>\n              <ion-col col-4 class="loyaltorch">\n                <div class="leftline"></div>\n                <h1 style="margin:25% auto 25% 25%;">\n                  <ion-icon class="txtshad" name="add"></ion-icon>\n                  <span class="txtshad">{{ share.org_promo_points_scan_owner }}</span>\n                </h1>\n              </ion-col>\n              <ion-col col-8 class="backviol" (click)="goShareDetails()">\n                <ion-card class="inloyalcard">\n                  <div class="rightline"></div>\n                  <ion-card-header no-padding text-wrap color="light">\n                    <h3 class="whitey">ВАМ</h3>\n                  </ion-card-header>\n                  <ion-card-content no-padding text-wrap color="light">\n                    если друг воспользуется нашими услугами\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n\n    <ion-card class="loyalcardback" *ngIf="share.org_promo_points_scan_involved != \'0\'">\n      <ion-grid no-padding>\n        <ion-row>\n          <ion-col col-12>\n            <ion-row>\n              <ion-col col-4 class="loyaltorch">\n                <div class="leftline"></div>\n                <h1 style="margin:25% auto 25% 25%;">\n                  <ion-icon class="txtshad" name="add"></ion-icon>\n                  <span class="txtshad">{{ share.org_promo_points_scan_involved }}</span>\n                </h1>\n              </ion-col>\n              <ion-col col-8 class="backviol" (click)="goShareDetails()">\n                <ion-card class="inloyalcard">\n                  <div class="rightline"></div>\n                  <ion-card-header no-padding text-wrap color="light">\n                    <h3 class="whitey">ДРУГУ</h3>\n                  </ion-card-header>\n                  <ion-card-content no-padding text-wrap color="light">\n                    если друг ввел Ваш промокод\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n\n    <ion-card class="backtransparent">\n      <ion-grid no-padding>\n        <ion-row justify-content-around>\n          <ion-col col-6>\n            <button ion-button block icon-right color="viol" (click)="goShareDetails()">\n              Поделиться\n              <ion-icon name="arrow-forward"></ion-icon>\n            </button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/share/share.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */]])
], SharePage);

//# sourceMappingURL=share.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SharedetailsPage = (function () {
    function SharedetailsPage(navCtrl, navParams, backendProv, socialSharing) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.socialSharing = socialSharing;
        this.myid = 0;
        this.sharedetails = 0;
        this.myid = backendProv.myid;
        this.sharedetails = backendProv.getShareDetails();
        this.sharelink = backendProv.sharelink;
    }
    SharedetailsPage.prototype.sharePromo = function () {
        this.socialSharing.share('Скачай приложение «Белый Лотос», используя промо-код ' + this.myid + ' и получи бонусы.', 'Белый Лотос', 'http://www.olegtronics.com/admin/img/icons/whitelotos.png', this.sharelink).then().catch();
    };
    SharedetailsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad SharedetailsPage');
    };
    return SharedetailsPage;
}());
SharedetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-sharedetails',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/sharedetails/sharedetails.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-title>Поделиться</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-card style="text-align:center;" *ngFor="let share of sharedetails" class="doubleline" padding>\n    <ion-card-title>\n      Поделитесь своим промокодом с друзьями\n    </ion-card-title>\n    <img style="width:90%;margin:0 auto;" src="assets/img/logo.png"/>\n    <ion-card-content>\n      <h2>Промокод:</h2>\n      <h1>{{ myid }}</h1>\n      <button ion-button full color="viol" (click)="sharePromo()">Поделиться</button>\n    </ion-card-content>\n    <button ion-button clear icon-only color="viol">\n      <ion-icon name="logo-facebook"></ion-icon>\n    </button>\n    <button ion-button clear icon-only color="viol">\n      <ion-icon name="logo-twitter"></ion-icon>\n    </button>\n    <button ion-button clear icon-only color="viol">\n      <ion-icon name="logo-skype"></ion-icon>\n    </button>\n    <button ion-button clear icon-only color="viol">\n      <ion-icon name="logo-instagram"></ion-icon>\n    </button>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/sharedetails/sharedetails.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__["a" /* SocialSharing */]])
], SharedetailsPage);

//# sourceMappingURL=sharedetails.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GiftsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__giftsdetail_giftsdetail__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GiftsPage = (function () {
    function GiftsPage(navCtrl, navParams, backendProv, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.loadingCtrl = loadingCtrl;
        this.gifts = [];
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        backendProv.getGifts().then(function (res) {
            loading.dismiss();
            _this.gifts = res;
        }).catch(function (e) { return console.log(e); });
        this.giftsPicLink = backendProv.giftsPicLink;
    }
    GiftsPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.giftsPicLink + 'pic/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    GiftsPage.prototype.goGiftsDetail = function (val) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__giftsdetail_giftsdetail__["a" /* GiftsdetailPage */], { gift: val });
    };
    GiftsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad GiftsPage');
    };
    return GiftsPage;
}());
GiftsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-gifts',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/gifts/gifts.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-title>Подарки</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n    <ion-card class="backtransparent">\n      <ion-grid no-padding>\n        <div *ngFor="let gift of gifts; let i=index">\n          <ion-row justify-content-around margin-bottom *ngIf="i % 2 === 0">\n            <ion-col col-5 class="card-background-page" (click)="goGiftsDetail(gifts[i])" *ngIf="i < gifts.length">\n              <img src="{{ checkPic(gifts[i].gifts_pic) }}"/>\n              <div class="card-title everyline">\n                <div class="centering" [innerHTML]="gifts[i].gifts_name"></div>\n              </div>\n            </ion-col>\n            <ion-col col-5 class="card-background-page" (click)="goGiftsDetail(gifts[i+1])" *ngIf="i + 1 < gifts.length">\n              <img src="{{ checkPic(gifts[i+1].gifts_pic) }}"/>\n              <div class="card-title everyline">\n                <div class="centering" [innerHTML]="gifts[i+1].gifts_name"></div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-grid>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/gifts/gifts.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], GiftsPage);

//# sourceMappingURL=gifts.js.map

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SocialPage = (function () {
    function SocialPage(navCtrl, navParams, iab, backendProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.iab = iab;
        this.backendProv = backendProv;
    }
    SocialPage.prototype.goInstagram = function () {
        this.iab.create(this.backendProv.groupIG, '_system', { location: 'yes' });
    };
    SocialPage.prototype.goFacebook = function () {
        this.iab.create(this.backendProv.groupFB, '_system', { location: 'yes' });
    };
    SocialPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad SocialPage');
    };
    return SocialPage;
}());
SocialPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-social',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/social/social.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-title>Наши группы</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card class="backtransparent">\n    <ion-card-content>Вступайте в наши группы и получайте подарки</ion-card-content>\n      <button ion-button block icon-left color="viol" (click)="goFacebook()">\n        <ion-icon name="logo-facebook"></ion-icon>\n        Facebook\n      </button>\n\n      <button ion-button block icon-left color="viol" (click)="goInstagram()">\n        <ion-icon name="logo-instagram"></ion-icon>\n        Instagram\n      </button>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/social/social.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */]])
], SocialPage);

//# sourceMappingURL=social.js.map

/***/ }),

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactPage = (function () {
    function ContactPage(navCtrl, platform, backendProv, iab, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.backendProv = backendProv;
        this.iab = iab;
        this.loadingCtrl = loadingCtrl;
        this.lat = 53.8991;
        this.lon = 27.5546;
        this.zoom = 14;
        this.cluster = 0;
        this.screenheight = 0;
        this.offices = [];
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        this.screenheight = platform.height() / 2;
        backendProv.getOffices().then(function (res) {
            loading.dismiss();
            _this.offices = res;
        })
            .catch(function (e) { return console.log(e); });
    }
    ContactPage.prototype.ionViewDidLoad = function () {
    };
    ContactPage.prototype.goMail = function () {
        this.iab.create('mailto:whitelotus2@mail.ru', '_system', { location: 'yes' });
    };
    ContactPage.prototype.call = function () {
        this.iab.create('tel:7579', '_system', { location: 'yes' });
    };
    ContactPage.prototype.goPath = function (val) {
        if (val == '0') {
            this.iab.create('https://maps.google.com?saddr=Current+Location&daddr=vulica+Kirava+9+Minsk', '_system', { location: 'yes' });
        }
        else if (val == '1') {
            this.iab.create('https://maps.google.com?saddr=Current+Location&daddr=vulica+Kiryly+i+Miafodzija+8+Minsk', '_system', { location: 'yes' });
        }
    };
    ContactPage.prototype.clickedMarker = function (label, index) {
        console.log("clicked the marker: " + (label || index));
    };
    ContactPage.prototype.strToNum = function (x) {
        return parseFloat(x);
    };
    ContactPage.prototype.getDistanceFromLatLonInKm = function (lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
        var dLon = this.deg2rad(lon2 - lon1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return (d).toFixed(2);
    };
    ContactPage.prototype.deg2rad = function (deg) {
        return deg * (Math.PI / 180);
    };
    return ContactPage;
}());
ContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-contact',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar color="viol">\n    <ion-title>\n      Контакты\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-card no-padding>\n    <ion-card-content>\n      <ion-list>\n        <ion-item>\n          <ion-avatar item-start>\n              <img src="assets/img/logo.png">\n          </ion-avatar>\n          <h2>Белый Лотос</h2>\n        </ion-item>\n        <br/>\n        <button ion-item text-wrap color="viol" (click)="call()">\n          <ion-icon name="call" item-start></ion-icon>\n          7579\n        </button>\n        <br/>\n        <button ion-item text-wrap color="viol">\n          <ion-icon name="time" item-start></ion-icon>\n          Ежедневно с 10:00 до 02:00\n        </button>\n        <br/>\n        <button ion-item text-wrap color="viol" (click)="goPath(0)">\n          <ion-icon name="pin" item-start></ion-icon>\n          УЛ. КИРОВА, Д.9\n        </button>\n        <br/>\n        <button ion-item text-wrap color="viol" (click)="goPath(1)">\n          <ion-icon name="pin" item-start></ion-icon>\n          КИРИЛЛА И МЕФОДИЯ, Д.8\n        </button>\n        <br/>\n        <button ion-item text-wrap color="viol" (click)="goMail()">\n          <ion-icon name="mail" item-start></ion-icon>\n          whitelotus2@mail.ru\n        </button>\n        <br/>\n      </ion-list>\n      <agm-map [latitude]="lat" [longitude]="lon" [zoom]="zoom" [zoomControl]="true" [streetViewControl]="false" [disableDefaultUI]="false" [disableDoubleClickZoom]="false" [gestureHandling]="auto" [mapDraggable]="true" [style.height.px]="screenheight">\n        <!-- <agm-marker *ngFor="let inst of institutions" [latitude]="strToNum(inst.lat)" [longitude]="strToNum(inst.lon)" [iconUrl]="inst.img" [label]="inst.adress" (markerClick)="clickedMarker(m.label, i)"></agm-marker> -->\n        <agm-marker *ngFor="let office of offices" [latitude]="strToNum(office.office_lat)" [longitude]="strToNum(office.office_lon)">\n          <agm-info-window  [disableAutoPan]="true">\n            <strong>{{ office.office_adress }}</strong>\n          </agm-info-window>\n        </agm-marker>\n      </agm-map>\n      <br/>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/contact/contact.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], ContactPage);

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceRitualsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_details_service_details__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServiceRitualsPage = (function () {
    function ServiceRitualsPage(navCtrl, navParams, backendProv, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.loadingCtrl = loadingCtrl;
        this.rituals = [];
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        backendProv.getRituals('182')
            .then(function (res) {
            _this.rituals = res;
            loading.dismiss();
        })
            .catch(function (e) { return console.log(e); });
        this.menuePicLink = backendProv.menuePicLink;
    }
    ServiceRitualsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad ServiceRitualsPage');
    };
    ServiceRitualsPage.prototype.priceConvert = function (val) {
        return (val / 100).toFixed(0);
    };
    ServiceRitualsPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.menuePicLink + '300/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    ServiceRitualsPage.prototype.goRitualDetails = function (val) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__service_details_service_details__["a" /* ServiceDetailsPage */], { ritual: val });
    };
    return ServiceRitualsPage;
}());
ServiceRitualsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-service-rituals',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/service-rituals/service-rituals.html"*/'<ion-header>\n\n  <ion-navbar color="viols">\n    <ion-title>Ритуалы</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="card-background-page">\n\n  <ion-card *ngFor="let ritual of rituals" (click)="goRitualDetails(ritual)">\n    <img src="{{ checkPic(ritual.menue_pic) }}"/>\n    <div class="card-title textshadow" [innerHtml]="ritual.menue_name"></div>\n    <div class="card-subtitle textshadow">\n      <ion-row>\n        <ion-col>\n          от {{ priceConvert(ritual.menue_cost) }} BYN\n          <br/>\n          <ion-note>цена</ion-note>\n        </ion-col>\n        <ion-col>\n          {{ ritual.menue_interval }}\n          <br/>\n          <ion-note>продолжительность сеанса</ion-note>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/service-rituals/service-rituals.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], ServiceRitualsPage);

//# sourceMappingURL=service-rituals.js.map

/***/ }),

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceProgrammsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_details_service_details__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServiceProgrammsPage = (function () {
    function ServiceProgrammsPage(navCtrl, navParams, backendProv, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.loadingCtrl = loadingCtrl;
        this.programms = [];
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        backendProv.getRituals('185')
            .then(function (res) {
            loading.dismiss();
            _this.programms = res;
        })
            .catch(function (e) { return console.log(e); });
        this.menuePicLink = backendProv.menuePicLink;
    }
    ServiceProgrammsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad ServiceProgrammsPage');
    };
    ServiceProgrammsPage.prototype.priceConvert = function (val) {
        return (val / 100).toFixed(0);
    };
    ServiceProgrammsPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.menuePicLink + '300/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    ServiceProgrammsPage.prototype.goRitualDetails = function (val) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__service_details_service_details__["a" /* ServiceDetailsPage */], { ritual: val });
    };
    return ServiceProgrammsPage;
}());
ServiceProgrammsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-service-programms',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/service-programms/service-programms.html"*/'<ion-header>\n\n  <ion-navbar color="viols">\n    <ion-title>SPA-программы</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content no-padding class="card-background-page">\n\n  <ion-card *ngFor="let programm of programms" (click)="goRitualDetails(programm)">\n    <img src="{{ checkPic(programm.menue_pic) }}"/>\n    <div class="card-title textshadow" [innerHtml]="programm.menue_name"></div>\n    <div class="card-subtitle textshadow">\n      <ion-row>\n        <ion-col>\n          от {{ priceConvert(programm.menue_cost) }} BYN\n          <br/>\n          <ion-note>цена</ion-note>\n        </ion-col>\n        <ion-col>\n          {{ programm.menue_interval }}\n          <br/>\n          <ion-note>продолжительность сеанса</ion-note>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/service-programms/service-programms.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], ServiceProgrammsPage);

//# sourceMappingURL=service-programms.js.map

/***/ }),

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceGiftcardsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_giftcards_detail_service_giftcards_detail__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServiceGiftcardsPage = (function () {
    function ServiceGiftcardsPage(navCtrl, navParams, backendProv, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.loadingCtrl = loadingCtrl;
        this.gifts = [];
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        backendProv.getCards()
            .then(function (res) {
            loading.dismiss();
            _this.gifts = res;
        })
            .catch(function (e) { return console.log(e); });
        this.menuePicLink = backendProv.menuePicLink;
    }
    ServiceGiftcardsPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.menuePicLink + '300/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    ServiceGiftcardsPage.prototype.goGiftsDetail = function (val) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__service_giftcards_detail_service_giftcards_detail__["a" /* ServiceGiftcardsDetailPage */], { gift: val });
    };
    ServiceGiftcardsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad ServiceGiftcardsPage');
    };
    return ServiceGiftcardsPage;
}());
ServiceGiftcardsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-service-giftcards',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/service-giftcards/service-giftcards.html"*/'<ion-header>\n\n  <ion-navbar color="viols">\n    <ion-title>Подарочные карты</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding>\n\n  <ion-card class="backtransparent">\n    <ion-grid no-padding>\n      <div *ngFor="let gift of gifts; let i=index">\n        <ion-row justify-content-around margin-bottom *ngIf="i % 2 === 0">\n          <ion-col col-5 class="card-background-page" (click)="goGiftsDetail(gifts[i])" *ngIf="i < gifts.length">\n            <img src="{{ checkPic(gifts[i].menue_pic) }}"/>\n            <div class="card-title everyline">\n              <div class="centering">{{ gifts[i].menue_cost/100 }} руб</div>\n              <div class="centering" [innerHTML]="gifts[i].menue_name"></div>\n            </div>\n          </ion-col>\n          <ion-col col-5 class="card-background-page" (click)="goGiftsDetail(gifts[i+1])" *ngIf="i + 1 < gifts.length">\n            <img src="{{ checkPic(gifts[i+1].menue_pic) }}"/>\n            <div class="card-title everyline">\n              <div class="centering">{{ gifts[i+1].menue_cost/100 }} руб</div>\n              <div class="centering" [innerHTML]="gifts[i+1].menue_name"></div>\n            </div>\n          </ion-col>\n        </ion-row>\n      </div>\n    </ion-grid>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/service-giftcards/service-giftcards.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], ServiceGiftcardsPage);

//# sourceMappingURL=service-giftcards.js.map

/***/ }),

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceGiftcardsDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ServiceGiftcardsDetailPage = (function () {
    function ServiceGiftcardsDetailPage(navCtrl, navParams, backendProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.gifts = [];
        this.gifts.push(navParams.get('gift'));
        this.menuePicLink = backendProv.menuePicLink;
    }
    ServiceGiftcardsDetailPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.menuePicLink + '300/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    ServiceGiftcardsDetailPage.prototype.ionViewDidLoad = function () {
    };
    return ServiceGiftcardsDetailPage;
}());
ServiceGiftcardsDetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-service-giftcards-detail',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/service-giftcards-detail/service-giftcards-detail.html"*/'<ion-header>\n  \n  <ion-navbar color="viol">\n    <ion-title [innerHTML]="gifts[0].menue_name"></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-card *ngFor="let gift of gifts">\n    <img src="{{ checkPic(gift.menue_pic) }}">\n    <ion-card-content>\n      <ion-card-title [innerHTML]="gift.menue_name"></ion-card-title>\n      <p>{{ gift.menue_cost/100 }} руб</p>\n      <p [innerHTML]="gift.menue_desc"></p>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>\n  '/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/service-giftcards-detail/service-giftcards-detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__["a" /* BackendProvider */]])
], ServiceGiftcardsDetailPage);

//# sourceMappingURL=service-giftcards-detail.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AboutPage = (function () {
    function AboutPage(navCtrl, menuCtrl, navParams, backendProv) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.abouts = [];
        this.abouts = backendProv.getAbout();
    }
    AboutPage.prototype.openMenu = function () {
        this.menuCtrl.open();
    };
    AboutPage.prototype.closeMenu = function () {
        this.menuCtrl.close();
    };
    AboutPage.prototype.toggleMenu = function () {
        this.menuCtrl.toggle();
    };
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-about',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar color="viol">\n    <ion-buttons left>\n      <button ion-button icon-end color="light" (click)="toggleMenu()" style="font-size:20px;">\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>\n      {{ abouts[0].title }}\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-padding>\n\n  <ion-card>\n      <img src="assets/img/{{abouts[0].img}}">\n      <ion-card-content>\n        <ion-card-title>\n          {{ abouts[0].title }}\n        </ion-card-title>\n        <p *ngFor="let about of abouts">\n          {{ about.text }}\n        </p>\n      </ion-card-content>\n    </ion-card>\n    \n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/about/about.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__["a" /* BackendProvider */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profiledetails_profiledetails__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams, menuCtrl, backendProv, loadingCtrl, datePipe, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.backendProv = backendProv;
        this.loadingCtrl = loadingCtrl;
        this.datePipe = datePipe;
        this.alertCtrl = alertCtrl;
        this.piclink = 0;
        this.promobtn = 0;
        this.profiles = [];
        this.visited = [];
        this.ordersAll = [];
        this.piclink = backendProv.piclink;
        this.menuePicLink = backendProv.menuePicLink;
        backendProv.getOffices()
            .then(function (res) {
            backendProv.getHistory()
                .then(function (res2) {
                if (res2) {
                    _this.visited[0].visitTime = _this.datePipe.transform(res2[0].order_start * 1000, 'dd') + '.' + _this.datePipe.transform(res2[0].order_start * 1000, 'MM') + '.' + _this.datePipe.transform(res2[0].order_start * 1000, 'y');
                    for (var i = 0; i < res[0].length; i++) {
                        console.log(res[0].office_id + ' | ' + res2[0].order_office + ' | ' + res[0].office_name);
                        if (res[0].office_id === res2[0].order_office) {
                            _this.visited[0].visitPlace = res[0].office_name;
                        }
                    }
                }
            })
                .catch(function (e) { return console.log(e); });
        })
            .catch(function (e) { return console.log(e); });
        backendProv.getOrders()
            .then(function (res) {
            if (res) {
                _this.ordersAll = res;
            }
        })
            .catch(function (e) { return console.log(e); });
    }
    ProfilePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        this.backendProv.getProfile()
            .then(function (res) {
            loading.dismiss();
            _this.profiles = res;
            _this.promobtn = res[0].user_promo;
        })
            .catch(function (e) { return console.log(e); });
    };
    ProfilePage.prototype.beforeCancelOrder = function (val) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Внимание',
            message: 'Отменить запись?',
            buttons: [{
                    text: 'Да',
                    role: 'cancel',
                    handler: function (data) {
                        _this.backendProv.cancelOrder(val)
                            .then(function (res) {
                            _this.backendProv.getOrders()
                                .then(function (res) {
                                if (res) {
                                    _this.ordersAll = res;
                                }
                            })
                                .catch(function (e) { return console.log(e); });
                        })
                            .catch(function (e) { return console.log(e); });
                    }
                },
                {
                    text: 'Закрыть',
                    handler: function (data) {
                    }
                }]
        });
        alert.present();
    };
    ProfilePage.prototype.formatTime = function (val) {
        return this.backendProv.formatTime(val);
    };
    ProfilePage.prototype.promoSend = function (promo) {
        var _this = this;
        this.backendProv.httpRequest(promo)
            .subscribe(function (res) {
            var promoOk = parseInt(res[0].promoOK);
            if (promoOk == 0) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Внимание',
                    subTitle: 'Обратитесь в техподдержку.',
                    buttons: ['Закрыть']
                });
                alert_1.present();
            }
            else if (promoOk > 3) {
                _this.backendProv.promoConfirm(res);
                _this.promobtn = promoOk;
                var alert_2 = _this.alertCtrl.create({
                    title: 'Благодарим',
                    subTitle: 'Промокод внесен верно!',
                    buttons: ['Закрыть']
                });
                alert_2.present();
            }
            else if (promoOk == 2) {
                _this.backendProv.promoConfirm(res);
                var alert_3 = _this.alertCtrl.create({
                    title: 'Внимание',
                    subTitle: 'Доступ к данной услуге был закрыт.',
                    buttons: ['Закрыть']
                });
                alert_3.present();
            }
            else if (promoOk == 3) {
                var alert_4 = _this.alertCtrl.create({
                    title: 'Внимание',
                    subTitle: 'Неверно внесенный промокод!',
                    buttons: [{
                            text: 'Еще раз',
                            handler: function (data) {
                                _this.enterPromo();
                            }
                        }]
                });
                alert_4.present();
            }
        }, function (e) { return console.log(e); });
    };
    ProfilePage.prototype.enterPromo = function () {
        var _this = this;
        if (this.profiles[0].user_work_pos > '1') {
            var alert_5 = this.alertCtrl.create({
                title: 'Внимание',
                subTitle: 'Доступ к данной услуге был закрыт.',
                buttons: ['Закрыть']
            });
            alert_5.present();
        }
        else if (this.profiles[0].user_mob_confirm == '1') {
            var prompt_1 = this.alertCtrl.create({
                title: 'Промокод',
                message: "Введите промокод друга",
                inputs: [
                    {
                        name: 'promo',
                        placeholder: '12345'
                    },
                ],
                buttons: [
                    {
                        text: 'Отмена',
                        handler: function (data) {
                            // console.log(JSON.stringify(data))
                        }
                    },
                    {
                        text: 'Принять',
                        handler: function (data) {
                            var promo = JSON.stringify({
                                device_id: _this.backendProv.uuid,
                                inst_id: _this.backendProv.institution,
                                promo: data.promo,
                                newusr: 'promo'
                            });
                            _this.promoSend(promo);
                        }
                    }
                ]
            });
            prompt_1.present();
        }
        else {
            var alert_6 = this.alertCtrl.create({
                title: 'Внимание',
                subTitle: 'Необходимо подтвердить номер телефона в личном кабинете',
                buttons: [
                    {
                        text: 'Закрыть'
                    },
                    {
                        text: 'Принять',
                        handler: function (data) {
                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__profiledetails_profiledetails__["a" /* ProfiledetailsPage */], { profile: _this.profiles[0] });
                        }
                    }
                ]
            });
            alert_6.present();
        }
    };
    ProfilePage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.piclink + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    ProfilePage.prototype.checkMenuePic = function (val) {
        if (val && val != '0') {
            return this.menuePicLink + '300/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    ProfilePage.prototype.checkZero = function (val) {
        if (val) {
            if (val == '0') {
                return '';
            }
            else {
                return val;
            }
        }
        else {
            return '';
        }
    };
    ProfilePage.prototype.openMenu = function () {
        this.menuCtrl.open();
    };
    ProfilePage.prototype.closeMenu = function () {
        this.menuCtrl.close();
    };
    ProfilePage.prototype.toggleMenu = function () {
        this.menuCtrl.toggle();
    };
    ProfilePage.prototype.goProfileDetails = function (val) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__profiledetails_profiledetails__["a" /* ProfiledetailsPage */], { profile: val });
    };
    return ProfilePage;
}());
ProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-profile',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/profile/profile.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-buttons left>\n      <button ion-button icon-end color="light" (click)="toggleMenu()" style="font-size:20px;">\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Личный кабинет</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card class="backtransparent" *ngFor="let profile of profiles">\n    <ion-grid no-padding>\n      <ion-row justify-content-around>\n        <ion-col col-8 padding class="card-background-page" (click)="goProfileDetails(profile)">\n          <img src="{{ checkPic(profile.user_pic) }}" />\n          <div class="card-title doubleline whitey">\n            <div class="centering">\n              &nbsp;\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row justify-content-around class="nopadd">\n        <ion-col col-8>\n          <h2>{{ checkZero(profile.user_name) }}</h2>\n          <h2>{{ checkZero(profile.user_surname) }}</h2>\n          <!-- <p *ngIf="visited?.length==0">Еще не был в "Белый Лотос"</p>\n          <p *ngIf="visited?.length>0">Был последний раз {{ visited[0].visitTime }}</p>\n          <p *ngIf="visited?.length>0">Был последний раз {{ visited[0].visitPlace }}</p> -->\n        </ion-col>\n        <ion-col col-2>\n          &nbsp;\n        </ion-col>\n      </ion-row>\n      <ion-row justify-content-around class="nopadd">\n        <ion-col col-8>\n          <p>Мой промокод</p>\n          <h2>{{ profile.user_real_id }}</h2>\n        </ion-col>\n        <ion-col col-2>\n          <button ion-button clear icon-only large (click)="goProfileDetails(profile)">\n            <ion-icon name="create"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n      <ion-row justify-content-around *ngIf="promobtn != 1 && promobtn < 4">\n        <ion-col>\n          <button ion-button color="viol" (click)="enterPromo()">Задать промокод</button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n  <ion-list *ngIf="ordersAll?.length > 0" class="text-wrap">\n    <ion-item *ngFor="let order of ordersAll" class="text-wrap">\n      <ion-thumbnail item-start>\n        <img src="{{ checkPic(order.order_worker_pic_phone) }}">\n      </ion-thumbnail>\n      <h2 class="text-wrap">{{ order.order_name }}</h2>\n      <p class="text-wrap">\n        в {{ formatTime(order.order_start) }}\n        <button ion-button clear *ngIf="order.order_status == 0" color="viol">Ожидает подтверждение</button>\n        <button ion-button clear *ngIf="order.order_status == 1" color="primary">Подтверждено</button>\n        <button ion-button clear *ngIf="order.order_status == 2" color="secondary">Подтверждено</button>\n        <button ion-button clear *ngIf="order.order_status == 3" color="danger">Отклоненно</button>\n        <button ion-button clear *ngIf="order.order_status == 4" color="danger">Отмененно пользователем</button>\n      </p>\n      <button ion-button clear item-end color="danger" (click)="beforeCancelOrder(order.order_id)">\n        <ion-icon name="trash"></ion-icon>\n      </button>\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/profile/profile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_4__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__angular_common__["c" /* DatePipe */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], ProfilePage);

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportFaqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__support_chat_support_chat__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SupportFaqPage = (function () {
    function SupportFaqPage(navCtrl, navParams, menuCtrl, backendProv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.backendProv = backendProv;
        this.getfaq = [];
        this.shownGroup = [];
        this.toggleGroup = function (faq) {
            if (this.isGroupShown(faq)) {
                this.shownGroup = null;
            }
            else {
                this.shownGroup = faq;
            }
        };
        this.isGroupShown = function (faq) {
            return this.shownGroup === faq;
        };
        this.getfaq = backendProv.getFAQ();
    }
    SupportFaqPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad SupportFaqPage');
    };
    SupportFaqPage.prototype.goChat = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__support_chat_support_chat__["a" /* SupportChatPage */]);
    };
    SupportFaqPage.prototype.openMenu = function () {
        this.menuCtrl.open();
    };
    SupportFaqPage.prototype.closeMenu = function () {
        this.menuCtrl.close();
    };
    SupportFaqPage.prototype.toggleMenu = function () {
        this.menuCtrl.toggle();
    };
    return SupportFaqPage;
}());
SupportFaqPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-support-faq',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/support-faq/support-faq.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-buttons left>\n      <button ion-button icon-end color="light" (click)="toggleMenu()" style="font-size:20px;">\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Техподдержка</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-list no-lines>\n    <div *ngFor="let faq of getfaq">\n      <ion-item text-wrap (click)="toggleGroup(faq)" [class]="{active: isGroupShown(faq)}">\n        <span [innerHtml]="faq.question"></span>\n        <ion-icon item-end name="ios-arrow-forward" *ngIf="!isGroupShown(faq)"></ion-icon>\n        <ion-icon item-end name="ios-arrow-down" *ngIf="isGroupShown(faq)"></ion-icon>\n      </ion-item>\n      <ion-item text-wrap *ngIf="isGroupShown(faq)">\n        <span [innerHtml]="faq.answer"></span>\n      </ion-item>\n    </div>\n  </ion-list>\n\n  <button ion-button block large color="viol" (click)="goChat()">Прямой чат <ion-icon name="paper-plane"></ion-icon></button>\n    \n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/support-faq/support-faq.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_3__providers_backend_backend__["a" /* BackendProvider */]])
], SupportFaqPage);

//# sourceMappingURL=support-faq.js.map

/***/ }),

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SupportChatPage = (function () {
    function SupportChatPage(platform, navCtrl, navParams, keyboard, renderer, loadingCtrl, backendProv, http) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.keyboard = keyboard;
        this.renderer = renderer;
        this.loadingCtrl = loadingCtrl;
        this.backendProv = backendProv;
        this.http = http;
        this.messages = [];
        this.message = '';
        this.lineHeight = "22px";
        this.millis = 200;
        this.profiles = [];
        this.piclink = 0;
        this.mypic = 'assets/img/logo.png';
        this.lastchat = 1;
        this.piclink = backendProv.piclink;
    }
    SupportChatPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.piclink + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    SupportChatPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        if (this.platform.is('ios')) {
            this.addKeyboardListeners();
        }
        this.scrollContentElelment = this.content.getScrollElement();
        this.footerElement = document.getElementsByTagName('page-support-chat')[0].getElementsByTagName('ion-footer')[0];
        this.inputElement = document.getElementsByTagName('page-support-chat')[0].getElementsByTagName('textarea')[0];
        this.footerElement.style.cssText = this.footerElement.style.cssText + "transition: all " + this.millis + "ms; -webkit-transition: all " +
            this.millis + "ms; -webkit-transition-timing-function: ease-out; transition-timing-function: ease-out;";
        this.scrollContentElelment.style.cssText = this.scrollContentElelment.style.cssText + "transition: all " + this.millis + "ms; -webkit-transition: all " +
            this.millis + "ms; -webkit-transition-timing-function: ease-out; transition-timing-function: ease-out;";
        this.textareaHeight = Number(this.inputElement.style.height.replace('px', ''));
        this.initialTextAreaHeight = this.textareaHeight;
        this.platform.ready().then(function () {
            _this.keyboard.disableScroll(true);
        });
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n          <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n        "
        });
        loading.present();
        this.backendProv.getProfile().then(function (res) {
            loading.dismiss();
            _this.profiles = res;
            _this.messages.push({
                img: 'assets/img/face.jpg',
                content: 'Здравствуйте! Чем Вам помочь?',
                position: 'left',
                time: new Date().toLocaleTimeString(),
                senderName: 'Поддержка'
            });
            _this.getMessages();
            _this.mypic = _this.checkPic(_this.profiles[0].user_pic);
        }).catch(function (e) { return console.log(e); });
    };
    SupportChatPage.prototype.ionViewWillLeave = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.keyboard.disableScroll(false);
            if (_this.platform.is('ios')) {
                _this.removeKeyboardListeners();
            }
        });
    };
    SupportChatPage.prototype.footerTouchStart = function (event) {
        var _this = this;
        //console.log('footerTouchStart: ', event.type, event.target.localName, '...')
        if (event.target.localName !== "textarea") {
            event.preventDefault();
            // console.log('preventing')
        }
        setTimeout(function () {
            _this.content.scrollToBottom(300);
        }, 500);
    };
    SupportChatPage.prototype.textAreaChange = function () {
        var newHeight = Number(this.inputElement.style.height.replace('px', ''));
        if (newHeight !== this.textareaHeight) {
            var diffHeight = newHeight - this.textareaHeight;
            this.textareaHeight = newHeight;
            var newNumber = Number(this.scrollContentElelment.style.marginBottom.replace('px', '')) + diffHeight;
            var marginBottom = newNumber + 'px';
            this.renderer.setElementStyle(this.scrollContentElelment, 'marginBottom', marginBottom);
        }
    };
    SupportChatPage.prototype.removeKeyboardListeners = function () {
        this.keyboardHideSub.unsubscribe();
        this.keybaordShowSub.unsubscribe();
    };
    SupportChatPage.prototype.addKeyboardListeners = function () {
        var _this = this;
        this.keyboardHideSub = this.keyboard.onKeyboardHide().subscribe(function () {
            var newHeight = _this.textareaHeight - _this.initialTextAreaHeight + 44;
            var marginBottom = newHeight + 'px';
            _this.renderer.setElementStyle(_this.scrollContentElelment, 'marginBottom', marginBottom);
            _this.renderer.setElementStyle(_this.footerElement, 'marginBottom', '0px');
        });
        this.keybaordShowSub = this.keyboard.onKeyboardShow().subscribe(function (e) {
            var newHeight = (e['keyboardHeight']) + _this.textareaHeight - _this.initialTextAreaHeight;
            var marginBottom = newHeight + 44 + 'px';
            _this.renderer.setElementStyle(_this.scrollContentElelment, 'marginBottom', marginBottom);
            _this.renderer.setElementStyle(_this.footerElement, 'marginBottom', e['keyboardHeight'] + 'px');
        });
    };
    SupportChatPage.prototype.contentMouseDown = function (event) {
        //console.log('blurring input element :- > event type:', event.type);
        this.inputElement.blur();
    };
    SupportChatPage.prototype.touchSendButton = function (event) {
        //console.log('touchSendButton, event type:', event.type);
        event.preventDefault();
        this.sendMessage();
    };
    SupportChatPage.prototype.sendMessage = function () {
        if (this.message != '') {
            var supportstr = JSON.stringify({
                device_id: this.backendProv.uuid,
                inst_id: this.backendProv.institution,
                getsend: 'send',
                name: 'support',
                txt: this.message,
                newusr: 'chat'
            });
            // console.log("SEND =========================> "+supportstr)
            this.writeMessages(supportstr);
        }
    };
    SupportChatPage.prototype.getMessages = function () {
        var _this = this;
        var getChat = "SELECT * FROM chat WHERE chat_name != ? AND chat_del='0' ORDER BY chat_id ASC";
        this.backendProv.database.executeSql(getChat, ['Запрос']).then(function (res) {
            if (res.rows.length > 0) {
                for (var i = 0; i < res.rows.length; i++) {
                    if (res.rows.item(i).chat_message != 'Скидочная карта') {
                        var mestime = _this.backendProv.timezoneAdd(res.rows.item(i).chat_when);
                        if (res.rows.item(i).chat_from == _this.backendProv.myid) {
                            _this.messages.push({
                                img: _this.mypic,
                                position: 'right',
                                content: _this.backendProv.decodeEntities(res.rows.item(i).chat_message),
                                senderName: 'Я',
                                time: new Date(mestime * 1000).toLocaleTimeString()
                            });
                        }
                        else if (res.rows.item(i).chat_from != _this.backendProv.myid) {
                            _this.messages.push({
                                img: 'assets/img/face.jpg',
                                position: 'left',
                                content: _this.backendProv.decodeEntities(res.rows.item(i).chat_message),
                                senderName: 'Поддержка',
                                time: new Date(mestime * 1000).toLocaleTimeString()
                            });
                        }
                        if (res.rows.item(i).chat_when > _this.lastchat) {
                            _this.lastchat = res.rows.item(i).chat_when;
                        }
                        if (i == res.rows.length - 1) {
                            setTimeout(function () {
                                _this.content.scrollToBottom(300);
                            }, 1000);
                            _this.checkMessages();
                        }
                    }
                }
            }
            else {
                _this.checkMessages();
            }
        })
            .catch(function (e) { console.log(e); });
    };
    SupportChatPage.prototype.checkMessages = function () {
        var checkmesstr = JSON.stringify({
            device_id: this.backendProv.uuid,
            inst_id: this.backendProv.institution,
            getsend: 'get',
            lastchat: this.lastchat,
            newusr: 'chat'
        });
        this.writeMessages(checkmesstr);
    };
    SupportChatPage.prototype.writeMessages = function (messtr) {
        var _this = this;
        this.backendProv.httpRequest(messtr)
            .subscribe(function (res) {
            var data = res;
            // console.log("WRITE ====================================> "+JSON.stringify(data));
            if (data[0].chatOK == '1') {
                _this.chatArrFunc(0, data[0].chatArr);
            }
            else if (data[0].chatOK > '1') {
                var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                _this.backendProv.database.executeSql(chatIns, [data[0].chatID, _this.backendProv.myid, '1', 'support', _this.message, '0', _this.backendProv.institution, '0', data[0].chatOK, '0']).then(function () {
                    var mestime = _this.backendProv.timezoneAdd(data[0].chatOK);
                    _this.messages.push({
                        img: _this.mypic,
                        position: 'right',
                        content: _this.message,
                        senderName: 'Я',
                        time: new Date(mestime * 1000).toLocaleTimeString()
                    });
                    _this.message = '';
                    var currentHeight = _this.scrollContentElelment.style.marginBottom.replace('px', '');
                    var newHeight = currentHeight - _this.textareaHeight + _this.initialTextAreaHeight;
                    var top = newHeight + 'px';
                    _this.renderer.setElementStyle(_this.scrollContentElelment, 'marginBottom', top);
                    _this.textareaHeight = _this.initialTextAreaHeight;
                    setTimeout(function () {
                        _this.content.scrollToBottom(300);
                    }, 100);
                })
                    .catch(function (e) { return console.log(e); });
            }
            setTimeout(function () {
                _this.checkMessages();
            }, 3000);
        }, function (e) {
            console.log(e);
            setTimeout(function () {
                _this.checkMessages();
            }, 3000);
        });
    };
    SupportChatPage.prototype.chatArrFunc = function (id, chatArr) {
        var _this = this;
        var chat_id = chatArr[id]['chat_id'];
        var chat_from = chatArr[id]['chat_from'];
        var chat_to = chatArr[id]['chat_to'];
        var chat_name = chatArr[id]['chat_name'];
        var chat_message = chatArr[id]['chat_message'];
        var chat_read = chatArr[id]['chat_read'];
        var chat_institution = chatArr[id]['chat_institution'];
        var chat_answered = chatArr[id]['chat_answered'];
        var chat_when = chatArr[id]['chat_when'];
        var chat_del = chatArr[id]['chat_del'];
        this.lastchat = chat_when;
        var queryChat = "SELECT * FROM chat WHERE chat_id = ? AND chat_del='0'";
        this.backendProv.database.executeSql(queryChat, [chat_id]).then(function (suc) {
            if (suc.rows.length > 0) {
                var chatUpd = "UPDATE chat SET chat_read=?, chat_answered=?, chat_when=?, chat_del=? WHERE chat_id=?";
                _this.backendProv.database.executeSql(chatUpd, [chat_read, chat_answered, chat_when, chat_del, chat_id])
                    .then(function () {
                    id++;
                    if (id < chatArr.length) {
                        _this.chatArrFunc(id, chatArr);
                    }
                })
                    .catch(function (e) { return console.log(e); });
            }
            else {
                var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                _this.backendProv.database.executeSql(chatIns, [chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del]).then(function () {
                    var mestime = _this.backendProv.timezoneAdd(chat_when);
                    if (chat_from == _this.backendProv.myid) {
                        _this.messages.push({
                            img: _this.mypic,
                            position: 'right',
                            content: _this.backendProv.decodeEntities(chat_message),
                            senderName: 'Я',
                            time: new Date(mestime * 1000).toLocaleTimeString()
                        });
                    }
                    else if (chat_from != _this.backendProv.myid) {
                        _this.messages.push({
                            img: 'assets/img/face.jpg',
                            position: 'left',
                            content: _this.backendProv.decodeEntities(chat_message),
                            senderName: 'Поддержка',
                            time: new Date(mestime * 1000).toLocaleTimeString()
                        });
                    }
                    setTimeout(function () {
                        _this.content.scrollToBottom(300);
                    }, 100);
                    id++;
                    if (id < chatArr.length) {
                        _this.chatArrFunc(id, chatArr);
                    }
                })
                    .catch(function (e) { return console.log(e); });
            }
        })
            .catch(function (e) { return console.log(e); });
    };
    return SupportChatPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* Content */])
], SupportChatPage.prototype, "content", void 0);
SupportChatPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-support-chat',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/support-chat/support-chat.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-title>Техподдержка</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding fixed scrollDownOnLoad="true" (tap)="contentMouseDown($event)" ionScrollEnd="hideKeybaord()">\n\n  <ion-list no-lines tappable>\n    <ion-item *ngFor="let msg of messages" >\n      <div class="chatBubble">\n        <img class="profile-pic {{msg.position}}" src="{{msg.img}}">\n        <div class="chat-bubble {{msg.position}}">\n          <div class="message">{{msg.content}}</div>\n          <div class="message-detail">\n              <span style="font-weight:bold;">{{msg.senderName}} </span>,\n              <span>{{msg.time}}</span>\n          </div>\n        </div>\n      </div>\n    </ion-item>\n  </ion-list>\n\n  <!-- <div item-content style="width:100%;">\n\n    <ion-textarea spellcheck="on" autocomplete="true" placeholder="Ваше сообщение.." lineHeight="lineHeight" [(ngModel)]="message"></ion-textarea>\n    <ion-buttons right>\n      <button style="min-width:45px;" (click)="sendMessage()">\n        <ion-icon name="send"></ion-icon>\n      </button>\n    </ion-buttons>\n\n  </div> -->\n\n</ion-content>\n\n<ion-footer (touchstart)="footerTouchStart($event)">\n  <ion-toolbar>\n\n    <ion-buttons left>\n      <button class="add-image-button" item-right (touchstart)="touchImageButton($event)">\n        <ion-icon class="add-image-icon" name="images"></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <textarea spellcheck="true" autoComplete="on" autocorrect="on" (ngModelChange)="textAreaChange()" rows="1" fz-elastic class="chat-input" [(ngModel)]="message" placeholder="Ваше сообщение.." (keyup.enter)="sendMessage()"></textarea>\n\n    <ion-buttons right>\n      <button [disabled]="!message" class="send-button" item-right id="send-button" (touchstart)="touchSendButton($event)">\n        <ion-icon class="send-icon" name="send"></ion-icon>\n      </button>\n    </ion-buttons>\n\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/support-chat/support-chat.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["s" /* Platform */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_keyboard__["a" /* Keyboard */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], SupportChatPage);

//# sourceMappingURL=support-chat.js.map

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_intl__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_intl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_intl_locale_data_jsonp_en__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_intl_locale_data_jsonp_en___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_intl_locale_data_jsonp_en__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_dynamic__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_module__ = __webpack_require__(351);





Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["enableProdMode"])();
Object(__WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_4__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 345:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic2_super_tabs__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_qrcode2__ = __webpack_require__(657);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_barcode_scanner__ = __webpack_require__(677);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__agm_core__ = __webpack_require__(678);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser_animations__ = __webpack_require__(683);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_keyboard__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(686);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_device__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_sqlite__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_network__ = __webpack_require__(690);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_in_app_browser__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_http__ = __webpack_require__(691);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_file__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_push__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_social_sharing__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21_angular2_text_mask__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21_angular2_text_mask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_21_angular2_text_mask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_camera__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_image_picker__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_about_about__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_contact_contact__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_home_home__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_spa_spa__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_ritual_ritual__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_express_express__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_history_history__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_expresschoice_expresschoice__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_qrsumm_qrsumm__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_qrcode_qrcode__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_review_review__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_news_news__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_newsdetail_newsdetail__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_terms_terms__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_share_share__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_sharedetails_sharedetails__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_gifts_gifts__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_giftsdetail_giftsdetail__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_profile_profile__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_profiledetails_profiledetails__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_social_social__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_service_rituals_service_rituals__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_service_programms_service_programms__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_service_giftcards_service_giftcards__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_service_details_service_details__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_support_faq_support_faq__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_support_chat_support_chat__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_groups_groups__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_ritualchoice_ritualchoice__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_intro_intro__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_service_giftcards_detail_service_giftcards_detail__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_ordered_ordered__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pages_ordered2_ordered2__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_surveys_surveys__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__pages_orders_orders__ = __webpack_require__(694);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__pages_tabs_tabs__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__pages_modalrituals_modalrituals__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_modalmasters_modalmasters__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__providers_backend_backend__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__ionic_native_status_bar__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__ionic_native_splash_screen__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65_ionic2_rating__ = __webpack_require__(695);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__pipes_timepoints_timepoints__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__angular_common__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__components_fa_icon_fa_icon_component__ = __webpack_require__(697);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





































































var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_24__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_spa_spa__["a" /* SpaPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_ritual_ritual__["a" /* RitualPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_express_express__["a" /* ExpressPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_history_history__["a" /* HistoryPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_expresschoice_expresschoice__["a" /* ExpresschoicePage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_qrsumm_qrsumm__["a" /* QrsummPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_qrcode_qrcode__["a" /* QrcodePage */],
            __WEBPACK_IMPORTED_MODULE_59__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_review_review__["a" /* ReviewPage */],
            __WEBPACK_IMPORTED_MODULE_35__pages_news_news__["a" /* NewsPage */],
            __WEBPACK_IMPORTED_MODULE_36__pages_newsdetail_newsdetail__["a" /* NewsdetailPage */],
            __WEBPACK_IMPORTED_MODULE_37__pages_terms_terms__["a" /* TermsPage */],
            __WEBPACK_IMPORTED_MODULE_38__pages_share_share__["a" /* SharePage */],
            __WEBPACK_IMPORTED_MODULE_39__pages_sharedetails_sharedetails__["a" /* SharedetailsPage */],
            __WEBPACK_IMPORTED_MODULE_40__pages_gifts_gifts__["a" /* GiftsPage */],
            __WEBPACK_IMPORTED_MODULE_41__pages_giftsdetail_giftsdetail__["a" /* GiftsdetailPage */],
            __WEBPACK_IMPORTED_MODULE_42__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_43__pages_profiledetails_profiledetails__["a" /* ProfiledetailsPage */],
            __WEBPACK_IMPORTED_MODULE_44__pages_social_social__["a" /* SocialPage */],
            __WEBPACK_IMPORTED_MODULE_45__pages_service_rituals_service_rituals__["a" /* ServiceRitualsPage */],
            __WEBPACK_IMPORTED_MODULE_46__pages_service_programms_service_programms__["a" /* ServiceProgrammsPage */],
            __WEBPACK_IMPORTED_MODULE_47__pages_service_giftcards_service_giftcards__["a" /* ServiceGiftcardsPage */],
            __WEBPACK_IMPORTED_MODULE_48__pages_service_details_service_details__["a" /* ServiceDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_49__pages_support_faq_support_faq__["a" /* SupportFaqPage */],
            __WEBPACK_IMPORTED_MODULE_50__pages_support_chat_support_chat__["a" /* SupportChatPage */],
            __WEBPACK_IMPORTED_MODULE_51__pages_groups_groups__["a" /* GroupsPage */],
            __WEBPACK_IMPORTED_MODULE_52__pages_ritualchoice_ritualchoice__["a" /* RitualchoicePage */],
            __WEBPACK_IMPORTED_MODULE_53__pages_intro_intro__["a" /* IntroPage */],
            __WEBPACK_IMPORTED_MODULE_54__pages_service_giftcards_detail_service_giftcards_detail__["a" /* ServiceGiftcardsDetailPage */],
            __WEBPACK_IMPORTED_MODULE_55__pages_ordered_ordered__["a" /* OrderedPage */],
            __WEBPACK_IMPORTED_MODULE_56__pages_ordered2_ordered2__["a" /* Ordered2Page */],
            __WEBPACK_IMPORTED_MODULE_57__pages_surveys_surveys__["a" /* SurveysPage */],
            __WEBPACK_IMPORTED_MODULE_58__pages_orders_orders__["a" /* OrdersPage */],
            __WEBPACK_IMPORTED_MODULE_60__pages_modalrituals_modalrituals__["a" /* ModalritualsPage */],
            __WEBPACK_IMPORTED_MODULE_61__pages_modalmasters_modalmasters__["a" /* ModalmastersPage */],
            __WEBPACK_IMPORTED_MODULE_66__pipes_timepoints_timepoints__["a" /* TimepointsPipe */],
            __WEBPACK_IMPORTED_MODULE_68__components_fa_icon_fa_icon_component__["a" /* FaIconComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {
                'backButtonText': '',
                'backButtonIcon': 'arrow-back',
                ios: {
                    scrollAssist: false,
                    autoFocusAssist: false,
                    inputBlurring: false
                },
                monthNames: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
                monthShortNames: ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
                dayNames: ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'],
                dayShortNames: ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'],
            }),
            __WEBPACK_IMPORTED_MODULE_5_ionic2_super_tabs__["b" /* SuperTabsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* JsonpModule */],
            __WEBPACK_IMPORTED_MODULE_6_ngx_qrcode2__["a" /* NgxQRCodeModule */],
            __WEBPACK_IMPORTED_MODULE_65_ionic2_rating__["a" /* Ionic2RatingModule */],
            __WEBPACK_IMPORTED_MODULE_8__agm_core__["a" /* AgmCoreModule */].forRoot({
                apiKey: 'AIzaSyCXpNcMG9kjvXRsU6C9-VDop7F6icGVmjM'
            }),
            __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_21_angular2_text_mask__["TextMaskModule"]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_24__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_spa_spa__["a" /* SpaPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_ritual_ritual__["a" /* RitualPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_express_express__["a" /* ExpressPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_history_history__["a" /* HistoryPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_expresschoice_expresschoice__["a" /* ExpresschoicePage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_qrsumm_qrsumm__["a" /* QrsummPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_qrcode_qrcode__["a" /* QrcodePage */],
            __WEBPACK_IMPORTED_MODULE_59__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_review_review__["a" /* ReviewPage */],
            __WEBPACK_IMPORTED_MODULE_35__pages_news_news__["a" /* NewsPage */],
            __WEBPACK_IMPORTED_MODULE_36__pages_newsdetail_newsdetail__["a" /* NewsdetailPage */],
            __WEBPACK_IMPORTED_MODULE_37__pages_terms_terms__["a" /* TermsPage */],
            __WEBPACK_IMPORTED_MODULE_38__pages_share_share__["a" /* SharePage */],
            __WEBPACK_IMPORTED_MODULE_39__pages_sharedetails_sharedetails__["a" /* SharedetailsPage */],
            __WEBPACK_IMPORTED_MODULE_40__pages_gifts_gifts__["a" /* GiftsPage */],
            __WEBPACK_IMPORTED_MODULE_41__pages_giftsdetail_giftsdetail__["a" /* GiftsdetailPage */],
            __WEBPACK_IMPORTED_MODULE_42__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_43__pages_profiledetails_profiledetails__["a" /* ProfiledetailsPage */],
            __WEBPACK_IMPORTED_MODULE_44__pages_social_social__["a" /* SocialPage */],
            __WEBPACK_IMPORTED_MODULE_45__pages_service_rituals_service_rituals__["a" /* ServiceRitualsPage */],
            __WEBPACK_IMPORTED_MODULE_46__pages_service_programms_service_programms__["a" /* ServiceProgrammsPage */],
            __WEBPACK_IMPORTED_MODULE_47__pages_service_giftcards_service_giftcards__["a" /* ServiceGiftcardsPage */],
            __WEBPACK_IMPORTED_MODULE_48__pages_service_details_service_details__["a" /* ServiceDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_49__pages_support_faq_support_faq__["a" /* SupportFaqPage */],
            __WEBPACK_IMPORTED_MODULE_50__pages_support_chat_support_chat__["a" /* SupportChatPage */],
            __WEBPACK_IMPORTED_MODULE_51__pages_groups_groups__["a" /* GroupsPage */],
            __WEBPACK_IMPORTED_MODULE_52__pages_ritualchoice_ritualchoice__["a" /* RitualchoicePage */],
            __WEBPACK_IMPORTED_MODULE_53__pages_intro_intro__["a" /* IntroPage */],
            __WEBPACK_IMPORTED_MODULE_54__pages_service_giftcards_detail_service_giftcards_detail__["a" /* ServiceGiftcardsDetailPage */],
            __WEBPACK_IMPORTED_MODULE_55__pages_ordered_ordered__["a" /* OrderedPage */],
            __WEBPACK_IMPORTED_MODULE_56__pages_ordered2_ordered2__["a" /* Ordered2Page */],
            __WEBPACK_IMPORTED_MODULE_57__pages_surveys_surveys__["a" /* SurveysPage */],
            __WEBPACK_IMPORTED_MODULE_58__pages_orders_orders__["a" /* OrdersPage */],
            __WEBPACK_IMPORTED_MODULE_60__pages_modalrituals_modalrituals__["a" /* ModalritualsPage */],
            __WEBPACK_IMPORTED_MODULE_61__pages_modalmasters_modalmasters__["a" /* ModalmastersPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_63__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_64__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* IonicErrorHandler */] },
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["LOCALE_ID"], useValue: 'ru-RU' },
            __WEBPACK_IMPORTED_MODULE_62__providers_backend_backend__["a" /* BackendProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic2_super_tabs__["a" /* SuperTabsController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_keyboard__["a" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_15__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_16__ionic_native_http__["a" /* HTTP */],
            __WEBPACK_IMPORTED_MODULE_17__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_19__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_66__pipes_timepoints_timepoints__["a" /* TimepointsPipe */],
            __WEBPACK_IMPORTED_MODULE_67__angular_common__["c" /* DatePipe */],
            __WEBPACK_IMPORTED_MODULE_20__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_22__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_23__ionic_native_image_picker__["a" /* ImagePicker */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_news_news__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_about_about__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_profile_profile__ = __webpack_require__(325);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_support_faq_support_faq__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_intro_intro__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_surveys_surveys__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_push__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var MyApp = (function () {
    function MyApp(app, platform, statusBar, menuCtrl, splashScreen, backendProv, push) {
        var _this = this;
        this.app = app;
        this.menuCtrl = menuCtrl;
        this.backendProv = backendProv;
        this.push = push;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_9__pages_intro_intro__["a" /* IntroPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            // CHECK AND REGISTER FOR PUSH-NOTIFICATION
            _this.regPush();
        });
    }
    MyApp.prototype.goIndex = function () {
        this.nav = this.app.getRootNavById('n4');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_intro_intro__["a" /* IntroPage */]);
    };
    MyApp.prototype.goSurveys = function () {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_10__pages_surveys_surveys__["a" /* SurveysPage */];
    };
    MyApp.prototype.goSupport = function () {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_8__pages_support_faq_support_faq__["a" /* SupportFaqPage */];
    };
    MyApp.prototype.goProfile = function () {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_profile_profile__["a" /* ProfilePage */];
    };
    MyApp.prototype.goAbout = function () {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */];
    };
    MyApp.prototype.goHome = function () {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
    };
    MyApp.prototype.goNews = function () {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_news_news__["a" /* NewsPage */];
    };
    MyApp.prototype.regPush = function () {
        var _this = this;
        if (this.backendProv.uuid) {
            // to check if we have permission
            this.push.hasPermission()
                .then(function (res) {
                if (res.isEnabled) {
                    // alert('We have permission to send push notifications');
                }
                else {
                    // alert('We do not have permission to send push notifications');
                }
            });
            var push_options = {
                android: {
                    sound: true
                },
                ios: {
                    clearBadge: true,
                    alert: true,
                    badge: true,
                    sound: true
                },
                windows: {},
                browser: {}
            };
            var pushObject = this.push.init(push_options);
            pushObject.on('notification').subscribe(function (notification) {
                // alert('Received a notification: ' + notification)
            });
            pushObject.on('registration').subscribe(function (registration) {
                var gcmstr = {
                    device: _this.backendProv.model,
                    device_id: _this.backendProv.uuid,
                    device_serial: _this.backendProv.serial,
                    device_version: _this.backendProv.version,
                    device_os: _this.backendProv.platform,
                    inst_id: _this.backendProv.institution,
                    gcm: registration.registrationId,
                    newusr: 'gcmreg'
                };
                // alert('PUSH START: '+JSON.stringify(gcmstr)+' REGDATA: '+JSON.stringify(registration));
                _this.backendProv.httpRequest(JSON.stringify(gcmstr))
                    .subscribe(function (pushsuc) {
                    // alert('PUSH SUCCESS: '+JSON.stringify(pushsuc));
                }, function (error) {
                    // alert('PUSH ERROR: '+JSON.stringify(error));
                });
            });
            pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin', error); });
        }
    };
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/app/app.html"*/'<ion-menu [content]="mycontent">\n    <ion-content>\n        <ion-list>\n            <ion-item menuClose>\n                <ion-avatar item-start>\n                        <img src="assets/img/logo.png">\n                </ion-avatar>\n                <h2>Белый Лотос</h2>\n                <h3>г.Минск</h3>\n            </ion-item>\n            <button ion-item menuClose (click)="goIndex()">\n                Главная\n            </button>\n            <button ion-item menuClose (click)="goHome()">\n                Карта клиента\n            </button>\n            <button ion-item menuClose (click)="goNews()">\n                Новости\n            </button>\n            <button ion-item menuClose (click)="goProfile()">\n                Личный кабинет\n            </button>\n            <button ion-item menuClose (click)="goSurveys()">\n                Опросы\n            </button>\n            <button ion-item menuClose (click)="goSupport()">\n                Техподдержка\n            </button>\n            <button ion-item menuClose (click)="goAbout()">\n                О нас\n            </button>\n        </ion-list>\n    </ion-content>\n</ion-menu>\n\n<ion-nav #mycontent [root]="rootPage"></ion-nav>\n\n<!-- <ion-nav [root]="rootPage"></ion-nav> -->'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_12__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_11__ionic_native_push__["a" /* Push */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimepointsPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TimepointsPipe = (function () {
    function TimepointsPipe() {
    }
    TimepointsPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return value.toString().slice(0, 2) + ':' + value.toString().slice(2, 4);
    };
    return TimepointsPipe;
}());
TimepointsPipe = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'timepoints',
    })
], TimepointsPipe);

//# sourceMappingURL=timepoints.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfiledetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_keyboard__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_text_mask_addons_dist_emailMask__ = __webpack_require__(656);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_text_mask_addons_dist_emailMask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_text_mask_addons_dist_emailMask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_image_picker__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ProfiledetailsPage = (function () {
    function ProfiledetailsPage(platform, navCtrl, navParams, backendProv, keyboard, alertCtrl, camera, file, transfer, loadingCtrl, imagePicker, actionSheetCtrl) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.backendProv = backendProv;
        this.keyboard = keyboard;
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.file = file;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.imagePicker = imagePicker;
        this.actionSheetCtrl = actionSheetCtrl;
        this.profiles = [];
        this.piclink = 0;
        this.maxdate = new Date();
        this.emailMask = __WEBPACK_IMPORTED_MODULE_3_text_mask_addons_dist_emailMask___default.a;
        this.smsOrdered = 0;
        this.smsConfirmed = 0;
        this.confcode = '';
        this.notSaved = 0;
        this.profiles.push(navParams.get('profile'));
        this.piclink = backendProv.piclink;
        this.maxdate = this.maxdate.getFullYear() - 15 + '-' + this.maxdate.getMonth() + '-' + this.maxdate.getDate();
        this.masks = {
            phoneNumber: ['+', /[1-9]/, /\d/, /\d/, '(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/],
            phoneEmail: ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
            cardNumber: [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
            cardExpiry: [/[0-1]/, /\d/, '/', /[1-2]/, /\d/],
            orderCode: [/[a-zA-z]/, ':', /\d/, /\d/, /\d/, /\d/]
        };
        this.smsOrdered = this.profiles[0].user_conf_req;
        this.smsConfirmed = this.profiles[0].user_mob_confirm;
    }
    ProfiledetailsPage.prototype.onKeyBio = function (event) {
        this.profiles[0].user_info = event.target.value;
        this.notSaved = 0;
    };
    ProfiledetailsPage.prototype.onKeyConf = function (event) {
        this.confcode = event.target.value;
        this.notSaved = 0;
    };
    ProfiledetailsPage.prototype.onKeyCity = function (event) {
        this.profiles[0].user_city = event.target.value;
        this.notSaved = 0;
    };
    ProfiledetailsPage.prototype.onKeyEmail = function (event) {
        this.profiles[0].user_email = event.target.value;
        this.notSaved = 0;
    };
    ProfiledetailsPage.prototype.onKeyName = function (event) {
        this.profiles[0].user_name = event.target.value;
        this.notSaved = 0;
    };
    ProfiledetailsPage.prototype.onKeyFam = function (event) {
        this.profiles[0].user_surname = event.target.value;
        this.notSaved = 0;
    };
    ProfiledetailsPage.prototype.onKeyMob = function (event) {
        this.profiles[0].user_mob = event.target.value.replace(/\D+/g, '');
        this.notSaved = 0;
    };
    ProfiledetailsPage.prototype.saveProfile = function () {
        var _this = this;
        for (var key in this.profiles[0]) {
            if (this.profiles[0].hasOwnProperty(key)) {
                if (this.profiles[0][key] == null || this.profiles[0][key] == '') {
                    this.profiles[0][key] = 0;
                }
            }
        }
        var updstr = {
            device: this.backendProv.model,
            device_id: this.backendProv.uuid,
            device_version: this.backendProv.version,
            device_os: this.backendProv.platform,
            user_name: this.profiles[0].user_name,
            user_surname: this.profiles[0].user_surname,
            user_middlename: this.profiles[0].user_middlename,
            user_email: this.profiles[0].user_email,
            user_tel: this.profiles[0].user_tel,
            user_mob: this.profiles[0].user_mob,
            user_info: this.profiles[0].user_info,
            user_pic: this.profiles[0].user_pic,
            user_gender: this.profiles[0].user_gender,
            user_birthday: this.profiles[0].user_birthday,
            user_country: 0,
            user_region: 0,
            user_city: this.profiles[0].user_city,
            user_adress: 0,
            user_install_where: this.profiles[0].user_install_where,
            inst_id: this.backendProv.institution,
            newusr: 'upd'
        };
        this.backendProv.httpRequest(JSON.stringify(updstr))
            .subscribe(function (updsuc) {
            _this.backendProv.updateProfile(_this.profiles[0]);
            var alert = _this.alertCtrl.create({
                title: 'Внимание',
                subTitle: 'Данные сохраннены!',
                buttons: ['Закрыть']
            });
            alert.present();
            _this.notSaved = 0;
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    ProfiledetailsPage.prototype.confirmSms = function () {
        var _this = this;
        var smsstr = {
            device_id: this.backendProv.uuid,
            inst_id: this.backendProv.institution,
            sms: this.confcode,
            newusr: 'sms'
        };
        this.backendProv.httpRequest(JSON.stringify(smsstr))
            .subscribe(function (smssuc) {
            var data = smssuc;
            if (data[0].smsOK == '5') {
                _this.smsConfirmed = 1;
                _this.backendProv.smsConfirmOk(data);
                var alert_1 = _this.alertCtrl.create({
                    title: 'Спасибо',
                    subTitle: 'Номер подтвержден!',
                    buttons: ['Закрыть']
                });
                alert_1.present();
            }
            else if (data[0].smsOK == '6') {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Внимание',
                    subTitle: 'Неверно заданный код!',
                    buttons: ['Закрыть']
                });
                alert_2.present();
            }
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    ProfiledetailsPage.prototype.requestSms = function (val) {
        var _this = this;
        this.backendProv.updateProfile(val);
        for (var key in this.profiles[0]) {
            if (this.profiles[0].hasOwnProperty(key)) {
                if (this.profiles[0][key] == null || this.profiles[0][key] == '') {
                    this.profiles[0][key] = 0;
                }
            }
        }
        var updstr = {
            device: this.backendProv.model,
            device_id: this.backendProv.uuid,
            device_version: this.backendProv.version,
            device_os: this.backendProv.platform,
            user_name: this.profiles[0].user_name,
            user_surname: this.profiles[0].user_surname,
            user_middlename: this.profiles[0].user_middlename,
            user_email: this.profiles[0].user_email,
            user_tel: this.profiles[0].user_tel,
            user_mob: this.profiles[0].user_mob,
            user_gender: this.profiles[0].user_gender,
            user_birthday: this.profiles[0].user_birthday,
            user_country: 0,
            user_region: 0,
            user_city: 0,
            user_adress: 0,
            user_install_where: this.profiles[0].user_install_where,
            inst_id: this.backendProv.institution,
            newusr: 'upd'
        };
        this.backendProv.httpRequest(JSON.stringify(updstr))
            .subscribe(function (updsuc) {
            // let unmaskedData = {
            //     phoneNumber: this.phoneNumber.replace(/\D+/g, ''),
            //     cardNumber: this.cardNumber.replace(/\D+/g, ''),
            //     cardExpiry: this.cardExpiry.replace(/\D+/g, ''),
            //     orderCode: this.orderCode.replace(/[^a-zA-Z0-9 -]/g, '')
            // };
            // console.log(unmaskedData);
            var smsstr = {
                device_id: _this.backendProv.uuid,
                inst_id: _this.backendProv.institution,
                sms: '0',
                newusr: 'sms'
            };
            _this.backendProv.httpRequest(JSON.stringify(smsstr))
                .subscribe(function (ressms) {
                var data = ressms;
                if (data[0].smsOK == '2') {
                    var alert_3 = _this.alertCtrl.create({
                        title: 'Внимание',
                        subTitle: 'Повторный запрос только через 5 минут!',
                        buttons: ['Закрыть']
                    });
                    alert_3.present();
                }
                else if (data[0].smsOK == '3') {
                    _this.smsOrdered = 1;
                    _this.backendProv.smsRequestOk(data[0].when);
                }
                else if (data[0].smsOK == '4') {
                    var alert_4 = _this.alertCtrl.create({
                        title: 'Внимание',
                        subTitle: 'Укажите Ваш номер телефона!',
                        buttons: ['Закрыть']
                    });
                    alert_4.present();
                }
            }, function (error) { console.log(error); });
        }, function (error) { console.log(error); });
    };
    ProfiledetailsPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.piclink + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    ProfiledetailsPage.prototype.checkZero = function (val) {
        if (val) {
            if (val == '0') {
                return '';
            }
            else {
                return val;
            }
        }
        else {
            return '';
        }
    };
    ProfiledetailsPage.prototype.checkLegth = function (val) {
        return val.length;
    };
    ProfiledetailsPage.prototype.makePic = function () {
        var _this = this;
        var optionsselfie = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            targetWidth: 700,
            targetHeight: 700,
            correctOrientation: true
        };
        this.camera.getPicture(optionsselfie).then(function (imageURI) {
            var when = Math.floor(new Date().getTime() / 1000);
            var randpicname = _this.backendProv.myid + '_' + when;
            var namefilesplit = imageURI.split('/');
            var namefile = namefilesplit[namefilesplit.length - 1];
            var oldurlsplit = imageURI.split(namefile);
            var oldurl = oldurlsplit[0];
            var topath = _this.file.dataDirectory + _this.backendProv.instdir + '/' + randpicname + '.jpg';
            var tourl = _this.file.dataDirectory + _this.backendProv.instdir + '/';
            _this.file.moveFile(oldurl, namefile, tourl, randpicname + '.jpg')
                .then(function (res) {
                _this.savePic(topath, randpicname);
            })
                .catch(function (e) { return console.log(e); });
        }, function (err) {
            // Handle error
        });
    };
    ProfiledetailsPage.prototype.selPic = function () {
        var _this = this;
        var optionsImg = {
            maximumImagesCount: 1,
            width: 700,
            height: 700,
            quality: 100
        };
        this.imagePicker.getPictures(optionsImg)
            .then(function (results) {
            if (results.length > 0) {
                var _loop_1 = function (i) {
                    // DATE - TIME IN SECONDS
                    var when = Math.floor(new Date().getTime() / 1000);
                    var randpicname = _this.backendProv.myid + '_' + when;
                    var namefilesplit = results[i].split('/');
                    var namefile = namefilesplit[namefilesplit.length - 1];
                    var oldurlsplit = results[i].split(namefile);
                    var oldurl = oldurlsplit[0];
                    var topath = _this.file.dataDirectory + _this.backendProv.instdir + '/' + randpicname + '.jpg';
                    var tourl = _this.file.dataDirectory + _this.backendProv.instdir + '/';
                    _this.file.copyFile(oldurl, namefile, tourl, randpicname + '.jpg').then(function (success) {
                        _this.savePic(topath, randpicname);
                    }, function (er) { });
                };
                for (var i = 0; i < results.length; i++) {
                    _loop_1(i);
                }
            }
        }, function (error) {
        });
    };
    ProfiledetailsPage.prototype.savePic = function (imgpath, randpicname) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        // UPLOADING SOUND
        var options = {
            fileKey: "file",
            fileName: randpicname + '.jpg',
            chunkedMode: false,
            mimeType: 'image/jpeg'
        };
        var fileTransfer = this.transfer.create();
        fileTransfer.upload(imgpath, "http://www.olegtronics.com/admin/coms/upload.php?usrupl=1&preview=1&user_id=" + this.backendProv.myid, options)
            .then(function (result) {
            var srtringify = JSON.stringify(result.response);
            var parsingres = JSON.parse(JSON.parse(srtringify));
            var messent = parsingres.user_upd;
            if (messent > 0) {
                _this.profiles[0].user_pic = randpicname + '.jpg';
                _this.file.removeFile(_this.file.dataDirectory + _this.backendProv.instdir + '/', randpicname + '.jpg');
            }
            loading.dismiss();
        }, function (err) {
            loading.dismiss();
        });
    };
    // document.getElementById('loadprog').innerHTML = Math.round((progress.loaded / progress.total) * 100) + ' %';
    ProfiledetailsPage.prototype.chImg = function () {
        var _this = this;
        var entryRequestAS = this.actionSheetCtrl.create({
            title: 'Изменить Аватар',
            subTitle: 'Выбрать из галереи или снять новую фотографию?',
            buttons: [
                {
                    text: 'Выбрать из галереи',
                    handler: function () {
                        _this.selPic();
                    }
                },
                {
                    text: 'Снять новое фото',
                    handler: function () {
                        _this.makePic();
                    }
                },
                {
                    text: 'Отменить',
                    role: 'cancel',
                    handler: function () {
                        // console.log('Cancel clicked');
                    }
                }
            ]
        });
        entryRequestAS.present();
    };
    ProfiledetailsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.keyboard.disableScroll(true);
        });
    };
    return ProfiledetailsPage;
}());
ProfiledetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-profiledetails',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/profiledetails/profiledetails.html"*/'<ion-header>\n\n  <ion-navbar color="viol">\n    <ion-title>Редактирование</ion-title>\n    <ion-buttons right>\n      <button ion-button icon-begin color="light" (click)="saveProfile()">\n        <span style="font-size:24px;" class="fa fa-floppy-o"></span>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding>\n  <div *ngIf="profiles?.length>0">\n    <ion-grid *ngFor="let profile of profiles" class="form-style-7">\n\n      <ion-row style="border:none;background:none;" justify-content-around>\n        <ion-col col-6 padding-bottom (click)="chImg()">\n          <img src="{{ checkPic(profile.user_pic) }}" />\n          <span style="display:inline-block;bottom:24px;padding:5px;border-radius:50%;background-color:#fff;border:2px solid #000;color:#000;position:absolute;font-size:18px;" class="fa fa-camera"></span>\n        </ion-col>\n      </ion-row>\n\n      <ul>\n        <li>\n            <label for="name">Имя</label>\n            <input type="text" name="name" maxlength="100" value="{{ checkZero(profile.user_name) }}" (keyup)="onKeyName($event)">\n            <span class="info">Внесите Ваше Имя</span>\n        </li>\n        <li>\n            <label for="surname">Фамилия</label>\n            <input type="text" name="surname" maxlength="100" value="{{ checkZero(profile.user_surname) }}" (keyup)="onKeyFam($event)">\n            <span class="info">Внесите Вашу Фамилию</span>\n        </li>\n        <li style="position:relative;display: block;padding: 9px;border:1px solid #DDDDDD;margin-bottom: 30px;border-radius: 3px;" *ngIf="smsConfirmed == \'1\'">\n            <label for="mobile">Телефон</label>\n            <input style="background-color:#fff;" type="text" name="mobile" maxlength="100" value="{{ checkZero(profile.user_mob) }}" disabled>\n            <span class="info">Ваш Телефон</span>\n        </li>\n        <li style="position:relative;display: block;padding: 9px;border:1px solid #DDDDDD;margin-bottom: 30px;border-radius: 3px;" *ngIf="smsConfirmed != \'1\'">\n            <label for="mobileconf">Телефон</label>\n            <input type="tel" name="mobileconf" maxlength="100" placeholder="{{ checkZero(profile.user_mob) }}" pattern="\+\d{3} \(\d{2}\) \d{3}-\d{2}-\d{2}" [textMask]="{mask: masks.phoneNumber}" (keyup)="onKeyMob($event)" >\n            <button *ngIf="profile.user_mob.toString()?.length >= 9 && smsOrdered == \'0\'" ion-button small color="viol" style="position:absolute;top:30%;transform: translateY(-50%);right:10px;" (click)="requestSms(profile)">Подтвердить</button>\n            <button *ngIf="profile.user_mob.toString()?.length >= 9 && smsOrdered != \'0\'" ion-button small color="viol" style="position:absolute;top:30%;transform: translateY(-50%);right:10px;" (click)="requestSms(profile)">Прислать СМС</button>\n            <span class="info">Внесите Ваш номер телефона</span>\n        </li>\n        <li style="position:relative;display: block;padding: 9px;border:1px solid #DDDDDD;margin-bottom: 30px;border-radius: 3px;" *ngIf="smsConfirmed != \'1\' && smsOrdered != \'0\'">\n            <label for="confcode">Код подтверждения</label>\n            <input type="text" name="confcode" maxlength="100" (keyup)="onKeyConf($event)">\n            <button ion-button small color="secondary" (click)="confirmSms(profile)" style="position:absolute;top:30%;transform: translateY(-50%);right:10px;">Подтвердить</button>\n            <span class="info">Код придет по СМС в ближайшее время</span>\n        </li>\n      </ul>\n      <ion-row>\n        <ion-col no-margin no-padding>\n          <ion-label stacked>Дата рождения</ion-label>\n          <ion-datetime displayFormat="DD.MMM.YYYY" doneText="Принять" cancelText="Отмена" [(ngModel)]="profile.user_birthday"></ion-datetime>\n          <span class="info">Внесите Ваш день рождения</span>\n        </ion-col>\n      </ion-row>\n      <ul>\n        <li>\n            <label for="email">Email</label>\n            <input type="text" name="email" maxlength="100" placeholder="{{ checkZero(profile.user_email) }}" [textMask]="{mask: emailMask}" (keyup)="onKeyEmail($event)">\n            <span class="info">Внесите Вашу электронную почту</span>\n        </li>\n        <li>\n            <label for="city">Ваш город</label>\n            <input type="text" name="city" maxlength="100" value="{{ checkZero(profile.user_city) }}" (keyup)="onKeyCity($event)">\n            <span class="info">Выберите Ваш город</span>\n        </li>\n        <li>\n            <label for="bio">О Вас</label>\n            <textarea name="bio" value="{{ checkZero(profile.user_info) }}" (keyup)="onKeyBio($event)"></textarea>\n            <span class="info">Пару слов о Вас</span>\n        </li>\n        <li style="background:none;">\n          <button ion-button icon-begin color="viol" (click)="saveProfile()">\n            <span style="height:20px;width:20px;" class="fa fa-floppy-o"></span> Сохранить\n          </button>\n        </li>\n      </ul>\n\n    </ion-grid>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/profiledetails/profiledetails.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_8__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_keyboard__["a" /* Keyboard */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_image_picker__["a" /* ImagePicker */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */]])
], ProfiledetailsPage);

//# sourceMappingURL=profiledetails.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalmastersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_timepoints_timepoints__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ModalmastersPage = (function () {
    function ModalmastersPage(navCtrl, navParams, viewCtrl, backendProv, tpp, datePipe, actionSheetCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.backendProv = backendProv;
        this.tpp = tpp;
        this.datePipe = datePipe;
        this.actionSheetCtrl = actionSheetCtrl;
        this.loadingCtrl = loadingCtrl;
        this.masterwork = [];
        this.steaming = 'Без распаривания.';
        this.master = this.navParams.get('master');
        this.mastername = this.navParams.get('mastername');
        this.takedate = this.navParams.get('takedate');
        this.dateplace = this.navParams.get('dateplace');
        this.piclink = backendProv.piclink;
    }
    ModalmastersPage.prototype.ionViewDidLoad = function () {
        this.navCtrl.swipeBackEnabled = false;
        this.slides.loop = false;
        this.slides.pager = false;
        this.slides.effect = "slide";
        this.slides.initialSlide = 0;
    };
    ModalmastersPage.prototype.showSchedule = function () {
        this.slides.slideTo(0, 200);
    };
    ModalmastersPage.prototype.showInfo = function () {
        this.slides.slideTo(1, 200);
    };
    ModalmastersPage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.piclink + val;
        }
        else {
            return 'assets/img/girl3.png';
        }
    };
    ModalmastersPage.prototype.preEntry = function (selwork, unixtime) {
        var _this = this;
        if (unixtime && selwork) {
            var preRequestAS = this.actionSheetCtrl.create({
                title: 'Нужно распаривание?',
                cssClass: 'action-subtitle',
                buttons: [
                    {
                        text: 'Да',
                        handler: function () {
                            _this.steaming = 'Нужно распаривание!';
                            _this.entryRequest(selwork, unixtime);
                        }
                    },
                    {
                        text: 'Нет',
                        handler: function () {
                            _this.steaming = 'Без распаривания.';
                            _this.entryRequest(selwork, unixtime);
                        }
                    }
                ]
            });
            preRequestAS.present();
        }
    };
    ModalmastersPage.prototype.entryRequest = function (selwork, unixtime) {
        var _this = this;
        var entryRequestAS = this.actionSheetCtrl.create({
            title: this.mastername,
            subTitle: this.datePipe.transform(this.takedate, 'dd') + ' ' + this.datePipe.transform(this.takedate, 'EEE') + ' ' + this.tpp.transform(selwork),
            cssClass: 'action-subtitle',
            buttons: [
                {
                    text: 'Записаться',
                    handler: function () {
                        var data = { dateday: _this.datePipe.transform(_this.takedate, 'dd'), datemonth: _this.datePipe.transform(_this.takedate, 'MMMM'), datemonths: _this.datePipe.transform(_this.takedate, 'MM'), dateyears: _this.datePipe.transform(_this.takedate, 'y'), datetime: selwork, dateplace: _this.dateplace, unixtime: unixtime, comments: _this.steaming };
                        _this.viewCtrl.dismiss(data);
                    }
                },
                {
                    text: 'Отменить',
                    role: 'cancel',
                    handler: function () {
                        // console.log('Cancel clicked');
                    }
                }
            ]
        });
        entryRequestAS.present();
    };
    ModalmastersPage.prototype.goRitual = function () {
        var data = {
            site: 'ritual'
        };
        this.viewCtrl.dismiss(data);
    };
    ModalmastersPage.prototype.goSpa = function () {
        var data = {
            site: 'spa'
        };
        this.viewCtrl.dismiss(data);
    };
    ModalmastersPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    return ModalmastersPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('slidesOne'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], ModalmastersPage.prototype, "slides", void 0);
ModalmastersPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-modalmasters',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/modalmasters/modalmasters.html"*/'<ion-header no-border>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button (click)="close()" color="viol" [ngStyle]="{\'font-size\': \'200%\', \'text-shadow\': \'2px 2px 2px #fff\'}">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons right>\n      <button ion-button (click)="showSchedule()" color="viol" [ngStyle]="{\'font-size\': \'200%\', \'text-shadow\': \'2px 2px 2px #fff\'}">\n        <ion-icon name="ios-time-outline"></ion-icon>\n      </button>\n      <button ion-button (click)="showInfo()" color="viol" [ngStyle]="{\'font-size\': \'200%\', \'text-shadow\': \'2px 2px 2px #fff\'}">\n        <ion-icon name="ios-information-circle-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n  \n<ion-content class="violback">\n\n  <ion-grid no-padding>\n    <ion-row justify-content-around>\n      <ion-col [ngStyle]="{\'background-image\':\'url(\'+checkPic(master.user_pic)+\')\',\'background-size\':\'cover\',\'background-repeat\':\'no-repeat\',\'background-position\':\'center center\'}" col-12 class="card-background-page">\n        <ion-card class="backtransparent">\n          <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle">\n            <polygon points="20,10 0,30 40,30"/>\n          </svg>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row justify-content-around>\n      <ion-col col-12 class="card-background-page">\n        <ion-slides class="sliderone" #slidesOne>\n          <ion-slide>\n            <ion-card style="width:100%;padding-left:0px;padding-right:0px;margin:0;" class="backtransparent" no-padding>\n              <ion-card-header>\n                <h1 class="whitey">\n                    {{ master.user_name }}\n                </h1>\n                <h2 class="whitey">\n                  {{ master.user_prof }}\n                </h2>\n              </ion-card-header>\n              <ion-card-content>\n                <button style="border-radius:0px;margin:5px;min-width:20vw;" ion-button no-padding outline color="light" *ngFor="let work of master.user_work_time; let i = index" (click)="preEntry(work, master.unixtime[i])">{{ work | timepoints }}</button>\n              </ion-card-content>\n            </ion-card>\n          </ion-slide>\n          <ion-slide>\n            <ion-card class="backtransparent" no-padding>\n              <ion-card-header>\n                <h1 class="whitey">\n                    {{ master.user_name }}\n                </h1>\n              </ion-card-header>\n              <ion-card-content text-wrap>\n                <h2 class="whitey">\n                  {{ master.user_info }}\n                </h2>\n              </ion-card-content>\n            </ion-card>\n          </ion-slide>\n        </ion-slides>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n    \n</ion-content>\n  '/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/modalmasters/modalmasters.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ViewController */], __WEBPACK_IMPORTED_MODULE_4__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_3__pipes_timepoints_timepoints__["a" /* TimepointsPipe */], __WEBPACK_IMPORTED_MODULE_2__angular_common__["c" /* DatePipe */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], ModalmastersPage);

//# sourceMappingURL=modalmasters.js.map

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ritualchoice_ritualchoice__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profiledetails_profiledetails__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var IntroPage = (function () {
    function IntroPage(platf, navCtrl, navParams, menuCtrl, backendProv, alertCtrl, loadingCtrl) {
        this.platf = platf;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.backendProv = backendProv;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.points = '';
        this.profiles = [];
    }
    IntroPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.platf.ready().then(function () {
            _this.platf.registerBackButtonAction(function () {
                if (_this.navCtrl.canGoBack()) {
                    _this.navCtrl.pop();
                }
            });
        });
    };
    IntroPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.platf.ready().then(function () {
            var loading = _this.loadingCtrl.create({
                spinner: 'hide',
                cssClass: 'loader',
                content: "\n          <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n        "
            });
            loading.present();
            _this.backendProv.startCheck()
                .then(function (res1) {
                _this.backendProv.getProfile()
                    .then(function (res2) {
                    _this.profiles = res2;
                    setTimeout(function () {
                        _this.backendProv.getWallet()
                            .then(function (res3) {
                            if (res3) {
                                loading.dismiss();
                                _this.points = res3[0].wallet_total;
                            }
                        })
                            .catch(function (e) {
                            loading.dismiss();
                            console.log(e);
                        });
                    }, 2000);
                    // CHECK AND REGISTER FOR PUSH-NOTIFICATION
                    // this.backendProv.regPush();
                })
                    .catch(function (e) {
                    loading.dismiss();
                    console.log(e);
                });
            })
                .catch(function (e) {
                loading.dismiss();
                console.log(e);
            });
        });
    };
    IntroPage.prototype.checkPoints = function () {
        var _this = this;
        if (this.profiles.length > 0) {
            this.backendProv.getWallet()
                .then(function (res) {
                var alert = _this.alertCtrl.create({
                    title: 'Баланс',
                    message: 'На Вашем счете ' + res[0].wallet_total + ' баллов',
                    buttons: [
                        {
                            text: 'Ок',
                            role: 'cancel',
                            handler: function () {
                            }
                        }
                    ]
                });
                alert.present();
            })
                .catch(function (e) { return console.log(e); });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Внимание',
                subTitle: 'Необходима перезагрузка приложения с подключенным интернетом!',
                buttons: ['Закрыть']
            });
            alert_1.present();
        }
    };
    IntroPage.prototype.goRituals = function () {
        var _this = this;
        if (this.profiles.length > 0) {
            if (this.profiles[0].user_work_pos > '1') {
                var alert_2 = this.alertCtrl.create({
                    title: 'Внимание',
                    subTitle: 'Доступ к данной услуге был закрыт.',
                    buttons: ['Закрыть']
                });
                alert_2.present();
            }
            else if (this.profiles[0].user_mob_confirm == '1') {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__ritualchoice_ritualchoice__["a" /* RitualchoicePage */]);
            }
            else {
                var alert_3 = this.alertCtrl.create({
                    title: 'Внимание',
                    subTitle: 'Необходимо подтвердить номер телефона в личном кабинете',
                    buttons: [
                        {
                            text: 'Закрыть'
                        },
                        {
                            text: 'Принять',
                            handler: function (data) {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__profiledetails_profiledetails__["a" /* ProfiledetailsPage */], { profile: _this.profiles[0] });
                            }
                        }
                    ]
                });
                alert_3.present();
            }
        }
        else {
            var alert_4 = this.alertCtrl.create({
                title: 'Внимание',
                subTitle: 'Необходима перезагрузка приложения с подключенным интернетом!',
                buttons: ['Закрыть']
            });
            alert_4.present();
        }
    };
    IntroPage.prototype.toggleMenu = function () {
        this.menuCtrl.toggle();
    };
    IntroPage.prototype.goHome = function () {
        var _this = this;
        if (this.profiles.length > 0) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */])
                .then(function () {
                _this.navCtrl.popToRoot();
            });
        }
        else {
            var alert_5 = this.alertCtrl.create({
                title: 'Внимание',
                subTitle: 'Необходима перезагрузка приложения с подключенным интернетом!',
                buttons: ['Закрыть']
            });
            alert_5.present();
        }
    };
    return IntroPage;
}());
IntroPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-intro',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/intro/intro.html"*/'<ion-header no-border>\n  <ion-navbar>\n    <!-- <ion-buttons left>\n      <button ion-button icon-end color="light" (click)="toggleMenu()" style="font-size:20px;">\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons> -->\n    <ion-title>\n    </ion-title>\n    <ion-buttons right>\n      <button ion-button icon-begin color="light" (click)="checkPoints()">\n        <h2 style="display:inline-block;font-size:20px;margin:5px;">{{ points }}</h2>\n        <ion-icon style="height:20px;width:20px;" name="custom_star"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-bounce>\n\n  <ion-grid class="violback">\n    <ion-row>\n      <ion-col text-center>\n        <ion-card class="backtransparent">\n          <img src="assets/img/logo.png"/>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n<ion-footer class="backtransparent" no-padding no-border no-shadow>\n  <ion-toolbar class="backtransparent">\n    <ion-grid class="backtransparent">\n      <ion-row class="backtransparent">\n        <ion-col>\n            <button ion-button block large color="goldy" (click)="goRituals()">Записаться</button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n            <button ion-button block large color="goldy" (click)="goHome()">Карта клиента</button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/intro/intro.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_5__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], IntroPage);

//# sourceMappingURL=intro.js.map

/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GroupsPage = (function () {
    function GroupsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    GroupsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad GroupsPage');
    };
    return GroupsPage;
}());
GroupsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-groups',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/groups/groups.html"*/'<!--\n  Generated template for the GroupsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>groups</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/groups/groups.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */]])
], GroupsPage);

//# sourceMappingURL=groups.js.map

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrdersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrdersPage = (function () {
    function OrdersPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    OrdersPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad OrdersPage');
    };
    return OrdersPage;
}());
OrdersPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-orders',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/orders/orders.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>orders</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/orders/orders.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */]])
], OrdersPage);

//# sourceMappingURL=orders.js.map

/***/ }),

/***/ 697:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaIconComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FaIconComponent = (function (_super) {
    __extends(FaIconComponent, _super);
    function FaIconComponent(config, elementRef, renderer) {
        return _super.call(this, config, elementRef, renderer, "fa") || this;
    }
    Object.defineProperty(FaIconComponent.prototype, "fixedWidth", {
        set: function (fixedWidth) {
            this.setElementClass("fa-fw", this.isTrueProperty(fixedWidth));
        },
        enumerable: true,
        configurable: true
    });
    FaIconComponent.prototype.ngOnChanges = function (changes) {
        if (changes.name) {
            this.unsetPrevAndSetCurrentClass(changes.name);
        }
        if (changes.size) {
            this.unsetPrevAndSetCurrentClass(changes.size);
        }
    };
    FaIconComponent.prototype.isTrueProperty = function (val) {
        if (typeof val === "string") {
            val = val.toLowerCase().trim();
            return (val === "true" || val === "on" || val === "");
        }
        return !!val;
    };
    ;
    FaIconComponent.prototype.unsetPrevAndSetCurrentClass = function (change) {
        this.setElementClass("fa-" + change.previousValue, false);
        this.setElementClass("fa-" + change.currentValue, true);
    };
    return FaIconComponent;
}(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Ion */]));
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], FaIconComponent.prototype, "name", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], FaIconComponent.prototype, "size", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])("fixed-width"),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [String])
], FaIconComponent.prototype, "fixedWidth", null);
FaIconComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "fa-icon",
        template: "",
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Config */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]])
], FaIconComponent);

//# sourceMappingURL=fa-icon.component.js.map

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__qrsumm_qrsumm__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__news_news__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__terms_terms__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__share_share__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__review_review__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__gifts_gifts__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__social_social__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__giftsdetail_giftsdetail__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__contact_contact__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__service_rituals_service_rituals__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__service_programms_service_programms__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__service_giftcards_service_giftcards__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__qrcode_qrcode__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__profiledetails_profiledetails__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__intro_intro__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__surveys_surveys__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ritualchoice_ritualchoice__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_backend_backend__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};























var HomePage = (function () {
    function HomePage(app, platform, navCtrl, navParams, menuCtrl, backendProv, zone, alertCtrl, iab, loadingCtrl) {
        var _this = this;
        this.app = app;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.backendProv = backendProv;
        this.zone = zone;
        this.alertCtrl = alertCtrl;
        this.iab = iab;
        this.loadingCtrl = loadingCtrl;
        this.gifts = [];
        this.realexpression = 0;
        this.uuid = 1234567890;
        this.points = '';
        this.profiles = [];
        this.stateChng = 0;
        this.firstGift = [];
        this.scrollAmount = 0;
        this.platform.ready().then(function () {
            _this.platform.registerBackButtonAction(function () {
                if (_this.navCtrl.canGoBack()) {
                    _this.navCtrl.pop();
                }
                else {
                    _this.nav = _this.app.getRootNavById('n4');
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_18__intro_intro__["a" /* IntroPage */]);
                }
            });
        });
        this.logoState = 'notSlided';
        var loading = this.loadingCtrl.create({
            spinner: 'hide',
            cssClass: 'loader',
            content: "\n        <img src=\"assets/img/loading.png\" class=\"ld ldt-bounce-in infinite\" width=\"52\" height=\"52\" /><p style=\"color:#fff !important;\">\u0417\u0430\u0433\u0440\u0443\u0437\u043A\u0430..</p>\n      "
        });
        loading.present();
        backendProv.getLastGifts()
            .then(function (res) {
            loading.dismiss();
            _this.gifts = res;
        })
            .catch(function (e) { return console.log(e); });
        this.backendProv.getProfile()
            .then(function (res) {
            _this.profiles = res;
            _this.backendProv.getWallet()
                .then(function (res) {
                if (res) {
                    _this.points = res[0].wallet_total;
                }
            })
                .catch(function (e) { return console.log(e); });
        })
            .catch(function (e) { return console.log(e); });
        this.backendProv.getFirstGifts()
            .then(function (res) {
            _this.firstGift = res;
        })
            .catch(function (e) { return console.log(e); });
        this.uuid = backendProv.uuid;
        this.giftsPicLink = backendProv.giftsPicLink;
    }
    HomePage.prototype.ionViewOnEnter = function () {
    };
    HomePage.prototype.goSurveys = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_19__surveys_surveys__["a" /* SurveysPage */]);
    };
    HomePage.prototype.checkPoints = function () {
        var _this = this;
        this.backendProv.getWallet()
            .then(function (res) {
            var alert = _this.alertCtrl.create({
                title: 'Баланс',
                message: 'На Вашем счете ' + res[0].wallet_total + ' баллов',
                buttons: [
                    {
                        text: 'Ок',
                        role: 'cancel',
                        handler: function () {
                        }
                    }
                ]
            });
            alert.present();
        })
            .catch(function (e) { return console.log(e); });
    };
    HomePage.prototype.checkPic = function (val) {
        if (val && val != '0') {
            return this.giftsPicLink + 'pic/' + val;
        }
        else {
            return 'assets/img/logo.png';
        }
    };
    HomePage.prototype.goQRCode = function () {
        var _this = this;
        if (this.profiles.length > 0) {
            if (this.profiles[0].user_work_pos > '1') {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__qrcode_qrcode__["a" /* QrcodePage */], {
                    type: 'points',
                    realexpression: this.realexpression,
                    uuid: this.uuid,
                });
            }
            else if (this.profiles[0].user_mob_confirm == '1') {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__qrcode_qrcode__["a" /* QrcodePage */], {
                    type: 'points',
                    realexpression: this.realexpression,
                    uuid: this.uuid,
                });
            }
            else {
                var alert_1 = this.alertCtrl.create({
                    title: 'Внимание',
                    subTitle: 'Необходимо подтвердить номер телефона в личном кабинете',
                    buttons: [
                        {
                            text: 'Закрыть'
                        },
                        {
                            text: 'Подтвердить',
                            handler: function (data) {
                                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__profiledetails_profiledetails__["a" /* ProfiledetailsPage */], { profile: _this.profiles[0] });
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: 'Внимание',
                subTitle: 'Необходима перезагрузка приложения с подключенным интернетом!',
                buttons: ['Закрыть']
            });
            alert_2.present();
        }
    };
    HomePage.prototype.toggleLogo = function () {
        this.logoState = (this.logoState == 'notSlided') ? 'slided' : 'notSlided';
    };
    HomePage.prototype.scrollHandler = function (event) {
        var _this = this;
        this.zone.run(function () {
            if (event.scrollTop <= 10 && _this.logoState == 'slided') {
                _this.logoState = 'notSlided';
            }
            else if (event.scrollTop > 10 && _this.logoState == 'notSlided') {
                _this.logoState = 'slided';
            }
        });
    };
    HomePage.prototype.call = function () {
        this.iab.create('tel:7579', '_system', { location: 'yes' });
    };
    HomePage.prototype.goContact = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__contact_contact__["a" /* ContactPage */]);
    };
    HomePage.prototype.goGiftsDetail = function (val) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__giftsdetail_giftsdetail__["a" /* GiftsdetailPage */], { gift: val });
    };
    HomePage.prototype.goSocial = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__social_social__["a" /* SocialPage */]);
    };
    HomePage.prototype.goGifts = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__gifts_gifts__["a" /* GiftsPage */]);
    };
    HomePage.prototype.goReview = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__review_review__["a" /* ReviewPage */]);
    };
    HomePage.prototype.goShare = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__share_share__["a" /* SharePage */]);
    };
    HomePage.prototype.goTerms = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__terms_terms__["a" /* TermsPage */]);
    };
    HomePage.prototype.goNews = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__news_news__["a" /* NewsPage */]);
    };
    HomePage.prototype.goQRSumm = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__qrsumm_qrsumm__["a" /* QrsummPage */]);
    };
    HomePage.prototype.goTabs = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
    };
    HomePage.prototype.goRituals = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_20__ritualchoice_ritualchoice__["a" /* RitualchoicePage */]);
    };
    HomePage.prototype.goGiftCards = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__service_giftcards_service_giftcards__["a" /* ServiceGiftcardsPage */]);
    };
    HomePage.prototype.goSpaRitual = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__service_rituals_service_rituals__["a" /* ServiceRitualsPage */]);
    };
    HomePage.prototype.goSpaProgramm = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__service_programms_service_programms__["a" /* ServiceProgrammsPage */]);
    };
    HomePage.prototype.openMenu = function () {
        this.menuCtrl.open();
    };
    HomePage.prototype.closeMenu = function () {
        this.menuCtrl.close();
    };
    HomePage.prototype.toggleMenu = function () {
        this.menuCtrl.toggle();
    };
    return HomePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Content */])
], HomePage.prototype, "content", void 0);
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/home/home.html"*/'<ion-header>\n\n  <ion-navbar hideBackButton="true" color="viol">\n\n    <ion-buttons left>\n\n      <button ion-button icon-end color="light" (click)="toggleMenu()" style="font-size:20px;">\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>\n\n    </ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button icon-begin color="light" (click)="checkPoints()">\n\n        <h2 style="display:inline-block;font-size:20px;margin:5px;">{{ points }}</h2>\n\n        <ion-icon style="height:20px;width:20px;" name="custom_star"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-card [@logoSliding]="logoState" class="violback">\n\n  <img src="assets/img/logo.png"/>\n\n</ion-card>\n\n\n\n<ion-content (ionScroll)="scrollHandler($event)" no-bounce>\n\n\n\n  <img class="topback" src="assets/img/caribbean.jpg" *ngIf="profiles?.length>0" />\n\n\n\n  <ion-card class="loyalcardback" style="margin-top:-3px;" (click)="goQRCode()" *ngIf="profiles?.length>0">\n\n    <ion-grid no-padding>\n\n      <ion-row>\n\n        <ion-col col-12>\n\n          <ion-row class="vheight20">\n\n            <ion-col col-4 class="loyaltorch">\n\n              <div class="leftline"></div>\n\n              &nbsp;\n\n            </ion-col>\n\n            <ion-col col-8 class="backviol">\n\n              <ion-card class="inloyalcard">\n\n                <div class="rightline"></div>\n\n                <ion-card-content no-padding text-wrap class="whitey">\n\n                  <h3><strong>КАРТА ЛЮБИМОГО КЛИЕНТА</strong></h3>\n\n                  <p class="whitey">Предъявить карту и<br/>получить баллы</p>\n\n                </ion-card-content>\n\n              </ion-card>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n  <ion-card class="backtransparent" style="margin:8px;" *ngIf="profiles?.length>0">\n\n    <ion-row>\n\n      <ion-col>\n\n          <h2 class="violet">УСЛУГИ И ТОВАРЫ</h2>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-card>\n\n  <ion-card class="backtransparent" *ngIf="profiles?.length>0">\n\n    <ion-grid no-padding>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-5 class="card-background-page" (click)="goSpaRitual()">\n\n          <img src="assets/img/spa.jpg"/>\n\n          <div class="card-title everyline">\n\n            <div class="centering">\n\n              <ion-icon color="light" name="custom_stones"></ion-icon>\n\n              <br/>РИТУАЛЫ\n\n              <br/>&nbsp;\n\n            </div>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-5 class="card-background-page" (click)="goGiftCards()">\n\n          <img src="assets/img/gifts.jpg"/>\n\n          <div class="card-title everyline">\n\n            <div class="centering">\n\n              <ion-icon name="custom_towel"></ion-icon>\n\n              <br/>ПОДАРОЧНЫЕ\n\n              <br/>КАРТЫ\n\n            </div>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        &nbsp;\n\n      </ion-row>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-5 class="card-background-page" (click)="goSpaProgramm()">\n\n          <img src="assets/img/order.jpg"/>\n\n          <div class="card-title everyline">\n\n            <div class="centering">\n\n              <ion-icon name="custom_torch"></ion-icon>\n\n              <br/>СПА\n\n              <br/>ПРОГРАММЫ\n\n            </div>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-5 class="card-background-page" (click)="goContact()">\n\n          <img src="assets/img/welcome.jpg"/>\n\n          <div class="card-title everyline">\n\n            <div class="centering">\n\n              <ion-icon name="custom_feets"></ion-icon>\n\n              <br/>НАЙТИ\n\n              <br/>НАС\n\n            </div>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n  <ion-card class="backlotos" *ngIf="profiles?.length>0">\n\n    <ion-grid no-padding>\n\n      <ion-row>\n\n        <ion-col>\n\n            <h2 class="whitey">ДЕЛИСЬ С ДРУЗЬЯМИ</h2>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-5 class="card-background-page" (click)="goSocial()">\n\n          <img src="assets/img/placeholder.png"/>\n\n          <div class="card-title doubleline whitey">\n\n            <div class="centering">\n\n              <ion-icon name="custom_vk"></ion-icon>\n\n              <br/>ВСТУПИТЬ\n\n            </div>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-5 class="card-background-page" (click)="goShare()">\n\n          <img src="assets/img/placeholder.png"/>\n\n          <div class="card-title doubleline whitey">\n\n            <div class="centering">\n\n              <ion-icon name="custom_star"></ion-icon>\n\n              <br/>ПОЛУЧИТЬ\n\n            </div>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n  <ion-card class="backtransparent" style="margin:8px;" *ngIf="profiles?.length>0">\n\n    <ion-row>\n\n      <ion-col>\n\n          <h2 class="violet">ПОДАРКИ ЗА БАЛЛЫ</h2>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-card>\n\n  <ion-card class="backtransparent" *ngIf="firstGift?.length>0">\n\n    <ion-grid no-padding>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-11 class="card-background-page" *ngFor="let gift of firstGift" (click)="goGiftsDetail(gift)">\n\n          <img src="{{ checkPic(gift.gifts_pic) }}"/>\n\n          <div class="card-title everyline">\n\n            <div class="centering">\n\n              <ion-icon color="light" name="{{ gift.gifts_icon }}" *ngIf="gift.gifts_icon"></ion-icon>\n\n              <p class="whitey" [innerHTML]="gift.gifts_name"></p>\n\n              <p class="whitey"><small>первый подарок</small></p>\n\n            </div>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n  <ion-card class="backtransparent" *ngIf="profiles?.length>0">\n\n    <ion-grid no-padding>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-5 class="card-background-page" *ngFor="let gift of gifts" (click)="goGiftsDetail(gift)">\n\n          <img src="{{ checkPic(gift.gifts_pic) }}"/>\n\n          <div class="card-title everyline">\n\n            <div class="centering">\n\n              <ion-icon color="light" name="{{ gift.gifts_icon }}"></ion-icon>\n\n              <p class="whitey" [innerHTML]="gift.gifts_name"></p>\n\n            </div>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        &nbsp;\n\n      </ion-row>\n\n      <ion-row>\n\n        <button ion-button small clear block color="viol" (click)="goGifts(0)">Еще</button>\n\n      </ion-row>\n\n      <ion-row>\n\n        &nbsp;\n\n      </ion-row>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-11>\n\n            <button ion-button full color="viol" style="padding-top:30px;padding-bottom:30px;font-weight:bold;" (click)="goSurveys()">ПРОЙТИ ОПРОС</button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n  <ion-card class="backviolet" *ngIf="profiles?.length>0">\n\n    <ion-grid no-padding>\n\n      <ion-row>\n\n        <ion-col>\n\n            <h2 class="whitey">&nbsp;</h2>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row justify-content-around>\n\n        <ion-col col-5 class="card-background-page" (click)="call()">\n\n          <img src="assets/img/placeholder.png"/>\n\n          <div class="card-title doubleline whitey">\n\n            <div class="centering">\n\n                <ion-icon name="custom_mobile"></ion-icon>\n\n                <br/>ПОЗВОНИТЬ\n\n            </div>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-5 class="card-background-page" (click)="goTerms()">\n\n          <img src="assets/img/placeholder.png"/>\n\n          <div class="card-title doubleline whitey">\n\n            <div class="centering">\n\n                <ion-icon name="custom_leaf"></ion-icon>\n\n                <br/>ПРАВИЛА\n\n            </div>\n\n          </div>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/home/home.html"*/,
        animations: [
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('logoSliding', [
                Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('notSlided', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    opacity: 1,
                    transform: 'scale(1) translateY(0px)'
                })),
                Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('slided', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    opacity: 0,
                    transform: 'scale(0) translateY(7px)'
                })),
                Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('* => slided', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('200ms linear')),
                Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('* => notSlided', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('200ms linear'))
            ])
        ]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* MenuController */], __WEBPACK_IMPORTED_MODULE_21__providers_backend_backend__["a" /* BackendProvider */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* LoadingController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pipes_timepoints_timepoints__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__intro_intro__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OrderedPage = (function () {
    function OrderedPage(navCtrl, navParams, tpp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tpp = tpp;
        this.dateplace = [];
        this.unixtime = 0;
        this.dateday = this.navParams.get('dateday');
        this.datemonth = this.navParams.get('datemonth');
        this.unixtime = this.navParams.get('unixtime');
        this.datetime = this.tpp.transform(this.navParams.get('datetime'));
        this.dateplace = [this.navParams.get('dateplace')];
        // console.log(this.dateday+' | '+this.datemonth+' | '+this.unixtime+' | '+this.datetime+' | '+JSON.stringify(this.dateplace))
    }
    OrderedPage.prototype.ionViewDidLoad = function () {
    };
    OrderedPage.prototype.goToRoots = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__intro_intro__["a" /* IntroPage */]);
        this.navCtrl.popToRoot();
    };
    OrderedPage.prototype.formatBusHours = function (val) {
        return val.slice(0, 2) + ':' + val.slice(2, 4) + '-' + val.slice(4, 6) + ':' + val.slice(6, 8);
    };
    OrderedPage.prototype.recalcBusHours = function (val) {
        var valold = '';
        var startday = '';
        var endday = '';
        var valfinal = [];
        var days = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
        for (var i = 0; i < val.length; i++) {
            if (valold == val[i]) {
                endday = ' - ' + days[i];
                if (i == val.length - 1) {
                    valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                }
            }
            else {
                if (startday != '') {
                    valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    startday = days[i];
                    endday = '';
                    valold = val[i];
                    if (i == val.length - 1) {
                        valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    }
                }
                else {
                    startday = days[i];
                    valold = val[i];
                    if (i == val.length - 1) {
                        valfinal.push({ day: startday + endday, times: this.formatBusHours(valold) });
                    }
                }
            }
        }
        return valfinal;
    };
    OrderedPage.prototype.recalcPhone = function (tel) {
        if (!tel) {
            return '';
        }
        var value = tel.toString().trim().replace(/^\+/, '');
        if (value.match(/[^0-9]/)) {
            return tel;
        }
        var country, city, number;
        switch (value.length) {
            case 10:
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;
            case 11:
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;
            case 12:
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;
            case 13:
                country = value.slice(0, 2);
                city = value.slice(2, 5);
                number = value.slice(5);
                break;
            default:
                return tel;
        }
        if (country == 1) {
            country = "";
        }
        number = number.slice(0, 3) + '-' + number.slice(3);
        return (country + " (" + city + ") " + number).trim();
    };
    return OrderedPage;
}());
OrderedPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-ordered',template:/*ion-inline-start:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/ordered/ordered.html"*/'<ion-header>\n  <ion-navbar hideBackButton="true" color="viol">\n    <ion-title>\n      Вы записались!\n    </ion-title>\n    <ion-buttons right>\n      <button ion-button icon-end color="light" (click)="goToRoots()" style="font-size:20px;">\n        <ion-icon name="checkmark"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-bounce>\n  <div class="violback">\n    <ion-card class="backtransparent">\n      <ion-grid>\n        <ion-row style="margin-bottom:50px;" justify-content-around>\n          <ion-col col-6 class="card-background-page">\n            <img src="assets/img/placeholder.png"/>\n            <div class="card-title everyline">\n              <div class="centering">\n                <h1>{{ dateday }}</h1>\n                <h1>{{ datemonth }}</h1>\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row justify-content-around>\n          <ion-col col-6>\n              <h2 class="whitey">Ждем Вас в {{ datetime }}</h2>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf="dateplace.length > 0" justify-content-around>\n          <ion-col col-12 text-center>\n            <button ion-button icon-left clear class="infowhite"><ion-icon name="pin"></ion-icon> {{ dateplace[0].office_adress }}</button>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf="dateplace.length > 0" justify-content-around>\n          <ion-col text-center>\n            <button *ngIf="dateplace[0].office_tel[0]" ion-button small clear class="inforange">{{ recalcPhone(dateplace[0].office_tel[0]) }}</button>\n            <button *ngIf="dateplace[0].office_tel[1]" ion-button small clear class="inforange">{{ recalcPhone(dateplace[0].office_tel[1]) }}</button>\n            <button *ngIf="dateplace[0].office_tel[2]" ion-button small clear class="inforange">{{ recalcPhone(dateplace[0].office_tel[2]) }}</button>\n            <button *ngIf="dateplace[0].office_tel[3]" ion-button small clear class="inforange">{{ recalcPhone(dateplace[0].office_tel[3]) }}</button>\n            <button *ngIf="dateplace[0].office_tel[4]" ion-button small clear class="inforange">{{ recalcPhone(dateplace[0].office_tel[4]) }}</button>\n            <br/>\n            <button *ngFor="let bushour of recalcBusHours(dateplace[0].office_bus_hours)" ion-button small clear class="inforange">{{ bushour.day }} {{ bushour.times }}</button>\n          </ion-col>\n        </ion-row>\n        <ion-row justify-content-around>\n          <!-- <button ion-button outline color="light" (click)="goToRoots()">Редактировать запись</button> -->\n          <button ion-button outline color="light" (click)="goToRoots()">ОК</button>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n  </div>\n</ion-content>\n  '/*ion-inline-end:"/Volumes/WorkPalace/Apps/WhiteLotos2/src/pages/ordered/ordered.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__pipes_timepoints_timepoints__["a" /* TimepointsPipe */]])
], OrderedPage);

//# sourceMappingURL=ordered.js.map

/***/ }),

/***/ 9:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackendProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_do__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_device__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_file__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_push__ = __webpack_require__(147);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









// import { Storage } from '@ionic/storage';



var BackendProvider = (function () {
    function BackendProvider(platf, http, sqlite, device, file, push) {
        var _this = this;
        this.platf = platf;
        this.http = http;
        this.sqlite = sqlite;
        this.device = device;
        this.file = file;
        this.push = push;
        this.myid = 0;
        // APP NEEDED DATA
        this.masters = [];
        this.mastersearl = [];
        this.master = [];
        this.sliders = [];
        this.dates = [];
        this.history = [];
        this.services = [];
        this.ritual = [];
        this.news = [];
        this.terms = [];
        this.sharedetails = [];
        this.about = [];
        this.institutions = [];
        this.faq = [];
        this.datetoday = new Date();
        this.serverdate = 0;
        this.serverdateraw = 0;
        this.quests = [];
        // FOR MASTER SCHEDULE
        this.checkedmasters1 = 0;
        this.checkedmasters2 = 0;
        this.schedules = [];
        this.orderings = [];
        this.rooms = [];
        // FOR UPDATE
        this.lastpoints = 0;
        this.lastwallet = 0;
        this.lastgoods = 0;
        this.lastcat = 0;
        this.lastmenue = 0;
        this.lastingrs = 0;
        this.lastnews = 0;
        this.lastrevs = 0;
        this.lastasks = 0;
        this.lastgifts = 0;
        this.lastchat = 1;
        this.lastorder = 0;
        this.lastroom = 0;
        this.lastfifthgift = 0;
        this.lastreservs = 0;
        this.lastorganization = 0;
        // COMPANY DEPENDENT DATA
        this.institution = 28;
        this.instdir = 'whitelotos';
        this.gcm_android = '171848645940';
        this.generalscript = 'http://www.olegtronics.com/appscripts/getappdata.php';
        this.pushgotlink = 'http://www.olegtronics.com/admin/coms/pushgot.php';
        this.piclink = 'http://www.olegtronics.com/admin/img/user/' + this.institution + '/pic/';
        this.officePicLink = 'http://www.olegtronics.com/admin/img/icons/';
        this.menuePicLink = 'http://www.olegtronics.com/admin/img/menu/' + this.institution + '/';
        this.giftsPicLink = 'http://www.olegtronics.com/admin/img/gifts/' + this.institution + '/';
        this.newsPicLink = 'http://www.olegtronics.com/admin/img/news/' + this.institution + '/';
        this.iosLink = 'https://itunes.apple.com/us/app/whitelotos';
        this.androidLink = 'https://play.google.com/store/apps/details?id=by.olegtronics.whitelotos';
        this.groupFB = 'https://www.facebook.com/spa.whitelotus.minsk/';
        this.groupIG = 'https://www.instagram.com/thai_spa_whitelotus_minsk/';
        this.platf.ready().then(function () {
            _this.uuid = _this.device.uuid;
            _this.model = _this.device.model;
            _this.platform = _this.device.platform;
            _this.version = _this.device.version;
            _this.serial = _this.device.serial;
            if (_this.platform == 'iOS') {
                _this.sharelink = _this.iosLink;
            }
            else {
                _this.sharelink = _this.iosLink;
            }
            _this.sqlite.create({
                name: 'data.db',
                location: 'default'
            })
                .then(function (db) {
                _this.database = db;
                // this.db.close();
                // this.startCheck()
                // .then(res => {
                //   if(res == '1') {
                //     this.getInstitutions(this.institution).then(res => {this.companyIcon = this.officePicLink+res[0].org_logo;}).catch(e => console.log(e));
                //   }
                // })
                // .catch(e => console.log(e));
            })
                .catch(function (e) { return console.log(e); });
        });
        this.sliders = [{ id: 0 }, { id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }];
        this.datetoday.setHours(0, 0, 0, 0);
        this.history = [{ place: '1', service: '1', master: '2', date: this.datetoday }];
        this.terms = [{ id: 1, img: 'logo.png', title: 'Бонусная система', text: 'Уважаемые клиенты (посетители) и пользователи мобильного приложения «Белый Лотос»!' }, { id: 2, img: 'logo.png', title: 'Бонусная система', text: 'В мобильном приложении «Белый Лотос» есть Ваша персональная карта любимого клиента (бонусная программа лояльности).' }, { id: 3, img: 'logo.png', title: 'Бонусная система', text: 'Предъявляйте ее мастеру каждый раз, когда заходите к нам.' }, { id: 4, img: 'logo.png', title: 'Бонусная система', text: 'Карта любимого клиента позволяет Вам накапливать баллы и обменивать их на подарки из списка подарков в мобильном приложение.' }, { id: 5, img: 'logo.png', title: 'Бонусная система', text: 'Баллы начисляются в эквиваленте 5% от суммы Вашего чека.' }, { id: 6, img: 'logo.png', title: 'Бонусная система', text: 'Получить подарки за баллы можно только в «Белый Лотос».' }, { id: 7, img: 'logo.png', title: 'Бонусная система', text: 'В случае, если Вы не воспользуетесь картой постоянного клиента для начисления баллов или получения подарков в течении 6 месяцев неиспользованные баллы будут аннулированы.' }, { id: 8, img: 'logo.png', title: 'Бонусная система', text: 'Одновременное использование дисконтной программы лояльности и бонусной программы лояльности (в мобильном приложении) запрещено.' }, { id: 9, img: 'logo.png', title: 'Бонусная система', text: 'Администрация оставляет за собой право изменять условия бонусной программы лояльности в одностороннем порядке.' }, { id: 10, img: 'logo.png', title: 'Бонусная система', text: 'Начисление баллов не распространяется на специальные и акционные предложения!' }];
        this.sharedetails = [{ id: 1, title: 'Поделитесь своим промокодом с друзьями', sharetext: 'Установи приложение «Белый Лотос» и получи бонусы.', promotext: 'Промокод:' }];
        this.about = [{ id: 1, type: 0, img: 'logo.png', title: 'О нас', text: 'Первый салон «Белый Лотос» открыл двери гостям в 2014 году, и на сегодняшний день сеть предлагает самый большой выбор в Минске тайских SPA-ритуалов и программ от профессиональных мастеров из Таиланда.' }, { id: 2, type: 0, img: 'logo.png', title: 'О нас', text: 'CПА-комплекс «Белого Лотоса» основан на классических техниках тайской системы исцеления и включает 10 ритуалов и 8 СПА-программ. Каждая из процедур имеет свои особенности и эффекты, благодаря чему клиенты могут выбрать ритуал на любой вкус и потребность, от снятия усталости и расслабления, до растяжки и коррекции фигуры.' }, { id: 3, type: 0, img: 'logo.png', title: 'О нас', text: 'Миссия «Белого Лотоса» - помочь каждому гостю переключиться с ежедневных забот, а также восстановить физическое и душевное равновесие с помощью тайских методик глубокой релаксации.' }];
        this.faq = [{ id: 1, question: 'Администратор считал мою карту , но я не вижу зачисленных баллов в приложении. Что делать?', answer: 'Для того чтобы увидеть зачисленные баллы в этом случае, закройте приложение и запустите его заново. Если после этого Вы все еще не видите зачисленные баллы, проверьте подключение к Интернету. Наличие Интернета в Вашем смартфоне является необходимым условием для того, чтобы вы могли видеть Ваш актуальный баланс. Баллы могут начисляться в течении 24 часов.' }, { id: 2, question: 'За что я получаю бонусные баллы?', answer: 'Бонусные баллы вы можете получить в нескольких случаях: <ul><li>Оплатив услуги спа-салона "Белый лотос" и предъявив карту постоянного клиента администратору.</li><li>Поделившись с другом своим промокодом.</li><li>Введя промокод своего друга в личном кабинете.</li></ul>' }, { id: 3, question: 'Как использовать мой промо-код для начисления баллов?', answer: 'После того, как вы передали свой промокод своему другу, Вам будут начислены баллы в нескольких случаях: <ul><li>Друг скачал приложение, зарегистрировался и ввел ваш промокод в своем личном кабинете.</li><li>Ваш друг воспользовался услугами спа-салона "Белый лотос" и воспользовался картой постоянного клиента для начисления баллов.</li><li>Количество начисляемых баллов во втором случае будет больше, чем в первом. После того, как Ваш друг скачал приложение, он должен будет зарегистрировать свою карту постоянного гостя. При регистрации приложение предложит ввести промо-код.</li></ul>' }, { id: 4, question: 'Как мне перенести накопленные баллы если я поменял телефон?', answer: 'Все накопленные баллы привязаны к вашему номеру телефона. Скачайте приложение заново и зарегистрируйте свою карту заново на тот же номер телефона. Все баллы восстановятся в приложении. Если вы не видите баллы после регистрации, закройте приложение и откройте его заново. В данном случае обязательно наличие Интернета в Вашем смартфоне.' }, { id: 5, question: 'Какой срок хранения бонусных баллов, и происходит ли их обнуление?', answer: 'Обнуление бонусных баллов происходит каждые 6 месяцев.' }, { id: 6, question: 'Я вожу номер телефона а sms кодом не приходит, что делать?', answer: 'Скорость доставки sms сообщения с кодом зависит от sms-провайдера. К сожалению, иногда время доставки может достигать 30-ти минут. Если Вы не хотите ждать, пожалуйста обратитесь в службу технической поддержки по телефону: +375293803734' }, { id: 7, question: 'Могу ли я изменить свои регистрационные данные после того как привязал номер телефона?', answer: 'После того, как Вы ввели свои данные в анкете, в личном кабинете Вы сможете изменить любую информацию о себе кроме телефона.' }];
    }
    BackendProvider.prototype.startCheck = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql('SELECT * FROM users WHERE user_id = ?', [1])
                    .then(function (suc) {
                    _this.myid = suc.rows.item(0).user_real_id;
                    // UPDATE USER ONLINE
                    var queryPoints = "SELECT * FROM points ORDER BY points_when DESC LIMIT 1";
                    _this.database.executeSql(queryPoints, []).then(function (sucpoint) {
                        if (sucpoint.rows.length > 0) {
                            _this.lastpoints = sucpoint.rows.item(0).points_when;
                        }
                    })
                        .catch(function () { });
                    var queryWallet = "SELECT * FROM wallet ORDER BY wallet_when DESC LIMIT 1";
                    _this.database.executeSql(queryWallet, []).then(function (sucwall) {
                        if (sucwall.rows.length > 0) {
                            _this.lastwallet = sucwall.rows.item(0).wallet_when;
                        }
                    }).catch(function () { });
                    var queryGoods = "SELECT * FROM goods ORDER BY goods_when DESC LIMIT 1";
                    _this.database.executeSql(queryGoods, []).then(function (sucgood) {
                        if (sucgood.rows.length > 0) {
                            _this.lastgoods = sucgood.rows.item(0).goods_when;
                        }
                    }).catch(function () { });
                    var queryCat = "SELECT * FROM categories ORDER BY cat_when DESC LIMIT 1";
                    _this.database.executeSql(queryCat, []).then(function (succat) {
                        if (succat.rows.length > 0) {
                            _this.lastcat = succat.rows.item(0).cat_when;
                        }
                    }).catch(function () { });
                    var queryMenue = "SELECT * FROM menue ORDER BY menue_when DESC LIMIT 1";
                    _this.database.executeSql(queryMenue, []).then(function (sucmen) {
                        if (sucmen.rows.length > 0) {
                            _this.lastmenue = sucmen.rows.item(0).menue_when;
                        }
                    }).catch(function () { });
                    var queryIngrs = "SELECT * FROM ingredients ORDER BY ingr_when DESC LIMIT 1";
                    _this.database.executeSql(queryIngrs, []).then(function (sucingr) {
                        if (sucingr.rows.length > 0) {
                            _this.lastingrs = sucingr.rows.item(0).ingr_when;
                        }
                    }).catch(function () { });
                    var queryNews = "SELECT * FROM news ORDER BY news_when DESC LIMIT 1";
                    _this.database.executeSql(queryNews, []).then(function (sucnews) {
                        if (sucnews.rows.length > 0) {
                            _this.lastnews = sucnews.rows.item(0).news_when;
                        }
                    }).catch(function () { });
                    var queryRevs = "SELECT * FROM reviews ORDER BY reviews_when DESC LIMIT 1";
                    _this.database.executeSql(queryRevs, []).then(function (sucrev) {
                        if (sucrev.rows.length > 0) {
                            _this.lastrevs = sucrev.rows.item(0).reviews_when;
                        }
                    }).catch(function () { });
                    var queryAsks = "SELECT * FROM asks ORDER BY asks_when DESC LIMIT 1";
                    _this.database.executeSql(queryAsks, []).then(function (sucask) {
                        if (sucask.rows.length > 0) {
                            _this.lastasks = sucask.rows.item(0).asks_when;
                        }
                    }).catch(function () { });
                    var queryGifts = "SELECT * FROM gifts ORDER BY gifts_when DESC LIMIT 1";
                    _this.database.executeSql(queryGifts, []).then(function (sucgift) {
                        if (sucgift.rows.length > 0) {
                            _this.lastgifts = sucgift.rows.item(0).gifts_when;
                        }
                    }).catch(function () { });
                    var queryChats = "SELECT * FROM chat ORDER BY chat_when DESC LIMIT 1";
                    _this.database.executeSql(queryChats, []).then(function (succhat) {
                        if (succhat.rows.length > 0) {
                            _this.lastchat = succhat.rows.item(0).chat_when;
                        }
                    }).catch(function () { });
                    var queryOrdering = "SELECT * FROM ordering ORDER BY order_when DESC LIMIT 1";
                    _this.database.executeSql(queryOrdering, []).then(function (sucorder) {
                        if (sucorder.rows.length > 0) {
                            _this.lastorder = sucorder.rows.item(0).order_when;
                        }
                    }).catch(function () { });
                    var queryRooms = "SELECT * FROM rooms ORDER BY room_upd DESC LIMIT 1";
                    _this.database.executeSql(queryRooms, []).then(function (sucroom) {
                        if (sucroom.rows.length > 0) {
                            _this.lastroom = sucroom.rows.item(0).room_upd;
                        }
                    }).catch(function () { });
                    var queryFifthGift = "SELECT * FROM fifthgift ORDER BY fifth_when DESC LIMIT 1";
                    _this.database.executeSql(queryFifthGift, []).then(function (sucfifth) {
                        if (sucfifth.rows.length > 0) {
                            _this.lastfifthgift = sucfifth.rows.item(0).fifth_when;
                        }
                    }).catch(function () { });
                    var queryReservation = "SELECT * FROM reservation ORDER BY reservation_when DESC LIMIT 1";
                    _this.database.executeSql(queryReservation, []).then(function (sucres) {
                        if (sucres.rows.length > 0) {
                            _this.lastreservs = sucres.rows.item(0).reservation_when;
                        }
                    }).catch(function () { });
                    var queryOrganization = "SELECT * FROM organizations ORDER BY org_log DESC LIMIT 1";
                    _this.database.executeSql(queryOrganization, []).then(function (sucorg) {
                        if (sucorg.rows.length > 0) {
                            _this.lastorganization = sucorg.rows.item(0).org_log;
                        }
                        setTimeout(function () {
                            var updstr = {
                                device: _this.model,
                                device_id: _this.uuid,
                                device_serial: _this.serial,
                                device_version: _this.version,
                                device_os: _this.platform,
                                inst_id: _this.institution,
                                points: _this.lastpoints,
                                wallet: _this.lastwallet,
                                goods: _this.lastgoods,
                                cat: _this.lastcat,
                                menue: _this.lastmenue,
                                ingrs: _this.lastingrs,
                                news: _this.lastnews,
                                revs: _this.lastrevs,
                                asks: _this.lastasks,
                                gifts: _this.lastgifts,
                                chat: _this.lastchat,
                                order: _this.lastorder,
                                room: _this.lastroom,
                                fifthgift: _this.lastfifthgift,
                                reservs: _this.lastreservs,
                                organization: _this.lastorganization,
                                newusr: 'check'
                            };
                            _this.httpRequest(JSON.stringify(updstr))
                                .subscribe(function (updsuc) {
                                _this.updUser(updsuc, suc.rows.item(0));
                                setTimeout(function () {
                                    resolve(1);
                                }, 1000);
                            }, function (error) {
                                console.log('ERROR HTTP REQ ' + JSON.stringify(error));
                                resolve(0);
                            });
                        }, 1000);
                    })
                        .catch(function () {
                        _this.getInstitutions(_this.institution)
                            .then(function (res) {
                            if (res) {
                                _this.companyIcon = _this.officePicLink + res[0].org_logo;
                            }
                        })
                            .catch(function (e) { return console.log(e); });
                        resolve(1);
                    });
                })
                    .catch(function (er) {
                    // CREATE DIRECTORIES
                    _this.crDir();
                    // REGISTER NEW USER ONLINE
                    var regstr = {
                        device: _this.model,
                        device_id: _this.uuid,
                        device_serial: _this.serial,
                        device_version: _this.version,
                        device_os: _this.platform,
                        inst_id: _this.institution,
                        newusr: 'newusr'
                    };
                    _this.httpRequest(JSON.stringify(regstr))
                        .subscribe(function (data) {
                        _this.newUser(data);
                        setTimeout(function () {
                            resolve(1);
                        }, 2000);
                    }, function (error) {
                        resolve(0);
                        // console.log('ERROR '+JSON.stringify(error));
                    });
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getCalender = function (data, notconf) {
        var _this = this;
        var dataorders = data;
        // DB ORDER
        var orderArr = dataorders[0].ordersArr;
        if (orderArr.length > 0) {
            this.orderArrFuncIns(0, orderArr);
        }
        setTimeout(function () {
            _this.getInstitutions(_this.institution)
                .then(function (res) {
                if (res) {
                    _this.companyIcon = _this.officePicLink + res[0].org_logo;
                }
            })
                .catch(function (e) { return console.log(e); });
        }, 1000);
    };
    BackendProvider.prototype.getWaiter = function (data, notconf) {
        var _this = this;
        var dataworkers = data;
        // DB WAITERS
        var usrArr = dataworkers[0].usrArr;
        if (usrArr.length > 0) {
            this.usrArrFunc(0, usrArr);
        }
        // DB PROFESSIONS
        var profArr = dataworkers[0].profArr;
        if (profArr.length > 0) {
            this.profArrFunc(0, profArr);
        }
        // DB OFFICE
        var offArr = dataworkers[0].offArr;
        if (offArr.length > 0) {
            this.offArrFunc(0, offArr);
        }
        // DB ROOMS
        var roomArr = dataworkers[0].roomArr;
        if (roomArr.length > 0) {
            this.roomArrFunc(0, roomArr);
        }
        // DB SCHEDULE
        var schArr = dataworkers[0].schArr;
        if (schArr.length > 0) {
            this.schArrFunc(0, schArr);
        }
        var calendstr = {
            device: this.model,
            device_id: this.uuid,
            device_serial: this.serial,
            device_version: this.version,
            device_os: this.platform,
            inst_id: this.institution,
            newusr: 'calender'
        };
        this.httpRequest(JSON.stringify(calendstr))
            .subscribe(function (calendsuc) {
            _this.getCalender(calendsuc, notconf);
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    BackendProvider.prototype.updUser = function (data, userupd) {
        var _this = this;
        if (data[0].check == '1') {
            // USER LOG
            if (data[0].user_log > '0') {
                var usrlog_1 = data[0].user_log;
                var corrtime = this.timezoneAdd(data[0].user_log);
                this.serverdate = new Date(corrtime * 1000);
                this.serverdateraw = data[0].user_log;
                setTimeout(function () {
                    // every day 2 weeks
                    for (var i = 0; i < 14; i++) {
                        _this.dates.push({ dat: _this.crnewdate(_this.serverdate, i) });
                    }
                }, 200);
                var queryUsrLog = "SELECT * FROM users WHERE user_id=? AND user_log=? LIMIT 1";
                this.database.executeSql(queryUsrLog, [1, usrlog_1]).then(function (suc) {
                    if (suc.rows.length == 0) {
                        var queryUsrLogUpd = "UPDATE users SET user_log=? WHERE user_id=?";
                        _this.database.executeSql(queryUsrLogUpd, [usrlog_1, 1])
                            .then(function (suc) { })
                            .catch(function () { });
                    }
                }).catch(function () { });
            }
            // USER DISCOUNT
            if (data[0].user_discount > '0') {
                var usrdiscount_1 = data[0].user_discount;
                var queryUsrDiscount = "SELECT * FROM users WHERE user_id=? AND user_discount=? LIMIT 1";
                this.database.executeSql(queryUsrDiscount, [1, usrdiscount_1])
                    .then(function (suc) {
                    if (suc.rows.length == 0) {
                        var queryUsrDiscountUpd = "UPDATE users SET user_discount=? WHERE user_id=?";
                        _this.database.executeSql(queryUsrDiscountUpd, [usrdiscount_1, 1])
                            .then(function (suc) {
                            // $ionicPopup.alert({
                            //   title 'Внимание',
                            //   template 'Дисконтная карта добавленна!'
                            // });
                        })
                            .catch(function () { });
                    }
                })
                    .catch(function () { });
            }
            // USER WORK POSITION
            if (data[0].user_work_pos >= '2') {
                // alert(data[0].user_discount);
                var user_work_pos_1 = data[0].user_work_pos;
                var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? LIMIT 1";
                this.database.executeSql(queryUsrWP, [1, user_work_pos_1])
                    .then(function (suc) {
                    if (suc.rows.length == 0) {
                        var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                        _this.database.executeSql(queryUsrWPUpd, [user_work_pos_1, 1])
                            .then(function (suc) {
                            // $ionicPopup.alert({
                            //   title 'Внимание',
                            //   template 'Вы заблокированны!'
                            // });
                        })
                            .catch(function () { });
                    }
                })
                    .catch(function () { });
            }
            else if (data[0].user_work_pos != '1000') {
                var user_work_pos_2 = data[0].user_work_pos;
                var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? LIMIT 1";
                this.database.executeSql(queryUsrWP, [1, user_work_pos_2])
                    .then(function (suc) {
                    if (suc.rows.length == 0) {
                        var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                        _this.database.executeSql(queryUsrWPUpd, [user_work_pos_2, 1])
                            .then(function (suc) {
                            // $ionicPopup.alert({
                            //   title 'Внимание',
                            //   template 'Вы разблокированны!'
                            // });
                        })
                            .catch(function () { });
                    }
                })
                    .catch(function () { });
            }
            // USER DATA UPDATE IF ON PHONE IS OLDER THAN ONLINE
            if (data[0].user_upd > userupd.user_upd && data[0].user_id > 0) {
                var user_id_1 = data[0].user_id;
                var user_name_1 = data[0].user_name;
                var user_surname_1 = data[0].user_surname;
                var user_middlename_1 = data[0].user_middlename;
                var user_email_1 = data[0].user_email;
                var user_email_confirm_1 = data[0].user_email_confirm;
                var user_pwd_1 = data[0].user_pwd;
                var user_tel_1 = data[0].user_tel;
                var user_mob_confirm_1 = data[0].user_mob_confirm;
                var user_mob_1 = data[0].user_mob;
                var user_info_1 = data[0].user_info;
                var user_work_pos_3 = data[0].user_work_pos;
                var user_menue_exe_1 = data[0].user_menue_exe;
                var user_institution_1 = data[0].user_institution;
                var user_office_1 = data[0].user_office;
                var user_pic_1 = data[0].user_pic;
                var user_gender_1 = data[0].user_gender;
                var user_birthday_1 = data[0].user_birthday;
                var user_country_1 = data[0].user_country;
                var user_region_1 = data[0].user_region;
                var user_city_1 = data[0].user_city;
                var user_adress_1 = data[0].user_adress;
                var user_install_where_1 = data[0].user_install_where;
                var user_log_key_1 = data[0].user_log_key;
                var user_gcm_1 = data[0].user_gcm;
                var user_device_1 = data[0].user_device;
                var user_device_id_1 = data[0].user_device_id;
                var user_device_serial_1 = data[0].user_device_serial;
                var user_device_version_1 = data[0].user_device_version;
                var user_device_os_1 = data[0].user_device_os;
                var user_discount_1 = data[0].user_discount;
                var user_promo_1 = data[0].user_promo;
                var user_conf_req_1 = data[0].user_conf_req;
                var user_log_1 = data[0].user_log;
                var user_upd_1 = data[0].user_upd;
                var user_reg_1 = data[0].user_reg;
                var user_del_1 = data[0].user_del;
                var queryUsrLog = "SELECT * FROM users WHERE user_id=? LIMIT 1";
                this.database.executeSql(queryUsrLog, [1])
                    .then(function (suc) {
                    if (suc.rows.length > 0) {
                        var queryUsrDataUpd = "UPDATE users SET user_real_id=?, user_name=?, user_surname=?, user_middlename=?, user_email=?, user_email_confirm=?, user_pwd=?, user_tel=?, user_mob_confirm=?, user_mob=?, user_info=?, user_work_pos=?, user_menue_exe=?, user_institution=?, user_office=?, user_pic=?, user_gender=?, user_birthday=?, user_country=?, user_region=?, user_city=?, user_adress=?, user_install_where=?, user_log_key=?, user_gcm=?, user_device=?, user_device_id=?, user_device_serial=?, user_device_version=?, user_device_os=?, user_discount=?, user_promo=?, user_conf_req=?, user_log=?, user_upd=?, user_reg=?, user_del=? WHERE user_id=?";
                        _this.database.executeSql(queryUsrDataUpd, [user_id_1, user_name_1, user_surname_1, user_middlename_1, user_email_1, user_email_confirm_1, user_pwd_1, user_tel_1, user_mob_confirm_1, user_mob_1, user_info_1, user_work_pos_3, user_menue_exe_1, user_institution_1, user_office_1, user_pic_1, user_gender_1, user_birthday_1, user_country_1, user_region_1, user_city_1, user_adress_1, user_install_where_1, user_log_key_1, user_gcm_1, user_device_1, user_device_id_1, user_device_serial_1, user_device_version_1, user_device_os_1, user_discount_1, user_promo_1, user_conf_req_1, user_log_1, user_upd_1, user_reg_1, user_del_1, 1])
                            .then(function (suc) { })
                            .catch(function () { });
                    }
                })
                    .catch(function () { });
            }
            // DB POINTS
            var pointsArr = data[0].pointsArr;
            if (pointsArr.length > 0) {
                this.pointsArrFunc(0, pointsArr);
            }
            // DB WALLET
            var walletArr = data[0].walletArr;
            if (walletArr.length > 0) {
                var wallet_user_1 = data[0].walletArr[0]['wallet_user'];
                var wallet_institution_1 = data[0].walletArr[0]['wallet_institution'];
                var wallet_total_1 = data[0].walletArr[0]['wallet_total'];
                var wallet_warn_1 = data[0].walletArr[0]['wallet_warn'];
                var wallet_when_1 = data[0].walletArr[0]['wallet_when'];
                var wallet_del_1 = data[0].walletArr[0]['wallet_del'];
                var queryWallet = "SELECT * FROM wallet WHERE wallet_when < ?";
                this.database.executeSql(queryWallet, [wallet_when_1])
                    .then(function (suc) {
                    if (suc.rows.length > 0) {
                        var walletUpd = "UPDATE wallet SET wallet_institution=?, wallet_total=?, wallet_when=?, wallet_warn=?, wallet_del=?  WHERE wallet_user=?";
                        _this.database.executeSql(walletUpd, [wallet_institution_1, wallet_total_1, wallet_when_1, wallet_warn_1, wallet_del_1, wallet_user_1])
                            .then(function () { })
                            .catch(function () { });
                    }
                })
                    .catch(function () { });
            }
            // DB GOODS
            var goodsArr = data[0].goodsArr;
            if (goodsArr.length > 0) {
                this.goodsArrFunc(0, goodsArr);
            }
            // DB CATEGORIES
            var catArr = data[0].catArr;
            if (catArr.length > 0) {
                this.catArrFunc(0, catArr);
            }
            // DB MENU
            var menueArr = data[0].menueArr;
            if (menueArr.length > 0) {
                this.menueArrFunc(0, menueArr);
            }
            // DB INGREDIENTS
            var ingrArr = data[0].ingrArr;
            if (ingrArr.length > 0) {
                this.ingrArrFunc(0, ingrArr);
            }
            // DB NEWS
            var newsArr = data[0].newsArr;
            if (newsArr.length > 0) {
                this.newsArrFunc(0, newsArr);
            }
            // DB REVIEWS
            var reviewsArr = data[0].reviewsArr;
            if (reviewsArr.length > 0) {
                this.reviewsArrFunc(0, reviewsArr);
            }
            // DB ASKS
            var asksArr = data[0].asksArr;
            if (asksArr.length > 0) {
                this.asksArrFunc(0, asksArr);
            }
            // DB GIFTS
            var giftsArr = data[0].giftsArr;
            if (giftsArr.length > 0) {
                this.giftsArrFunc(0, giftsArr);
            }
            // DB CHAT
            var chatArr = data[0].chatArr;
            if (chatArr.length > 0) {
                this.chatArrFunc(0, chatArr);
            }
            // DB FIFTHGIFT
            var fifthArr = data[0].fifthArr;
            if (fifthArr.length > 0) {
                this.fifthArrFunc(0, fifthArr);
            }
            // DB RESERVATION
            var reservArr = data[0].reservArr;
            if (reservArr.length > 0) {
                this.reservArrFunc(0, reservArr);
            }
            // DB ORGANIZATION
            var orgArr = data[0].orgArr;
            if (orgArr.length > 0) {
                this.orgArrFunc(0, orgArr);
            }
            setTimeout(function () {
                var waiterstr = {
                    inst_id: _this.institution,
                    newusr: 'waiter'
                };
                _this.httpRequest(JSON.stringify(waiterstr))
                    .subscribe(function (waitsuc) {
                    if (userupd.user_mob_confirm == 1 && _this.validateEmail(userupd.user_email) && userupd.user_name) {
                        _this.getWaiter(waitsuc, 1);
                    }
                    else {
                        _this.getWaiter(waitsuc, 0);
                    }
                }, function (error) {
                    console.log(JSON.stringify(error));
                });
            }, 2000);
        }
    };
    BackendProvider.prototype.newUser = function (data) {
        var _this = this;
        // DB USER
        this.database.executeSql("CREATE TABLE IF NOT EXISTS users (user_id integer primary key, user_real_id integer, user_name text, user_surname text, user_middlename text, user_email text, user_email_confirm text, user_pwd text, user_tel integer, user_mob_confirm text, user_mob integer, user_info text, user_work_pos integer, user_menue_exe text, user_institution integer, user_office integer, user_pic text, user_gender integer, user_birthday text, user_country integer, user_region integer, user_city integer, user_adress text, user_install_where integer, user_log_key text, user_gcm text, user_device text, user_device_id text, user_device_serial text, user_device_version text, user_device_os text, user_discount text, user_promo integer, user_conf_req integer, user_log integer, user_upd integer, user_reg integer, user_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                var user_id_2 = data[0].usrData['user_id'];
                var user_name = data[0].usrData['user_name'];
                var user_surname = data[0].usrData['user_surname'];
                var user_middlename = data[0].usrData['user_middlename'];
                var user_email = data[0].usrData['user_email'];
                var user_email_confirm = data[0].usrData['user_email_confirm'];
                var user_pwd = data[0].usrData['user_pwd'];
                var user_tel = data[0].usrData['user_tel'];
                var user_mob_confirm_2 = data[0].usrData['user_mob_confirm'];
                var user_mob = data[0].usrData['user_mob'];
                var user_info = data[0].usrData['user_info'];
                var user_work_pos = data[0].usrData['user_work_pos'];
                var user_menue_exe = data[0].usrData['user_menue_exe'];
                var user_institution = data[0].usrData['user_institution'];
                var user_office = data[0].usrData['user_office'];
                var user_pic = data[0].usrData['user_pic'];
                var user_gender = data[0].usrData['user_gender'];
                var user_birthday = data[0].usrData['user_birthday'];
                var user_country = data[0].usrData['user_country'];
                var user_region = data[0].usrData['user_region'];
                var user_city = data[0].usrData['user_city'];
                var user_adress = data[0].usrData['user_adress'];
                var user_install_where = data[0].usrData['user_install_where'];
                var user_log_key = data[0].usrData['user_log_key'];
                var user_gcm = data[0].usrData['user_gcm'];
                var user_device = data[0].usrData['user_device'];
                var user_device_id = data[0].usrData['user_device_id'];
                var user_device_serial = data[0].usrData['user_device_serial'];
                var user_device_version = data[0].usrData['user_device_version'];
                var user_device_os = data[0].usrData['user_device_os'];
                var user_discount = data[0].usrData['user_discount'];
                var user_promo = data[0].usrData['user_promo'];
                var user_conf_req = data[0].usrData['user_conf_req'];
                var user_log = data[0].usrData['user_log'];
                var user_upd = data[0].usrData['user_upd'];
                var user_reg = data[0].usrData['user_reg'];
                var user_del = data[0].usrData['user_del'];
                var corrtime = _this.timezoneAdd(data[0].usrData['user_log']);
                _this.serverdate = new Date(corrtime * 1000);
                _this.serverdateraw = data[0].usrData['user_log'];
                setTimeout(function () {
                    // every day 2 weeks
                    for (var i = 0; i < 14; i++) {
                        _this.dates.push({ dat: _this.crnewdate(_this.serverdate, i) });
                    }
                }, 200);
                // DB USER
                var usrIns = "INSERT INTO users (user_id, user_real_id, user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_mob_confirm, user_mob, user_info, user_work_pos, user_menue_exe, user_institution, user_office, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_gcm, user_device, user_device_id, user_device_serial, user_device_version, user_device_os, user_discount, user_promo, user_conf_req, user_log, user_upd, user_reg, user_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(usrIns, [1, user_id_2, user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_mob_confirm_2, user_mob, user_info, user_work_pos, user_menue_exe, user_institution, user_office, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_gcm, user_device, user_device_id, user_device_serial, user_device_version, user_device_os, user_discount, user_promo, user_conf_req, user_log, user_upd, user_reg, user_del]).then(function () {
                    _this.myid = user_id_2;
                    setTimeout(function () {
                        var waiterstr = {
                            inst_id: _this.institution,
                            newusr: 'waiter'
                        };
                        _this.httpRequest(JSON.stringify(waiterstr))
                            .subscribe(function (waitsuc) {
                            _this.getWaiter(waitsuc, user_mob_confirm_2);
                        }, function (error) {
                            console.log(JSON.stringify(error));
                        });
                    }, 2000);
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
        // DB COUNTRY
        this.database.executeSql("CREATE TABLE IF NOT EXISTS country (id_country integer primary key, name text, country_del integer)", {})
            .then(function () { })
            .catch(function () { });
        // DB REGION
        this.database.executeSql("CREATE TABLE IF NOT EXISTS region (id_region integer primary key, id_country integer, name text, region_del integer)", {})
            .then(function () { })
            .catch(function () { });
        // DB CITY
        this.database.executeSql("CREATE TABLE IF NOT EXISTS city (id_city integer primary key, id_region integer, id_country integer, name text, city_del integer)", {})
            .then(function () { })
            .catch(function () { });
        // DB POINTS
        this.database.executeSql("CREATE TABLE IF NOT EXISTS points (points_id integer primary key, points_user integer, points_bill integer, points_discount integer, points_points integer, points_got_spend integer, points_waiter integer, points_institution integer, points_office integer,points_status integer, points_comment integer, points_proofed integer, points_gift integer, points_check integer, points_waitertime integer, points_usertime integer, points_when integer, points_time integer, points_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                // DB POINTS
                var pointsArr = data[0].pointsArr;
                if (pointsArr.length > 0) {
                    _this.pointsArrFuncIns(0, pointsArr);
                }
            }
        })
            .catch(function () { });
        // DB WALLET
        this.database.executeSql("CREATE TABLE IF NOT EXISTS wallet (wallet_id integer primary key, wallet_user integer, wallet_institution integer, wallet_total integer, wallet_warn integer, wallet_when integer, wallet_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                // DB WALLET
                var walletArr = data[0].walletArr;
                if (walletArr.length > 0) {
                    var wallet_user = data[0].walletArr[0]['wallet_user'];
                    var wallet_institution = data[0].walletArr[0]['wallet_institution'];
                    var wallet_total = data[0].walletArr[0]['wallet_total'];
                    var wallet_warn = data[0].walletArr[0]['wallet_warn'];
                    var wallet_when = data[0].walletArr[0]['wallet_when'];
                    var wallet_del = data[0].walletArr[0]['wallet_del'];
                    var walletIns = "INSERT INTO wallet (wallet_user, wallet_institution, wallet_total, wallet_warn, wallet_when, wallet_del) VALUES (?,?,?,?,?,?)";
                    _this.database.executeSql(walletIns, [wallet_user, wallet_institution, wallet_total, wallet_warn, wallet_when, wallet_del])
                        .then(function () { })
                        .catch(function () { });
                }
            }
        })
            .catch(function () { });
        // DB GOODS
        this.database.executeSql("CREATE TABLE IF NOT EXISTS goods (goods_id integer primary key, goods_name text, goods_desc text, goods_pic text, goods_type integer, goods_institution integer, goods_when integer, goods_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                // DB GOODS
                var goodsArr = data[0].goodsArr;
                if (goodsArr.length > 0) {
                    _this.goodsArrFuncIns(0, goodsArr);
                }
            }
        })
            .catch(function () { });
        // DB CATEGORIES
        this.database.executeSql("CREATE TABLE IF NOT EXISTS categories (cat_id integer primary key, cat_xid text, cat_name text, cat_desc text, cat_pic text, cat_ingr integer, cat_order integer, cat_institution integer, cat_when integer, cat_del integer)", {}).then(function () {
            if (data[0].newusr == '1') {
                // DB CATEGORIES
                var catArr = data[0].catArr;
                if (catArr.length > 0) {
                    _this.catArrFuncIns(0, catArr);
                }
            }
        })
            .catch(function () { });
        // DB MENUE
        this.database.executeSql("CREATE TABLE IF NOT EXISTS menue (menue_id integer primary key, menue_xid text, menue_cat_xid text, menue_cat integer, menue_name text, menue_desc text, menue_size integer, menue_cost integer, menue_costs text, menue_ingr text, menue_addition text, menue_addition_auto integer, menue_package text, menue_weight integer, menue_interval integer, menue_discount integer, menue_action text, menue_code text, menue_pic text, menue_icon text, menue_institution integer, menue_when integer, menue_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                // DB MENU
                var menueArr = data[0].menueArr;
                if (menueArr.length > 0) {
                    _this.menueArrFuncIns(0, menueArr);
                }
            }
        })
            .catch(function () { });
        // DB GIFTS
        this.database.executeSql("CREATE TABLE IF NOT EXISTS gifts (gifts_id integer primary key, gifts_name text, gifts_desc text, gifts_points integer, gifts_pic text, gifts_icon text, gifts_institution integer, gifts_when integer, gifts_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                // DB GIFTS
                var giftsArr = data[0].giftsArr;
                if (giftsArr.length > 0) {
                    _this.giftsArrFuncIns(0, giftsArr);
                }
            }
        })
            .catch(function () { });
        // DB INGREDIENTS
        this.database.executeSql("CREATE TABLE IF NOT EXISTS ingredients (ingr_id integer primary key, ingr_xid text, ingr_name text, ingr_desc text, ingr_cat integer, ingr_code text, ingr_pic text, ingr_size integer, ingr_cost integer, ingr_institution integer, ingr_when integer, ingr_del)", {}).then(function () {
            if (data[0].newusr == '1') {
                // DB INGREDIENTS
                var ingrArr = data[0].ingrArr;
                if (ingrArr.length > 0) {
                    _this.ingrArrFuncIns(0, ingrArr);
                }
            }
        })
            .catch(function () { });
        // DB NEWS
        this.database.executeSql("CREATE TABLE IF NOT EXISTS news (news_id integer primary key, news_name text, news_message text, news_pic text, news_institution integer, news_state integer, news_menue_id integer, news_cost integer, news_user integer, news_used integer, news_begin integer, news_end integer, news_when integer, news_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                // DB NEWS
                var newsArr = data[0].newsArr;
                if (newsArr.length > 0) {
                    _this.newsArrFuncIns(0, newsArr);
                }
            }
        })
            .catch(function () { });
        // DB REVIEWS
        this.database.executeSql("CREATE TABLE IF NOT EXISTS reviews (reviews_id integer primary key, reviews_from integer, reviews_to integer, reviews_message text, reviews_pic text, reviews_institution integer, reviews_answered integer, reviews_opened integer, reviews_when integer, reviews_del integer)", {}).then(function () {
            if (data[0].newusr == '1') {
                // DB REVIEWS
                var reviewsArr = data[0].reviewsArr;
                if (reviewsArr.length > 0) {
                    _this.reviewsArrFuncIns(0, reviewsArr);
                }
            }
        })
            .catch(function () { });
        // DB ASKS
        this.database.executeSql("CREATE TABLE IF NOT EXISTS asks (asks_id integer primary key, asks_name text, asks_message text, asks_type integer, asks_chained integer, asks_active integer, asks_img text, asks_answ text, asks_yes integer, asks_no integer, asks_reply text, asks_institution integer, asks_when integer, asks_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                // DB ASKS
                var asksArr = data[0].asksArr;
                if (asksArr.length > 0) {
                    _this.asksArrFuncIns(0, asksArr);
                }
            }
        })
            .catch(function () { });
        // DB CHAT
        this.database.executeSql("CREATE TABLE IF NOT EXISTS chat (chat_id integer primary key, chat_from integer, chat_to integer, chat_name text, chat_message text, chat_read integer, chat_institution integer, chat_answered integer, chat_when integer, chat_del integer)", {}).then(function () {
            if (data[0].newusr == '1') {
                // DB CHAT
                var chatArr = data[0].chatArr;
                if (chatArr.length > 0) {
                    _this.chatArrFuncIns(0, chatArr);
                }
            }
        })
            .catch(function () { });
        // DB ORDER
        this.database.executeSql("CREATE TABLE IF NOT EXISTS ordering (order_id integer primary key, order_user text, order_user_name_phone text, order_user_surname_phone text, order_user_middlename_phone text, order_user_adress_phone text, order_user_comment_phone text, order_name text, order_name_phone text, order_user_phone_phone text, order_user_email_phone text, order_desc text, order_worker integer, order_worker_name_phone text, order_worker_pic_phone text, order_worker_profession_phone text, order_reminder_phone text, order_institution integer, order_office integer, order_room integer, order_bill integer, order_goods integer, order_cats integer, order_order text, order_status integer, order_start integer, order_start_name_phone text, order_end integer, order_allday integer, order_mobile integer, order_mobile_confirm text, order_when integer, order_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
            }
        })
            .catch(function () { });
        // DB PROFESSIONS
        this.database.executeSql("CREATE TABLE IF NOT EXISTS professions (prof_id integer primary key, prof_name text, prof_desc text, prof_institution integer, prof_when integer, prof_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
            }
        })
            .catch(function () { });
        // DB OFFICES
        this.database.executeSql("CREATE TABLE IF NOT EXISTS organizations_office (office_id integer primary key, office_name text, office_start text, office_stop text, office_bus_hours text, office_country integer, office_city text, office_adress text, office_lat text, office_lon text, office_desc text, office_timezone integer, office_tel text, office_tel_n text, office_fax integer, office_mob integer, office_email text,office_pwd text, office_goods text, office_categories text, office_menue text, office_orders integer, office_skype text, office_site text, office_tax_id text, office_logo text, office_institution integer, office_log integer, office_reg integer, office_del integer)", {}).then(function () {
            if (data[0].newusr == '1') {
            }
        })
            .catch(function () { });
        // DB SCHEDULE
        this.database.executeSql("CREATE TABLE IF NOT EXISTS schedule (schedule_id integer primary key, schedule_employee integer, schedule_menue integer, schedule_office integer, schedule_start integer, schedule_stop integer, schedule_institution integer, schedule_when integer, schedule_del integer)", {}).then(function () {
            if (data[0].newusr == '1') {
            }
        })
            .catch(function () { });
        // DB FIFTHGIFT
        this.database.executeSql("CREATE TABLE IF NOT EXISTS fifthgift (fifth_id integer primary key, fifth_name text, fifth_desc text, fifth_user integer, fifth_menue_id integer, fifth_bill integer, fifth_got_spend integer, fifth_institution integer, fifth_office integer, fifth_when integer, fifth_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                var fifthArr = data[0].fifthArr;
                if (fifthArr.length > 0) {
                    _this.fifthArrFuncIns(0, fifthArr);
                }
            }
        })
            .catch(function () { });
        // DB RESERVATION
        this.database.executeSql("CREATE TABLE IF NOT EXISTS reservation (reservation_id integer primary key, reservation_userid integer, reservation_surname text, reservation_name text, reservation_middlename text, reservation_mobile integer, reservation_date integer, reservation_time integer, reservation_comment text, reservation_institution integer, reservation_when integer, reservation_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                var reservArr = data[0].reservArr;
                if (reservArr.length > 0) {
                    _this.reservArrFuncIns(0, reservArr);
                }
            }
        }).catch(function () { });
        // DB ORGANIZATION
        this.database.executeSql("CREATE TABLE IF NOT EXISTS organizations (org_id integer primary key, org_name text, org_country integer, org_city integer, org_adress text, org_timezone integer, org_tel text, org_fax text, org_mob text, org_email text, org_skype text, org_site text, org_tax_id text, org_logo text, org_sound integer, org_development integer, org_money_points integer, org_starting_points integer, org_money_percent integer, org_points_points integer, org_promo_points_owner integer, org_promo_points_scan_owner integer, org_promo_points_involved integer, org_promo_points_scan_involved integer, org_max_points integer, org_risk_summ integer, org_autoaprove integer, org_appvers integer, org_appurl text, org_log integer, org_reg integer, org_del integer)", {})
            .then(function () {
            if (data[0].newusr == '1') {
                // DB ORGANIZATION
                var orgArr = data[0].orgArr;
                if (orgArr.length > 0) {
                    _this.orgArrFuncIns(0, orgArr);
                }
            }
        })
            .catch(function () { });
        // DB ROOMS
        this.database.executeSql("CREATE TABLE IF NOT EXISTS rooms (room_id integer primary key, room_name text, room_desc text, room_employee integer, room_menue_exe text, room_priority integer, room_institution integer, room_office integer, room_upd integer, room_when integer, room_del integer)", {}).then(function () { })
            .catch(function () { });
    };
    BackendProvider.prototype.checkWaiter = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                var waiterstr = {
                    inst_id: _this.institution,
                    newusr: 'waiter'
                };
                _this.httpRequest(JSON.stringify(waiterstr))
                    .subscribe(function (waitsuc) {
                    resolve(1);
                    _this.getWaiter(waitsuc, 1);
                }, function (error) {
                    resolve(0);
                    console.log(JSON.stringify(error));
                });
            }, 2000);
        });
    };
    BackendProvider.prototype.orderArrFuncIns = function (id, orderArr) {
        var _this = this;
        var order_id = orderArr[id]['order_id'];
        var order_user = orderArr[id]['order_user'];
        var order_user_name_phone = orderArr[id]['order_user_name_phone'];
        var order_user_surname_phone = orderArr[id]['order_user_surname_phone'];
        var order_user_middlename_phone = orderArr[id]['order_user_middlename_phone'];
        var order_user_adress_phone = orderArr[id]['order_user_adress_phone'];
        var order_user_comment_phone = orderArr[id]['order_user_comment_phone'];
        var order_name = orderArr[id]['order_name'];
        var order_name_phone = orderArr[id]['order_name_phone'];
        var order_user_phone_phone = orderArr[id]['order_user_phone_phone'];
        var order_user_email_phone = orderArr[id]['order_user_email_phone'];
        var order_desc = orderArr[id]['order_desc'];
        var order_worker = orderArr[id]['order_worker'];
        var order_worker_name_phone = orderArr[id]['order_worker_name_phone'];
        var order_worker_pic_phone = orderArr[id]['order_worker_pic_phone'];
        var order_worker_profession_phone = orderArr[id]['order_worker_profession_phone'];
        var order_reminder_phone = orderArr[id]['order_reminder_phone'];
        var order_institution = orderArr[id]['order_institution'];
        var order_office = orderArr[id]['order_office'];
        var order_room = orderArr[id]['order_room'];
        var order_bill = orderArr[id]['order_bill'];
        var order_goods = orderArr[id]['order_goods'];
        var order_cats = orderArr[id]['order_cats'];
        var order_order = orderArr[id]['order_order'];
        var order_status = orderArr[id]['order_status'];
        var order_start = orderArr[id]['order_start'];
        var order_start_name_phone = orderArr[id]['order_start_name_phone'];
        var order_end = orderArr[id]['order_end'];
        var order_allday = orderArr[id]['order_allday'];
        var order_mobile_confirm = orderArr[id]['order_mobile_confirm'];
        var order_mobile = orderArr[id]['order_mobile'];
        var order_when = orderArr[id]['order_when'];
        var order_del = orderArr[id]['order_del'];
        var queryOrdering = "SELECT * FROM ordering WHERE order_id = ?";
        this.database.executeSql(queryOrdering, [order_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                // DB ORDERS
                var orderingUpd = "UPDATE ordering SET order_user=?, order_user_name_phone=?, order_user_surname_phone=?, order_user_middlename_phone=?, order_user_adress_phone=?, order_user_comment_phone=?, order_name=?, order_name_phone=?, order_user_phone_phone=?, order_user_email_phone=?, order_desc=?, order_worker=?, order_worker_name_phone=?, order_worker_pic_phone=?, order_worker_profession_phone=?, order_reminder_phone=?, order_institution=?, order_office=?, order_room=?, order_bill=?, order_goods=?, order_cats=?, order_order=?, order_status=?,  order_start=?, order_start_name_phone=?, order_end=?, order_allday=?, order_mobile_confirm=?, order_mobile=?, order_when=?, order_del=? WHERE order_id=?";
                _this.database.executeSql(orderingUpd, [order_user, order_user_name_phone, order_user_surname_phone, order_user_middlename_phone, order_user_adress_phone, order_user_comment_phone, order_name, order_name_phone, order_user_phone_phone, order_user_email_phone, order_desc, order_worker, order_worker_name_phone, order_worker_pic_phone, order_worker_profession_phone, order_reminder_phone, order_institution, order_office, order_room, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_start_name_phone, order_end, order_allday, order_mobile_confirm, order_mobile, order_when, order_del, order_id])
                    .then(function () {
                    id++;
                    if (id < orderArr.length) {
                        _this.orderArrFuncIns(id, orderArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var orderIns = "INSERT INTO ordering (order_id, order_user, order_user_name_phone, order_user_surname_phone, order_user_middlename_phone, order_user_adress_phone, order_user_comment_phone, order_name, order_name_phone, order_user_phone_phone, order_user_email_phone, order_desc, order_worker, order_worker_name_phone, order_worker_pic_phone, order_worker_profession_phone, order_reminder_phone, order_institution, order_office, order_room, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_start_name_phone, order_end, order_allday, order_mobile_confirm, order_mobile, order_when, order_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(orderIns, [order_id, order_user, order_user_name_phone, order_user_surname_phone, order_user_middlename_phone, order_user_adress_phone, order_user_comment_phone, order_name, order_name_phone, order_user_phone_phone, order_user_email_phone, order_desc, order_worker, order_worker_name_phone, order_worker_pic_phone, order_worker_profession_phone, order_reminder_phone, order_institution, order_office, order_room, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_start_name_phone, order_end, order_allday, order_mobile_confirm, order_mobile, order_when, order_del])
                    .then(function (ins) {
                    id++;
                    if (id < orderArr.length) {
                        _this.orderArrFuncIns(id, orderArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.schArrFunc = function (id, schArr) {
        var _this = this;
        var schedule_id = schArr[id]['schedule_id'];
        var schedule_employee = schArr[id]['schedule_employee'];
        var schedule_menue = schArr[id]['schedule_menue'];
        var schedule_office = schArr[id]['schedule_office'];
        var schedule_start = schArr[id]['schedule_start'];
        var schedule_stop = schArr[id]['schedule_stop'];
        var schedule_institution = schArr[id]['schedule_institution'];
        var schedule_when = schArr[id]['schedule_when'];
        var schedule_del = schArr[id]['schedule_del'];
        var querySchedule = "SELECT * FROM schedule WHERE schedule_id = ?";
        this.database.executeSql(querySchedule, [schedule_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                // DB SCHEDULE
                var scheduleUpd = "UPDATE schedule SET schedule_employee=?, schedule_menue=?, schedule_office=?, schedule_start=?, schedule_stop=?, schedule_institution=?, schedule_when=?, schedule_del=? WHERE schedule_id=?";
                _this.database.executeSql(scheduleUpd, [schedule_employee, schedule_menue, schedule_office, schedule_start, schedule_stop, schedule_institution, schedule_when, schedule_del, schedule_id])
                    .then(function () {
                    id++;
                    if (id < schArr.length) {
                        _this.schArrFunc(id, schArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                // DB SCHEDULE
                var scheduleIns = "INSERT INTO schedule (schedule_id, schedule_employee, schedule_menue, schedule_office, schedule_start, schedule_stop, schedule_institution, schedule_when, schedule_del) VALUES (?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(scheduleIns, [schedule_id, schedule_employee, schedule_menue, schedule_office, schedule_start, schedule_stop, schedule_institution, schedule_when, schedule_del])
                    .then(function () {
                    id++;
                    if (id < schArr.length) {
                        _this.schArrFunc(id, schArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.roomArrFunc = function (id, roomArr) {
        var _this = this;
        var room_id = roomArr[id]['room_id'];
        var room_name = roomArr[id]['room_name'];
        var room_desc = roomArr[id]['room_desc'];
        var room_employee = roomArr[id]['room_employee'];
        var room_menue_exe = roomArr[id]['room_menue_exe'];
        var room_priority = roomArr[id]['room_priority'];
        var room_institution = roomArr[id]['room_institution'];
        var room_office = roomArr[id]['room_office'];
        var room_upd = roomArr[id]['room_upd'];
        var room_when = roomArr[id]['room_when'];
        var room_del = roomArr[id]['room_del'];
        var queryRooms = "SELECT * FROM rooms WHERE room_id = ?";
        this.database.executeSql(queryRooms, [room_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                // DB ROOMS
                var roomUpd = "UPDATE rooms SET room_name=?, room_desc=?, room_employee=?, room_menue_exe=?, room_priority=?, room_institution=?, room_office=?, room_upd=?, room_when=?, room_del=? WHERE room_id=?";
                _this.database.executeSql(roomUpd, [room_name, room_desc, room_employee, room_menue_exe, room_priority, room_institution, room_office, room_upd, room_when, room_del, room_id])
                    .then(function () {
                    // alert('upd ok')
                    id++;
                    if (id < roomArr.length) {
                        _this.roomArrFunc(id, roomArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                // DB ROOMS
                var roomIns = "INSERT INTO rooms (room_id, room_name, room_desc, room_employee, room_menue_exe, room_priority, room_institution, room_office, room_upd, room_when, room_del) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(roomIns, [room_id, room_name, room_desc, room_employee, room_menue_exe, room_priority, room_institution, room_office, room_upd, room_when, room_del])
                    .then(function () {
                    // alert('ins ok')
                    id++;
                    if (id < roomArr.length) {
                        _this.roomArrFunc(id, roomArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.offArrFunc = function (id, offArr) {
        var _this = this;
        var office_id = offArr[id]['office_id'];
        var office_name = offArr[id]['office_name'];
        var office_start = offArr[id]['office_start'];
        var office_stop = offArr[id]['office_stop'];
        var office_bus_hours = offArr[id]['office_bus_hours'];
        var office_country = offArr[id]['office_country'];
        var office_city = offArr[id]['office_city'];
        var office_adress = offArr[id]['office_adress'];
        var office_lat = offArr[id]['office_lat'];
        var office_lon = offArr[id]['office_lon'];
        var office_desc = offArr[id]['office_desc'];
        var office_timezone = offArr[id]['office_timezone'];
        var office_tel = offArr[id]['office_tel'];
        var office_tel_n = offArr[id]['office_tel_n'];
        var office_fax = offArr[id]['office_fax'];
        var office_mob = offArr[id]['office_mob'];
        var office_email = offArr[id]['office_email'];
        var office_goods = offArr[id]['office_goods'];
        var office_categories = offArr[id]['office_categories'];
        var office_menue = offArr[id]['office_menue'];
        var office_orders = offArr[id]['office_orders'];
        var office_skype = offArr[id]['office_skype'];
        var office_site = offArr[id]['office_site'];
        var office_logo = offArr[id]['office_logo'];
        var office_institution = offArr[id]['office_institution'];
        var office_del = offArr[id]['office_del'];
        var queryOffice = "SELECT * FROM organizations_office WHERE office_id = ?";
        this.database.executeSql(queryOffice, [office_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                // DB OFFICE
                var officeUpd = "UPDATE organizations_office SET office_name=?, office_start=?, office_stop=?, office_bus_hours=?, office_country=?, office_city=?, office_adress=?, office_lat=?, office_lon=?, office_desc=?, office_timezone=?, office_tel=?, office_tel_n=?, office_fax=?, office_mob=?, office_email=?, office_goods=?, office_categories=?, office_menue=?, office_orders=?, office_skype=?, office_site=?, office_logo=?, office_institution=?, office_del=? WHERE office_id=?";
                _this.database.executeSql(officeUpd, [office_name, office_start, office_stop, office_bus_hours, office_country, office_city, office_adress, office_lat, office_lon, office_desc, office_timezone, office_tel, office_tel_n, office_fax, office_mob, office_email, office_goods, office_categories, office_menue, office_orders, office_skype, office_site, office_logo, office_institution, office_del, office_id])
                    .then(function () {
                    // alert('upd ok')
                    id++;
                    if (id < offArr.length) {
                        _this.offArrFunc(id, offArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                // DB OFFICE
                var officeIns = "INSERT INTO organizations_office (office_id, office_name, office_start, office_stop, office_bus_hours, office_country, office_city, office_adress, office_lat, office_lon, office_desc, office_timezone, office_tel, office_tel_n, office_fax, office_mob, office_email, office_goods, office_categories, office_menue, office_orders, office_skype, office_site, office_logo, office_institution, office_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(officeIns, [office_id, office_name, office_start, office_stop, office_bus_hours, office_country, office_city, office_adress, office_lat, office_lon, office_desc, office_timezone, office_tel, office_tel_n, office_fax, office_mob, office_email, office_goods, office_categories, office_menue, office_orders, office_skype, office_site, office_logo, office_institution, office_del])
                    .then(function () {
                    // alert('ins ok')
                    id++;
                    if (id < offArr.length) {
                        _this.offArrFunc(id, offArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.profArrFunc = function (id, profArr) {
        var _this = this;
        var prof_id = profArr[id]['prof_id'];
        var prof_name = profArr[id]['prof_name'];
        var prof_desc = profArr[id]['prof_desc'];
        var prof_institution = profArr[id]['prof_institution'];
        var prof_when = profArr[id]['prof_when'];
        var prof_del = profArr[id]['prof_del'];
        var queryProfs = "SELECT * FROM professions WHERE prof_id = ?";
        this.database.executeSql(queryProfs, [prof_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                // DB PROFESSIONS
                var profUpd = "UPDATE professions SET prof_name=?, prof_desc=?, prof_institution=?, prof_when=?, prof_del=? WHERE prof_id=?";
                _this.database.executeSql(profUpd, [prof_name, prof_desc, prof_institution, prof_when, prof_del, prof_id])
                    .then(function () {
                    // alert('upd ok')
                    id++;
                    if (id < profArr.length) {
                        _this.profArrFunc(id, profArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                // DB PROFESSIONS
                var profIns = "INSERT INTO professions (prof_id, prof_name, prof_desc, prof_institution, prof_when, prof_del) VALUES (?,?,?,?,?,?)";
                _this.database.executeSql(profIns, [prof_id, prof_name, prof_desc, prof_institution, prof_when, prof_del])
                    .then(function () {
                    // alert('ins ok')
                    id++;
                    if (id < profArr.length) {
                        _this.profArrFunc(id, profArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.usrArrFunc = function (id, usrArr) {
        var _this = this;
        var user_id = usrArr[id]['user_id'];
        var user_name = usrArr[id]['user_name'];
        var user_surname = usrArr[id]['user_surname'];
        var user_middlename = usrArr[id]['user_middlename'];
        var user_mob = usrArr[id]['user_mob'];
        var user_info = usrArr[id]['user_info'];
        var user_work_pos = usrArr[id]['user_work_pos'];
        var user_menue_exe = usrArr[id]['user_menue_exe'];
        var user_pic = usrArr[id]['user_pic'];
        var user_gender = usrArr[id]['user_gender'];
        var user_institution = usrArr[id]['user_institution'];
        var user_office = usrArr[id]['user_office'];
        var user_reg = usrArr[id]['user_reg'];
        var user_del = usrArr[id]['user_del'];
        var queryWaiters = "SELECT * FROM users WHERE user_real_id = ?";
        this.database.executeSql(queryWaiters, [user_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                // DB WAITERS
                var usrUpd = "UPDATE users SET user_name=?, user_surname=?, user_middlename=?, user_mob=?, user_info=?, user_work_pos=?, user_menue_exe=?, user_pic=?, user_gender=?, user_institution=?, user_office=?, user_reg=?, user_del=? WHERE user_real_id=?";
                _this.database.executeSql(usrUpd, [user_name, user_surname, user_middlename, user_mob, user_info, user_work_pos, user_menue_exe, user_pic, user_gender, user_institution, user_office, user_reg, user_del, user_id])
                    .then(function () {
                    id++;
                    if (id < usrArr.length) {
                        _this.usrArrFunc(id, usrArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                // DB WAITERS
                var usrIns = "INSERT INTO users (user_real_id, user_name, user_surname, user_middlename, user_mob, user_info, user_work_pos, user_menue_exe, user_pic, user_gender, user_institution, user_office, user_reg, user_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(usrIns, [user_id, user_name, user_surname, user_middlename, user_mob, user_info, user_work_pos, user_menue_exe, user_pic, user_gender, user_institution, user_office, user_reg, user_del])
                    .then(function () {
                    // alert('ins ok')
                    id++;
                    if (id < usrArr.length) {
                        _this.usrArrFunc(id, usrArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.orgArrFunc = function (id, orgArr) {
        var _this = this;
        var org_id = orgArr[id]['org_id'];
        var org_name = orgArr[id]['org_name'];
        var org_country = orgArr[id]['org_country'];
        var org_city = orgArr[id]['org_city'];
        var org_adress = orgArr[id]['org_adress'];
        var org_timezone = orgArr[id]['org_timezone'];
        var org_tel = orgArr[id]['org_tel'];
        var org_fax = orgArr[id]['org_fax'];
        var org_mob = orgArr[id]['org_mob'];
        var org_email = orgArr[id]['org_email'];
        var org_skype = orgArr[id]['org_skype'];
        var org_site = orgArr[id]['org_site'];
        var org_tax_id = orgArr[id]['org_tax_id'];
        var org_logo = orgArr[id]['org_logo'];
        var org_sound = orgArr[id]['org_sound'];
        var org_development = orgArr[id]['org_development'];
        var org_money_points = orgArr[id]['org_money_points'];
        var org_starting_points = orgArr[id]['org_starting_points'];
        var org_money_percent = orgArr[id]['org_money_percent'];
        var org_points_points = orgArr[id]['org_points_points'];
        var org_promo_points_owner = orgArr[id]['org_promo_points_owner'];
        var org_promo_points_scan_owner = orgArr[id]['org_promo_points_scan_owner'];
        var org_promo_points_involved = orgArr[id]['org_promo_points_involved'];
        var org_promo_points_scan_involved = orgArr[id]['org_promo_points_scan_involved'];
        var org_max_points = orgArr[id]['org_max_points'];
        var org_risk_summ = orgArr[id]['org_risk_summ'];
        var org_autoaprove = orgArr[id]['org_autoaprove'];
        var org_appvers = orgArr[id]['org_appvers'];
        var org_appurl = orgArr[id]['org_appurl'];
        var org_log = orgArr[id]['org_log'];
        var org_reg = orgArr[id]['org_reg'];
        var org_del = orgArr[id]['org_del'];
        var queryOrg = "SELECT * FROM organizations WHERE org_id = ?";
        this.database.executeSql(queryOrg, [org_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var orgUpd = "UPDATE organizations SET org_name=?, org_country=?, org_city=?, org_adress=?, org_timezone=?, org_tel=?, org_fax=?, org_mob=?, org_email=?, org_skype=?, org_site=?, org_tax_id=?, org_logo=?, org_sound=?, org_development=?, org_money_points=?, org_starting_points=?, org_money_percent=?, org_points_points=?, org_promo_points_owner=?, org_promo_points_scan_owner=?, org_promo_points_involved=?, org_promo_points_scan_involved=?, org_max_points=?, org_risk_summ=?, org_autoaprove=?, org_appvers=?, org_appurl=?, org_log=?, org_reg=?, org_del=? WHERE org_id=?";
                _this.database.executeSql(orgUpd, [org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del, org_id])
                    .then(function () {
                    id++;
                    if (id < orgArr.length) {
                        _this.orgArrFunc(id, orgArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var orgIns = "INSERT INTO organizations (org_id, org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(orgIns, [org_id, org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del])
                    .then(function () {
                    id++;
                    if (id < orgArr.length) {
                        _this.orgArrFunc(id, orgArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.orgArrFuncIns = function (id, orgArr) {
        var _this = this;
        var org_id = orgArr[id]['org_id'];
        var org_name = orgArr[id]['org_name'];
        var org_country = orgArr[id]['org_country'];
        var org_city = orgArr[id]['org_city'];
        var org_adress = orgArr[id]['org_adress'];
        var org_timezone = orgArr[id]['org_timezone'];
        var org_tel = orgArr[id]['org_tel'];
        var org_fax = orgArr[id]['org_fax'];
        var org_mob = orgArr[id]['org_mob'];
        var org_email = orgArr[id]['org_email'];
        var org_skype = orgArr[id]['org_skype'];
        var org_site = orgArr[id]['org_site'];
        var org_tax_id = orgArr[id]['org_tax_id'];
        var org_logo = orgArr[id]['org_logo'];
        var org_sound = orgArr[id]['org_sound'];
        var org_development = orgArr[id]['org_development'];
        var org_money_points = orgArr[id]['org_money_points'];
        var org_starting_points = orgArr[id]['org_starting_points'];
        var org_money_percent = orgArr[id]['org_money_percent'];
        var org_points_points = orgArr[id]['org_points_points'];
        var org_promo_points_owner = orgArr[id]['org_promo_points_owner'];
        var org_promo_points_scan_owner = orgArr[id]['org_promo_points_scan_owner'];
        var org_promo_points_involved = orgArr[id]['org_promo_points_involved'];
        var org_promo_points_scan_involved = orgArr[id]['org_promo_points_scan_involved'];
        var org_max_points = orgArr[id]['org_max_points'];
        var org_risk_summ = orgArr[id]['org_risk_summ'];
        var org_autoaprove = orgArr[id]['org_autoaprove'];
        var org_appvers = orgArr[id]['org_appvers'];
        var org_appurl = orgArr[id]['org_appurl'];
        var org_log = orgArr[id]['org_log'];
        var org_reg = orgArr[id]['org_reg'];
        var org_del = orgArr[id]['org_del'];
        var queryOrg = "SELECT * FROM organizations WHERE org_id = ?";
        this.database.executeSql(queryOrg, [org_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var orgUpd = "UPDATE organizations SET org_name=?, org_country=?, org_city=?, org_adress=?, org_timezone=?, org_tel=?, org_fax=?, org_mob=?, org_email=?, org_skype=?, org_site=?, org_tax_id=?, org_logo=?, org_sound=?, org_development=?, org_money_points=?, org_starting_points=?, org_money_percent=?, org_points_points=?, org_promo_points_owner=?, org_promo_points_scan_owner=?, org_promo_points_involved=?, org_promo_points_scan_involved=?, org_max_points=?, org_risk_summ=?, org_autoaprove=?, org_appvers=?, org_appurl=?, org_log=?, org_reg=?, org_del=? WHERE org_id=?";
                _this.database.executeSql(orgUpd, [org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del, org_id])
                    .then(function () {
                    id++;
                    if (id < orgArr.length) {
                        _this.orgArrFuncIns(id, orgArr);
                    }
                }).catch(function () { });
            }
            else {
                var orgIns = "INSERT INTO organizations (org_id, org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(orgIns, [org_id, org_name, org_country, org_city, org_adress, org_timezone, org_tel, org_fax, org_mob, org_email, org_skype, org_site, org_tax_id, org_logo, org_sound, org_development, org_money_points, org_starting_points, org_money_percent, org_points_points, org_promo_points_owner, org_promo_points_scan_owner, org_promo_points_involved, org_promo_points_scan_involved, org_max_points, org_risk_summ, org_autoaprove, org_appvers, org_appurl, org_log, org_reg, org_del])
                    .then(function () {
                    id++;
                    if (id < orgArr.length) {
                        _this.orgArrFuncIns(id, orgArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.reservArrFunc = function (id, reservArr) {
        var _this = this;
        var reservation_id = reservArr[id]['reservation_id'];
        var reservation_userid = reservArr[id]['reservation_userid'];
        var reservation_surname = reservArr[id]['reservation_surname'];
        var reservation_name = reservArr[id]['reservation_name'];
        var reservation_middlename = reservArr[id]['reservation_middlename'];
        var reservation_mobile = reservArr[id]['reservation_mobile'];
        var reservation_date = reservArr[id]['reservation_date'];
        var reservation_time = reservArr[id]['reservation_time'];
        var reservation_comment = reservArr[id]['reservation_comment'];
        var reservation_institution = reservArr[id]['reservation_institution'];
        var reservation_when = reservArr[id]['reservation_when'];
        var reservation_del = reservArr[id]['reservation_del'];
        var queryReserv = "SELECT * FROM reservation WHERE reservation_id = ?";
        this.database.executeSql(queryReserv, [reservation_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var chatUpd = "UPDATE chat SET reservation_userid=?, reservation_surname=?, reservation_name=?, reservation_middlename=?, reservation_mobile=?, reservation_date=?, reservation_time=?, reservation_comment=?, reservation_institution=?, reservation_when=?, reservation_del=? WHERE reservation_id=?";
                _this.database.executeSql(chatUpd, [reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del, reservation_id]).then(function () {
                    id++;
                    if (id < reservArr.length) {
                        _this.reservArrFunc(id, reservArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var reservIns = "INSERT INTO reservation (reservation_id, reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(reservIns, [reservation_id, reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del])
                    .then(function () {
                    id++;
                    if (id < reservArr.length) {
                        _this.reservArrFunc(id, reservArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.reservArrFuncIns = function (id, reservArr) {
        var _this = this;
        var reservation_id = reservArr[id]['reservation_id'];
        var reservation_userid = reservArr[id]['reservation_userid'];
        var reservation_surname = reservArr[id]['reservation_surname'];
        var reservation_name = reservArr[id]['reservation_name'];
        var reservation_middlename = reservArr[id]['reservation_middlename'];
        var reservation_mobile = reservArr[id]['reservation_mobile'];
        var reservation_date = reservArr[id]['reservation_date'];
        var reservation_time = reservArr[id]['reservation_time'];
        var reservation_comment = reservArr[id]['reservation_comment'];
        var reservation_institution = reservArr[id]['reservation_institution'];
        var reservation_when = reservArr[id]['reservation_when'];
        var reservation_del = reservArr[id]['reservation_del'];
        var queryReserv = "SELECT * FROM reservation WHERE reservation_id = ?";
        this.database.executeSql(queryReserv, [reservation_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var fifthUpd = "UPDATE fifthgift SET reservation_userid=?, reservation_surname=?, reservation_name=?, reservation_middlename=?, reservation_mobile=?, reservation_date=?, reservation_time=?, reservation_comment=?, reservation_institution=?, reservation_when=?, reservation_del=? WHERE reservation_id=?";
                _this.database.executeSql(fifthUpd, [reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del, reservation_id]).then(function () {
                    id++;
                    if (id < reservArr.length) {
                        _this.reservArrFuncIns(id, reservArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var reservIns = "INSERT INTO reservation (reservation_id, reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(reservIns, [reservation_id, reservation_userid, reservation_surname, reservation_name, reservation_middlename, reservation_mobile, reservation_date, reservation_time, reservation_comment, reservation_institution, reservation_when, reservation_del])
                    .then(function () {
                    id++;
                    if (id < reservArr.length) {
                        _this.reservArrFuncIns(id, reservArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.fifthArrFunc = function (id, fifthArr) {
        var _this = this;
        var fifth_id = fifthArr[id]['fifth_id'];
        var fifth_name = fifthArr[id]['fifth_name'];
        var fifth_desc = fifthArr[id]['fifth_desc'];
        var fifth_user = fifthArr[id]['fifth_user'];
        var fifth_menue_id = fifthArr[id]['fifth_menue_id'];
        var fifth_bill = fifthArr[id]['fifth_bill'];
        var fifth_got_spend = fifthArr[id]['fifth_got_spend'];
        var fifth_institution = fifthArr[id]['fifth_institution'];
        var fifth_office = fifthArr[id]['fifth_office'];
        var fifth_when = fifthArr[id]['fifth_when'];
        var fifth_del = fifthArr[id]['fifth_del'];
        var queryFifth = "SELECT * FROM fifthgift WHERE fifth_id = ?";
        this.database.executeSql(queryFifth, [fifth_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var chatUpd = "UPDATE fifthgift SET fifth_name=?, fifth_desc=?, fifth_user=?, fifth_menue_id=?, fifth_bill=?, fifth_got_spend=?, fifth_institution=?, fifth_office=?, fifth_when=?, fifth_del=? WHERE fifth_id=?";
                _this.database.executeSql(chatUpd, [fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del, fifth_id])
                    .then(function () {
                    id++;
                    if (id < fifthArr.length) {
                        _this.fifthArrFunc(id, fifthArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var fifthIns = "INSERT INTO fifthgift (fifth_id, fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(fifthIns, [fifth_id, fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del])
                    .then(function () {
                    id++;
                    if (id < fifthArr.length) {
                        _this.fifthArrFunc(id, fifthArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.fifthArrFuncIns = function (id, fifthArr) {
        var _this = this;
        var fifth_id = fifthArr[id]['fifth_id'];
        var fifth_name = fifthArr[id]['fifth_name'];
        var fifth_desc = fifthArr[id]['fifth_desc'];
        var fifth_user = fifthArr[id]['fifth_user'];
        var fifth_menue_id = fifthArr[id]['fifth_menue_id'];
        var fifth_bill = fifthArr[id]['fifth_bill'];
        var fifth_got_spend = fifthArr[id]['fifth_got_spend'];
        var fifth_institution = fifthArr[id]['fifth_institution'];
        var fifth_office = fifthArr[id]['fifth_office'];
        var fifth_when = fifthArr[id]['fifth_when'];
        var fifth_del = fifthArr[id]['fifth_del'];
        var queryFifth = "SELECT * FROM fifthgift WHERE fifth_id = ?";
        this.database.executeSql(queryFifth, [fifth_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var fifthUpd = "UPDATE fifthgift SET fifth_name=?, fifth_desc=?, fifth_user=?, fifth_menue_id=?, fifth_bill=?, fifth_got_spend=?, fifth_institution=?, fifth_office=?, fifth_when=?, fifth_del=? WHERE fifth_id=?";
                _this.database.executeSql(fifthUpd, [fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del, fifth_id])
                    .then(function () {
                    id++;
                    if (id < fifthArr.length) {
                        _this.fifthArrFuncIns(id, fifthArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var fifthIns = "INSERT INTO fifthgift (fifth_id, fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(fifthIns, [fifth_id, fifth_name, fifth_desc, fifth_user, fifth_menue_id, fifth_bill, fifth_got_spend, fifth_institution, fifth_office, fifth_when, fifth_del])
                    .then(function () {
                    id++;
                    if (id < fifthArr.length) {
                        _this.fifthArrFuncIns(id, fifthArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.chatArrFunc = function (id, chatArr) {
        var _this = this;
        var chat_id = chatArr[id]['chat_id'];
        var chat_from = chatArr[id]['chat_from'];
        var chat_to = chatArr[id]['chat_to'];
        var chat_name = chatArr[id]['chat_name'];
        var chat_message = chatArr[id]['chat_message'];
        var chat_read = chatArr[id]['chat_read'];
        var chat_institution = chatArr[id]['chat_institution'];
        var chat_answered = chatArr[id]['chat_answered'];
        var chat_when = chatArr[id]['chat_when'];
        var chat_del = chatArr[id]['chat_del'];
        var queryChat = "SELECT * FROM chat WHERE chat_id = ?";
        this.database.executeSql(queryChat, [chat_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var chatUpd = "UPDATE chat SET chat_read=?, chat_answered=?, chat_when=?, chat_del=? WHERE chat_id=?";
                _this.database.executeSql(chatUpd, [chat_read, chat_answered, chat_when, chat_del, chat_id])
                    .then(function () {
                    id++;
                    if (id < chatArr.length) {
                        _this.chatArrFunc(id, chatArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(chatIns, [chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del])
                    .then(function () {
                    id++;
                    if (id < chatArr.length) {
                        _this.chatArrFunc(id, chatArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.chatArrFuncIns = function (id, chatArr) {
        var _this = this;
        var chat_id = chatArr[id]['chat_id'];
        var chat_from = chatArr[id]['chat_from'];
        var chat_to = chatArr[id]['chat_to'];
        var chat_name = chatArr[id]['chat_name'];
        var chat_message = chatArr[id]['chat_message'];
        var chat_read = chatArr[id]['chat_read'];
        var chat_institution = chatArr[id]['chat_institution'];
        var chat_answered = chatArr[id]['chat_answered'];
        var chat_when = chatArr[id]['chat_when'];
        var chat_del = chatArr[id]['chat_del'];
        var chatIns = "INSERT INTO chat (chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
        this.database.executeSql(chatIns, [chat_id, chat_from, chat_to, chat_name, chat_message, chat_read, chat_institution, chat_answered, chat_when, chat_del]).then(function () {
            id++;
            if (id < chatArr.length) {
                _this.chatArrFuncIns(id, chatArr);
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.asksArrFunc = function (id, asksArr) {
        var _this = this;
        var asks_id = asksArr[id]['asks_id'];
        var asks_name = asksArr[id]['asks_name'];
        var asks_message = asksArr[id]['asks_message'];
        var asks_type = asksArr[id]['asks_type'];
        var asks_chained = asksArr[id]['asks_chained'];
        var asks_active = asksArr[id]['asks_active'];
        var asks_img = asksArr[id]['asks_img'];
        var asks_answ = asksArr[id]['asks_answ'];
        var asks_yes = asksArr[id]['asks_yes'];
        var asks_no = asksArr[id]['asks_no'];
        var asks_institution = asksArr[id]['asks_institution'];
        var asks_del = asksArr[id]['asks_del'];
        var asks_when = asksArr[id]['asks_when'];
        var queryAsks = "SELECT * FROM asks WHERE asks_id = ?";
        this.database.executeSql(queryAsks, [asks_id]).then(function (suc) {
            if (suc.rows.length == 0) {
                var newsIns = "INSERT INTO asks (asks_id, asks_name, asks_message, asks_type, asks_chained, asks_active, asks_img, asks_answ, asks_yes, asks_no, asks_reply, asks_institution, asks_when, asks_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(newsIns, [asks_id, asks_name, asks_message, asks_type, asks_chained, asks_active, asks_img, asks_answ, asks_yes, asks_no, 0, asks_institution, asks_when, asks_del])
                    .then(function () {
                    id++;
                    if (id < asksArr.length) {
                        _this.asksArrFunc(id, asksArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var asksIns = "UPDATE asks SET asks_name=?, asks_message=?, asks_type=?, asks_chained=?, asks_active=?, asks_img=?, asks_answ=?, asks_yes=?, asks_no=?, asks_institution=?, asks_when=?, asks_del=? WHERE asks_id=?";
                _this.database.executeSql(asksIns, [asks_name, asks_message, asks_type, asks_chained, asks_active, asks_img, asks_answ, asks_yes, asks_no, asks_institution, asks_when, asks_del, asks_id])
                    .then(function (ins) {
                    id++;
                    if (id < asksArr.length) {
                        _this.asksArrFunc(id, asksArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.asksArrFuncIns = function (id, asksArr) {
        var _this = this;
        var asks_id = asksArr[id]['asks_id'];
        var asks_name = asksArr[id]['asks_name'];
        var asks_message = asksArr[id]['asks_message'];
        var asks_type = asksArr[id]['asks_type'];
        var asks_chained = asksArr[id]['asks_chained'];
        var asks_active = asksArr[id]['asks_active'];
        var asks_img = asksArr[id]['asks_img'];
        var asks_answ = asksArr[id]['asks_answ'];
        var asks_yes = asksArr[id]['asks_yes'];
        var asks_no = asksArr[id]['asks_no'];
        var asks_institution = asksArr[id]['asks_institution'];
        var asks_when = asksArr[id]['asks_when'];
        var asks_del = asksArr[id]['asks_del'];
        var asksIns = "INSERT INTO asks (asks_id, asks_name, asks_message, asks_type, asks_chained, asks_active, asks_img, asks_answ, asks_yes, asks_no, asks_reply, asks_institution, asks_when, asks_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        this.database.executeSql(asksIns, [asks_id, asks_name, asks_message, asks_type, asks_chained, asks_active, asks_img, asks_answ, asks_yes, asks_no, '0', asks_institution, asks_when, asks_del])
            .then(function () {
            id++;
            if (id < asksArr.length) {
                _this.asksArrFuncIns(id, asksArr);
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.reviewsArrFunc = function (id, reviewsArr) {
        var _this = this;
        var reviews_id = reviewsArr[id]['reviews_id'];
        var reviews_from = reviewsArr[id]['reviews_from'];
        var reviews_to = reviewsArr[id]['reviews_to'];
        var reviews_message = reviewsArr[id]['reviews_message'];
        var reviews_pic = reviewsArr[id]['reviews_pic'];
        var reviews_institution = reviewsArr[id]['reviews_institution'];
        var reviews_answered = reviewsArr[id]['reviews_answered'];
        var reviews_opened = reviewsArr[id]['reviews_opened'];
        var reviews_when = reviewsArr[id]['reviews_when'];
        var reviews_del = reviewsArr[id]['reviews_del'];
        var queryReviews = "SELECT * FROM reviews WHERE reviews_id = ?";
        this.database.executeSql(queryReviews, [reviews_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var reviewsUpd = "UPDATE reviews SET reviews_from=?, reviews_to=?, reviews_message=?, reviews_pic=?, reviews_institution=?, reviews_answered=?, reviews_opened=?, reviews_when=?, reviews_del=? WHERE reviews_id=?";
                _this.database.executeSql(reviewsUpd, [reviews_from, reviews_to, reviews_message, reviews_pic, reviews_institution, reviews_answered, reviews_opened, reviews_when, reviews_del, reviews_id]).then(function () {
                    id++;
                    if (id < reviewsArr.length) {
                        _this.reviewsArrFunc(id, reviewsArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var newsIns = "INSERT INTO reviews (reviews_id, reviews_from, reviews_to, reviews_message, reviews_pic, reviews_institution, reviews_answered, reviews_opened, reviews_when, reviews_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(newsIns, [reviews_id, reviews_from, reviews_to, reviews_message, reviews_pic, reviews_institution, reviews_answered, reviews_when, reviews_del])
                    .then(function () {
                    id++;
                    if (id < reviewsArr.length) {
                        _this.reviewsArrFunc(id, reviewsArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.reviewsArrFuncIns = function (id, reviewsArr) {
        var _this = this;
        var reviews_id = reviewsArr[id]['reviews_id'];
        var reviews_from = reviewsArr[id]['reviews_from'];
        var reviews_to = reviewsArr[id]['reviews_to'];
        var reviews_message = reviewsArr[id]['reviews_message'];
        var reviews_pic = reviewsArr[id]['reviews_pic'];
        var reviews_institution = reviewsArr[id]['reviews_institution'];
        var reviews_answered = reviewsArr[id]['reviews_answered'];
        var reviews_opened = reviewsArr[id]['reviews_opened'];
        var reviews_when = reviewsArr[id]['reviews_when'];
        var reviews_del = reviewsArr[id]['reviews_del'];
        var newsIns = "INSERT INTO reviews (reviews_id, reviews_from, reviews_to, reviews_message, reviews_pic, reviews_institution, reviews_answered, reviews_opened, reviews_when, reviews_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
        this.database.executeSql(newsIns, [reviews_id, reviews_from, reviews_to, reviews_message, reviews_pic, reviews_institution, reviews_answered, reviews_opened, reviews_when, reviews_del])
            .then(function () {
            id++;
            if (id < reviewsArr.length) {
                _this.reviewsArrFuncIns(id, reviewsArr);
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.newsArrFunc = function (id, newsArr) {
        var _this = this;
        var news_id = newsArr[id]['news_id'];
        var news_state = newsArr[id]['news_state'];
        var news_menue_id = 0;
        var news_cost = 0;
        var news_user = 0;
        var news_used = 0;
        var news_begin = 0;
        var news_end = 0;
        var news_when = newsArr[id]['news_when'];
        var news_del = newsArr[id]['news_del'];
        var news_name = 0;
        var news_message = 0;
        var news_pic = 0;
        var news_institution = 0;
        if (news_state == '1') {
            news_name = newsArr[id]['news_name'];
            news_message = newsArr[id]['news_message'];
            news_pic = newsArr[id]['news_pic'];
            news_institution = newsArr[id]['news_institution'];
            news_menue_id = newsArr[id]['news_menue_id'];
            news_cost = newsArr[id]['news_cost'];
            news_user = newsArr[id]['news_user'];
            news_used = newsArr[id]['news_used'];
            news_begin = newsArr[id]['news_begin'];
            news_end = newsArr[id]['news_end'];
        }
        var queryNews = "SELECT * FROM news WHERE news_id = ?";
        this.database.executeSql(queryNews, [news_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var newsUpd = "UPDATE news SET news_name=?, news_message=?, news_pic=?, news_institution=?, news_state=?, news_menue_id=?, news_cost=?, news_user=?, news_used=?, news_begin=?, news_end=?, news_when=?, news_del=? WHERE news_id=?";
                _this.database.executeSql(newsUpd, [news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del, news_id])
                    .then(function () {
                    id++;
                    if (id < newsArr.length) {
                        _this.newsArrFunc(id, newsArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var newsIns = "INSERT INTO news (news_id, news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(newsIns, [news_id, news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del])
                    .then(function () {
                    id++;
                    if (id < newsArr.length) {
                        _this.newsArrFunc(id, newsArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.newsArrFuncIns = function (id, newsArr) {
        var _this = this;
        var news_id = newsArr[id]['news_id'];
        var news_state = newsArr[id]['news_state'];
        var news_menue_id = 0;
        var news_cost = 0;
        var news_user = 0;
        var news_used = 0;
        var news_begin = 0;
        var news_end = 0;
        var news_when = newsArr[id]['news_when'];
        var news_del = newsArr[id]['news_del'];
        var news_name = 0;
        var news_message = 0;
        var news_pic = 0;
        var news_institution = 0;
        if (news_state == '1') {
            news_name = newsArr[id]['news_name'];
            news_message = newsArr[id]['news_message'];
            news_pic = newsArr[id]['news_pic'];
            news_institution = newsArr[id]['news_institution'];
            news_menue_id = newsArr[id]['news_menue_id'];
            news_cost = newsArr[id]['news_cost'];
            news_user = newsArr[id]['news_user'];
            news_used = newsArr[id]['news_used'];
            news_begin = newsArr[id]['news_begin'];
            news_end = newsArr[id]['news_end'];
        }
        var queryNews = "SELECT * FROM news WHERE news_id = ?";
        this.database.executeSql(queryNews, [news_id]).then(function (suc) {
            if (suc.rows.length > 0) {
                var newsUpd = "UPDATE news SET news_name=?, news_message=?, news_pic=?, news_institution=?, news_state=?, news_menue_id=?, news_cost=?, news_user=?, news_used=?, news_begin=?, news_end=?, news_when=?, news_del=? WHERE news_id=?";
                _this.database.executeSql(newsUpd, [news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del, news_id])
                    .then(function () {
                    id++;
                    if (id < newsArr.length) {
                        _this.newsArrFuncIns(id, newsArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var newsIns = "INSERT INTO news (news_id, news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(newsIns, [news_id, news_name, news_message, news_pic, news_institution, news_state, news_menue_id, news_cost, news_user, news_used, news_begin, news_end, news_when, news_del])
                    .then(function () {
                    id++;
                    if (id < newsArr.length) {
                        _this.newsArrFuncIns(id, newsArr);
                    }
                })
                    .catch(function () { });
            }
        }).catch(function () { });
    };
    BackendProvider.prototype.ingrArrFunc = function (id, ingrArr) {
        var _this = this;
        var ingr_id = ingrArr[id]['ingr_id'];
        var ingr_xid = 0;
        var ingr_when = ingrArr[id]['ingr_when'];
        var ingr_del = ingrArr[id]['ingr_del'];
        var ingr_name = 0;
        var ingr_desc = 0;
        var ingr_cat = 0;
        var ingr_code = 0;
        var ingr_pic = 0;
        var ingr_size = 0;
        var ingr_cost = 0;
        var ingr_institution = 0;
        if (ingr_when != '1') {
            ingr_xid = ingrArr[id]['ingr_xid'];
            ingr_name = ingrArr[id]['ingr_name'];
            ingr_desc = ingrArr[id]['ingr_desc'];
            ingr_cat = ingrArr[id]['ingr_cat'];
            ingr_code = ingrArr[id]['ingr_code'];
            ingr_pic = ingrArr[id]['ingr_pic'];
            ingr_size = ingrArr[id]['ingr_size'];
            ingr_cost = ingrArr[id]['ingr_cost'];
            ingr_institution = ingrArr[id]['ingr_institution'];
        }
        var queryIngrs = "SELECT * FROM ingredients WHERE ingr_id = ?";
        this.database.executeSql(queryIngrs, [ingr_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var menueUpd = "UPDATE ingredients SET ingr_xid=?, ingr_name=?, ingr_desc=?, ingr_cat=?, ingr_code=?, ingr_pic=?, ingr_size=?, ingr_cost=?, ingr_institution=?, ingr_when=?, ingr_del=? WHERE ingr_id=?";
                _this.database.executeSql(menueUpd, [ingr_xid, ingr_name, ingr_desc, ingr_cat, ingr_code, ingr_pic, ingr_size, ingr_cost, ingr_institution, ingr_when, ingr_del, ingr_id])
                    .then(function () {
                    id++;
                    if (id < ingrArr.length) {
                        _this.ingrArrFunc(id, ingrArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var ingrsIns = "INSERT INTO ingredients (ingr_id, ingr_xid, ingr_name, ingr_desc, ingr_cat, ingr_code, ingr_pic, ingr_size, ingr_cost, ingr_institution, ingr_when, ingr_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(ingrsIns, [ingr_id, ingr_xid, ingr_name, ingr_desc, ingr_cat, ingr_code, ingr_pic, ingr_size, ingr_cost, ingr_institution, ingr_when, ingr_del])
                    .then(function () {
                    id++;
                    if (id < ingrArr.length) {
                        _this.ingrArrFunc(id, ingrArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.ingrArrFuncIns = function (id, ingrArr) {
        var _this = this;
        var ingr_id = ingrArr[id]['ingr_id'];
        var ingr_xid = ingrArr[id]['ingr_xid'];
        var ingr_name = ingrArr[id]['ingr_name'];
        var ingr_desc = ingrArr[id]['ingr_desc'];
        var ingr_cat = ingrArr[id]['ingr_cat'];
        var ingr_code = ingrArr[id]['ingr_code'];
        var ingr_pic = ingrArr[id]['ingr_pic'];
        var ingr_size = ingrArr[id]['ingr_size'];
        var ingr_cost = ingrArr[id]['ingr_cost'];
        var ingr_institution = ingrArr[id]['ingr_institution'];
        var ingr_when = ingrArr[id]['ingr_when'];
        var ingr_del = ingrArr[id]['ingr_del'];
        var ingrsIns = "INSERT INTO ingredients (ingr_id, ingr_xid, ingr_name, ingr_desc, ingr_cat, ingr_code, ingr_pic, ingr_size, ingr_cost, ingr_institution, ingr_when, ingr_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        this.database.executeSql(ingrsIns, [ingr_id, ingr_xid, ingr_name, ingr_desc, ingr_cat, ingr_code, ingr_pic, ingr_size, ingr_cost, ingr_institution, ingr_when, ingr_del])
            .then(function () {
            id++;
            if (id < ingrArr.length) {
                _this.ingrArrFuncIns(id, ingrArr);
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.giftsArrFunc = function (id, giftsArr) {
        var _this = this;
        var gifts_id = giftsArr[id]['gifts_id'];
        var gifts_name = giftsArr[id]['gifts_name'];
        var gifts_desc = giftsArr[id]['gifts_desc'];
        var gifts_points = giftsArr[id]['gifts_points'];
        var gifts_pic = giftsArr[id]['gifts_pic'];
        var gifts_icon = giftsArr[id]['gifts_icon'];
        var gifts_institution = giftsArr[id]['gifts_institution'];
        var gifts_when = giftsArr[id]['gifts_when'];
        var gifts_del = giftsArr[id]['gifts_del'];
        var queryGifts = "SELECT * FROM gifts WHERE gifts_id = ?";
        this.database.executeSql(queryGifts, [gifts_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var giftsUpd = "UPDATE gifts SET gifts_name=?, gifts_desc=?, gifts_points=?, gifts_pic=?, gifts_icon=?, gifts_institution=?, gifts_when=?, gifts_del=? WHERE gifts_id=?";
                _this.database.executeSql(giftsUpd, [gifts_name, gifts_desc, gifts_points, gifts_pic, gifts_icon, gifts_institution, gifts_when, gifts_del, gifts_id])
                    .then(function () {
                    id++;
                    if (id < giftsArr.length) {
                        _this.giftsArrFunc(id, giftsArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var giftsIns = "INSERT INTO gifts (gifts_id, gifts_name, gifts_desc, gifts_points, gifts_pic, gifts_icon, gifts_institution, gifts_when, gifts_del) VALUES (?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(giftsIns, [gifts_id, gifts_name, gifts_desc, gifts_points, gifts_pic, gifts_icon, gifts_institution, gifts_when, gifts_del])
                    .then(function () {
                    id++;
                    if (id < giftsArr.length) {
                        _this.giftsArrFunc(id, giftsArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.giftsArrFuncIns = function (id, giftsArr) {
        var _this = this;
        var gifts_id = giftsArr[id]['gifts_id'];
        var gifts_name = giftsArr[id]['gifts_name'];
        var gifts_desc = giftsArr[id]['gifts_desc'];
        var gifts_points = giftsArr[id]['gifts_points'];
        var gifts_pic = giftsArr[id]['gifts_pic'];
        var gifts_icon = giftsArr[id]['gifts_icon'];
        var gifts_institution = giftsArr[id]['gifts_institution'];
        var gifts_when = giftsArr[id]['gifts_when'];
        var gifts_del = giftsArr[id]['gifts_del'];
        var giftsIns = "INSERT INTO gifts (gifts_id, gifts_name, gifts_desc, gifts_points, gifts_pic, gifts_icon, gifts_institution, gifts_when, gifts_del) VALUES (?,?,?,?,?,?,?,?,?)";
        this.database.executeSql(giftsIns, [gifts_id, gifts_name, gifts_desc, gifts_points, gifts_pic, gifts_icon, gifts_institution, gifts_when, gifts_del]).then(function () {
            id++;
            if (id < giftsArr.length) {
                _this.giftsArrFuncIns(id, giftsArr);
            }
        }).catch(function () { });
    };
    BackendProvider.prototype.catArrFunc = function (id, catArr) {
        var _this = this;
        var cat_id = catArr[id]['cat_id'];
        var cat_xid = 0;
        var cat_when = catArr[id]['cat_when'];
        var cat_del = catArr[id]['cat_del'];
        var cat_name = 0;
        var cat_desc = 0;
        var cat_pic = 0;
        var cat_ingr = 0;
        var cat_order = 0;
        var cat_institution = 0;
        if (cat_when != '1') {
            cat_xid = catArr[id]['cat_xid'];
            cat_name = catArr[id]['cat_name'];
            cat_desc = catArr[id]['cat_desc'];
            cat_pic = catArr[id]['cat_pic'];
            cat_ingr = catArr[id]['cat_ingr'];
            cat_order = catArr[id]['cat_order'];
            cat_institution = catArr[id]['cat_institution'];
        }
        var queryCats = "SELECT * FROM categories WHERE cat_id = ?";
        this.database.executeSql(queryCats, [cat_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var catsUpd = "UPDATE categories SET cat_xid=?, cat_name=?, cat_desc=?, cat_pic=?, cat_ingr=?, cat_order=?, cat_institution=?, cat_when=?, cat_del=? WHERE cat_id=?";
                _this.database.executeSql(catsUpd, [cat_xid, cat_name, cat_desc, cat_pic, cat_ingr, cat_order, cat_institution, cat_when, cat_del, cat_id])
                    .then(function () {
                    id++;
                    if (id < catArr.length) {
                        _this.catArrFunc(id, catArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var catsIns = "INSERT INTO categories (cat_id, cat_xid, cat_name, cat_desc, cat_pic, cat_ingr, cat_order, cat_institution, cat_when, cat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(catsIns, [cat_id, cat_xid, cat_name, cat_desc, cat_pic, cat_ingr, cat_order, cat_institution, cat_when, cat_del])
                    .then(function () {
                    id++;
                    if (id < catArr.length) {
                        _this.catArrFunc(id, catArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.catArrFuncIns = function (id, catArr) {
        var _this = this;
        var cat_id = catArr[id]['cat_id'];
        var cat_xid = catArr[id]['cat_xid'];
        var cat_name = catArr[id]['cat_name'];
        var cat_desc = catArr[id]['cat_desc'];
        var cat_pic = catArr[id]['cat_pic'];
        var cat_ingr = catArr[id]['cat_ingr'];
        var cat_order = catArr[id]['cat_order'];
        var cat_institution = catArr[id]['cat_institution'];
        var cat_when = catArr[id]['cat_when'];
        var cat_del = catArr[id]['cat_del'];
        var catsIns = "INSERT INTO categories (cat_id, cat_xid, cat_name, cat_desc, cat_pic, cat_ingr, cat_order, cat_institution, cat_when, cat_del) VALUES (?,?,?,?,?,?,?,?,?,?)";
        this.database.executeSql(catsIns, [cat_id, cat_xid, cat_name, cat_desc, cat_pic, cat_ingr, cat_order, cat_institution, cat_when, cat_del]).then(function () {
            id++;
            if (id < catArr.length) {
                _this.catArrFuncIns(id, catArr);
            }
        }).catch(function () { });
    };
    BackendProvider.prototype.menueArrFunc = function (id, menueArr) {
        var _this = this;
        var menue_id = menueArr[id]['menue_id'];
        var menue_xid = 0;
        var menue_cat_xid = 0;
        var menue_when = menueArr[id]['menue_when'];
        var menue_del = menueArr[id]['menue_del'];
        var menue_cat = 0;
        var menue_name = 0;
        var menue_desc = 0;
        var menue_size = 0;
        var menue_cost = 0;
        var menue_costs = 0;
        var menue_ingr = 0;
        var menue_addition = 0;
        var menue_addition_auto = 0;
        var menue_package = 0;
        var menue_weight = 0;
        var menue_interval = 0;
        var menue_discount = 0;
        var menue_action = 0;
        var menue_code = 0;
        var menue_pic = 0;
        var menue_icon = 0;
        var menue_institution = 0;
        if (menue_when != '1') {
            menue_xid = menueArr[id]['menue_xid'];
            menue_cat_xid = menueArr[id]['menue_cat_xid'];
            menue_cat = menueArr[id]['menue_cat'];
            menue_name = menueArr[id]['menue_name'];
            menue_desc = menueArr[id]['menue_desc'];
            menue_size = menueArr[id]['menue_size'];
            menue_cost = menueArr[id]['menue_cost'];
            menue_costs = menueArr[id]['menue_costs'];
            menue_ingr = menueArr[id]['menue_ingr'];
            menue_addition = menueArr[id]['menue_addition'];
            menue_addition_auto = menueArr[id]['menue_addition_auto'];
            menue_package = menueArr[id]['menue_package'];
            menue_weight = menueArr[id]['menue_weight'];
            menue_discount = menueArr[id]['menue_discount'];
            menue_action = menueArr[id]['menue_action'];
            menue_code = menueArr[id]['menue_code'];
            menue_interval = menueArr[id]['menue_interval'];
            menue_pic = menueArr[id]['menue_pic'];
            menue_icon = menueArr[id]['menue_icon'];
            menue_institution = menueArr[id]['menue_institution'];
        }
        var queryMenue = "SELECT * FROM menue WHERE menue_id = ?";
        this.database.executeSql(queryMenue, [menue_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var menueUpd = "UPDATE menue SET menue_xid=?, menue_cat_xid=?, menue_cat=?, menue_name=?, menue_desc=?, menue_size=?, menue_cost=?, menue_costs=?, menue_ingr=?, menue_addition=?, menue_addition_auto=?, menue_package=?, menue_weight=?, menue_discount=?, menue_action=?, menue_code=?, menue_interval=?, menue_pic=?, menue_icon=?, menue_institution=?, menue_when=?, menue_del=? WHERE menue_id=?";
                _this.database.executeSql(menueUpd, [menue_xid, menue_cat_xid, menue_cat, menue_name, menue_desc, menue_size, menue_cost, menue_costs, menue_ingr, menue_addition, menue_addition_auto, menue_package, menue_weight, menue_discount, menue_action, menue_code, menue_interval, menue_pic, menue_icon, menue_institution, menue_when, menue_del, menue_id])
                    .then(function () {
                    id++;
                    if (id < menueArr.length) {
                        _this.menueArrFunc(id, menueArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var menueIns = "INSERT INTO menue (menue_id, menue_xid, menue_cat_xid, menue_cat, menue_name, menue_desc, menue_size, menue_cost, menue_costs, menue_ingr, menue_addition, menue_addition_auto, menue_package, menue_weight, menue_discount, menue_action, menue_code, menue_interval, menue_pic, menue_icon, menue_institution, menue_when, menue_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(menueIns, [menue_id, menue_xid, menue_cat_xid, menue_cat, menue_name, menue_desc, menue_size, menue_cost, menue_costs, menue_ingr, menue_addition, menue_addition_auto, menue_package, menue_weight, menue_discount, menue_action, menue_code, menue_interval, menue_pic, menue_icon, menue_institution, menue_when, menue_del])
                    .then(function () {
                    id++;
                    if (id < menueArr.length) {
                        _this.menueArrFunc(id, menueArr);
                    }
                })
                    .catch(function () { });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.menueArrFuncIns = function (id, menueArr) {
        var _this = this;
        var menue_id = menueArr[id]['menue_id'];
        var menue_xid = menueArr[id]['menue_xid'];
        var menue_cat_xid = menueArr[id]['menue_cat_xid'];
        var menue_cat = menueArr[id]['menue_cat'];
        var menue_name = menueArr[id]['menue_name'];
        var menue_desc = menueArr[id]['menue_desc'];
        var menue_size = menueArr[id]['menue_size'];
        var menue_cost = menueArr[id]['menue_cost'];
        var menue_costs = menueArr[id]['menue_costs'];
        var menue_ingr = menueArr[id]['menue_ingr'];
        var menue_addition = menueArr[id]['menue_addition'];
        var menue_addition_auto = menueArr[id]['menue_addition_auto'];
        var menue_package = menueArr[id]['menue_package'];
        var menue_weight = menueArr[id]['menue_weight'];
        var menue_interval = menueArr[id]['menue_interval'];
        var menue_discount = menueArr[id]['menue_discount'];
        var menue_action = menueArr[id]['menue_action'];
        var menue_code = menueArr[id]['menue_code'];
        var menue_pic = menueArr[id]['menue_pic'];
        var menue_icon = menueArr[id]['menue_icon'];
        var menue_institution = menueArr[id]['menue_institution'];
        var menue_when = menueArr[id]['menue_when'];
        var menue_del = menueArr[id]['menue_del'];
        var menueIns = "INSERT INTO menue (menue_id, menue_xid, menue_cat_xid, menue_cat, menue_name, menue_desc, menue_size, menue_cost, menue_costs, menue_ingr, menue_addition, menue_addition_auto, menue_package, menue_weight, menue_interval, menue_discount, menue_action, menue_code, menue_pic, menue_icon, menue_institution, menue_when, menue_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        this.database.executeSql(menueIns, [menue_id, menue_xid, menue_cat_xid, menue_cat, menue_name, menue_desc, menue_size, menue_cost, menue_costs, menue_ingr, menue_addition, menue_addition_auto, menue_package, menue_weight, menue_interval, menue_discount, menue_action, menue_code, menue_pic, menue_icon, menue_institution, menue_when, menue_del]).then(function () {
            id++;
            if (id < menueArr.length) {
                _this.menueArrFuncIns(id, menueArr);
            }
        }).catch(function () { });
    };
    BackendProvider.prototype.goodsArrFunc = function (id, goodsArr) {
        var _this = this;
        var goods_id = goodsArr[id]['goods_id'];
        var goods_when = goodsArr[id]['goods_when'];
        var goods_del = goodsArr[id]['goods_del'];
        var goods_name = 0;
        var goods_desc = 0;
        var goods_pic = 0;
        var goods_type = 0;
        var goods_institution = 0;
        if (goods_when != '1') {
            goods_name = goodsArr[id]['goods_name'];
            goods_desc = goodsArr[id]['goods_desc'];
            goods_pic = goodsArr[id]['goods_pic'];
            goods_type = goodsArr[id]['goods_type'];
            goods_institution = goodsArr[id]['goods_institution'];
        }
        var queryGoods = "SELECT * FROM goods WHERE goods_id = ?";
        this.database.executeSql(queryGoods, [goods_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var goodsUpd = "UPDATE goods SET goods_name=?, goods_desc=?, goods_pic=?, goods_type=?, goods_institution=?, goods_when=?, goods_del=? WHERE goods_id=?";
                _this.database.executeSql(goodsUpd, [goods_name, goods_desc, goods_pic, goods_type, goods_institution, goods_when, goods_del, goods_id])
                    .then(function () {
                    id++;
                    if (id < goodsArr.length) {
                        _this.goodsArrFunc(id, goodsArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var goodsIns = "INSERT INTO goods (goods_id, goods_name, goods_desc, goods_pic, goods_type, goods_institution, goods_when, goods_del) VALUES (?,?,?,?,?,?,?,?)";
                _this.database.executeSql(goodsIns, [goods_id, goods_name, goods_desc, goods_pic, goods_type, goods_institution, goods_when, goods_del]).then(function () {
                    id++;
                    if (id < goodsArr.length) {
                        _this.goodsArrFunc(id, goodsArr);
                    }
                })
                    .catch(function () { });
            }
        }).catch(function () { });
    };
    BackendProvider.prototype.goodsArrFuncIns = function (id, goodsArr) {
        var _this = this;
        var goods_id = goodsArr[id]['goods_id'];
        var goods_when = goodsArr[id]['goods_when'];
        var goods_name = goodsArr[id]['goods_name'];
        var goods_desc = goodsArr[id]['goods_desc'];
        var goods_pic = goodsArr[id]['goods_pic'];
        var goods_type = goodsArr[id]['goods_type'];
        var goods_institution = goodsArr[id]['goods_institution'];
        var goods_del = goodsArr[id]['goods_del'];
        var goodsIns = "INSERT INTO goods (goods_id, goods_name, goods_desc, goods_pic, goods_type, goods_institution, goods_when, goods_del) VALUES (?,?,?,?,?,?,?,?)";
        this.database.executeSql(goodsIns, [goods_id, goods_name, goods_desc, goods_pic, goods_type, goods_institution, goods_when, goods_del]).then(function () {
            id++;
            if (id < goodsArr.length) {
                _this.goodsArrFuncIns(id, goodsArr);
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.pointsArrFunc = function (id, pointsArr) {
        var _this = this;
        var points_id = pointsArr[id]['points_id'];
        var points_user = pointsArr[id]['points_user'];
        var points_bill = pointsArr[id]['points_bill'];
        var points_discount = pointsArr[id]['points_discount'];
        var points_points = pointsArr[id]['points_points'];
        var points_got_spend = pointsArr[id]['points_got_spend'];
        var points_waiter = pointsArr[id]['points_waiter'];
        var points_institution = pointsArr[id]['points_institution'];
        var points_office = pointsArr[id]['points_office'];
        var points_status = pointsArr[id]['points_status'];
        var points_comment = pointsArr[id]['points_comment'];
        var points_proofed = pointsArr[id]['points_proofed'];
        var points_gift = pointsArr[id]['points_gift'];
        var points_check = pointsArr[id]['points_check'];
        var points_waitertime = pointsArr[id]['points_waitertime'];
        var points_usertime = pointsArr[id]['points_usertime'];
        var points_when = pointsArr[id]['points_when'];
        var points_time = pointsArr[id]['points_time'];
        var points_del = pointsArr[id]['points_del'];
        var queryPoints = "SELECT * FROM points WHERE points_id = ?";
        this.database.executeSql(queryPoints, [points_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var pointsUpd = "UPDATE points SET points_status=?, points_proofed=?, points_when=?, points_del=? WHERE points_id=?";
                _this.database.executeSql(pointsUpd, [points_status, points_proofed, points_when, points_del, points_id])
                    .then(function () {
                    id++;
                    if (id < pointsArr.length) {
                        _this.pointsArrFunc(id, pointsArr);
                    }
                })
                    .catch(function () { });
            }
            else {
                var pointsIns = "INSERT INTO points (points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(pointsIns, [points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del])
                    .then(function () {
                    id++;
                    if (id < pointsArr.length) {
                        _this.pointsArrFunc(id, pointsArr);
                    }
                })
                    .catch(function () { });
            }
        }).catch(function () { });
    };
    BackendProvider.prototype.pointsArrFuncIns = function (id, pointsArr) {
        var _this = this;
        var points_id = pointsArr[id]['points_id'];
        var points_user = pointsArr[id]['points_user'];
        var points_bill = pointsArr[id]['points_bill'];
        var points_discount = pointsArr[id]['points_discount'];
        var points_points = pointsArr[id]['points_points'];
        var points_got_spend = pointsArr[id]['points_got_spend'];
        var points_waiter = pointsArr[id]['points_waiter'];
        var points_institution = pointsArr[id]['points_institution'];
        var points_office = pointsArr[id]['points_office'];
        var points_status = pointsArr[id]['points_status'];
        var points_comment = pointsArr[id]['points_comment'];
        var points_proofed = pointsArr[id]['points_proofed'];
        var points_gift = pointsArr[id]['points_gift'];
        var points_check = pointsArr[id]['points_check'];
        var points_waitertime = pointsArr[id]['points_waitertime'];
        var points_usertime = pointsArr[id]['points_usertime'];
        var points_when = pointsArr[id]['points_when'];
        var points_time = pointsArr[id]['points_time'];
        var points_del = pointsArr[id]['points_del'];
        var pointsIns = "INSERT INTO points (points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        this.database.executeSql(pointsIns, [points_id, points_user, points_bill, points_discount, points_points, points_got_spend, points_waiter, points_institution, points_office, points_status, points_comment, points_proofed, points_gift, points_check, points_waitertime, points_usertime, points_when, points_time, points_del]).then(function () {
            id++;
            if (id < pointsArr.length) {
                _this.pointsArrFuncIns(id, pointsArr);
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.orderSentRecord = function (gotdata) {
        var _this = this;
        var order_id = gotdata['order_id'];
        var order_user = gotdata['order_user'];
        var order_name = gotdata['order_name'];
        var order_desc = gotdata['order_desc'];
        var order_worker = gotdata['order_worker'];
        var order_institution = gotdata['order_institution'];
        var order_office = gotdata['order_office'];
        var order_bill = gotdata['order_bill'];
        var order_goods = gotdata['order_goods'];
        var order_cats = gotdata['order_cats'];
        var order_order = gotdata['order_order'];
        var order_status = gotdata['order_status'];
        var order_start = gotdata['order_start'];
        var order_end = gotdata['order_end'];
        var order_allday = gotdata['order_allday'];
        var order_mobile = gotdata['order_mobile'];
        var order_when = gotdata['order_when'];
        var order_del = gotdata['order_del'];
        var queryOrdering = "SELECT * FROM ordering WHERE order_id = ?";
        this.database.executeSql(queryOrdering, [order_id])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var orderingUpd = "UPDATE ordering SET order_user=?, order_name=?, order_desc=?, order_worker=?, order_institution=?, order_office=?, order_bill=?, order_goods=?, order_cats=?, order_order=?, order_status=?, order_start=?, order_end=?, order_allday=?, order_mobile=?, order_when=?, order_del=? WHERE order_id=?";
                _this.database.executeSql(orderingUpd, [order_user, order_name, order_desc, order_worker, order_institution, order_office, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_end, order_allday, order_mobile, order_when, order_del, order_id]).then(function (suc2) { }).catch(function (e) { return console.log(e); });
            }
            else {
                var orderIns = "INSERT INTO ordering (order_id, order_user, order_name, order_desc, order_worker, order_institution, order_office, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_end, order_allday, order_mobile, order_when, order_del) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                _this.database.executeSql(orderIns, [order_id, order_user, order_name, order_desc, order_worker, order_institution, order_office, order_bill, order_goods, order_cats, order_order, order_status, order_start, order_end, order_allday, order_mobile, order_when, order_del]).then(function (suc2) { }).catch(function (e) { return console.log(e); });
            }
        })
            .catch(function (e) { return console.log(e); });
    };
    ;
    BackendProvider.prototype.crDir = function () {
        var _this = this;
        // CHECK AND CREATE DIRECTION
        this.file.checkDir(this.file.documentsDirectory, this.instdir).then(function () { }).catch(function () {
            _this.file.createDir(_this.file.documentsDirectory, _this.instdir, false).then(function () { }).catch(function () { });
        });
        // CHECK AND CREATE DIRECTION
        this.file.checkDir(this.file.dataDirectory, this.instdir).then(function () { }).catch(function () {
            _this.file.createDir(_this.file.dataDirectory, _this.instdir, false).then(function () { }).catch(function () { });
        });
    };
    BackendProvider.prototype.regPush = function () {
        var _this = this;
        if (this.uuid) {
            // to check if we have permission
            this.push.hasPermission()
                .then(function (res) {
                if (res.isEnabled) {
                    // alert('We have permission to send push notifications');
                }
                else {
                    // alert('We do not have permission to send push notifications');
                }
            });
            var push_options = {
                android: {
                    sound: true
                },
                ios: {
                    clearBadge: true,
                    alert: true,
                    badge: true,
                    sound: true
                },
                windows: {},
                browser: {}
            };
            var pushObject = this.push.init(push_options);
            pushObject.on('notification').subscribe(function (notification) {
                alert('Received a notification: ' + notification);
            });
            pushObject.on('registration').subscribe(function (registration) {
                var gcmstr = {
                    device: _this.model,
                    device_id: _this.uuid,
                    device_serial: _this.serial,
                    device_version: _this.version,
                    device_os: _this.platform,
                    inst_id: _this.institution,
                    gcm: registration.registrationId,
                    newusr: 'gcmreg'
                };
                alert('PUSH START: ' + JSON.stringify(gcmstr) + ' REGDATA: ' + JSON.stringify(registration));
                _this.httpRequest(JSON.stringify(gcmstr))
                    .subscribe(function (pushsuc) {
                    alert('PUSH SUCCESS: ' + JSON.stringify(pushsuc));
                }, function (error) {
                    alert('PUSH ERROR: ' + JSON.stringify(error));
                });
            });
            pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin', error); });
        }
    };
    BackendProvider.prototype.ifNotExists = function (tablename, columnname, columntype, defaultvalue) {
        var _this = this;
        // FOR OLDER VERSION OF APP - ADD SOME NEW MENUE COLUMNS
        var queryAlter = "SELECT * FROM " + tablename + " WHERE " + columnname + "=?";
        this.database.executeSql(queryAlter, [0])
            .then(function () { })
            .catch(function () {
            var queryAdd = "ALTER TABLE " + tablename + " ADD COLUMN " + columnname + " " + columntype;
            _this.database.executeSql(queryAdd, [])
                .then(function () {
                var queryUpd = "UPDATE " + tablename + " SET " + columnname + "=" + defaultvalue;
                _this.database.executeSql(queryUpd, [])
                    .then(function () { })
                    .catch(function () { });
            })
                .catch(function () { });
        });
    };
    BackendProvider.prototype.validateEmail = function (email) {
        var mailproof = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return mailproof.test(email);
    };
    BackendProvider.prototype.makeid = function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    };
    BackendProvider.prototype.crnewdate = function (dat, adday) {
        return new Date(dat.getFullYear(), dat.getMonth(), dat.getDate() + adday, 0, 0, 0, 0);
    };
    BackendProvider.prototype.httpRequest = function (json) {
        // let headers = new Headers({
        // 	'Content-Type': 'application/x-www-form-urlencoded'
        // });
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ headers: headers });
        return this.http.post(this.generalscript, json, options)
            .do(this.logResponse)
            .map(this.extractData)
            .catch(this.handleError);
    };
    BackendProvider.prototype.logResponse = function (res) {
        // console.log(JSON.stringify(res));
    };
    BackendProvider.prototype.extractData = function (res) {
        return res.json();
    };
    BackendProvider.prototype.handleError = function (res) {
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(res.json().error || 'Fail to connect to the server');
    };
    BackendProvider.prototype.updateProfile = function (val) {
        var _this = this;
        for (var key in val) {
            if (val.hasOwnProperty(key)) {
                if (val[key] == null || val[key] == '') {
                    val[key] = 0;
                }
            }
        }
        var user_real_id = val.user_real_id;
        var user_name = val.user_name;
        var user_surname = val.user_surname;
        var user_middlename = val.user_middlename;
        var user_email = val.user_email;
        var user_email_confirm = val.user_email_confirm;
        var user_pwd = val.user_pwd;
        var user_tel = val.user_tel;
        var user_mob_confirm = val.user_mob_confirm;
        var user_mob = val.user_mob;
        var user_info = val.user_info;
        var user_work_pos = val.user_work_pos;
        var user_menue_exe = val.user_menue_exe;
        var user_institution = val.user_institution;
        var user_office = val.user_office;
        var user_pic = val.user_pic;
        var user_gender = val.user_gender;
        var user_birthday = val.user_birthday;
        var user_country = val.user_country;
        var user_region = val.user_region;
        var user_city = val.user_city;
        var user_adress = val.user_adress;
        var user_install_where = val.user_install_where;
        var user_log_key = val.user_log_key;
        var user_gcm = val.user_gcm;
        var user_device = val.user_device;
        var user_device_id = val.user_device_id;
        var user_device_serial = val.user_device_serial;
        var user_device_version = val.user_device_version;
        var user_device_os = val.user_device_os;
        var user_discount = val.user_discount;
        var user_promo = val.user_promo;
        var user_conf_req = val.user_conf_req;
        var user_log = val.user_log;
        var user_upd = val.user_upd;
        var user_reg = val.user_reg;
        var user_del = val.user_del;
        var corrtime = this.timezoneAdd(val.user_log);
        this.serverdate = new Date(corrtime * 1000);
        this.serverdateraw = val.user_log;
        setTimeout(function () {
            // every day 2 weeks
            for (var i = 0; i < 14; i++) {
                _this.dates.push({ dat: _this.crnewdate(_this.serverdate, i) });
            }
        }, 200);
        var queryUsrLog = "SELECT * FROM users WHERE user_id=? LIMIT 1";
        this.database.executeSql(queryUsrLog, [1])
            .then(function (suc) {
            if (suc.rows.length > 0) {
                var queryUsrDataUpd = "UPDATE users SET user_real_id=?, user_name=?, user_surname=?, user_middlename=?, user_email=?, user_email_confirm=?, user_pwd=?, user_tel=?, user_mob_confirm=?, user_mob=?, user_info=?, user_work_pos=?, user_menue_exe=?, user_institution=?, user_office=?, user_pic=?, user_gender=?, user_birthday=?, user_country=?, user_region=?, user_city=?, user_adress=?, user_install_where=?, user_log_key=?, user_gcm=?, user_device=?, user_device_id=?, user_device_serial=?, user_device_version=?, user_device_os=?, user_discount=?, user_promo=?, user_conf_req=?, user_log=?, user_upd=?, user_reg=?, user_del=? WHERE user_id=?";
                _this.database.executeSql(queryUsrDataUpd, [user_real_id, user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_mob_confirm, user_mob, user_info, user_work_pos, user_menue_exe, user_institution, user_office, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_gcm, user_device, user_device_id, user_device_serial, user_device_version, user_device_os, user_discount, user_promo, user_conf_req, user_log, user_upd, user_reg, user_del, 1])
                    .then(function (suc) { })
                    .catch(function (e) { return console.log('ERROR UPD PROF ' + JSON.stringify(e)); });
            }
        })
            .catch(function () { });
    };
    BackendProvider.prototype.promoConfirm = function (data) {
        var promoOK = parseInt(data[0].promoOK);
        if (promoOK == 2) {
            promoOK = 1;
        }
        var updateMob = "UPDATE users SET user_promo=? WHERE user_id=?";
        this.database.executeSql(updateMob, [promoOK, 1])
            .then(function (suc) { })
            .catch(function (e) { return console.log(e); });
    };
    BackendProvider.prototype.smsConfirmOk = function (data) {
        var _this = this;
        var updateMob = "UPDATE users SET user_mob_confirm=?, user_upd=? WHERE user_id = ?";
        this.database.executeSql(updateMob, [1, data[0].when, 1]).then(function (suc) {
            if (data[0].oldUsr) {
                var oldUsr = [data[0].oldUsr];
                if (typeof data[0].oldUsr !== 'undefined' && data[0].oldUsr !== null && data[0].oldUsr.length > 0) {
                    // USER DISCOUNT
                    if (oldUsr[0].usrData.user_discount > '0') {
                        var usrdiscount_2 = oldUsr[0].usrData.user_discount;
                        var queryUsrDiscount = "SELECT * FROM users WHERE user_id=? AND user_discount=? AND user_del='0' LIMIT 1";
                        _this.database.executeSql(queryUsrDiscount, [1, usrdiscount_2]).then(function (suc2) {
                            if (suc2.rows.length == 0) {
                                var queryUsrDiscountUpd = "UPDATE users SET user_discount=? WHERE user_id=?";
                                _this.database.executeSql(queryUsrDiscountUpd, [usrdiscount_2, 1]).then(function (res) { }).catch(function (e) { return console.log(e); });
                            }
                        }).catch(function (e) { return console.log(e); });
                    }
                    // USER WORK POSITION
                    if (oldUsr[0].usrData.user_work_pos == '2' || oldUsr[0].usrData.user_work_pos == '3' || oldUsr[0].usrData.user_work_pos == '4') {
                        var user_work_pos_4 = oldUsr[0].usrData.user_work_pos;
                        var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? AND user_del = '0' LIMIT 1";
                        _this.database.executeSql(queryUsrWP, [1, user_work_pos_4]).then(function (suc3) {
                            if (suc3.rows.length == 0) {
                                var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                                _this.database.executeSql(queryUsrWPUpd, [user_work_pos_4, 1]).then(function (suc4) { }).catch(function (e) { return console.log(e); });
                            }
                        }).catch(function (e) { return console.log(e); });
                    }
                    else if (oldUsr[0].usrData.user_work_pos != '1000') {
                        var user_work_pos_5 = oldUsr[0].usrData.user_work_pos;
                        var queryUsrWP = "SELECT * FROM users WHERE user_id=? AND user_work_pos=? AND user_del = '0' LIMIT 1";
                        _this.database.executeSql(queryUsrWP, [1, user_work_pos_5])
                            .then(function (suc2) {
                            if (suc2.rows.length == 0) {
                                var queryUsrWPUpd = "UPDATE users SET user_work_pos=? WHERE user_id=?";
                                _this.database.executeSql(queryUsrWPUpd, [user_work_pos_5, 1])
                                    .then(function (suc3) { })
                                    .catch(function (e) { return console.log(e); });
                            }
                        })
                            .catch(function (e) { return console.log(e); });
                    }
                    // DB POINTS
                    var pointsArr = oldUsr[0].pointsArr;
                    if (pointsArr.length > 0) {
                        _this.pointsArrFunc(0, pointsArr);
                    }
                    // DB WALLET
                    var walletArr = oldUsr[0].walletArr;
                    if (walletArr.length > 0) {
                        var wallet_user_2 = walletArr[0]['wallet_user'];
                        var wallet_institution_2 = walletArr[0]['wallet_institution'];
                        var wallet_total_2 = walletArr[0]['wallet_total'];
                        var wallet_warn_2 = walletArr[0]['wallet_warn'];
                        var wallet_when_2 = walletArr[0]['wallet_when'];
                        var wallet_del_2 = walletArr[0]['wallet_del'];
                        var queryWallet = "SELECT * FROM wallet WHERE wallet_when < ?";
                        _this.database.executeSql(queryWallet, [wallet_when_2])
                            .then(function (suc) {
                            if (suc.rows.length > 0) {
                                var walletUpd = "UPDATE wallet SET wallet_institution=?, wallet_total=?, wallet_when=?, wallet_warn=?, wallet_del=?  WHERE wallet_user=?";
                                _this.database.executeSql(walletUpd, [wallet_institution_2, wallet_total_2, wallet_when_2, wallet_warn_2, wallet_del_2, wallet_user_2])
                                    .then(function () { })
                                    .catch(function () { });
                            }
                        })
                            .catch(function () { });
                    }
                    // DB CHAT
                    var chatArr = oldUsr[0].chatArr;
                    if (chatArr.length > 0) {
                        _this.chatArrFunc(0, chatArr);
                    }
                    var user_name = oldUsr[0].usrData['user_name'];
                    var user_surname = oldUsr[0].usrData['user_surname'];
                    var user_middlename = oldUsr[0].usrData['user_middlename'];
                    var user_email = oldUsr[0].usrData['user_email'];
                    var user_email_confirm = oldUsr[0].usrData['user_email_confirm'];
                    var user_pwd = oldUsr[0].usrData['user_pwd'];
                    var user_tel = oldUsr[0].usrData['user_tel'];
                    var user_info = oldUsr[0].usrData['user_info'];
                    var user_pic = oldUsr[0].usrData['user_pic'];
                    var user_gender = oldUsr[0].usrData['user_gender'];
                    var user_birthday = oldUsr[0].usrData['user_birthday'];
                    var user_country = oldUsr[0].usrData['user_country'];
                    var user_region = oldUsr[0].usrData['user_region'];
                    var user_city = oldUsr[0].usrData['user_city'];
                    var user_adress = oldUsr[0].usrData['user_adress'];
                    var user_install_where = oldUsr[0].usrData['user_install_where'];
                    var user_log_key = oldUsr[0].usrData['user_log_key'];
                    var user_promo = oldUsr[0].usrData['user_promo'];
                    var user_del = oldUsr[0].usrData['user_del'];
                    // DB USER
                    var usrUpd = "UPDATE users SET user_name=?, user_surname=?, user_middlename=?, user_email=?, user_email_confirm=?, user_pwd=?, user_tel=?, user_info=?, user_pic=?, user_gender=?, user_birthday=?, user_country=?, user_region=?, user_city=?, user_adress=?, user_install_where=?, user_log_key=?, user_promo=?, user_del=? WHERE user_id=?";
                    _this.database.executeSql(usrUpd, [user_name, user_surname, user_middlename, user_email, user_email_confirm, user_pwd, user_tel, user_info, user_pic, user_gender, user_birthday, user_country, user_region, user_city, user_adress, user_install_where, user_log_key, user_promo, user_del, 1]).then(function (res) { }).catch(function (e) { return console.log(e); });
                }
            }
        })
            .catch(function (e) { return console.log(e); });
    };
    BackendProvider.prototype.smsRequestOk = function (val) {
        var queryUsrSmsUpd = "UPDATE users SET user_conf_req=?, user_upd=? WHERE user_id=?";
        this.database.executeSql(queryUsrSmsUpd, [val, val, 1])
            .then(function (suc) { })
            .catch(function (e) { return console.log(e); });
    };
    BackendProvider.prototype.cancelOrder = function (val) {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                var jsonstr = {
                    inst_id: _this.institution,
                    newusr: 'calender',
                    device_id: _this.uuid,
                    getset: 2,
                    orderId: val
                };
                _this.httpRequest(JSON.stringify(jsonstr))
                    .subscribe(function (suc) {
                    if (suc[0].orderOK == '7') {
                        resolve(0);
                    }
                    else if (suc[0].orderOK == '6') {
                        _this.database.executeSql("UPDATE ordering SET order_status=? WHERE order_id=?", [4, val])
                            .then(function (res) {
                            resolve(1);
                        })
                            .catch(function (e) { return resolve(e); });
                    }
                }, function (e) { return resolve(e); });
            });
        });
    };
    BackendProvider.prototype.getOrders = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM ordering WHERE order_user = ? AND order_status != ? AND order_start > ? AND order_del = ? ORDER BY order_id DESC LIMIT 20", [_this.myid, '4', _this.serverdateraw, '0'])
                    .then(function (suc) {
                    var ordering = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            ordering.push(suc.rows.item(i));
                        }
                        resolve(ordering);
                    }
                    else {
                        resolve(ordering);
                    }
                })
                    .catch(function (e) { return resolve(e); });
            }, 1000);
        });
    };
    BackendProvider.prototype.saveRequestRes = function (val, item, answer) {
        var asksreq = val[0];
        if (asksreq.asksOK == 1) {
            var answers = 'reply: ' + answer;
            this.database.executeSql("UPDATE asks SET asks_reply=?, asks_when=?, asks_active='1' WHERE asks_id=?", [answers, parseInt(asksreq.asks_when), parseInt(item)])
                .then(function (res) { })
                .catch(function (e) { return console.log(e); });
        }
    };
    BackendProvider.prototype.questsPush = function (x) {
        if (x.asks_type == '0') {
            var answsplit = x.asks_answ.split(',');
            var answarr = [];
            for (var a = 0; a < answsplit.length; a++) {
                answarr.push(answsplit[a]);
            }
            x.asks_answ = answarr;
            this.quests.push(x);
        }
        else {
            this.quests.push(x);
        }
        return this.quests;
    };
    BackendProvider.prototype.getSurveys = function () {
        var _this = this;
        this.quests = [];
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM asks WHERE asks_reply = ? AND asks_active='0' AND asks_del = '0' ORDER BY asks_id DESC, asks_chained ASC", ['0'])
                    .then(function (suc) {
                    if (suc.rows.length > 0) {
                        var ischained = 0;
                        var istaken = 0;
                        for (var i = 0; i < suc.rows.length; i++) {
                            if (suc.rows.item(i).asks_del == '0' && suc.rows.item(i).asks_active == '0') {
                                if ((suc.rows.item(i).asks_chained != '0' && istaken == 0) || (ischained != 0 && istaken == 0)) {
                                    if (suc.rows.item(i).asks_chained != '0' && ischained == 0) {
                                        ischained = suc.rows.item(i).asks_chained;
                                        _this.questsPush(suc.rows.item(i));
                                    }
                                    else if (ischained == suc.rows.item(i).asks_chained) {
                                        _this.questsPush(suc.rows.item(i));
                                    }
                                }
                                else if (istaken == 0 && ischained == 0) {
                                    istaken = 1;
                                    if (suc.rows.item(i).asks_type == '0') {
                                        _this.questsPush(suc.rows.item(i));
                                    }
                                }
                            }
                        }
                        return resolve(_this.quests);
                    }
                    else {
                        return resolve(_this.quests);
                    }
                })
                    .catch(function (e) { return console.log(e); });
            }, 1000);
        });
    };
    BackendProvider.prototype.getFAQ = function () {
        return this.faq;
    };
    BackendProvider.prototype.getCards = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM menue WHERE menue_action='giftcard' AND menue_del='0' ORDER BY menue_when DESC", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            suc.rows.item(i).menue_name = _this.decodeEntities(suc.rows.item(i).menue_name);
                            suc.rows.item(i).menue_desc = _this.decodeEntities(suc.rows.item(i).menue_desc);
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getInstitutions = function (val) {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM organizations WHERE org_id='" + val + "' AND org_del = '0'", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            suc.rows.item(i).org_name = _this.decodeEntities(suc.rows.item(i).org_name);
                            suc.rows.item(i).org_adress = _this.decodeEntities(suc.rows.item(i).org_adress);
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getLastGifts = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM gifts WHERE gifts_del='0' AND gifts_points != '0' ORDER BY gifts_when DESC LIMIT 2", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            suc.rows.item(i).gifts_name = _this.decodeEntities(suc.rows.item(i).gifts_name);
                            suc.rows.item(i).gifts_desc = _this.decodeEntities(suc.rows.item(i).gifts_desc);
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getWallet = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM wallet WHERE wallet_user = '" + _this.myid + "' LIMIT 1", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        res.push(suc.rows.item(0));
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getProfile = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM users WHERE user_id = '1'", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            suc.rows.item(i).user_name = _this.decodeEntities(suc.rows.item(i).user_name);
                            suc.rows.item(i).user_surname = _this.decodeEntities(suc.rows.item(i).user_surname);
                            suc.rows.item(i).user_middlename = _this.decodeEntities(suc.rows.item(i).user_middlename);
                            suc.rows.item(i).user_email = _this.decodeEntities(suc.rows.item(i).user_email);
                            suc.rows.item(i).user_adress = _this.decodeEntities(suc.rows.item(i).user_adress);
                            if (suc.rows.item(i).user_birthday == '0000-00-00') {
                                suc.rows.item(i).user_birthday = _this.datetoday.getFullYear() - 15 + '-' + _this.datetoday.getMonth() + '-' + _this.datetoday.getDate();
                            }
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getAbout = function () {
        return this.about;
    };
    BackendProvider.prototype.getGifts = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM gifts WHERE gifts_del='0' AND gifts_points!='0' ORDER BY gifts_when DESC", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            suc.rows.item(i).gifts_name = _this.decodeEntities(suc.rows.item(i).gifts_name);
                            suc.rows.item(i).gifts_desc = _this.decodeEntities(suc.rows.item(i).gifts_desc);
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getFirstGifts = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM gifts WHERE gifts_id != '0' AND gifts_when = '2' AND gifts_del = '0' LIMIT 1", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        res.push(suc.rows.item(0));
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getShareDetails = function () {
        return this.sharedetails;
    };
    BackendProvider.prototype.getShare = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM organizations WHERE org_del = '0'", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            suc.rows.item(i).org_name = _this.decodeEntities(suc.rows.item(i).org_name);
                            suc.rows.item(i).org_adress = _this.decodeEntities(suc.rows.item(i).org_adress);
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getTerms = function () {
        return this.terms;
    };
    BackendProvider.prototype.getNews = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM news WHERE news_state='1' AND news_del='0' ORDER BY news_when DESC", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            suc.rows.item(i).news_name = _this.decodeEntities(suc.rows.item(i).news_name);
                            suc.rows.item(i).news_message = _this.decodeEntities(suc.rows.item(i).news_message);
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getGoods = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM goods WHERE goods_del='0' ORDER BY goods_id DESC", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getRituals = function (type) {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM menue WHERE menue_cat='" + type + "' AND menue_id != '1577' AND menue_id != '1578' AND menue_id != '1579' AND menue_action != 'giftcard' AND menue_del='0' ORDER BY menue_when ASC", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            if (suc.rows.item(i).menue_interval > 0) {
                                suc.rows.item(i).menue_name = _this.decodeEntities(suc.rows.item(i).menue_name);
                                suc.rows.item(i).menue_desc = _this.decodeEntities(suc.rows.item(i).menue_desc);
                                suc.rows.item(i).menue_action = _this.decodeEntities(suc.rows.item(i).menue_action);
                                suc.rows.item(i).in_id = i;
                                res.push(suc.rows.item(i));
                            }
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getServices = function () {
        return this.services;
    };
    BackendProvider.prototype.getDates = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                var res = [];
                var queryUsrLog = "SELECT * FROM users WHERE user_id=? LIMIT 1";
                _this.database.executeSql(queryUsrLog, [1])
                    .then(function (suc) {
                    if (suc.rows.length > 0) {
                        var corrtime = _this.timezoneAdd(suc.rows.item(0).user_log);
                        _this.serverdate = new Date(corrtime * 1000);
                        _this.serverdateraw = suc.rows.item(0).user_log;
                        setTimeout(function () {
                            // every day 2 weeks
                            for (var i = 0; i < 14; i++) {
                                res.push({ dat: _this.crnewdate(_this.serverdate, i) });
                            }
                            resolve(res);
                        }, 200);
                    }
                })
                    .catch(function (e) { return resolve(e); });
            }, 1000);
        });
    };
    BackendProvider.prototype.getHistory = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM ordering WHERE order_order > '0' AND order_del = '0' ORDER BY order_id DESC LIMIT 1", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            res.push(suc.rows.item(i));
                        }
                        resolve(res);
                    }
                    else {
                        resolve(res);
                    }
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getOffices = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM organizations_office ORDER BY office_id DESC", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            suc.rows.item(i).in_id = i;
                            suc.rows.item(i).office_tel = suc.rows.item(i).office_tel.split(',');
                            suc.rows.item(i).office_bus_hours = suc.rows.item(i).office_bus_hours.split(',');
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getProfessions = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM professions ORDER BY prof_id DESC", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.getProfession = function (val) {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM professions WHERE prof_id='" + val + "'", {})
                    .then(function (suc) {
                    var res = [];
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            res.push(suc.rows.item(i));
                        }
                    }
                    resolve(res);
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    BackendProvider.prototype.loadSchedule = function () {
        var _this = this;
        var serverdaterawnew = new Date(this.serverdateraw * 1000);
        var datebegin = (this.crnewdate(serverdaterawnew, 0).setHours(0, 0, 0, 0) / 1000).toFixed(0);
        var dateend = (this.crnewdate(serverdaterawnew, 14).setHours(23, 59, 59, 999) / 1000).toFixed(0);
        this.database.executeSql("SELECT * FROM schedule WHERE schedule_stop >= '" + datebegin + "' AND schedule_stop < '" + dateend + "' AND schedule_del = '0'", {})
            .then(function (suc) {
            if (suc.rows.length > 0) {
                for (var i = 0; i < suc.rows.length; i++) {
                    _this.schedules.push(suc.rows.item(i));
                }
            }
        })
            .catch(function (er) {
            console.log(er);
        });
    };
    BackendProvider.prototype.loadOrdering = function () {
        var _this = this;
        var serverdaterawnew = new Date(this.serverdateraw * 1000);
        var datebegin = (this.crnewdate(serverdaterawnew, 0).setHours(0, 0, 0, 0) / 1000).toFixed(0);
        this.database.executeSql("SELECT * FROM ordering WHERE order_start >= '" + datebegin + "' AND order_del = '0' ORDER BY order_start ASC", {})
            .then(function (suc) {
            if (suc.rows.length > 0) {
                for (var i = 0; i < suc.rows.length; i++) {
                    _this.orderings.push(suc.rows.item(i));
                }
            }
        })
            .catch(function (er) {
            console.log(er);
        });
    };
    BackendProvider.prototype.loadRoom = function () {
        var _this = this;
        this.database.executeSql("SELECT * FROM rooms WHERE room_del = '0' ORDER BY room_priority DESC", {})
            .then(function (suc) {
            if (suc.rows.length > 0) {
                for (var i = 0; i < suc.rows.length; i++) {
                    _this.rooms.push(suc.rows.item(i));
                }
            }
        })
            .catch(function (er) {
            console.log(er);
        });
    };
    // BEGIN OF - TAKE MASTER AND MASTER HISTORY ---------------------------------
    BackendProvider.prototype.checkWorkTimeForRandomHist = function () {
        var _this = this;
        // console.log('YES 11')
        return new Promise(function (resolve) {
            setTimeout(function () {
                var res = [];
                if (_this.mastersearl.length > 0) {
                    var l = _this.mastersearl.length - 1;
                    while (_this.mastersearl[l]) {
                        if (_this.mastersearl[l].in_id > 0) {
                            if (_this.mastersearl[l].user_work_time.length > 0) {
                                var times = _this.mastersearl[l].user_work_time;
                                for (var i = 0; i < times.length; i++) {
                                    var exists = 0;
                                    var masterlength = _this.mastersearl[0].user_work_time.length;
                                    if (masterlength > 0) {
                                        for (var j = 0; j < masterlength; j++) {
                                            if (_this.mastersearl[0].user_work_time[j].unixtime == times[i].unixtime && exists == 0) {
                                                exists = 1;
                                            }
                                            else if (j == masterlength - 1 && exists == 0) {
                                                _this.mastersearl[0].user_work_time.push(times[i]);
                                            }
                                        }
                                    }
                                    else if (masterlength == 0) {
                                        _this.mastersearl[0].user_work_time.push(times[i]);
                                    }
                                }
                            }
                            else if (_this.mastersearl[l].user_work_time.length == 0) {
                                _this.mastersearl.splice(l, 1);
                            }
                        }
                        l--;
                    }
                }
                resolve(res);
            }, 1000);
        });
    };
    BackendProvider.prototype.getWorkingTimeHist = function (user_work_time, onlDateDate, onlDateTime, mastr, v, limit) {
        // console.log('YES 10')
        var stopgo = 0;
        if (user_work_time.length == 0) {
            this.checkedmasters1++;
            user_work_time.push({ dayday: onlDateDate, daytime: onlDateTime, unixtime: v });
        }
        else if (user_work_time.length > 0) {
            this.checkedmasters1++;
            for (var i = 0; i < user_work_time.length; i++) {
                this.checkedmasters1++;
                if (stopgo == 0) {
                    if (user_work_time[i].daytime == onlDateTime) {
                        stopgo = 1;
                    }
                    else if (i == user_work_time.length - 1) {
                        user_work_time.push({ dayday: onlDateDate, daytime: onlDateTime, unixtime: v });
                        stopgo = 1;
                    }
                }
            }
        }
    };
    BackendProvider.prototype.getOrderingRoomsHist = function (userid, v, officesel, officeselmenue, roomid, limit, interval) {
        // console.log('YES 9 ')
        var months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        // GET ORDERING ROOMS (IF IS FREE AT THIS TIME)
        var orderinglength = this.orderings.length;
        var orderexist = 0;
        if (orderinglength > 0) {
            for (var i = 0; i < orderinglength; i++) {
                this.checkedmasters1++;
                var orderstart = this.orderings[i].order_start;
                var orderstop = this.orderings[i].order_end;
                var vEnd = v + interval;
                if (this.orderings[i].order_office == officesel.office_id && this.orderings[i].order_room == roomid && orderstart <= v && orderstop > v) {
                    orderexist = 1;
                }
                else if (orderstart < vEnd && orderstop > vEnd) {
                    orderexist = 1;
                }
                if (i == orderinglength - 1) {
                    if (orderexist == 0) {
                        var gottime = this.timezoneAdd(v);
                        gottime = gottime * 1000;
                        var onltime = new Date(gottime);
                        var onlDateTime = void 0;
                        var onlDateDate = void 0;
                        // let onlMonth;
                        var onlDay = void 0;
                        var onlHour = void 0;
                        var onlMin = void 0;
                        // if(onltime.getMonth() < 9) {
                        //   onlMonth = '0' + (onltime.getMonth() + 1);
                        // } 
                        // else {
                        //   onlMonth = onltime.getMonth() + 1;
                        // }
                        if (onltime.getDate() < 10) {
                            onlDay = '0' + onltime.getDate();
                        }
                        else {
                            onlDay = onltime.getDate();
                        }
                        if (onltime.getHours() < 10) {
                            onlHour = '0' + onltime.getHours();
                        }
                        else {
                            onlHour = onltime.getHours();
                        }
                        if (onltime.getMinutes() < 10) {
                            onlMin = '0' + onltime.getMinutes();
                        }
                        else {
                            onlMin = onltime.getMinutes();
                        }
                        onlDateTime = onlHour + '' + onlMin;
                        onlDateDate = onlDay + ' ' + months[onltime.getMonth()];
                        for (var b = 0; b < this.mastersearl.length; b++) {
                            this.checkedmasters1++;
                            if (userid == this.mastersearl[b].user_real_id) {
                                if (this.mastersearl[b].user_work_time && this.serverdate.getTime() < onltime.getTime()) {
                                    this.getWorkingTimeHist(this.mastersearl[b].user_work_time, onlDateDate, onlDateTime, this.mastersearl[b], v, limit);
                                    orderexist = 1;
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (orderinglength == 0) {
            if (orderexist == 0) {
                this.checkedmasters1++;
                var gottime = this.timezoneAdd(v);
                gottime = gottime * 1000;
                var onltime = new Date(gottime);
                var onlDateTime = void 0;
                var onlDateDate = void 0;
                // let onlMonth;
                var onlDay = void 0;
                var onlHour = void 0;
                var onlMin = void 0;
                // if(onltime.getMonth() < 9) {
                //   onlMonth = '0' + (onltime.getMonth() + 1);
                // } 
                // else {
                //   onlMonth = onltime.getMonth() + 1;
                // }
                if (onltime.getDate() < 10) {
                    onlDay = '0' + onltime.getDate();
                }
                else {
                    onlDay = onltime.getDate();
                }
                if (onltime.getHours() < 10) {
                    onlHour = '0' + onltime.getHours();
                }
                else {
                    onlHour = onltime.getHours();
                }
                if (onltime.getMinutes() < 10) {
                    onlMin = '0' + onltime.getMinutes();
                }
                else {
                    onlMin = onltime.getMinutes();
                }
                onlDateTime = onlHour + '' + onlMin;
                onlDateDate = onlDay + ' ' + months[onltime.getMonth()];
                for (var b = 0; b < this.mastersearl.length; b++) {
                    this.checkedmasters1++;
                    if (userid == this.mastersearl[b].user_real_id) {
                        if (this.mastersearl[b].user_work_time && this.serverdate.getTime() < onltime.getTime()) {
                            this.getWorkingTimeHist(this.mastersearl[b].user_work_time, onlDateDate, onlDateTime, this.mastersearl[b], v, limit);
                            orderexist = 1;
                        }
                    }
                }
            }
        }
    };
    BackendProvider.prototype.selRoomHist = function (userid, v, officesel, officeselmenue, limit, interval) {
        // console.log('YES 8')
        // GET ROOMS WHITH EXECUTION OF THIS MENU
        var roomslength = this.rooms.length;
        var varmenue1 = ',' + officeselmenue + ',';
        var varmenue2 = officeselmenue + ',';
        var varmenue3 = ',' + officeselmenue;
        var varmenue4 = officeselmenue;
        var varemployee1 = ',' + userid + ',';
        var varemployee2 = userid + ',';
        var varemployee3 = ',' + userid;
        var varemployee4 = userid;
        if (roomslength > 0) {
            for (var i = 0; i < roomslength; i++) {
                this.checkedmasters1++;
                if ((this.rooms[i].room_menue_exe.indexOf(varmenue1) >= 0 || this.rooms[i].room_menue_exe.indexOf(varmenue2) >= 0 || this.rooms[i].room_menue_exe.indexOf(varmenue3) >= 0 || this.rooms[i].room_menue_exe == varmenue4) && (this.rooms[i].room_employee.indexOf(varemployee1) >= 0 || this.rooms[i].room_employee.indexOf(varemployee2) >= 0 || this.rooms[i].room_employee.indexOf(varemployee3) >= 0 || this.rooms[i].room_employee == varemployee4) && this.rooms[i].room_office == officesel.office_id) {
                    this.getOrderingRoomsHist(userid, v, officesel, officeselmenue, this.rooms[i].room_id, limit, interval);
                }
            }
        }
    };
    BackendProvider.prototype.checkRoomsHist = function (userid, v, officesel, limit, interval) {
        // console.log('YES 7')
        var officeselmenue = officesel.office_menue.split(',');
        for (var i = 0; i < officeselmenue.length; i++) {
            this.checkedmasters1++;
            this.selRoomHist(userid, v, officesel, officeselmenue[i], limit, interval);
        }
    };
    BackendProvider.prototype.checkOrderingHist = function (userid, v, interval, officesel, limit) {
        // console.log('YES 6')
        // GET ORDERING (IF EMPLOYEE IS FREE AT THIS TIME)
        var lastV = v - interval;
        var orderinglength = this.orderings.length;
        var orderexist = 0;
        if (orderinglength > 0) {
            for (var i = 0; i < orderinglength; i++) {
                this.checkedmasters1++;
                var orderstart = this.orderings[i].order_start;
                if (this.orderings[i].order_worker == userid && orderstart <= v && orderstart > lastV) {
                    orderexist = 1;
                }
                else if (i == orderinglength - 1) {
                    if (orderexist == 0) {
                        this.checkRoomsHist(userid, v, officesel, limit, interval);
                    }
                }
            }
        }
        else if (orderinglength == 0) {
            this.checkRoomsHist(userid, v, officesel, limit, interval);
        }
    };
    BackendProvider.prototype.checkScheduleOrderHist = function (userid, schedule, interval, officesel, limit) {
        // console.log('YES 5')
        for (var i = schedule.schedule_start; i < schedule.schedule_stop; i += interval) {
            this.checkedmasters1++;
            this.checkOrderingHist(userid, i, interval, officesel, limit);
        }
    };
    BackendProvider.prototype.checkScheduleHist3Days = function (userid, interval, dates, officesel, limit) {
        // console.log('YES 4')
        var dated = dates;
        if (this.serverdateraw > dates) {
            dated = parseInt(this.serverdateraw);
        }
        var datebegin = (new Date(dated * 1000).setHours(0, 0, 0, 0) / 1000).toFixed(0);
        var dateend = (new Date(dated * 1000).setHours(23, 59, 59, 999) / 1000).toFixed(0);
        if (this.serverdateraw > dated) {
            datebegin = (new Date(this.serverdateraw * 1000).setHours(0, 0, 0, 0) / 1000).toFixed(0);
            dateend = (new Date(this.serverdateraw * 1000).setHours(23, 59, 59, 999) / 1000).toFixed(0);
        }
        var schedulelength = this.schedules.length;
        if (schedulelength > 0) {
            for (var i = 0; i < schedulelength; i++) {
                this.checkedmasters1++;
                var schedulestart = this.schedules[i].schedule_start;
                if (this.schedules[i].schedule_employee == userid && schedulestart >= datebegin && schedulestart < dateend) {
                    this.checkScheduleOrderHist(userid, this.schedules[i], interval, officesel, limit);
                }
            }
        }
    };
    BackendProvider.prototype.checkScheduleHist = function (userid, interval, officesel, limit, datefrom) {
        // console.log('YES 3')
        var daysfuture = 3;
        for (var d = 0; d < daysfuture; d++) {
            var dates = parseInt(datefrom) + (86400 * d);
            if (this.serverdateraw > datefrom) {
                dates = parseInt(this.serverdateraw) + (86400 * d);
            }
            this.checkScheduleHist3Days(userid, interval, dates, officesel, limit);
        }
    };
    BackendProvider.prototype.checkOfficeHist = function (userid, interval, officesel, limit, datefrom) {
        // console.log('YES 2')
        for (var i = 0; i < officesel.length; i++) {
            this.checkedmasters1++;
            this.checkScheduleHist(userid, interval, officesel[i], limit, datefrom);
        }
    };
    BackendProvider.prototype.getMastersHist = function (ritual, interval, officesel, userid, limit, datefrom) {
        var _this = this;
        interval = 30 * 60;
        // console.log('YES 1 ')
        this.mastersearl = [];
        this.checkedmasters1 = 0;
        this.checkedmasters2 = 0;
        return new Promise(function (resolve) {
            setTimeout(function () {
                var sqlreq = "SELECT * FROM users WHERE user_work_pos>'1' AND (user_menue_exe LIKE '%," + ritual + ",%' OR user_menue_exe LIKE '" + ritual + ",%' OR user_menue_exe LIKE '%," + ritual + "' OR user_menue_exe = '" + ritual + "') AND user_del='0' ORDER BY user_id ASC";
                if (ritual == 0) {
                    sqlreq = "SELECT * FROM users WHERE user_id>'1' AND user_work_pos>'1' AND user_del='0' ORDER BY user_id ASC";
                }
                if (userid == '-') {
                    _this.mastersearl.push({ "in_id": 0, "user_city": null, "user_device_serial": null, "user_log": null, "user_pic": null, "user_reg": null, "user_menue_exe": "all", "user_institution": _this.institution, "user_del": 0, "user_id": null, "user_email_confirm": null, "user_real_id": null, "user_name": "любой мастер", "user_region": null, "user_pwd": null, "user_device_version": null, "user_upd": null, "user_conf_req": null, "user_middlename": "", "user_country": null, "user_discount": null, "user_gender": null, "user_adress": null, "user_tel": null, "user_gcm": null, "user_log_key": null, "user_birthday": null, "user_mob": null, "user_info": null, "user_office": null, "user_install_where": null, "user_promo": null, "user_email": null, "user_device": null, "user_device_id": null, "user_surname": "", "user_device_os": null, "user_work_pos": 2, "user_mob_confirm": null, "user_work_time": [] });
                }
                _this.database.executeSql(sqlreq, {})
                    .then(function (suc) {
                    if (suc.rows.length > 0) {
                        if (userid > 0) {
                            for (var i = 0; i < suc.rows.length; i++) {
                                if (userid == suc.rows.item(i).user_real_id) {
                                    suc.rows.item(i).in_id = i + 1;
                                    suc.rows.item(i).user_work_time = [];
                                    _this.mastersearl.push(suc.rows.item(i));
                                    _this.checkOfficeHist(suc.rows.item(i).user_real_id, interval, officesel, limit, datefrom);
                                }
                                if (i == suc.rows.length - 1) {
                                    _this.checkintervalearl = setInterval(function () {
                                        if (_this.checkedmasters1 == _this.checkedmasters2) {
                                            clearInterval(_this.checkintervalearl);
                                            if (userid == '-') {
                                                _this.checkWorkTimeForRandomHist().then(function () {
                                                    var p = _this.mastersearl.length - 1;
                                                    while (_this.mastersearl.length) {
                                                        if (_this.mastersearl[p].in_id != 0) {
                                                            _this.mastersearl.splice(p, 1);
                                                        }
                                                        if (p == 0) {
                                                            resolve(_this.mastersearl);
                                                        }
                                                        p--;
                                                    }
                                                });
                                            }
                                            else {
                                                if (_this.mastersearl.length > 0) {
                                                    for (var p = 0; p < _this.mastersearl.length; p++) {
                                                        _this.mastersearl[p].in_id = p;
                                                        if (p == _this.mastersearl.length - 1) {
                                                            resolve(_this.mastersearl);
                                                        }
                                                    }
                                                }
                                                else {
                                                    resolve(_this.mastersearl);
                                                }
                                            }
                                        }
                                        else {
                                            _this.checkedmasters2 = _this.checkedmasters1;
                                        }
                                    }, 1000);
                                }
                            }
                        }
                        else {
                            for (var i = 0; i < suc.rows.length; i++) {
                                suc.rows.item(i).in_id = i + 1;
                                suc.rows.item(i).user_work_time = [];
                                _this.mastersearl.push(suc.rows.item(i));
                                if (i == suc.rows.length - 1) {
                                    _this.checkOfficeHist(suc.rows.item(i).user_real_id, interval, officesel, limit, datefrom);
                                    _this.checkintervalearl = setInterval(function () {
                                        if (_this.checkedmasters1 == _this.checkedmasters2) {
                                            clearInterval(_this.checkintervalearl);
                                            if (userid == '-') {
                                                _this.checkWorkTimeForRandomHist().then(function () {
                                                    var p = _this.mastersearl.length - 1;
                                                    while (_this.mastersearl.length) {
                                                        if (_this.mastersearl[p].in_id != 0) {
                                                            _this.mastersearl.splice(p, 1);
                                                        }
                                                        if (p == 0) {
                                                            resolve(_this.mastersearl);
                                                        }
                                                        p--;
                                                    }
                                                });
                                            }
                                            else {
                                                if (_this.mastersearl.length > 0) {
                                                    for (var p = 0; p < _this.mastersearl.length; p++) {
                                                        _this.mastersearl[p].in_id = p;
                                                        if (p == _this.mastersearl.length - 1) {
                                                            resolve(_this.mastersearl);
                                                        }
                                                    }
                                                }
                                                else {
                                                    resolve(_this.mastersearl);
                                                }
                                            }
                                        }
                                        else {
                                            _this.checkedmasters2 = _this.checkedmasters1;
                                        }
                                    }, 1000);
                                }
                                else {
                                    _this.checkOfficeHist(suc.rows.item(i).user_real_id, interval, officesel, limit, datefrom);
                                }
                            }
                        }
                    }
                    else {
                        resolve(_this.mastersearl);
                    }
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    // END OF - TAKE MASTER AND MASTER HISTORY ---------------------------------
    // BEGIN OF - TAKE MASTER AND MASTER EARLIEST WORKTIME SECTION ---------------------------------
    BackendProvider.prototype.getWorkingTimeEarl = function (user_work_time, onlDateDate, onlDateTime, mastr, v) {
        // console.log('YES 10')
        var stopgo = 0;
        if (user_work_time.length == 0) {
            mastr.user_work_time.push({ dayday: onlDateDate, daytime: onlDateTime, unixtime: v });
        }
        else {
            for (var i = 0; i < user_work_time.length; i++) {
                if (stopgo == 0) {
                    if (user_work_time[i].dayday == onlDateDate) {
                        stopgo = 1;
                    }
                    else if (i == user_work_time.length - 1 && user_work_time[i].dayday != onlDateDate) {
                        mastr.user_work_time.push({ dayday: onlDateDate, daytime: onlDateTime, unixtime: v });
                    }
                }
            }
        }
    };
    BackendProvider.prototype.getOrderingRoomsEarl = function (userid, v, ritual, officesel, officeselmenue, roomid, interval) {
        // console.log('YES 9')
        var months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        // GET ORDERING ROOMS (IF IS FREE AT THIS TIME)
        var orderinglength = this.orderings.length;
        var orderexist = 0;
        if (orderinglength > 0) {
            for (var i = 0; i < orderinglength; i++) {
                this.checkedmasters1++;
                var orderstart = this.orderings[i].order_start;
                var orderstop = this.orderings[i].order_end;
                var vEnd = v + interval;
                if (this.orderings[i].order_office == officesel.office_id && this.orderings[i].order_room == roomid && orderstart <= v && orderstop > v) {
                    orderexist = 1;
                }
                else if (orderstart < vEnd && orderstop > vEnd) {
                    orderexist = 1;
                }
                if (i == orderinglength - 1) {
                    if (orderexist == 0) {
                        var gottime = this.timezoneAdd(v);
                        gottime = gottime * 1000;
                        var onltime = new Date(gottime);
                        var onlDateTime = void 0;
                        var onlDateDate = void 0;
                        // let onlMonth;
                        var onlDay = void 0;
                        var onlHour = void 0;
                        var onlMin = void 0;
                        // if(onltime.getMonth() < 9) {
                        //   onlMonth = '0' + (onltime.getMonth() + 1);
                        // } else {
                        //   onlMonth = onltime.getMonth() + 1;
                        // }
                        if (onltime.getDate() < 10) {
                            onlDay = '0' + onltime.getDate();
                        }
                        else {
                            onlDay = onltime.getDate();
                        }
                        if (onltime.getHours() < 10) {
                            onlHour = '0' + onltime.getHours();
                        }
                        else {
                            onlHour = onltime.getHours();
                        }
                        if (onltime.getMinutes() < 10) {
                            onlMin = '0' + onltime.getMinutes();
                        }
                        else {
                            onlMin = onltime.getMinutes();
                        }
                        onlDateTime = onlHour + '' + onlMin;
                        onlDateDate = onlDay + ' ' + months[onltime.getMonth()];
                        for (var b = 0; b < this.mastersearl.length; b++) {
                            this.checkedmasters1++;
                            if (userid == this.mastersearl[b].user_real_id) {
                                if (this.mastersearl[b].user_work_time && this.serverdate.getTime() < onltime.getTime()) {
                                    this.getWorkingTimeEarl(this.mastersearl[b].user_work_time, onlDateDate, onlDateTime, this.mastersearl[b], v);
                                    orderexist = 1;
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (orderinglength == 0) {
            if (orderexist == 0) {
                var gottime = this.timezoneAdd(v);
                gottime = gottime * 1000;
                var onltime = new Date(gottime);
                var onlDateTime = void 0;
                var onlDateDate = void 0;
                // let onlMonth;
                var onlDay = void 0;
                var onlHour = void 0;
                var onlMin = void 0;
                // if(onltime.getMonth() < 9) {
                //   onlMonth = '0' + (onltime.getMonth() + 1);
                // } else {
                //   onlMonth = onltime.getMonth() + 1;
                // }
                if (onltime.getDate() < 10) {
                    onlDay = '0' + onltime.getDate();
                }
                else {
                    onlDay = onltime.getDate();
                }
                if (onltime.getHours() < 10) {
                    onlHour = '0' + onltime.getHours();
                }
                else {
                    onlHour = onltime.getHours();
                }
                if (onltime.getMinutes() < 10) {
                    onlMin = '0' + onltime.getMinutes();
                }
                else {
                    onlMin = onltime.getMinutes();
                }
                onlDateTime = onlHour + '' + onlMin;
                onlDateDate = onlDay + ' ' + months[onltime.getMonth()];
                for (var b = 0; b < this.mastersearl.length; b++) {
                    this.checkedmasters1++;
                    if (userid == this.mastersearl[b].user_real_id) {
                        if (this.mastersearl[b].user_work_time && this.serverdate.getTime() < onltime.getTime()) {
                            this.getWorkingTimeEarl(this.mastersearl[b].user_work_time, onlDateDate, onlDateTime, this.mastersearl[b], v);
                            orderexist = 1;
                        }
                    }
                }
            }
        }
    };
    BackendProvider.prototype.selRoomEarl = function (userid, v, ritual, officesel, officeselmenue, interval) {
        // console.log('YES 8')
        // GET ROOMS WHITH EXECUTION OF THIS MENU
        var roomslength = this.rooms.length;
        var varmenue1 = ',' + officeselmenue + ',';
        var varmenue2 = officeselmenue + ',';
        var varmenue3 = ',' + officeselmenue;
        var varmenue4 = officeselmenue;
        var varemployee1 = ',' + userid + ',';
        var varemployee2 = userid + ',';
        var varemployee3 = ',' + userid;
        var varemployee4 = userid;
        if (roomslength > 0) {
            for (var i = 0; i < roomslength; i++) {
                this.checkedmasters1++;
                if ((this.rooms[i].room_menue_exe.indexOf(varmenue1) >= 0 || this.rooms[i].room_menue_exe.indexOf(varmenue2) >= 0 || this.rooms[i].room_menue_exe.indexOf(varmenue3) >= 0 || this.rooms[i].room_menue_exe == varmenue4) && (this.rooms[i].room_employee.indexOf(varemployee1) >= 0 || this.rooms[i].room_employee.indexOf(varemployee2) >= 0 || this.rooms[i].room_employee.indexOf(varemployee3) >= 0 || this.rooms[i].room_employee == varemployee4) && this.rooms[i].room_office == officesel.office_id) {
                    this.checkedmasters1++;
                    this.getOrderingRoomsEarl(userid, v, ritual, officesel, officeselmenue, this.rooms[i].room_id, interval);
                }
            }
        }
    };
    BackendProvider.prototype.checkRoomsEarl = function (userid, v, ritual, officesel, interval) {
        // console.log('YES 7')
        var officeselmenue = officesel.office_menue.split(',');
        for (var i = 0; i < officeselmenue.length; i++) {
            this.checkedmasters1++;
            this.selRoomEarl(userid, v, ritual, officesel, officeselmenue[i], interval);
        }
    };
    BackendProvider.prototype.checkOrderingEarl = function (userid, v, interval, ritual, officesel) {
        // console.log('YES 6')
        // GET ORDERING (IF EMPLOYEE IS FREE AT THIS TIME)
        var lastV = v - interval;
        var orderinglength = this.orderings.length;
        var orderexist = 0;
        if (orderinglength > 0) {
            for (var i = 0; i < orderinglength; i++) {
                this.checkedmasters1++;
                var orderstart = this.orderings[i].order_start;
                if (this.orderings[i].order_worker == userid && orderstart <= v && orderstart > lastV) {
                    orderexist = 1;
                }
                else if (i == orderinglength - 1) {
                    if (orderexist == 0) {
                        this.checkRoomsEarl(userid, v, ritual, officesel, interval);
                    }
                }
            }
        }
        else if (orderinglength == 0) {
            this.checkRoomsEarl(userid, v, ritual, officesel, interval);
        }
    };
    BackendProvider.prototype.checkScheduleOrderEarl = function (userid, schedule, interval, ritual, officesel) {
        // console.log('YES 5')
        for (var i = schedule.schedule_start; i < schedule.schedule_stop; i += interval) {
            this.checkedmasters1++;
            this.checkOrderingEarl(userid, i, interval, ritual, officesel);
        }
    };
    BackendProvider.prototype.checkScheduleEarl3Days = function (userid, interval, dates, ritual, officesel) {
        // console.log('YES 4')
        var datebegin = (new Date(dates * 1000).setHours(0, 0, 0, 0) / 1000).toFixed(0);
        var dateend = (new Date(dates * 1000).setHours(23, 59, 59, 999) / 1000).toFixed(0);
        if (this.serverdateraw > dates) {
            datebegin = (new Date(this.serverdateraw * 1000).setHours(0, 0, 0, 0) / 1000).toFixed(0);
            dateend = (new Date(this.serverdateraw * 1000).setHours(23, 59, 59, 999) / 1000).toFixed(0);
        }
        var schedulelength = this.schedules.length;
        if (schedulelength > 0) {
            for (var i = 0; i < schedulelength; i++) {
                this.checkedmasters1++;
                var schedulestart = this.schedules[i].schedule_start;
                if (this.schedules[i].schedule_employee == userid && schedulestart >= datebegin && schedulestart < dateend) {
                    this.checkScheduleOrderEarl(userid, this.schedules[i], interval, ritual, officesel);
                }
            }
        }
    };
    BackendProvider.prototype.checkScheduleEarl = function (userid, interval, ritual, officesel, datefrom) {
        // console.log('YES 3')
        var daysfuture = 3;
        for (var d = 0; d < daysfuture; d++) {
            var dates = parseInt(datefrom) + (86400 * d);
            if (this.serverdateraw > datefrom) {
                dates = parseInt(this.serverdateraw) + (86400 * d);
            }
            this.checkScheduleEarl3Days(userid, interval, dates, ritual, officesel);
        }
    };
    BackendProvider.prototype.checkOfficeEarl = function (userid, interval, ritual, officesel, datefrom) {
        // console.log('YES 2')
        for (var i = 0; i < officesel.length; i++) {
            this.checkScheduleEarl(userid, interval, ritual, officesel[i], datefrom);
        }
    };
    BackendProvider.prototype.getMastersEarl = function (ritual, interval, officesel, datefrom) {
        var _this = this;
        interval = 30 * 60;
        // console.log('YES 1')
        this.mastersearl = [];
        this.checkedmasters1 = 0;
        this.checkedmasters2 = 0;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM users WHERE user_work_pos>'1' AND (user_menue_exe LIKE '%," + ritual + ",%' OR user_menue_exe LIKE '" + ritual + ",%' OR user_menue_exe LIKE '%," + ritual + "' OR user_menue_exe = '" + ritual + "') AND user_del='0' ORDER BY user_id ASC", {})
                    .then(function (suc) {
                    if (suc.rows.length > 0) {
                        for (var i = 0; i < suc.rows.length; i++) {
                            // console.log('YES 11')
                            suc.rows.item(i).in_id = i + 1;
                            suc.rows.item(i).user_work_time = [];
                            _this.mastersearl.push(suc.rows.item(i));
                            if (i == suc.rows.length - 1) {
                                // console.log('YES 12')
                                _this.checkOfficeEarl(_this.mastersearl[i].user_real_id, interval, ritual, officesel, datefrom);
                                _this.checkintervalearl = setInterval(function () {
                                    // console.log('YES 14')
                                    if (_this.checkedmasters1 == _this.checkedmasters2) {
                                        // console.log('YES 16')
                                        clearInterval(_this.checkintervalearl);
                                        for (var p = 0; p < _this.mastersearl.length; p++) {
                                            _this.mastersearl[p].in_id = p;
                                            if (p == _this.mastersearl.length - 1) {
                                                resolve(_this.mastersearl);
                                            }
                                        }
                                    }
                                    else {
                                        // console.log('YES 15')
                                        _this.checkedmasters2 = _this.checkedmasters1;
                                    }
                                }, 2000);
                            }
                            else {
                                // console.log('YES 13')
                                _this.checkOfficeEarl(suc.rows.item(i).user_real_id, interval, ritual, officesel, datefrom);
                            }
                        }
                    }
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    // END OF - TAKE MASTER AND MASTER EARLIEST WORKTIME SECTION ---------------------------------
    // BEGIN OF - TAKE MASTER AND MASTER WORKTIME SECTION ---------------------------------
    BackendProvider.prototype.setWorkTimeForRandom = function (times, unix) {
        // console.log('YES 9 ')
        for (var i = 0; i < times.length; i++) {
            if (this.masters[0].user_work_time.indexOf(times[i]) == '-1') {
                this.masters[0].user_work_time.push(times[i]);
                this.masters[0].unixtime.push(unix[i]);
            }
        }
    };
    BackendProvider.prototype.checkWorkTimeForRandom = function () {
        var _this = this;
        // console.log('YES 8 ')
        return new Promise(function (resolve) {
            setTimeout(function () {
                var res = [];
                if (_this.masters.length > 0) {
                    var i = _this.masters.length - 1;
                    while (_this.masters[i]) {
                        if (_this.masters[i].in_id > 0) {
                            if (_this.masters[i].user_work_time.length > 0) {
                                _this.setWorkTimeForRandom(_this.masters[i].user_work_time, _this.masters[i].unixtime);
                            }
                            else if (_this.masters[i].user_work_time.length == 0) {
                                _this.masters.splice(i, 1);
                            }
                        }
                        i--;
                    }
                }
                resolve(res);
            }, 1000);
        });
    };
    BackendProvider.prototype.getOrderingRooms = function (userid, v, ritual, officesel, officeselmenue, roomid, interval, fixedint) {
        // console.log('YES 7 ')
        // GET ORDERING ROOMS (IF IS FREE AT THIS TIME)
        var orderinglength = this.orderings.length;
        var orderexist = 0;
        if (orderinglength > 0) {
            // console.log('YES 70 ')
            for (var i = 0; i < orderinglength; i++) {
                this.checkedmasters1++;
                var orderstart = this.orderings[i].order_start;
                var orderstop = this.orderings[i].order_end;
                var prevorderstart = 0;
                var prevorderstop = 0;
                var prevEnd = 0;
                if (i > 0) {
                    prevorderstart = this.orderings[i - 1].order_start;
                    prevorderstop = this.orderings[i - 1].order_end;
                    prevEnd = v - interval;
                }
                var vEnd = v + fixedint;
                if (this.orderings[i + 1].order_start <= vEnd) {
                    console.log('YES YES YES ================================== YES YES YES');
                    orderexist = 1;
                }
                else if (this.orderings[i].order_office == officesel.office_id && this.orderings[i].order_room == roomid && orderstart <= v && orderstop > v) {
                    console.log('LOLO LOLO LOLO ================================== LOLO LOLO LOLO');
                    orderexist = 1;
                    // console.log('YES 71 ')
                }
                else if (prevorderstart != 0 && prevorderstop != 0 && prevEnd != 0 && prevorderstart < v && prevorderstop > v && prevorderstop > orderstart) {
                    console.log('LILI LILI LILI ================================== LILI LILI LILI');
                    orderexist = 1;
                }
                else if (orderstart < vEnd && orderstop > vEnd) {
                    orderexist = 1;
                }
                if (i == orderinglength - 1) {
                    console.log('NO NO NO ================================== NO NO NO');
                    // console.log('YES 72 ')
                    if (orderexist == 0) {
                        var gottime = this.timezoneAdd(v);
                        gottime = gottime * 1000;
                        var onltime = new Date(gottime);
                        var onlDateTime = void 0;
                        var onlHour = void 0;
                        var onlMin = void 0;
                        // HOURS AND MINUTES AGO
                        if (onltime.getHours() < 10) {
                            onlHour = '0' + onltime.getHours();
                        }
                        else {
                            onlHour = onltime.getHours();
                        }
                        if (onltime.getMinutes() < 10) {
                            onlMin = '0' + onltime.getMinutes();
                        }
                        else {
                            onlMin = onltime.getMinutes();
                        }
                        onlDateTime = onlHour + '' + onlMin;
                        for (var b = 0; b < this.masters.length; b++) {
                            this.checkedmasters1++;
                            if (userid == this.masters[b].user_real_id) {
                                // console.log(this.serverdate.getTime() + ' | ' + onltime.getTime())
                                if (this.masters[b].user_work_time && this.serverdate.getTime() < onltime.getTime()) {
                                    if (this.masters[b].user_work_time.indexOf(onlDateTime) == '-1') {
                                        this.masters[b].user_work_time.push(onlDateTime);
                                        this.masters[b].unixtime.push(v);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (orderinglength == 0) {
            console.log('YES YES YES ================================== YES YES YES');
            // console.log('YES 73 ')
            if (orderexist == 0) {
                // console.log('YES 74 ')
                var gottime = this.timezoneAdd(v);
                gottime = gottime * 1000;
                var onltime = new Date(gottime);
                var onlDateTime = void 0;
                var onlHour = void 0;
                var onlMin = void 0;
                // HOURS AND MINUTES AGO
                if (onltime.getHours() < 10) {
                    onlHour = '0' + onltime.getHours();
                }
                else {
                    onlHour = onltime.getHours();
                }
                if (onltime.getMinutes() < 10) {
                    onlMin = '0' + onltime.getMinutes();
                }
                else {
                    onlMin = onltime.getMinutes();
                }
                onlDateTime = onlHour + '' + onlMin;
                for (var b = 0; b < this.masters.length; b++) {
                    this.checkedmasters1++;
                    if (userid == this.masters[b].user_real_id) {
                        // console.log('YES 75 ')
                        // console.log(this.serverdate.getTime() + ' | ' + onltime.getTime())
                        if (this.masters[b].user_work_time && this.serverdate.getTime() < onltime.getTime()) {
                            // console.log('YES 76 '+this.serverdate.getTime()+' | '+onltime.getTime())
                            if (this.masters[b].user_work_time.indexOf(onlDateTime) == '-1') {
                                // console.log('YES 77 ')
                                this.masters[b].user_work_time.push(onlDateTime);
                                this.masters[b].unixtime.push(v);
                            }
                        }
                    }
                }
            }
        }
    };
    BackendProvider.prototype.selRoom = function (userid, v, ritual, officesel, officeselmenue, interval, fixedint) {
        // console.log('YES 6 ')
        // GET ROOMS WHITH EXECUTION OF THIS MENU
        var roomslength = this.rooms.length;
        var varmenue1 = ',' + officeselmenue + ',';
        var varmenue2 = officeselmenue + ',';
        var varmenue3 = ',' + officeselmenue;
        var varmenue4 = officeselmenue;
        var varemployee1 = ',' + userid + ',';
        var varemployee2 = userid + ',';
        var varemployee3 = ',' + userid;
        var varemployee4 = userid;
        if (roomslength > 0) {
            for (var i = 0; i < roomslength; i++) {
                this.checkedmasters1++;
                if ((this.rooms[i].room_menue_exe.indexOf(varmenue1) >= 0 || this.rooms[i].room_menue_exe.indexOf(varmenue2) >= 0 || this.rooms[i].room_menue_exe.indexOf(varmenue3) >= 0 || this.rooms[i].room_menue_exe == varmenue4) && (this.rooms[i].room_employee.indexOf(varemployee1) >= 0 || this.rooms[i].room_employee.indexOf(varemployee2) >= 0 || this.rooms[i].room_employee.indexOf(varemployee3) >= 0 || this.rooms[i].room_employee == varemployee4) && this.rooms[i].room_office == officesel.office_id) {
                    this.checkedmasters1++;
                    this.getOrderingRooms(userid, v, ritual, officesel, officeselmenue, this.rooms[i].room_id, interval, fixedint);
                }
            }
        }
    };
    BackendProvider.prototype.checkRooms = function (userid, v, ritual, officesel, interval, fixedint) {
        // console.log('YES 5 ')
        var officeselmenue = officesel.office_menue.split(',');
        for (var i = 0; i < officeselmenue.length; i++) {
            this.checkedmasters1++;
            this.selRoom(userid, v, ritual, officesel, officeselmenue[i], interval, fixedint);
        }
    };
    BackendProvider.prototype.checkOrdering = function (userid, v, interval, fixedint, ritual, officesel) {
        // console.log('YES 4 ')
        // GET ORDERING (IF EMPLOYEE IS FREE AT THIS TIME)
        var lastV = v - interval;
        var orderinglength = this.orderings.length;
        var orderexist = 0;
        if (orderinglength > 0) {
            for (var i = 0; i < orderinglength; i++) {
                this.checkedmasters1++;
                var orderstart = this.orderings[i].order_start;
                if (this.orderings[i].order_worker == userid && orderstart <= v && orderstart > lastV) {
                    orderexist = 1;
                }
                else if (i == orderinglength - 1) {
                    if (orderexist == 0) {
                        this.checkRooms(userid, v, ritual, officesel, interval, fixedint);
                    }
                }
            }
        }
        else if (orderinglength == 0) {
            this.checkRooms(userid, v, ritual, officesel, interval, fixedint);
        }
    };
    BackendProvider.prototype.checkScheduleOrder = function (userid, schedule, interval, fixedint, ritual, officesel) {
        // console.log('YES 3 ')
        for (var i = schedule.schedule_start; i < schedule.schedule_stop; i += fixedint) {
            this.checkedmasters1++;
            this.checkOrdering(userid, i, interval, fixedint, ritual, officesel);
        }
    };
    BackendProvider.prototype.checkSchedule = function (userid, interval, fixedint, ritual, officesel, datefrom) {
        // console.log('YES 2 ')
        var datebegin = (new Date(datefrom * 1000).setHours(0, 0, 0, 0) / 1000).toFixed(0);
        var dateend = (new Date(datefrom * 1000).setHours(23, 59, 59, 999) / 1000).toFixed(0);
        if (this.serverdateraw > datefrom) {
            datebegin = (new Date(this.serverdateraw * 1000).setHours(0, 0, 0, 0) / 1000).toFixed(0);
            dateend = (new Date(this.serverdateraw * 1000).setHours(23, 59, 59, 999) / 1000).toFixed(0);
        }
        var schedulelength = this.schedules.length;
        if (schedulelength > 0) {
            for (var i = 0; i < schedulelength; i++) {
                this.checkedmasters1++;
                var schedulestart = this.schedules[i].schedule_start;
                if (this.schedules[i].schedule_employee == userid && schedulestart >= datebegin && schedulestart < dateend) {
                    this.checkScheduleOrder(userid, this.schedules[i], interval, fixedint, ritual, officesel);
                }
            }
        }
    };
    BackendProvider.prototype.getMasters = function (ritual, interval, officesel, datefrom) {
        var _this = this;
        // console.log('YES 1 '+datefrom)
        this.masters = [];
        this.checkedmasters1 = 0;
        this.checkedmasters2 = 0;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.database.executeSql("SELECT * FROM users WHERE user_work_pos>'1' AND (user_menue_exe LIKE '%," + ritual + ",%' OR user_menue_exe LIKE '" + ritual + ",%' OR user_menue_exe LIKE '%," + ritual + "' OR user_menue_exe = '" + ritual + "') AND user_del='0' ORDER BY user_id ASC", {})
                    .then(function (suc) {
                    if (suc.rows.length > 0) {
                        _this.masters.push({ "in_id": 0, "user_city": null, "user_device_serial": null, "user_log": null, "user_pic": null, "user_reg": null, "user_menue_exe": "all", "user_institution": _this.institution, "user_del": 0, "user_id": null, "user_email_confirm": null, "user_real_id": null, "user_name": "любой мастер", "user_region": null, "user_pwd": null, "user_device_version": null, "user_upd": null, "user_conf_req": null, "user_middlename": "", "user_country": null, "user_discount": null, "user_gender": null, "user_adress": null, "user_tel": null, "user_gcm": null, "user_log_key": null, "user_birthday": null, "user_mob": null, "user_info": null, "user_office": null, "user_install_where": null, "user_promo": null, "user_email": null, "user_device": null, "user_device_id": null, "user_surname": "", "user_device_os": null, "user_work_pos": 2, "user_mob_confirm": null, "user_work_time": [], "unixtime": [] });
                        // console.log('SUC LENGTH ======> '+suc.rows.length)
                        for (var i = 0; i < suc.rows.length; i++) {
                            // console.log('USERS GOT ======> '+suc.rows.length+' | '+suc.rows.item(i).user_real_id + ' | ' + interval + ' | ' + ritual + ' | ' + JSON.stringify(officesel) + ' | ' + JSON.stringify(suc.rows.item(i)))
                            suc.rows.item(i).in_id = i + 1;
                            suc.rows.item(i).user_work_time = [];
                            suc.rows.item(i).unixtime = [];
                            _this.masters.push(suc.rows.item(i));
                            if (i == suc.rows.length - 1) {
                                _this.checkSchedule(suc.rows.item(i).user_real_id, interval, 1800, ritual, officesel, datefrom);
                                _this.checkinterval = setInterval(function () {
                                    // console.log(this.checkedmasters1 + ' === ' + this.checkedmasters2)
                                    if (_this.checkedmasters1 == _this.checkedmasters2) {
                                        clearInterval(_this.checkinterval);
                                        _this.checkWorkTimeForRandom().then(function () {
                                            for (var p = 0; p < _this.masters.length; p++) {
                                                _this.masters[p].in_id = p;
                                                if (p == _this.masters.length - 1) {
                                                    resolve(_this.masters);
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        _this.checkedmasters2 = _this.checkedmasters1;
                                    }
                                }, 1000);
                            }
                            else {
                                _this.checkSchedule(suc.rows.item(i).user_real_id, interval, 1800, ritual, officesel, datefrom);
                            }
                        }
                    }
                })
                    .catch(function (er) {
                    resolve(er);
                });
            }, 1000);
        });
    };
    // END OF - TAKE MASTER AND MASTER WORKTIME SECTION ---------------------------------
    BackendProvider.prototype.getMaster = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            if (id > 0) {
                setTimeout(function () {
                    var res = [];
                    _this.database.executeSql("SELECT * FROM users WHERE user_real_id='" + id + "' LIMIT 1", {})
                        .then(function (suc) {
                        if (suc.rows.length > 0) {
                            for (var i = 0; i < suc.rows.length; i++) {
                                res.push(suc.rows.item(i));
                            }
                        }
                        resolve(res);
                    })
                        .catch();
                }, 1000);
            }
            else {
                setTimeout(function () {
                    var res = [{ "in_id": 0, "user_city": null, "user_device_serial": null, "user_log": null, "user_pic": null, "user_reg": null, "user_menue_exe": "all", "user_institution": _this.institution, "user_del": 0, "user_id": null, "user_email_confirm": null, "user_real_id": null, "user_name": "любой мастер", "user_region": null, "user_pwd": null, "user_device_version": null, "user_upd": null, "user_conf_req": null, "user_middlename": "", "user_country": null, "user_discount": null, "user_gender": null, "user_adress": null, "user_tel": null, "user_gcm": null, "user_log_key": null, "user_birthday": null, "user_mob": null, "user_info": null, "user_office": null, "user_install_where": null, "user_promo": null, "user_email": null, "user_device": null, "user_device_id": null, "user_surname": "", "user_device_os": null, "user_work_pos": 2, "user_mob_confirm": null, "user_work_time": [], "unixtime": [] }];
                    resolve(res);
                }, 1000);
            }
        });
    };
    BackendProvider.prototype.formatTime = function (x) {
        // ОПРЕДЕЛЕНИЕ ВРЕМЯ
        var gottime = this.timezoneAdd(x);
        gottime = gottime * 1000;
        var onltime = new Date(gottime);
        var onlDateTime;
        var onlMonth;
        var onlDay;
        var onlHour;
        var onlMin;
        var days = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
        var pushday1 = days[onltime.getDay()];
        if (onltime.getMonth() < 9) {
            onlMonth = '0' + (onltime.getMonth() + 1);
        }
        else {
            onlMonth = onltime.getMonth() + 1;
        }
        if (onltime.getDate() < 10) {
            onlDay = '0' + onltime.getDate();
        }
        else {
            onlDay = onltime.getDate();
        }
        if (onltime.getHours() < 10) {
            onlHour = '0' + onltime.getHours();
        }
        else {
            onlHour = onltime.getHours();
        }
        if (onltime.getMinutes() < 10) {
            onlMin = '0' + onltime.getMinutes();
        }
        else {
            onlMin = onltime.getMinutes();
        }
        onlDateTime = pushday1 + ' ' + onlDay + '.' + onlMonth + '.' + onltime.getFullYear() + ' ' + onlHour + ':' + onlMin;
        return onlDateTime;
    };
    // TIME TAKE FROM DB
    BackendProvider.prototype.timezoneAdd = function (val) {
        var thetime = parseInt(val) * 1000;
        var tzoff = new Date().getTimezoneOffset();
        var nsec = tzoff * 60 * 1000;
        var serverzone = 3 * 60 * 60 * 1000;
        var timediff = nsec + serverzone;
        return ((thetime + timediff) / 1000).toFixed(0);
    };
    // TIME SAVE TO DB OR TO SERVER
    BackendProvider.prototype.timezoneSub = function (val) {
        var thetime = parseInt(val) * 1000;
        var tzoff = new Date().getTimezoneOffset();
        var nsec = tzoff * 60 * 1000;
        var serverzone = 3 * 60 * 60 * 1000;
        var timediff = nsec + serverzone;
        return ((thetime - timediff) / 1000).toFixed(0);
    };
    BackendProvider.prototype.decodeEntities = function (val) {
        var element = document.getElementById('htmlentitydecodediv');
        if (val && typeof val === 'string') {
            // strip script/html tags
            val = val.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
            val = val.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
            element.innerHTML = val;
            val = element.textContent;
            element.textContent = '';
        }
        return val;
    };
    return BackendProvider;
}());
BackendProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_9_ionic_angular__["s" /* Platform */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_device__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_11__ionic_native_push__["a" /* Push */]])
], BackendProvider);

//# sourceMappingURL=backend.js.map

/***/ })

},[342]);
//# sourceMappingURL=main.js.map