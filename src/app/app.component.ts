import { Component } from '@angular/core';
import { App, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { NewsPage } from '../pages/news/news';
import { AboutPage } from '../pages/about/about';
import { ProfilePage } from '../pages/profile/profile';
import { SupportFaqPage } from '../pages/support-faq/support-faq';
import { IntroPage } from '../pages/intro/intro';
import { SurveysPage } from '../pages/surveys/surveys';
import { Push, PushObject, PushOptions } from '@ionic-native/push';

import { BackendProvider } from '../providers/backend/backend';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  
  rootPage:any = IntroPage;
  nav: any;

  constructor(private app: App, platform: Platform, statusBar: StatusBar, public menuCtrl: MenuController, splashScreen: SplashScreen, public backendProv: BackendProvider, private push: Push) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      // CHECK AND REGISTER FOR PUSH-NOTIFICATION
      this.regPush();
    });
  }

  goIndex() {
    this.nav = this.app.getRootNavById('n4')
    this.nav.setRoot(IntroPage);
  }

  goSurveys() {
    this.rootPage = SurveysPage;
  }

  goSupport() {
    this.rootPage = SupportFaqPage;
  }

  goProfile() {
    this.rootPage = ProfilePage;
  }

  goAbout() {
    this.rootPage = AboutPage;
  }

  goHome() {
    this.rootPage = HomePage;
  }

  goNews() {
    this.rootPage = NewsPage;
  }

  regPush() {
    if(this.backendProv.uuid) {
      // to check if we have permission
      this.push.hasPermission()
      .then((res: any) => {

        if (res.isEnabled) {
          // alert('We have permission to send push notifications');
        } else {
          // alert('We do not have permission to send push notifications');
        }

      });
      let push_options: PushOptions = {
        android: {
          sound: true
        },
        ios: {
          clearBadge: true,
          alert: true,
          badge: true,
          sound: true
        },
        windows: {},
        browser: {
            // pushServiceURL: 'http://push.api.phonegap.com/v1/push'
        }
      };
    
    let pushObject: PushObject = this.push.init(push_options);
    
    pushObject.on('notification').subscribe((notification: any) => {
      // alert('Received a notification: ' + notification)
    });
    
    pushObject.on('registration').subscribe((registration: any) => {

      let gcmstr = {
        device: this.backendProv.model,
        device_id: this.backendProv.uuid,
        device_serial: this.backendProv.serial,
        device_version: this.backendProv.version,
        device_os: this.backendProv.platform,
        inst_id: this.backendProv.institution,
        gcm: registration.registrationId,
        newusr: 'gcmreg'
      }
      // alert('PUSH START: '+JSON.stringify(gcmstr)+' REGDATA: '+JSON.stringify(registration));
      this.backendProv.httpRequest(JSON.stringify(gcmstr))
      .subscribe(pushsuc => {
        // alert('PUSH SUCCESS: '+JSON.stringify(pushsuc));
      }, error => {
        // alert('PUSH ERROR: '+JSON.stringify(error));
      });

    });
    
    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));

   }

  }

}
