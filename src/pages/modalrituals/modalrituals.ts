import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

import { RitualPage } from '../ritual/ritual';
import { ExpressPage } from '../express/express';
import { HistoryPage } from '../history/history';

@Component({
  selector: 'page-modalrituals',
  templateUrl: 'modalrituals.html',
})
export class ModalritualsPage {

  choicerituals: any = false;
  choiceexpress: any = false;
  choicehist: any = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public loadingCtrl: LoadingController) {
    // let loading = this.loadingCtrl.create({
        // spinner: 'hide',
        // cssClass: 'loader',
        // content: `
        //   <img src="assets/img/loading.png" class="ld ldt-bounce-in infinite" width="52" height="52" /><p style="color:#fff !important;">Загрузка..</p>
        // `
    // });
    // loading.present();
    // setTimeout(() => {
    //   loading.dismiss();
    // }, 2000);
  }

  ionViewDidLoad() {
    
  }
  
  goHistory() {
    this.choicerituals = false;
    this.choiceexpress = false;
    this.choicehist = true;
    this.navCtrl.push(HistoryPage, {menue_cat: 182, menue_good: 23});
    setTimeout(() => {
      this.choicerituals = false;
      this.choiceexpress = false;
      this.choicehist = false;
    }, 300);
  }

  goExpress() {
    this.choicerituals = false;
    this.choiceexpress = true;
    this.choicehist = false;
    this.navCtrl.push(ExpressPage, {menue_cat: 182, menue_good: 23});
    setTimeout(() => {
      this.choicerituals = false;
      this.choiceexpress = false;
      this.choicehist = false;
    }, 300);
  }

  goRitual() {
    this.choicerituals = true;
    this.choiceexpress = false;
    this.choicehist = false;
    this.navCtrl.push(RitualPage, {menue_cat: 182, menue_good: 23});
    setTimeout(() => {
      this.choicerituals = false;
      this.choiceexpress = false;
      this.choicehist = false;
    }, 300);
  }

}
