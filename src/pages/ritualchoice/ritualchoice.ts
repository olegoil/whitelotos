import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

import { SpaPage } from '../spa/spa';
import { ModalritualsPage } from '../modalrituals/modalrituals';
import { BackendProvider } from '../../providers/backend/backend';

@Component({
  selector: 'page-ritualchoice',
  templateUrl: 'ritualchoice.html',
})
export class RitualchoicePage {

  choicerituals: any = false;
  choicespas: any = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public loadingCtrl: LoadingController, public backendProv: BackendProvider) {
    // let loading = this.loadingCtrl.create({
        // spinner: 'hide',
        // cssClass: 'loader',
        // content: `
        //   <img src="assets/img/loading.png" class="ld ldt-bounce-in infinite" width="52" height="52" /><p style="color:#fff !important;">Загрузка..</p>
        // `
    // });
    // loading.present();
    // setTimeout(() => {
    //   loading.dismiss();
    // }, 2000);
  }

  ionViewWillEnter() {
    this.backendProv.startCheck();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad RitualchoicePage');
  }

  goSpas() {
    this.choicerituals = false;
    this.choicespas = true;
    this.navCtrl.push(SpaPage, {menue_cat: 185});
    setTimeout(() => {
      this.choicerituals = false;
      this.choicespas = false;
    }, 300);
  }

  goRituals() {
    this.choicerituals = true;
    this.choicespas = false;
    this.navCtrl.push(ModalritualsPage);
    setTimeout(() => {
      this.choicerituals = false;
      this.choicespas = false;
    }, 300);
  }

}
