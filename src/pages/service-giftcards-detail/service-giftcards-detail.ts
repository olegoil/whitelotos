import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { BackendProvider } from '../../providers/backend/backend';

@Component({
  selector: 'page-service-giftcards-detail',
  templateUrl: 'service-giftcards-detail.html',
})
export class ServiceGiftcardsDetailPage {

  gifts: any = [];
  menuePicLink: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public backendProv: BackendProvider) {
    this.gifts.push(navParams.get('gift'));
    this.menuePicLink = backendProv.menuePicLink;
  }

  checkPic(val) {
    if(val && val != '0') {
      return this.menuePicLink + '300/' + val;
    }
    else {
      return 'assets/img/logo.png';
    }
  }

  ionViewDidLoad() {
    
  }

}
